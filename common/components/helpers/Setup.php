<?php

/* 
 * This file was created by Rashad Aliyev (RAliyev@avagr.com)
 *  on Mar 16, 2015 
 */

namespace common\components\helpers;

use common\models\Page;
use common\models\PageLang;
use common\models\Language;
use yii\imagine\Image;
use yii\helpers\ArrayHelper;


class Setup {
  const DATE_FORMAT = 'php:Y-m-d';
  const DATETIME_FORMAT = 'php:Y-m-d H:i:s';
  const TIME_FORMAT = 'php:H:i:s';

  public static function convert($dateStr, $type='date', $format = null){
    $dateStr = self::_monthName2en($dateStr);
    if ($type === 'datetime') {
      $fmt = ($format == null) ? self::DATETIME_FORMAT : $format;
    }
    elseif ($type === 'time') {
      $fmt = ($format == null) ? self::TIME_FORMAT : $format;
    }
    else {
      $fmt = ($format == null) ? self::DATE_FORMAT : $format;
    }
    return \Yii::$app->formatter->asDate($dateStr, $fmt);
  }


  public function dateTimeUnixConverter($data){

    list($part1,$part2) = explode(',', $data);
    list($day, $month, $year) = explode('.', $part1);
    list($hours, $minutes, $seconds) = explode(':', $part2);
    $timeto = mktime($hours, $minutes, $seconds, $month, $day, $year);
    return $timeto;
  }

  //This method will translit all other language characters to English character
	public static function str2url($str){
    	// Translate to translit
		$str = self::_translit2en($str);
		// To lower string
		$str = strtolower($str);
		// Disappear all others with nothign
    $str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);
		// Delete first and last '-'
		$str = trim($str, " ");
		return $str;
	}


	private static function _translit2en($string){
    $converter = [
      //russian
			'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'e',
			'ж' => 'zh', 'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k', 'л' => 'l', 'м' => 'm',  
			'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't',  'у' => 'u', 
			'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch', 'ь' => '', 
			'ы' => 'y', 'ъ' => '', 'э' => 'e', 'ю' => 'yu', 'я' => 'ya',
			
			//russian uppercase
			'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'E',   
			'Ж' => 'Zh', 'З' => 'Z', 'И' => 'I', 'Й' => 'Y', 'К' => 'K', 'Л' => 'L', 'М' => 'M',   
			'Н' => 'N', 'О' => 'O', 'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T',  'У' => 'U',
			'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C', 'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sch', 'Ь' => '',  
			'Ы' => 'Y', 'Ъ' => '', 'Э' => 'E', 'Ю' => 'Yu', 'Я' => 'Ya',
        
			//Azeri and Turkish lowercase
			'ə' => 'e', 'ü' => 'u', 'ö' => 'o', 'ğ' => 'gh', 'ı' => 'i', 'ç' => 'ch', 'ş' => 'sh', 'i' => 'i',
        
			//Azeri and Turkish uppercase        
			'Ə' => 'E', 'Ü' => 'U', 'Ö' => 'O', 'Ğ' => 'GH', 'I' => 'I', 'Ç' => 'CH', 'Ş' => 'SH', 'İ' => 'i',
		];
		return strtr($string, $converter);
  }
  
  private static function _monthName2en($string){
    $converter = [
      //russian
			'Янв' => 'Jan', 'Фев' => 'Feb', 'Мар' => 'Mar', 'Апр' => 'Apr', 
      'Май' => 'May', 'Июн' => 'Jun', 'Июл' => 'Jul', 'Авг' => 'Aug', 
      'Сен' => 'Sep', 'Окт' => 'Oct', 'Ноя' => 'Nov', 'Дек' => 'Dec',
			
			//Azeri
			'Yan' => 'Jan', 'Fev' => 'Feb', 'Mar' => 'Mar', 'Apr' => 'Apr', 
      'May' => 'May', 'İyun' => 'Jun', 'İyul' => 'Jul', 'Avq' => 'Aug', 
      'Sen' => 'Sep', 'Okt' => 'Oct', 'Noy' => 'Nov', 'Dek' => 'Dec',        

		];
		return strtr($string, $converter);
  }

  /**
   *
   * @param $model
   * @return mixed|string
   */
  public static function generateUniqueUrl($model){
    $output = '';
    $dateTime = date('m-y')."-".time();
    $url = empty($model->url_name) ? self::str2url($model->title) : $model->url_name;
    $link = self::str2url($url);
    if($model::find()->where(['url_name' => $link])->exists() && 
        ($model::findOne(['url_name' => $link])->id !== $model->id)){
      $output = $link.'-'.$dateTime;
    } else
      $output = $link;
    
    return $output;
  }


  /**
  *
  * @param $model
  * @return mixed|string
  */
  public static function generatePropertyUniqueUrl($model){
    $output = '';
    $dateTime = date('m-y')."-".time();
    $url = empty($model->slug) ? self::str2url($model->location) : $model->slug;
    $link = mb_substr(self::str2url($url), 0, 180, 'utf-8');
    if($model::find()->where(['slug' => $link])->exists() &&
        ($model::findOne(['slug' => $link])->id !== $model->id)){
      $output = $link.'-'.$dateTime;
    } else
      $output = $link;
    return $output;
  }


  /**
  *
  * @param $model
  * @return mixed|string
  */
  public static function generateAgencyUniqueUrl($model){
    $output = '';
    $dateTime = date('m-y')."-".time();
    $url = empty($model->slug) ? self::str2url($model->title) : $model->slug;
    $link = mb_substr(self::str2url($url), 0, 180, 'utf-8');
    if($model::find()->where(['slug' => $link])->exists() &&
        ($model::findOne(['slug' => $link])->id !== $model->id)){
      $output = $link.'-'.$dateTime;
    } else
      $output = $link;
    return $output;
  }

  /**
   * 
   * We can call this array of prioritized values with
   * <?= \common\components\helpers\Setup::getPriorityList($model, $form) ?>
   * 
   * @param type $model
   * @param type $form
   * @return array
   */
  public static function getPriorityList($model, $form){
		//$modelCount  = $model::find()->groupBy('page_type_id')->count();
    $modelCount = $model::find()
      ->select(['COUNT(*) AS cnt'])
      //->where('status = 1')
      //->groupBy(['page_type_id'])
      ->count();
		$priorityRange = array_combine(range(1, $modelCount + 1), range(1, $modelCount + 1));
		$modelArr = ArrayHelper::map($model::find()->all(), 'id', 'priority');
    //$modelArr = ArrayHelper::map($model::find()->groupBy(['page_type_id'])->all(), 'id', 'priority');
		# Will check difference and return $priorityRange without value matched with $modelArr
		$sanitizedArr = array_diff($priorityRange, $modelArr);
    
		if (!$model->isNewRecord)
			$currentPriority = [$model->priority => $model->priority];
		else
			$currentPriority = [];
		$output = $form->field($model, 'priority')->dropDownList($sanitizedArr + $currentPriority);
		return $output;
	}

  /**
   * truncateHtml can truncate a string up to a number of characters while preserving whole words and HTML tags
   *
   * @param string $text String to truncate.
   * @param integer $length Length of returned string, including ellipsis.
   * @param string $ending Ending to be appended to the trimmed string.
   * @param boolean $exact If false, $text will not be cut mid-word
   * @param boolean $considerHtml If true, HTML tags would be handled correctly
   *
   * @return string Trimmed string.
   */
  public static function truncate($text, $length = 100, $ending = '...', $exact = false, $considerHtml = true) {
    if ($considerHtml) {
      // if the plain text is shorter than the maximum length, return the whole text
      if (strlen(preg_replace('/<.*?>/', '', $text)) <= $length) {
        return $text;
      }
      // splits all html-tags to scanable lines
      preg_match_all('/(<.+?>)?([^<>]*)/s', $text, $lines, PREG_SET_ORDER);
      $total_length = strlen($ending);
      $open_tags = array();
      $truncate = '';
      foreach ($lines as $line_matchings) {
        // if there is any html-tag in this line, handle it and add it (uncounted) to the output
        if (!empty($line_matchings[1])) {
          // if it's an "empty element" with or without xhtml-conform closing slash
          if (preg_match('/^<(\s*.+?\/\s*|\s*(img|br|input|hr|area|base|basefont|col|frame|isindex|link|meta|param)(\s.+?)?)>$/is', $line_matchings[1])) {
            // do nothing
            // if tag is a closing tag
          } else if (preg_match('/^<\s*\/([^\s]+?)\s*>$/s', $line_matchings[1], $tag_matchings)) {
            // delete tag from $open_tags list
            $pos = array_search($tag_matchings[1], $open_tags);
            if ($pos !== false) {
              unset($open_tags[$pos]);
            }
            // if tag is an opening tag
          } else if (preg_match('/^<\s*([^\s>!]+).*?>$/s', $line_matchings[1], $tag_matchings)) {
            // add tag to the beginning of $open_tags list
            array_unshift($open_tags, strtolower($tag_matchings[1]));
          }
          // add html-tag to $truncate'd text
          $truncate .= $line_matchings[1];
        }
        // calculate the length of the plain text part of the line; handle entities as one character
        $content_length = strlen(preg_replace('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', ' ', $line_matchings[2]));
        if ($total_length+$content_length> $length) {
          // the number of characters which are left
          $left = $length - $total_length;
          $entities_length = 0;
          // search for html entities
          if (preg_match_all('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', $line_matchings[2], $entities, PREG_OFFSET_CAPTURE)) {
            // calculate the real length of all entities in the legal range
            foreach ($entities[0] as $entity) {
              if ($entity[1]+1-$entities_length <= $left) {
                $left--;
                $entities_length += strlen($entity[0]);
              } else {
                // no more characters left
                break;
              }
            }
          }
          $truncate .= substr($line_matchings[2], 0, $left+$entities_length);
          // maximum lenght is reached, so get off the loop
          break;
        } else {
          $truncate .= $line_matchings[2];
          $total_length += $content_length;
        }
        // if the maximum length is reached, get off the loop
        if($total_length>= $length) {
          break;
        }
      }
    } else {
      if (strlen($text) <= $length) {
        return $text;
      } else {
        $truncate = substr($text, 0, $length - strlen($ending));
      }
    }
    // if the words shouldn't be cut in the middle...
    if (!$exact) {
      // ...search the last occurance of a space...
      $spacepos = strrpos($truncate, ' ');
      if (isset($spacepos)) {
        // ...and cut the text in this position
        $truncate = substr($truncate, 0, $spacepos);
      }
    }
    // add the defined ending to the text
    $truncate .= $ending;
    if($considerHtml) {
      // close all unclosed html-tags
      foreach ($open_tags as $tag) {
        $truncate .= '</' . $tag . '>';
      }
    }
    return $truncate;
  }




  public static function getPageContent($keyword){
    $lang_id = Language::findOne(['code' => \Yii::$app->language])->id;
    $block_id = Page::findOne(['name' => $keyword])->id;
    
    return PageLang::findOne(['lang_id' => $lang_id, 'page_block_id' => $block_id])->content;
  }
  
  public static function cropImageFromCenter($fileName, $path, $img_w = Null, $thumb_w = Null, $thumb_h = Null, $img_h = Null){
    $size = getimagesize($path);
    if( $img_w > 0 && $img_h >0){
      $x_coordinate = ($img_w > $size[0]) ? 0 : (($size[0]/2) - ($img_w/2));
      $y_coordinate = ($img_h > $size[1]) ? 0 : (($size[1]/2) - ($img_h/2));
      
      if(($img_w < $size[0]) || ($img_h < $size[1]))
        Image::crop($path, $img_w, $img_h, [$x_coordinate, $y_coordinate])->save($path,['quality' => 71]);
    }
    if( ($thumb_w > 0 && $thumb_h >0) && (($thumb_w < $size[0]) || ($thumb_h < $size[1])))
      Image::thumbnail($path, $thumb_w, $thumb_h)->save(dirname($path)."/thumb/".$fileName);
  }
  
  public static function resizeImageFromCenter($fileName, $path, $img_w = Null, $thumb_w = Null, $thumb_h = Null, $img_h = Null){
    $img = Image::getImagine()->open($path);
    $size = $img->getSize();
    $ratio = $size->getWidth()/$size->getHeight();

    $width = $img_w;
    $height = round($width/$ratio);
    
    $box = new \Imagine\Image\Box( $width, $height);
    $img->resize($box)->save($path);
    
    if($thumb_w > 0) {
      $thumb_h = ($thumb_h > 0) ? $thumb_h : round($thumb_w/$ratio);
      $thumb = new \Imagine\Image\Box($thumb_w, $thumb_h);
      $thumb_path = dirname($path)."/thumb";
      if(!is_dir($thumb_path)) mkdir($thumb_path);
      $img->resize($thumb)->save($thumb_path."/".$fileName);
    }
  }
  
  /**
   * Glob function doesn't return the hidden files, therefore scandir can be more useful, 
   * when trying to delete recursively a tree. 
   * @param type $dir
   * @return type
   */
  
  public static function delTree($dir) { 
   $files = array_diff(scandir($dir), array('.','..')); 
    foreach ($files as $file) { 
      (is_dir("$dir/$file")) ? self::delTree("$dir/$file") : unlink("$dir/$file"); 
    } 
    return rmdir($dir); 
  } 
  
  
  public static function keywrodGenerator($string, $minWordLen = 3, $minWordOccurrences = 2, $asArray = false, $maxWords = 20, $restrict = false){
    $str = str_replace(array("?","!",";","(",")",":","[","]"), " ", $string);
    $str = str_replace(array("\n","\r","  "), " ", $str);
    strtolower($str);

//    function keyword_count_sort($first, $sec) {
//      return $sec[1] - $first[1];
//    }

    $str = preg_replace('/[^\p{L}0-9 ]/', ' ', $str);
    $str = trim(preg_replace('/\s+/', ' ', $str));

    $words = explode(' ', $str);

    /* 	
    Only compare to common words if $restrict is set to false
    Tags are returned based on any word in text
    If we don't restrict tag usage, we'll remove common words from array 
    */

    if ($restrict == false) {
     /* Full list of common words in the downloadable code */
     $commonWords = array('a','able','about','above','abroad','according');
     $words = array_udiff($words, $commonWords,'strcasecmp');
    }

    /* Restrict Keywords based on values in the $allowedWords array */
    /* Use if you want to limit available tags */
    if ($restrict == true) {
     $allowedWords =  array('engine','boeing','electrical','pneumatic','ice');
     $words = array_uintersect($words, $allowedWords,'strcasecmp');
    }

    $keywords = array();

    while(($c_word = array_shift($words)) !== null) {
     if(strlen($c_word) < $minWordLen) continue;

     $c_word = strtolower($c_word);
     if(array_key_exists($c_word, $keywords)) $keywords[$c_word][1]++;
     else $keywords[$c_word] = array($c_word, 1);
    }

    usort($keywords, function($first, $sec){return $sec[1] - $first[1];});
    $final_keywords = array();

    foreach($keywords as $keyword_det) {
     if($keyword_det[1] < $minWordOccurrences) break;
     array_push($final_keywords, $keyword_det[0]);
    }

    $final_keywords = array_slice($final_keywords, 0, $maxWords);
    return $asArray ? $final_keywords : implode(', ', $final_keywords);
  }

  /**
   * Return Ip address of visitor
   * @return mixed
   */
  public static function getUserIpAddress(){
    # region: Moskva, city: Moscow, country: RU
    $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = $_SERVER['REMOTE_ADDR'];

    if(filter_var($client, FILTER_VALIDATE_IP)){$ip = $client;}
    elseif(filter_var($forward, FILTER_VALIDATE_IP)){$ip = $forward;}
    else{$ip = $remote;}

    return $ip;
  }

  /**
   * Replace file_get_contents() php function for getting json data
   * @param $Url
   * @return mixed
   */
  public static function url_get_contents ($Url) {
    if (!function_exists('curl_init')){ die('CURL is not installed!');}

    $ch = curl_init();curl_setopt($ch, CURLOPT_URL, $Url);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $output = curl_exec($ch);curl_close($ch);

    return $output;
  }

  public static function exchange_rate_value($currency_id, $property_currency_id, $value){
    $user_currency_model = \common\models\Currency::findOne($currency_id);
    $property_currency_model = \common\models\Currency::findOne($property_currency_id);

    if($currency_id == 2){
      $calculated_value = number_format($value * $property_currency_model->usd_rate, 2, ".", " ");
    } else {
      $calculated_value = number_format($value * $user_currency_model->usd_rate, 2, ".", " ");
    }

    $icon = empty($user_currency_model->icon) ? $user_currency_model->code : '<i class="fa fa-'.$user_currency_model->icon.'"></i>';
    return $calculated_value." ".$icon;
  }

  public function run($fileName){

    return (int)unlink(Yii::getAlias($this->uploadDir) . '/' . $fileName);
  }

  public static function drop_multi_level_array_element($array, $key_name, $looking_value){
    $key = '';
    foreach($array as $k => $v){
      foreach($v as $l=>$m){
        if($l == $key_name){
          if($looking_value == $m){
            $key = $k;
          }
        }
      }
    }
    if(array_key_exists($key, $array)){
      unset($array[$key]);
      return true;
    }
  }

  //delete full directory with it files
  public static function deleteDir($dirPath) {
    array_map('unlink', glob("$dirPath/*.*"));
    rmdir($dirPath);
  }
  
  /**
   * @param $bytes
   * @return string
   */
  public static function formatBytes($bytes) {
    if ($bytes >= 1073741824) {
      $bytes = number_format($bytes / 1073741824, 2) . ' GB';
    } elseif ($bytes >= 1048576) {
      $bytes = number_format($bytes / 1048576, 2) . ' MB';
    } elseif ($bytes >= 1024) {
      $bytes = number_format($bytes / 1024, 2) . ' KB';
    } elseif ($bytes > 1) {
      $bytes = $bytes . ' bytes';
    } elseif ($bytes == 1) {
      $bytes = $bytes . ' byte';
    } else {
      $bytes = '0 bytes';
    }

    return $bytes;
  }

}