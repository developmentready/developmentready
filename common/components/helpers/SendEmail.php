<?php

/* 
 * This file was created by Rashad Aliyev (RAliyev@avagr.com)
 *  on Apr 24, 2015 
 */

namespace common\components\helpers;

use Yii;
use common\models\Settings;

class SendEmail{
  public static function sendNotificationMessage($model, $subject = 'New message from Contact form.', $theme = 'contact'){
    $settings = new Settings();
    $messages = [];
    $mail_list = explode(";", $settings->findKeyword('email-recipient-list'));
            
    foreach ($mail_list as $k => $email){
      $messages[] = Yii::$app->mailer->compose($theme, ['model' => $model])
        ->setSubject(Yii::t('frontend', "{app-name} | {$subject}", [
          'app-name'=>Yii::$app->name
        ]))
        ->setTo(trim($email));
    }
    Yii::$app->mailer->sendMultiple($messages);
  }
}