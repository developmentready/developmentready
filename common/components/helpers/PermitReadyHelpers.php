<?php
/**
 * Created by PhpStorm.
 * User: rashad
 * Date: 3/3/16
 * Time: 11:06
 */

namespace common\components\helpers;

use yii;

class PermitReadyHelpers {
  public static function convertStateToId() {
    $state = \Yii::$app->geoLocation->locate();

    if($state['geoplugin_countryCode'] == 'AU'){
      if(strlen($state['geoplugin_region']) > 3 && !empty($state['geoplugin_region'])){
        switch ($state['geoplugin_region']){
          case 'Australian Capital Territory':
            $state_code = 'ACT'; break;
          case 'New South Wales':
            $state_code = 'NSW'; break;
          case 'Northern Territory':
            $state_code = 'NT'; break;
          case 'Queensland':
            $state_code = 'QLD'; break;
          case 'South Australia':
            $state_code = 'SA'; break;
          case 'Tasmania':
            $state_code = 'TAS'; break;
          case 'Western Australia':
            $state_code = 'WA'; break;
          default:
            $state_code = 'VIC'; break;
        }
      } else {
        $state_code = 'VIC';
      }
    } else {
      $state_code = 'VIC';
    }

    $state_object = \common\models\State::find()->where(['title' => $state_code])->active()->one();

    if($state_object)
      return $state_object->id;
    else
      return 7;
  }
}