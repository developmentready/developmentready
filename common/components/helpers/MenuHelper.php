<?php

/* 
 * This file was created by Rashad Aliyev (RAliyev@avagr.com)
 *  on Mar 16, 2015 
 */

namespace common\components\helpers;

use common\models\Page;
use yii\helpers\Url;
use yii\helpers\Html;

/**
 * 
 * echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => app\components\MenuHelper::getMenu(),
*/


class MenuHelper{
  
  public $menu_list = [1];
  
  public static function getHeaderFooterMenuList($header = TRUE){
    $html = '';
    $items = Page::find()
      ->where(['menu_type' => 1 ])
      ->andWhere(['parent_id' => 1 ])
      ->active()
      ->currentLang()
      ->joinWith('translation')
      ->orderBy('priority')
      ->limit(6)
      ->all();
    
    # We use hear header as a optional, if in future we will require footer position also
    # the we will add new statement where header is false
    if($header):
      foreach ($items as $item):
        $html .= "<li>".Html::a($item->menu_label, Url::to("/".substr(\Yii::$app->language, 0, 2)."/site/".$item->url_name), [])."</li>";
      endforeach;
    else:
      foreach ($items as $item):
        $html .= "<li>".Html::a($item->menu_label, Url::to("/page/".$item->url_name), [])."</li>";
      endforeach;
    endif;
    
    return $html;
  }

  public static function getNavigationMenu(){
    $output  = '<ul id="nav-main" class="nav navbar-nav">';
    $output .= static::getBigMenuRecruisive();
    $output .= "</ul>";

    return $output;
  }
  
  public static function getBigMenuRecruisive($parent = 1, $menu_type = NULL, $child = false){
    $html = "";
    $menu = Page::find()->where('menu_type IN (1,2) ')
      ->andWhere(['parent_id' => $parent])->active()->orderBy('priority');
    $child ? '' : $menu->limit(8);
    $items = $menu->all();
    
    if(count($items) > 0):
      foreach ($items as $item):
        $sub_menu = Page::find()->where('menu_type IN (1,2) ')->andWhere(['parent_id' => $item->id])
          ->active()->all();
        if(count($sub_menu) > 0):
          $html .= "<li class='dropdown'>";
          $html .= "<a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>{$item->menu_label}<span class='caret'></span></a>";
          $html .= "<ul class='dropdown-menu'>";
          $html .= static::getBigMenuRecruisive($item->id, $item->menu_type, true);
          $html .= "</ul></li>";
        else:
          if($child){
            if($item->page_template_id == 2){
              $html .= "<li><a href='". Url::to(["/property/index"])."'> {$item->menu_label}</a></li>";
            } elseif($item->page_template_id == 3){
              $html .= "<li><a href='".Url::to(["/blog/index"])."'> {$item->menu_label}</a></li>";
            } elseif($item->page_template_id == 1){
              $html .= "<li><a href='".Url::to(["/site/index"])."'> {$item->menu_label}</a></li>";
            } elseif($item->page_template_id == 5){
              $html .= "<li><a href='". Url::to(["/{$item->url_name}"])."'> {$item->menu_label}</a></li>";
            } elseif($item->page_template_id == 6){
            $html .= "<li><a href='". Url::to(["site/testimonials"])."'> {$item->menu_label}</a></li>";
            } else {
              $html .= "<li><a href='". Url::to(['/page/view', 'slug' => $item->url_name])."'> {$item->menu_label}</a></li>";
            }
          } else {
            if($item->page_template_id == 2){
              $class = \Yii::$app->controller->action->uniqueId == 'property/index' ? 'active' : '';
              $html .= "<li class='{$class}'>";
              $html .= "<a href='".Url::to(["/property/index"])."'> {$item->menu_label} </a>";
              $html .= "</li>";
            } elseif($item->page_template_id == 3){
              $class = \Yii::$app->controller->action->uniqueId == 'blog/index' ? 'active' : '';
              $html .= "<li class='{$class}'>";
              $html .= "<a href='".Url::to(["/blog/index"])."'> {$item->menu_label} </a>";
              $html .= "</li>";
            } elseif($item->page_template_id == 1){
              $class = \Yii::$app->controller->action->uniqueId == 'site/index' ? 'active' : '';
              $html .= "<li class='{$class}'>";
              $html .= "<a href='".Url::to(["/site/index"])."'> {$item->menu_label} </a>";
              $html .= "</li>";
            } elseif($item->page_template_id == 5){
              $class = \Yii::$app->controller->action->uniqueId == $item->url_name ? 'active' : '';
              $html .= "<li class='{$class}'>";
              $html .= "<a href='".Url::to(["/{$item->url_name}"])."'> {$item->menu_label} </a>";
              $html .= "</li>";
            } elseif($item->page_template_id == 6){
              $class = \Yii::$app->controller->action->uniqueId == 'testimonials' ? 'active' : '';
              $html .= "<li class='{$class}'>";
              $html .= "<a href='".Url::to(["site/testimonials"])."'> {$item->menu_label} </a>";
              $html .= "</li>";
            } else {
              $class = \Yii::$app->controller->action->uniqueId == "page/{$item->url_name}" ? 'active' : '';
              $html .= "<li>";
              $html .= "<a href='" . Url::to(['/page/view', 'slug' => $item->url_name]) . "'> {$item->menu_label} </a>";
              $html .= "</li>";};
          }
        endif;
      endforeach;
    endif;

    if(!$child){
      $html .= "<span id='mobile-login-block'>";
      if(\Yii::$app->user->isGuest):
        $html.="<li><a href='/user/login'><span class='glyphicon glyphicon-log-in'></span>Login</a></li>";
        $html.="<li><a href='/user/signup'><span class='glyphicon glyphicon-edit'></span>Register</a></li>";
      else:
        $html.="<li><a href='/account/index'><span class='glyphicon glyphicon-user'></span>My Account</a></li>";
        $html.="<li><a href='/user/logout'><span class='glyphicon glyphicon-log-out'></span>Log Out</a></li>";
      endif;
      $html .= "</span>";
    }

    return $html;
  }

    // For Bootstrap installed navigation menu
  public static function getMenu(){
    $role_id = 1;
    $result = static::getMenuRecrusive($role_id);
    return $result;
  }

  public static function getMenuStructure($parent = 1, $menu_type = NULL, $child = false){
    $html = "";
    $menu = Page::find()->where('menu_type IN (1,2) ')
      ->andWhere(['parent_id' => $parent])->active()->orderBy('priority');
    $child ? '' : $menu->limit(8);
    $items = $menu->all();

    if(count($items) > 0):
      foreach ($items as $item):
        $sub_menu = Page::find()->where('menu_type IN (1,2) ')->andWhere(['parent_id' => $item->id])
          ->active()->all();
        if(count($sub_menu) > 0):
          $html .= "<li><a href='/page/update?id={$item->id}'> {$item->menu_label}</a></li>";
          $html .= "<ul style='padding-left: 10px'>";
          $html .= static::getBigMenuRecruisive($item->id, $item->menu_type, true);
          $html .= "</ul></li>";
        else:
          $html .= "<li><a href='/page/update?id={$item->id}'> {$item->menu_label}</a></li>";
        endif;
      endforeach;
    endif;

    return $html;
  }

}