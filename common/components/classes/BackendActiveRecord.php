<?php

/* 
 * This file was created by Rashad Aliyev (RAliyev@avagr.com)
 *  on Mar 1, 2015 
 */

namespace common\components\classes;

#use Yii;
use common\components\classes\CommonActiveRecord;
use yii\db\ActiveRecord;
use common\models\User;

class BackendActiveRecord extends CommonActiveRecord {

  public function behaviors(){
    return [
      'blameable' => [
        'class' => 'yii\behaviors\BlameableBehavior',
        'createdByAttribute' => 'creator_id',
        'updatedByAttribute' => 'updater_id',
      ],
      'timestamp' => [
        'class'      => 'yii\behaviors\TimestampBehavior',
        //'value'      => function () { return now(); },
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
          ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
        ],
      ],
    ];
  }
  
  /**
  * @return \yii\db\ActiveQuery
  */
  public function getCreator(){
    return $this->hasOne(User::className(), ['id' => 'creator_id']);
  }
  
  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUpdater(){
    return $this->hasOne(User::className(), ['id' => 'updater_id']);
  }
  
  public function getCreatorName(){
    return $this->creator ? $this->creator->name : '- no user -';
  }

  public function getUpdaterName(){
    return $this->updater ? $this->updater->name : '- no user -';
  }
  
  public function getUserNameById($user_id){
    # We will try to find by primary key
    $user = Yii::$app->getModule("user")->model("User")->findOne($user_id);
    return $user->profile->full_name;
  }
}

