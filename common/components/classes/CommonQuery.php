<?php
/* 
 * This file was created by Rashad Aliyev (RAliyev@avagr.com)
 *  on Dec 18, 2014 
 */

namespace common\components\classes;

use yii\db\ActiveQuery;
use common\models\Language;
use common\components\classes\CommonActiveRecord;

class CommonQuery extends ActiveQuery{
  const STATUS_ACTIVE = 1;

  /**
   * @return $this
   */
  public function active(){
    $this->andWhere(['status' => self::STATUS_ACTIVE]);
    return $this;
  }

  public function featuredAgency(){
    $this->andWhere(['status' => self::STATUS_ACTIVE]);
    $this->andWhere(['featured_aceny' => self::STATUS_ACTIVE]);
    return $this;
  }

  public function published(){
    $this->andWhere(['status' => CommonActiveRecord::STATUS_PUBLISHED]);
    $this->andWhere(['<', '{{%article}}.published_at', time()]);
    return $this;
  }

  public function notSold(){
    $this->andWhere(['{{%properties}}.sold' => 0]);
    $this->groupBy('{{%properties}}.id');
    return $this;
  }

  public function soldProperties(){
    $this->andWhere(['{{%properties}}.sold' => 1]);
    $this->groupBy('{{%properties}}.id');
    return $this;
  }

  public function featuredProperties(){
    $this->andFilterWhere(['>', '{{%properties}}.featured_till', time()]);
    $this->limit(12);
    $this->groupBy('{{%properties}}.id');
    return $this;
  }

  public function activeProperty(){
    $this->andWhere(['{{%properties}}.status' => self::STATUS_ACTIVE]);
    $this->andWhere(['{{%properties}}.sold' => 0]);
    $this->groupBy('{{%properties}}.id');
    return $this;
  }

  public function advertised(){
    $this->andWhere(['{{%properties}}.status' => self::STATUS_ACTIVE]);
    $this->andWhere(['>=', '{{%properties}}.advertised_till', time()]);
    $this->andWhere(['{{%properties}}.deleted' => 0]);
    $this->andWhere(['<', '{{%properties}}.priority', 5]);
    $this->groupBy('{{%properties}}.id');
    $this->orderBy(['{{%properties}}.priority' => SORT_ASC, '{{%properties}}.updated_at' => SORT_DESC]);
    return $this;
  }

  public function mostViewed(){
    $this->andWhere(['>=', '{{%properties}}.advertised_till', time()]);
    $this->andWhere(['{{%properties}}.deleted' => 0]);
    $this->orderBy(['{{%properties}}.view_count' => SORT_DESC, 'created_at' => SORT_DESC]);
    return $this;
  }

  public function currentLang(){
    $langCode = \Yii::$app->language;
    if(strlen($langCode) > 2 )
      $array = Language::findOne(['local' => $langCode]);
    else
      $array = Language::findOne(['code' => $langCode]);
    $id = $array['id'];

    return $this->andWhere(['lang_id' => $id]);
  }

  public function makeSort(){
    return $this->orderBy(['priority' => SORT_ASC, 'created_at' => SORT_DESC, 'id' => SORT_DESC]);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCountry(){
    return $this->hasOne(Country::className(), ['id' => 'country_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCity(){
    return $this->hasOne(City::className(), ['id' => 'city_id']);
  }

  /**
   * Will return ID of selected language
   * @param type $langCode
   * @return integer ID
   */
  public function getLanguageID($langCode){
    if(strlen($langCode) > 2 )
      $array = Language::findOne(['local' => $langCode]);
    else
      $array = Language::findOne(['code' => $langCode]);
    return $array['id'];
  }
}
