<?php
/**
 * Created by PhpStorm.
 * Created by : Rashad Aliyev (RAliyev@avagr.com)
 * Date: 7/10/15
 * Time: 20:49
 */


namespace common\components\classes;

use Yii;
use yii\db\ActiveRecord;
use common\models\MediaStorage;
use common\models\PropertyImages;
use common\models\Language;
use common\components\helpers\Setup;
use gregwar\image\Image;
use common\components\classes\CommonQuery;

class CommonActiveRecord extends ActiveRecord {

  const STATUS_PUBLISHED = 1;
  const STATUS_DRAFT = 0;

  public $fileAttribute;
  public $fileAttributes = [];

  public static function find(){
    return new CommonQuery(get_called_class());
  }

  /**
   * Will return ID of selected language
   * @param type $langCode
   * @return integer ID
   */
  public function getLanguageCode($langCode){
    if(strlen($langCode) > 2 )
      return $langCode;
    else
      return substr ($langCode, 0, 2);
  }

  /**
   * Will return ID of selected language
   * @param type $langCode
   * @return integer ID
   */
  public function getLanguageID($langCode){
    if(strlen($langCode) > 2 )
      $array = Language::findOne(['local' => $langCode]);
    else
      $array = Language::findOne(['code' => $langCode]);
    return $array['id'];
  }

  /**
   *
   * Will save many to one related data based on provided relation coumtn name
   *
   * @param model $junctionModelName
   * @param string $oneColumnName
   * @param string $manyColumnName
   * @return bool
   */
  public function saveJunctionData($junctionModelName, $oneColumnName, $manyColumnName){
    $junction_model_name_class = $junctionModelName::className();
    $junction_model_name = \yii\helpers\StringHelper::basename($junctionModelName::className());

    if(count(\Yii::$app->request->post($junction_model_name)["$manyColumnName"]) > 1){
      foreach(\Yii::$app->request->post($junction_model_name)["$manyColumnName"] as $k => $v):
        if($v > 0 && $v !==NULL){
          $item = new $junction_model_name_class();
          $item->$manyColumnName = $v;
          $item->$oneColumnName = $this->id;
          $item->save();
        }
      endforeach;
      return true;
    } else{
       return false;
    }
  }

  /**
   * Return MediaStorage object
   * @param $id
   * @return null|static
   */
  public function getMediaById($id){
    return MediaStorage::findOne($id);
  }


  /**
   *
   * @param $model
   * @param $file
   * @param string $related_id
   * @param null $width
   * @return bool
   */
  public function saveMediaFiles($model, $file, $related_id = 'media_id', $width = NULL){
      if($file){

        $path = \Yii::getAlias('@storage').$file['path'];

        if(!is_null($width)) Setup::resizeImageFromCenter($file['name'], $path, $width);

        if($model->$related_id > 0){
          $media_model = MediaStorage::findOne($model->$related_id);
          if(!empty($media_model)){
            if($file['path'] == $media_model->media_path) return true;
            else
              if(is_file($old_path = \Yii::getAlias('@storage').$media_model->media_path)) unlink ($old_path);
          } else {
            $media_model = new MediaStorage();
          }
        }

        else
          $media_model = new MediaStorage();

        $media_model->model = \yii\helpers\StringHelper::basename(get_class($model));
        $media_model->model_id = $model->id;
        $media_model->base_name = $file['name'];
        $media_model->media_path = $file['path'];
        $media_model->size = $file['size'];
        $media_model->type = $file['type'];
        $media_model->save();
        $model->$related_id = $media_model->id;
        $model->save();

        if(Yii::$app->session->has('_uploadedFiles')){
          Yii::$app->session->remove('_uploadedFiles');
        }

        return true;
      } else {
      return false;
    }

  }

  /**
   * Save uploaded files
   * @param $images
   * @param $folder_name
   * @param $id
   * @return bool
   */
  public function saveUploadedImages($images, $folder_name = 'properties', $id){

    $new_dir = \Yii::getAlias('@storage')."/{$folder_name}/{$id}";

    if(!is_dir($new_dir)){
      if(!mkdir($new_dir, 0777)) die('Failed to create folders...'); chmod($new_dir, 0777);

      $content = "Silence is power!";
      $fp = fopen($new_dir . "/index.html", "wb"); fwrite($fp, $content); fclose($fp);
    }

    if(!empty($images)){
      foreach($images as $item){
        $model = new PropertyImages();
        $file_name = explode("/", $item['path']);
        $file_name = $file_name[1];
        $image_path = \Yii::getAlias('@storage')."/source/".$item['path'];
        $new_path = $new_dir."/{$file_name}";
        Image::open($image_path)->scaleResize(650, 425, "#d8d8d8")->save($new_path);
        $model->property_id = $id;
        $model->file_name = $file_name;



        if(!$model->save()){ return false; };
      }
    }

    if(Yii::$app->session->has('_uploadedFiles')){
      Yii::$app->session->remove('_uploadedFiles');
    }
    return true;
  }

  /**
   * Save uploaded files
   * @param $image
   * @param $id
   * @param $model_name
   * @return bool
   */
  public function saveSingleImage($image, $id, $model_name = 'Page', $img_width = 535, $bg_color = "#ffffff", $img_height = 425){

    $new_dir = \Yii::getAlias('@storage')."/web/".strtolower($model_name);

    if(!is_dir($new_dir)){
      if(!mkdir($new_dir, 0777)) die('Failed to create folders...'); chmod($new_dir, 0777);

      $content = "Silence is power!";
      $fp = fopen($new_dir . "/index.html", "wb"); fwrite($fp, $content); fclose($fp);
    }

    $model = MediaStorage::find()->where(['model_id' => $id])->andWhere(['model' => $model_name])->one();

    if(empty($model)) $model = new MediaStorage();
    else {
      $old_file = \Yii::getAlias('@storage').$model->media_path;
      if($model->media_path != '/web/agencyagent/anymous.png')
        if(is_file($old_file)) unlink($old_file);
    }

    $file_name = explode("/", $image['path']);
    $file_name = $file_name[1];
    $image_path = \Yii::getAlias('@storage')."/source/".$image['path'];
    $new_path = $new_dir."/{$file_name}";
    Image::open($image_path)->scaleResize($img_width, $img_height, $bg_color)->save($new_path);

    $model->model = $model_name;
    $model->model_id = $id;
    $model->default = 1;
    $model->media_path = "/web/".strtolower($model_name)."/{$file_name}";
    $model->size = $image['size'];
    $model->type = $image['type'];
    $model->base_name = $file_name;

    if($model->save()) {
      $file_storage = \common\models\FileStorageItem::find()->where(['path' => $image['path']])->one();
      if(!empty($file_storage)) {
        $file_storage->delete();
        if(is_file($image_path)) unlink($image_path);
      }
    } else {
      return false;
    }

    if(Yii::$app->session->has('_uploadedFiles')){
      Yii::$app->session->remove('_uploadedFiles');
    }
    return true;
  }


  /**
   * Links a list of models to a parent model by creating entries into a mapping table. All existing relations
   * will be deleted. This method is for many-to-many relations, so parent and child might be confusing.
   *
   * @param ActiveRecord $parentModel The model that is the parent.
   * @param String $propertyName The name of the property at the parent model to which the list of child models belong.
   * @param String|String[] $childModelIds The ids of the child models.
   */
  public static function setLinks(ActiveRecord $parentModel, $propertyName, $childModelIds){
    $getter = 'get' . $propertyName;
    $linkedModelClassName = $parentModel->$getter()->modelClass;

    $parentModel->unlinkAll($propertyName, true);

    if (empty($childModelIds)) return;

    foreach ($childModelIds as $id) {
      $childModel = $linkedModelClassName::findOne($id);
      $parentModel->link($propertyName, $childModel);
    }
  }

}

