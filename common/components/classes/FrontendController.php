<?php

/* 
 * This file was created by Rashad Aliyev (RAliyev@avagr.com)
 *  on Mar 29, 2015 
 */


namespace common\components\classes;

use yii;

use yii\web\Controller;
use common\models\Page;
use common\models\Settings;
use common\models\Gallery;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class FrontendController extends Controller{

  /**
   * Returns a list of behaviors that this component should behave as.
   * Here we use RBAC in combination with AccessControl filter.
   *
   * @return array
   */
  public function behaviors(){
    return [
      'access' => [
        'class' => AccessControl::className(),
        'rules' => [
          [
            'controllers' => ['site', 'page', 'property', 'user', 'show', 'widget','report'],
            'allow' => true,
            'roles' => ['?', '@'],
          ],
          [
            'actions' => ['my-favorites'],
            'allow' => true,
            'roles' => ['@'],
          ],
          [
            'controllers' => ['account'],
            'allow' => true,
            'roles' => ['user','agencies'],
          ],
          [
            'actions' => ['agency', 'agents','report'],
            'allow' => true,
            'roles' => ['agencies'],
            'denyCallback' => function(){
              return Yii::$app->controller->redirect(['/account/index']);
            }
          ],
          [
            'actions' => ['delete' => 'post'],
            'allow' => true,
            'roles' => ['administrator'],
          ],
        ], // rules
      ], // access
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ], // verbs
    ]; // return
  } // behaviors

  public function init() {
    \Yii::$app->keyStorage->set('articles-per-page', 20);
    $session = Yii::$app->session;
    $cookies = \Yii::$app->request->cookies;
    if (!$session->isActive) $session->open();
    $location = \Yii::$app->geoLocation->locate();

    if(!empty($location['geoplugin_region'])) {

      $state_id = \common\components\helpers\PermitReadyHelpers::convertStateToId();

      if(!$cookies->has('userState')) {
        $cookie = new \yii\web\Cookie([
          'name' => 'userState',
          'value' => $state_id,
          'expire' => time() + (60*60*24*180), // for 180 days
        ]);
        $session->set('state', $state_id);
        Yii::$app->response->cookies->add($cookie);

      }

      if(!$cookies->has('userStateTitle')) {
        $state_title = \common\models\State::findOne($state_id)->title;
        $cookie2 = new \yii\web\Cookie([
          'name' => 'userStateTitle',
          'value' => $state_title,
          'expire' => time() + (60*60*24*180), // for 180 days
        ]);

        Yii::$app->response->cookies->add($cookie2);
      }
    }

    if( $cookies->has('userState') ) {
      if($cookies->getValue('userState') != $session->get('state')) {
        $state_id = (int)$cookies->getValue('userState');
        $session->set('state', $state_id);
      }
    }

    if(!$session->has('state')) {

      if(!empty($location['geoplugin_region'])) {

        $session->set('state', \common\components\helpers\PermitReadyHelpers::convertStateToId());

      } else {

        $session->set('state', 7);

      }
    }

    parent::init();
  }

  /**
  * @inheritdoc
  */
  public function actions(){
    return [
      'error' => [
        'class' => 'yii\web\ErrorAction',
      ],
    ];
  }
  
  protected function findAllByCriteria($model_name, $id, $column = 'page_type_id', $only_model = false, $limit = Null){
    $model_lang = strtolower($model_name)."Lang";
    $model = $model_name::find()
      ->where([$column => $id, 'status'=>Page::STATUS_PUBLISHED])
      ->currentLang()->joinWith($model_lang);
    
    if($only_model) return $model;
    
    if($limit > 0) $model = $model->limit ($limit);
    $output = $model->all();
    
    return $output;
  }  
  
  protected function findAllPagesByCriteria($id, $column = 'page_type_id', $only_model = false, $limit = Null){
    $model = Page::find()
      ->where([$column => $id, 'status'=>Page::STATUS_PUBLISHED])
      ->currentLang()->joinWith('pageLang')->orderBy(['priority' => SORT_ASC,'updated_at' => SORT_DESC]);
    
    if($only_model) return $model;
    
    if($limit > 0) $model = $model->limit ($limit);
    $output = $model->all();
    
    return $output;
  }
  
  protected function findPageWithoutTranslation($id, $column = 'page_type_id', $only_model = false, $limit = Null){
    $model = Page::find()
      ->where([$column => $id, 'status'=>Page::STATUS_PUBLISHED])
      ->orderBy(['priority' => SORT_ASC,'updated_at' => SORT_DESC]);
    
    if($only_model) return $model;
    
    if($limit > 0) $model = $model->limit ($limit);
    $output = $model->all();
    
    return $output;
  }
  
  
  protected function findRandomPagesByCriteria($id, $column = 'page_type_id', $only_model = false, $limit = Null){
    $model = Page::find()
      ->where([$column => $id, 'status'=>Page::STATUS_PUBLISHED])
      ->currentLang()->joinWith('pageLang')->orderBy('RAND()');
    
    if($only_model) return $model;
    
    if($limit > 0) $model = $model->limit ($limit);
    $output = $model->all();
    
    return $output;
  }
  
  protected function findOnePagesByCriteria($id, $column = 'url_name'){
    return Page::find()
      ->where([$column => $id, 'status'=>Page::STATUS_PUBLISHED])
      ->currentLang()->joinWith('pageLang')->one();
  }
  
  protected function findAllMediaByCriteria($id, $column = 'parent_id', $only_model = false, $limit = Null){
    $model = Gallery::find()->where([$column => $id]);
    
    if($only_model) return $model;
    
    if($limit > 0) $model = $model->limit ($limit);
    $output = $model->all();
    return $output;
  }

}
