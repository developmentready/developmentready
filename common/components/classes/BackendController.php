<?php

/* 
 * This file was created by Rashad Aliyev (RAliyev@avagr.com)
 *  on Mar 1, 2015 
 */

namespace common\components\classes;

use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;
use common\models\MediaStorage;
use yii\filters\VerbFilter;
use common\components\helpers\Setup;

class BackendController extends Controller{

  public function behaviors(){
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
            'delete' => ['post'],
            'logout' => ['post'],
        ],
      ],
    ];
  }


  // fetch the list of Item models for updating
  public function getRelatedModel($model, $column = 'related_id', $id){
    if (($related_model = $model::find()->where([$column => $id])->all()) !== null) {
      return $related_model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  public function actionUploadImage(){
    if (Yii::$app->request->isPost) {
      $file = UploadedFile::getInstanceByName('_fileinput_w1');
      if ($this->_customValidateImage($file)){
        $security = \Yii::$app->security->generateRandomString();
        $base_url = \Yii::getAlias('@storageUrl');
        $new_name = substr($file->baseName, 0, 8).$security.'.' . $file->extension;
        $path = '/uploads/' . $new_name ;
        $full_path = \Yii::getAlias('@storage').'/uploads/' . $new_name;
        $path_url = \Yii::getAlias('@storageUrl').'/uploads/' . $new_name;
        $delete_url = "/page/delete-image-path?path=".$full_path;

        $file->saveAs($full_path);

        $items = ["files" => [["name" => $new_name, "type" => $file->type, "size"=> $file->size, "base_url" => $base_url, "path"=> $path, "url" => $path_url,  "delete_url"  => $delete_url ]]];
        \Yii::$app->response->format = 'json';
        return $items;
      } else {
        echo "Error occurred During saving file";
        return false;
      }
    }
    echo "Incorrect request"; return false;
  }

  private function _customValidateImage($file){
    if(preg_match("/^image\/(png|jpg|jpeg|gif)/i",$file->type)){
      return true;
    } else {
      return false;
    }
  }

  public function actionDeleteImagePath($path){
    if(is_file($path)) unlink ($path);
    return true;
  }

  public function actionSingleImageDelete($id){
    $model = MediaStorage::findOne($id);

    if(!empty($model)){
      $return_id = $model->model_id;
      $path = \Yii::getAlias('@storage').$model->media_path;
      $model->delete();
      if(is_file($path)) unlink ($path);
      Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-success'], 'body'=>Yii::t('backend', 'All data was successfully deleted')]);
      if(Yii::$app->request->referrer !== Null)
        return $this->redirect(Yii::$app->request->referrer);
      else
        return $this->redirect([$this->id."/update/?id={$return_id}"]);
    } else {
      Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-danger'], 'body'=>Yii::t('backend', 'Something went wrong')]);
      if(Yii::$app->request->referrer !== Null)
        return $this->redirect(Yii::$app->request->referrer);
      else
        $this->redirect([$this->id."/index"]);
    }
  }

  public function partialPageCreateAction($model, $img_w = NULL, $img_thumb_w = NULL, $img_thumb_h = NULL, $img_h = NULL){
    $model->url_name = Setup::generateUniqueUrl($model);
    $image = $model->uploadImage();
    if($model->save()){

      if ($image !== false){
        $path = $model->getImageFile();
        $image->saveAs($path);
        Setup::cropImageFromCenter($model->image_url, $path, $img_w, $img_thumb_w, $img_thumb_h, $img_h);
      }

      Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-success'], 'body'=>Yii::t('backend', 'All data was successfully saved')]);
      return $this->redirect("/".$this->id."/index");
    } else {
      Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-danger'], 'body'=>Yii::t('backend', 'Error occurred during saving all data')]);
      return $this->refresh();
    }
  }
  
  public function partialPageUpdateAction($model, $oldFile = NULL, $oldFileName = NULL, $img_w = NULL, $img_thumb_w = NULL, $img_thumb_h = NULL, $img_h = NULL){
    $model->url_name = Setup::generateUniqueUrl($model);
    $image = $model->uploadImage();
    // if no new image uploaded then old one will be rewrited
    if($image === false){
      $model->image_url = $oldFileName;
    }
    
    if($model->save()){
      // upload only if valid uploaded file instance found
      if ($image !== false && is_file($oldFile)){ // delete old and overwrite
        if(unlink($oldFile)){
          if(is_file(dirname($oldFile)."/thumb/".$oldFileName)) unlink(dirname($oldFile)."/thumb/".$oldFileName);
          
          $path = $model->getImageFile();
          $image->saveAs($path);
          Setup::cropImageFromCenter($model->image_url, $path, $img_w, $img_thumb_w, $img_thumb_h, $img_h);
        }
      } else {
        if ($image !== false){
          $path = $model->getImageFile();
          $image->saveAs($path);
          Setup::resizeImageFromCenter($model->image_url, $path, $img_w, $img_thumb_w, $img_thumb_h, $img_h);
        }
      }

      Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-success'], 'body'=>Yii::t('backend', 'All data was successfully saved')]);


      if(Yii::$app->request->referrer !== Null)
        return $this->redirect("/".$this->id."/index");
        #return $this->redirect(Yii::$app->request->referrer);
      else
        return $this->redirect("/".$this->id."/index");
      #return $this->redirect(\yii\helpers\Url::previous());
      #return $this->refresh();
    } else {
      Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-danger'], 'body'=>Yii::t('backend', 'Error occurred during saving all data')]);
      return $this->refresh();      
    }
  }
  
  public function partialCreateAction($model, $code = 1 ,$img_w = NULL, $img_thumb_w = NULL, $img_thumb_h = NULL, $img_h = NULL){
    $model->url_name = Setup::generateUniqueUrl($model);
    $image = $model->uploadImage();
    if($model->save()){

      if ($image !== false){
        $path = $model->getImageFile();
        $image->saveAs($path);
        Setup::cropImageFromCenter($model->image_url, $path, $img_w, $img_thumb_w, $img_thumb_h, $img_h);
      }

      $items = $this->_loadMultipleLangs($code);
      
      if(PageLang::loadMultiple($items, \Yii::$app->request->post())){
        foreach($items as $item):
          $item->relation_id = $model->id;
          if($item->save()){
            Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-success'], 'body'=>Yii::t('backend', 'All data was successfully saved')]);
          } else {
            Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-danger'], 'body'=>Yii::t('backend', 'Error occurred during saving all data')]);
          }
        endforeach;
      } else {
        Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-danger'], 'body'=>Yii::t('backend', 'Error occurred during saving language realted data')]);
      }
      if(Yii::$app->request->referrer !== Null)
        return $this->redirect(Yii::$app->request->referrer);
      else
        return $this->redirect("/".$this->id."/index");
    } else {
      Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-danger'], 'body'=>Yii::t('backend', 'Error occurred during saving all data')]);
      return $this->refresh();
    }
  }
  
  /**
   * code - is a private method which will call related language table, by default it's 
   * PageLang model. You can also add addtional models
   * 
   * @param type $model
   * @param type $file_model
   * @param type $img_w
   * @param type $img_thumb_w
   * @param type $img_thumb_h
   * @param type $img_h
   * @return type
   */
  public function partialCreateActionMultiFiles($model, $file_model, $code = 1, $media_type = 1 ,$img_w = NULL, $img_thumb_w = NULL, $img_thumb_h = NULL, $img_h = NULL){
    $model->url_name = Setup::generateUniqueUrl($model);
    if($model->save()){
      
      $file_model->uploadFiles($model->id, $file_model, $media_type, $img_w, $img_thumb_w, $img_thumb_h, $img_h);
        
      $items = $this->_loadMultipleLangs($code);
      
      if(PageLang::loadMultiple($items, \Yii::$app->request->post())){
        foreach($items as $item):
          ($code == 0) ? $item->page_id = $model->id : $item->relation_id = $model->id;
          if($item->save()){
            Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-success'], 'body'=>Yii::t('backend', 'All data was successfully saved')]);
          } else {
            Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-danger'], 'body'=>Yii::t('backend', 'Error occurred during saving all data')]);
          }
        endforeach;
      } else {
        Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-danger'], 'body'=>Yii::t('backend', 'Error occurred during saving language realted data')]);
      }
      if(Yii::$app->request->referrer !== Null)
        return $this->redirect(Yii::$app->request->referrer);
      else
        return $this->redirect("/".$this->id."/index");
    } else {
      Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-danger'], 'body'=>Yii::t('backend', 'Error occurred during saving all data')]);
      return $this->refresh();
    }
  }
  
  public function partialUpdateMultipleFiles($model, $ulmodel, $file_model, $media_type = 1 , $img_w = NULL, $img_thumb_w = NULL, $img_thumb_h = NULL, $img_h = NULL){
    
    $model->url_name = Setup::generateUniqueUrl($model);

    $file_model->uploadFiles($model->id, $file_model, $media_type, $img_w, $img_thumb_w, $img_thumb_h, $img_h);
    
    if($model->save()){
      
      // upload only if valid uploaded file instance found
      if(PageLang::loadMultiple($ulmodel, Yii::$app->request->post())
          && PageLang::validateMultiple($ulmodel)){
        foreach($ulmodel as $item):
          if(empty($item->meta_keywords) && !empty($item->meta_description)) $item->meta_keywords = Setup::keywrodGenerator($item->meta_description);
          if($item->save()){
            Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-success'], 'body'=>Yii::t('backend', 'All data was successfully saved')]);
          } else {
            Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-danger'], 'body'=>Yii::t('backend', 'Error occurred during saving all data')]);
          }
        endforeach;
      } else {
        Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-danger'], 'body'=>Yii::t('backend', 'Error occurred during saving language realted data')]);
      }
      if(Yii::$app->request->referrer !== Null)
        return $this->redirect(Yii::$app->request->referrer);
      else
        return $this->redirect("/".$this->id."/index");
      #return $this->redirect(\yii\helpers\Url::previous());
      #return $this->refresh();
    } else {
      Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-danger'], 'body'=>Yii::t('backend', 'Error occurred during saving all data')]);
      return $this->refresh();      
    }
  }
  
  public function actionDeleteImage($id){
    $model = MediaStorage::findOne($id);

    $filename = Yii::getAlias('@storage').$model->media_path;

    if(is_file($filename)) unlink ($filename);

    $model->delete();
    
    if(Yii::$app->request->referrer !== Null)
      return $this->redirect(Yii::$app->request->referrer);
    else
      return $this->redirect(Yii::$app->getHomeUrl());
  }
  
  /**
  * Finds the Page model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return Page the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findModel($id){
    if (($model = Page::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }
  
}