<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%property_direction_points}}".
 *
 * @property integer $id
 * @property integer $property_id
 * @property integer $call_count
 * @property integer $message_count
 */
class PropertyStats extends \common\components\classes\CommonActiveRecord{
  /**
   * @inheritdoc
   */
  public static function tableName(){
    return '{{%properties_stats}}';
  }

  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['property_id', 'message_count'], 'integer'],
      [['property_id', 'message_count'], 'required']
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels(){
    return [
      'id' => Yii::t('common', 'ID'),
      'property_id' => Yii::t('common', 'Property'),
      'message_count' => Yii::t('common', 'Messages count'),
      'call_count' => Yii::t('common', 'Calls count'),
    ];
  }
}
