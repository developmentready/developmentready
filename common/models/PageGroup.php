<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%page_group}}".
 *
 * @property integer $id
 * @property integer $status
 * @property integer $creator_id
 * @property integer $created_at
 * @property integer $updater_id
 * @property integer $updated_at
 *
 * @property PageGroupLang[] $pageGroupLangs
 */
class PageGroup extends \common\components\classes\BackendActiveRecord{
  /**
   * @inheritdoc
   */
  public static function tableName(){
    return '{{%page_group}}';
  }

  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['creator_id', 'created_at', 'updater_id', 'updated_at', 'status'], 'integer']
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels(){
    return [
      'id' => Yii::t('common', 'ID'),
      'status' => Yii::t('common', 'Status'),
      'creator_id' => Yii::t('common', 'Creator ID'),
      'created_at' => Yii::t('common', 'Created At'),
      'updater_id' => Yii::t('common', 'Updater ID'),
      'updated_at' => Yii::t('common', 'Updated At'),
    ];
  }
  
  /**
   * @return \yii\db\ActiveQuery
   */
  public function getPageGroupLangs() {
    return $this->hasMany(PageGroupLang::className(), ['relation_id' => 'id']);
  }
  
  /**
  * @return \yii\db\ActiveQuery
  */
  public function getTranslation(){
    $code = $this->getLanguageID(\Yii::$app->language);
    return $this->hasOne(PageGroupLang::className(), ['relation_id' => 'id'])
          ->where(['lang_id' => $code]);
  }
}
