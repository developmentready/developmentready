<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/26/15
 * Time: 10:29
 */

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%property_agency_agents_assn}}".
 *
 * @property integer $id
 * @property integer $property_id
 * @property integer $user_id
 */
class PropertyUser extends \common\components\classes\CommonActiveRecord{
  /**
   * @inheritdoc
   */
  public static function tableName(){
    return '{{%property_user_assn}}';
  }

  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['property_id', 'user_id'], 'required'],
      [['property_id', 'user_id'], 'integer']
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels(){
    return [
      'id' => Yii::t('app', 'ID'),
      'property_id' => Yii::t('app', 'Property ID'),
      'user_id' => Yii::t('app', 'Agency\\User'),
    ];
  }
}