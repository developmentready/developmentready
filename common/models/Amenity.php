<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%amenities}}".
 *
 * @property integer $id
 * @property integer $status
 * @property integer $priority
 * @property integer $creator_id
 * @property integer $created_at
 * @property integer $updater_id
 * @property integer $updated_at
 *
 * @property AmenitiesLang[] $amenitiesLangs
 * @property PropertyAmenities[] $propertyAmenities
 */
class Amenity extends \common\components\classes\BackendActiveRecord{
    /**
     * @inheritdoc
     */
    public static function tableName(){
        return '{{%amenities}}';
    }

    /**
     * @inheritdoc
     */
    public function rules(){
        return [
          [['status', 'priority', 'creator_id', 'created_at', 'updater_id', 'updated_at'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(){
        return [
          'id' => Yii::t('common', 'ID'),
          'status' => Yii::t('common', 'Status'),
          'priority' => Yii::t('common', 'Priority'),
          'creator_id' => Yii::t('common', 'Creator'),
          'created_at' => Yii::t('common', 'Created At'),
          'updater_id' => Yii::t('common', 'Updater'),
          'updated_at' => Yii::t('common', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAmenityLangs(){
        return $this->hasMany(AmenityLang::className(), ['relation_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyAmenities(){
      return $this->hasMany(PropertyAmenities::className(), ['amenity_id' => 'id']);
    }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getTranslation(){
    $code = $this->getLanguageID(\Yii::$app->language);
    return $this->hasOne(AmenityLang::className(), ['relation_id' => 'id'])
      ->where(['lang_id' => $code]);
  }
}
