<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%countries}}".
 *
 * @property integer $id
 * @property integer $status
 * @property string $code
 * @property string $title
 * @property string $default_country
 * @property integer $country_currency_id
 * @property integer $creator_id
 * @property integer $created_at
 * @property integer $updater_id
 * @property integer $updated_at
 *
 * @property User $creator
 * @property User $updater
 * @property Currency[] $currency
 */
class Country extends \common\components\classes\BackendActiveRecord{
  /**
   * @inheritdoc
   */
  public static function tableName(){
    return '{{%countries}}';
  }

  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['status', 'creator_id', 'created_at', 'updater_id', 'updated_at'], 'integer'],
      [['code'], 'string', 'max' => 3],
      [['title'], 'string', 'max' => 255],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels(){
    return [
      'id' => Yii::t('common', 'ID'),
      'status' => Yii::t('common', 'Status'),
      'code' => Yii::t('common', 'Code'),
      'title' => Yii::t('common', 'Name Of Country'),
      'default_country' => Yii::t('common', 'Is this default country for whole application?'),
      'country_currency_id' => Yii::t('common', 'Country Currency'),
      'creator_id' => Yii::t('common', 'Creator ID'),
      'created_at' => Yii::t('common', 'Created At'),
      'updater_id' => Yii::t('common', 'Updater ID'),
      'updated_at' => Yii::t('common', 'Updated At'),
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCreator(){
    return $this->hasOne(User::className(), ['id' => 'creator_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUpdater(){
    return $this->hasOne(User::className(), ['id' => 'updater_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getDefaultCity(){
    return $this->hasMany(City::className(), ['country_id' => 'id'])
      ->andWhere(['default' => 1]);
  }
}
