<?php
/**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 7/11/15
 * Time: 16:05
 */

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%cities}}".
 *
 * @property integer $id
 * @property integer $status
 * @property integer $creator_id
 * @property integer $created_at
 * @property integer $updater_id
 * @property integer $updated_at
 */
class Period extends \common\components\classes\BackendActiveRecord{
  /**
   * @inheritdoc
   */
  public static function tableName(){
    return '{{%periods}}';
  }

  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['status', 'priority', 'creator_id', 'created_at', 'updater_id', 'updated_at'], 'integer']
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels(){
    return [
      'id' => Yii::t('common', 'ID'),
      'status' => Yii::t('common', 'Status'),
      'priority' => Yii::t('common', 'Priority'),
      'creator_id' => Yii::t('common', 'Creator'),
      'created_at' => Yii::t('common', 'Created At'),
      'updater_id' => Yii::t('common', 'Updater'),
      'updated_at' => Yii::t('common', 'Updated At'),
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCreator(){
    return $this->hasOne(User::className(), ['id' => 'creator_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUpdater(){
    return $this->hasOne(User::className(), ['id' => 'updater_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getPeriodLangs(){
    return $this->hasMany(PeriodLang::className(), ['relation_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getTranslation(){
    $code = $this->getLanguageID(\Yii::$app->language);
    return $this->hasOne(PeriodLang::className(), ['relation_id' => 'id'])
      ->where(['lang_id' => $code]);
  }
}
