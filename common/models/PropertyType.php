<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%property_types_selection}}".
 *
 * @property integer $id
 * @property integer $status
 * @property string $title
 * @property integer $creator_id
 * @property integer $created_at
 * @property integer $updater_id
 * @property integer $updated_at
 */
class PropertyType extends \common\components\classes\BackendActiveRecord{
  /**
   * @inheritdoc
   */
  public static function tableName(){
    return '{{%property_types}}';
  }

  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['status', 'creator_id', 'created_at', 'updater_id', 'updated_at'], 'integer'],
      [['title'], 'string', 'max' => 255]
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels(){
    return [
      'id' => Yii::t('app', 'ID'),
      'status' => Yii::t('app', 'Status'),
      'title' => Yii::t('app', 'Title'),
      'creator_id' => Yii::t('app', 'Creator ID'),
      'created_at' => Yii::t('app', 'Created At'),
      'updater_id' => Yii::t('app', 'Updater ID'),
      'updated_at' => Yii::t('app', 'Updated At'),
    ];
  }
}