<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%languages}}".
 *
 * @property integer $id
 * @property string $code
 * @property string $title
 * @property integer $default
 * @property string $local
 * @property integer $priority
 * @property integer $status
 * @property string $image_url
 * @property integer $creator_id
 * @property integer $created_at
 * @property integer $updater_id
 * @property integer $updated_at
 *
 * @property CoursesLang[] $coursesLangs
 * @property FaqLang[] $faqLangs
 * @property User $creator
 * @property User $updater
 * @property NewsLang[] $newsLangs
 */
class Language extends \common\components\classes\BackendActiveRecord{
  //Variable, save current lang
  static $current = null;
  
  /**
   * @inheritdoc
   */
  public static function tableName(){
    return '{{%languages}}';
  }

  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['code','local'], 'required'],
      [['default_lang', 'priority', 'status', 'creator_id', 'created_at', 'updater_id', 'updated_at'], 'integer'],
      [['code'], 'string', 'max' => 3],
      [['title','local'], 'string', 'max' => 50],
      [['image_url'], 'string', 'max' => 1024]
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels(){
    return [
      'id' => Yii::t('app', 'ID'),
      'code' => Yii::t('app', 'Code'),
      'title' => Yii::t('app', 'Title'),
      'local' => Yii::t('app', 'Local'),
      'default_lang' => Yii::t('app', 'Default Lang'),
      'priority' => Yii::t('app', 'Priority'),
      'status' => Yii::t('app', 'Status'),
      'image_url' => Yii::t('app', 'Image Url'),
      'creator_id' => Yii::t('app', 'Creator ID'),
      'created_at' => Yii::t('app', 'Created At'),
      'updater_id' => Yii::t('app', 'Updater ID'),
      'updated_at' => Yii::t('app', 'Updated At'),
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCoursesLangs(){
    return $this->hasMany(CoursesLang::className(), ['lang_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getFaqLangs(){
    return $this->hasMany(FaqLang::className(), ['lang_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCreator(){
    return $this->hasOne(User::className(), ['id' => 'creator_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUpdater(){
    return $this->hasOne(User::className(), ['id' => 'updater_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getNewsLangs(){
    return $this->hasMany(NewsLang::className(), ['lang_id' => 'id']);
  }
  
  /**
   *  New defined methods
   * 
   */
  
  //Get current lang Object
  static function getCurrent(){
    if(self::$current === null){
      self::$current = self::getDefaultLang();
    }
    return self::$current;
  }
  
  //Установка текущего объекта языка и локаль пользователя
  static function setCurrent($url = null){
    $language = self::getLangByUrl($url);
    self::$current = ($language === null) ? self::getDefaultLang() : $language;
    Yii::$app->language = self::$current->local;
  }

  //Получения объекта языка по умолчанию
  static function getDefaultLang(){
    return Language::find()->where('`default` = :default', [':default' => 1])->one();
  }

  //Получения объекта языка по буквенному идентификатору
  static function getLangByUrl($url = null){
    if ($url === null) {
      return null;
    } else {
      $language = Language::find()->where('code = :code', [':code' => $url])->one();
      if ( $language === null ) {
        return null;
      }else{
        return $language;
      }
    }
  }
  
}
