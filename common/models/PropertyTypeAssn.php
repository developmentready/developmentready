<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%property_types}}".
 *
 * @property integer $id
 * @property integer $property_id
 * @property integer $property_type_id
 *
 * @property Properties[] $properties
 */
class PropertyTypeAssn extends \common\components\classes\CommonActiveRecord{
  /**
   * @inheritdoc
   */
  public static function tableName(){
    return '{{%property_types_assn}}';
  }

  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['property_type_id', 'property_id'], 'integer'],
      [['property_type_id'], 'required']
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels(){
    return [
      'id' => Yii::t('common', 'ID'),
      'property_type_id' => Yii::t('common', 'Property Type'),
      'property_id' => Yii::t('common', 'Property'),
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getProperty(){
    return $this->hasMany(Property::className(), ['property_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getTypes(){
    return $this->hasMany(PropertyType::className(), ['property_type_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getType(){
    return $this->hasOne(PropertyType::className(), ['id' => 'property_type_id']);
  }
}
