<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/18/15
 * Time: 00:56
 */

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%agency_positions}}".
 *
 * @property integer $id
 * @property integer $status
 * @property string $title
 * @property integer $creator_id
 * @property integer $created_at
 * @property integer $updater_id
 * @property integer $updated_at
 *
 * @property AgencyAgents[] $agencyAgents
 */
class AgencyPosition extends \common\components\classes\BackendActiveRecord{
  /**
   * @inheritdoc
   */
  public static function tableName(){
    return '{{%agency_positions}}';
  }

  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['status', 'creator_id', 'created_at', 'updater_id', 'updated_at'], 'integer'],
      [['title'], 'string', 'max' => 250]
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels(){
    return [
      'id' => Yii::t('app', 'ID'),
      'status' => Yii::t('app', 'Active ?'),
      'title' => Yii::t('app', 'Title'),
      'creator_id' => Yii::t('app', 'Creator ID'),
      'created_at' => Yii::t('app', 'Created At'),
      'updater_id' => Yii::t('app', 'Updater ID'),
      'updated_at' => Yii::t('app', 'Updated At'),
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getAgencyAgent(){
    return $this->hasMany(AgencyAgent::className(), ['position_id' => 'id']);
  }
}