<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%addresses}}".
 *
 * @property integer $id
 * @property integer $property_id
 * @property integer $suburb_id
 * @property double  $lat
 * @property double  $lng
 *
 * @property Suburb $suburb
 * @property Property $property
 */
class Address extends \common\components\classes\CommonActiveRecord{
  /**
   * @inheritdoc
   */
  public static function tableName(){
    return '{{%addresses}}';
  }

  public function behaviors(){
    return [
      'timestamp' => [
        'class'      => 'yii\behaviors\TimestampBehavior',
        'attributes' => [
          self::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
          self::EVENT_BEFORE_UPDATE => 'updated_at',
        ],
      ],
    ];
  }

  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['property_id', 'suburb_id'], 'integer'],
      [['lat', 'lng'], 'number'],
      [['address'], 'string', 'max' => 255],

      [['address'], 'required' ],

//      ['missing_location', 'required', 'when' => function($model) {return $model->new_city_name === 1;}
//        , 'whenClient' => "function (attribute, value) {return $('#address-city_id').val() == 1;}"
//        ,'message' => Yii::t('common','If there is no name of your city, please enter it.')],

      //[['email', 'contact_person_name', 'advertiser_type'], 'required', 'on' => 'guest-property'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels(){
    return [
      'id' => Yii::t('common', 'ID'),
      'property_id' => Yii::t('common', 'Property'),
      'suburb_id' => Yii::t('common', 'Suburb'),
      'address' => Yii::t('common', 'Address'),
      'lyd' => Yii::t('common', 'Latitude'),
      'lng' => Yii::t('common', 'Longitude'),
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getSuburb(){
    return $this->hasOne(Suburb::className(), ['id' => 'suburb_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getProperty(){
    return $this->hasOne(Property::className(), ['id' => 'property_id']);
  }
}
