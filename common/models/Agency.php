<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/16/15
 * Time: 15:05
 */

namespace common\models;

use Yii;
use gregwar\image\Image;
use Imagine\Image\Box;
use common\models\Suburb;
use common\models\PropertyUser;
use common\models\Property;
use yii\db\Query;

/**
 * This is the model class for table "{{%agencies}}".
 *
 * @property integer $id
 * @property integer $status
 * @property integer $priority
 * @property integer $user_id
 * @property integer $featured_aceny
 * @property integer $show_logo_single_page
 * @property string $title
 * @property string $slug
 * @property string $about
 * @property string $address
 * @property string $ext_id
 * @property double $lat
 * @property double $lng
 * @property string $phone
 * @property string $phone_2
 * @property string $email
 * @property string $mobile
 * @property string $fax
 * @property string $web
 * @property string $color
 * @property string $secondary_color
 * @property integer $main_logo_id
 * @property string $logo_thumbnail
 * @property integer $creator_id
 * @property integer $created_at
 * @property integer $updater_id
 * @property integer $updated_at
 *
 * @property User $creator
 * @property User $updater
 * @property AgencyEmployees[] $agencyEmployees
 */
class Agency extends \common\components\classes\BackendActiveRecord{

  /**
   * @inheritdoc
   */
  public static function tableName(){
    return '{{%agencies}}';
  }

  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['about'], 'string'],
      [['lat', 'lng'], 'number'],
      [['creator_id', 'priority', 'featured_aceny', 'main_logo_id', 'created_at', 'updater_id',
        'updated_at', 'status', 'user_id', 'show_logo_single_page'], 'integer'],
      [['title', 'address', 'ext_id'], 'string', 'max' => 255],
      [['phone', 'phone_2', 'email', 'mobile', 'fax', 'web'], 'string', 'max' => 50],
      [['color', 'secondary_color'], 'string', 'max' => 7],
      [['logo_thumbnail', 'slug'], 'string', 'max' => 1024],
      [['title', 'user_id'], 'required'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels() {
    return [
      'id' => 'ID',
      'status' => 'Active?',
      'slug' => 'Link name',
      'ext_id' => 'Agency External ID',
      'featured_aceny' => 'Featured Agency?',
      'priority' => 'Priority/Sort',
      'show_logo_single_page' => 'Show logo in agency page',
      'user_id' => 'Username',
      'title' => 'Name of Agency',
      'about' => 'About',
      'address' => 'Location',
      'lat' => 'Lat',
      'lng' => 'Lng',
      'phone' => 'Phone',
      'phone_2' => 'Phone 2',
      'email' => 'Email',
      'mobile' => 'Mobile',
      'fax' => 'Fax',
      'web' => 'Web',
      'color' => 'Color',
      'secondary_color' => 'Secondary Color',
      'main_logo_id' => 'Agency Logo',
      'logo_thumbnail' => 'Logo Thumbnail',
      'creator_id' => 'Creator',
      'created_at' => 'Created At',
      'updater_id' => 'Updater',
      'updated_at' => 'Updated At',
    ];
  }


  /**
   * @param bool $insert
   * @return bool
   */
  public function beforeSave($insert){
    if (parent::beforeSave($insert)) {
      $this->slug = \common\components\helpers\Setup::generateAgencyUniqueUrl($this);
      return true;
    } else {
      return false;
    }
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCreator(){
    return $this->hasOne(User::className(), ['id' => 'creator_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUpdater(){
    return $this->hasOne(User::className(), ['id' => 'updater_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUser(){
    return $this->hasOne(User::className(), ['id' => 'user_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getAgencyAgent(){
    return $this->hasMany(AgencyAgent::className(), ['agency_id' => 'id']);
  }

  public function viewAgencyAgents(){
    return \common\models\AgencyAgent::find()
      ->where('agency_id = :agency_id AND priority < 100', ['agency_id' => $this->id])
      ->orderBy(['priority' => SORT_ASC, 'updated_at' => SORT_DESC])
      ->all();
  }

  /**
   *
   * @return $this
   */
  public function getPropertyAgents($property_id){
    return \common\models\PropertyAgents::find()->joinWith('agent')
      ->where(['property_id' => $property_id])->all();
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getMedia(){
    return $this->hasMany(MediaStorage::className(), ['model_id' => 'id'])
      ->andOnCondition(['model' => 'Agency']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getImage(){
    return \common\models\MediaStorage::find()->where(['model_id' => $this->id])
      ->andWhere(['model' => 'Agency'])->andWhere(['default' => 1])->one();
  }

  /**
   * Create new watermark image to clip on default image of permit
   * @param $image
   * * @param $backend
   */
  public function createAgencyWatermark($image, $backend = true){
    $storage = \Yii::getAlias('@storage');

    # find model agency
    # main color of agency
    $color = !empty($this->color) ? str_replace("#","0x",$this->color) : '0xffffff';

    if(!empty($old_image = $this->logo_thumbnail)){
      if(is_file($storage.$old_image)){unlink($storage.$old_image);}
      if(is_file($file = $storage."/web/agency/watermarks/half/".basename($old_image))){unlink($file);}
    }

    # new uploaded logo file (we will resize it for watermarking purposes)
    $image_path = $storage."/source/".$image['path'];
    //return $image_path;
    $uploaded_logo_name = basename($image['path']);


    # creates new image with Agency color background
    $img_src = Image::create(800, 70)
      //->fill($color)    // Filling with a light red
      //->rectangle(0xff3333, 0, 100, 300, 200, true) // Drawing a red rectangle
      ->rectangle(0, 0, 800, 70, $color, true)
      ->png();

    # new created filled image file
    if($backend)
      $new_image = \Yii::getAlias('@backend/web/').$img_src;
    else
      $new_image = \Yii::getAlias('@frontend/web/').$img_src;

    # name of the new created background file
    $new_image_name = basename($new_image);

    $new_watermark_logo_path = $storage."/web/agency/watermarks/".$uploaded_logo_name;

    # new uploaded logo resized for watermark
    $new_watermark_logo_path = Image::open($image_path)->scaleResize(115, 70, $this->color)->save($new_watermark_logo_path);

    # resize new loaded logo and save it in new folder
    $new_watermark_bg = $storage."/web/agency/watermarks/".time()."-".$new_image_name;

    \yii\imagine\Image::watermark($new_image , $new_watermark_logo_path, [30, 0])->save($new_watermark_bg);

    $agency_logo = "/web/agency/watermarks/".basename($new_watermark_bg);

    $this->logo_thumbnail = $agency_logo; $this->save();

    Image::open($storage.$agency_logo)->crop(0, 0, 400, 70)->save($storage."/web/agency/watermarks/half/".basename($agency_logo));

    if(is_file($new_watermark_logo_path)){unlink($new_watermark_logo_path);}
    if(is_file($new_image)){unlink($new_image);}
  }

  /**
   * Create new watermark image to clip on default image of permit
   * @param $model_id
   * @param $image
   */
  public function createPropertyWatermark($model_id, $image){
    $storage = \Yii::getAlias('@storage');

    # find model agency
    $model = \common\models\Agency::findOne($model_id);
    # main color of agency
    $color = str_replace("#","0x",$model->color);

    # new uploaded logo file (we will resize it for watermarking purposes)
    $image_path = \Yii::getAlias('@storage')."/source/".$image['path'];
    $uploaded_logo_name = basename($image['path']);


    # creates new image with Agency color background
    $img_src = Image::create(800, 40)
      ->fill($color)    // Filling with color
      //->rectangle(0xff3333, 0, 100, 300, 200, true) // Drawing a red rectangle
      ->rectangle($color, 0, 800, 40, 800, true)
      ->png();

    # new created filled image file
    $new_image = \Yii::getAlias('@backend/web/').$img_src;

    # name of the new created background file
    $new_image_name = basename($new_image);

    $new_watermark_logo_path = $storage."/web/agency/watermarks/".$uploaded_logo_name;

    # new uploaded logo resized for watermark
    $new_watermark_logo_path = Image::open($image_path)->scaleResize(95, 40, $model->color)->save($new_watermark_logo_path);

    # resize new loaded logo and save it in new folder
    $new_watermark_bg = $storage."/web/agency/watermarks/".$new_image_name;

    \yii\imagine\Image::watermark($new_image , $new_watermark_logo_path, [20, 0])->save($new_watermark_bg);

    $agency_logo = "/web/agency/watermarks/".basename($new_watermark_bg);

    $model->logo_thumbnail = $agency_logo; $model->save();

    Image::open($storage.$agency_logo)->crop(0, 0, 400, 70)->save("/web/agency/watermarks/half/".basename($new_watermark_bg));

    if(is_file($new_watermark_logo_path)){unlink($new_watermark_logo_path);}
    if(is_file($new_image)){unlink($new_image);}
  }

  public function getFeatured($state_id,$limit = 6){

    $curr_state_user_model = (new Query())
        ->addSelect(['property_user.user_id'])
        ->from([PropertyUser::tableName() . ' property_user'])
        ->leftJoin(Property::tableName() . ' property', 'property.id = property_user.property_id')
        ->leftJoin(Suburb::tableName() . ' suburb', 'suburb.id = property.suburb_id')
        ->where(['suburb.state_id'=>$state_id])
        ->groupBy('property_user.user_id');
      $ress = $curr_state_user_model->indexBy('user_id')->all();

    $agency_feature_model = self::find()
        ->where(['in','user_id',$ress])
        ->andWhere(['featured_aceny'=>1])
        ->limit($limit);
    $ress2 = $agency_feature_model->all();

    return $ress2;
  }

}