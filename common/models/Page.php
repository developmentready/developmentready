<?php

namespace common\models;

use Yii;
use common\components\helpers\Setup;

/**
 * This is the model class for table "{{%pages}}".
 *
 * @property integer $id
 * @property integer $status
 * @property integer $priority
 * @property integer $page_robot_indexing
 * @property string $url_name
 * @property string $title
 * @property string $h1_tag
 * @property string $body
 * @property string $menu_label
 * @property string $show_contact_form
 * @property string $show_testimonial
 * @property string $additional_text
 * @property string $meta_keywords
 * @property string $meta_description
 * @property integer $page_type_id
 * @property integer $page_category_id
 * @property integer $page_template_id
 * @property integer $parent_id
 * @property integer $view_counter
 * @property integer $menu_type
 * @property string $video_link
 * @property integer $related_media_id
 * @property integer $publication_date
 * @property integer $creator_id
 * @property integer $created_at
 * @property integer $updater_id
 * @property integer $updated_at
 *
 * @property User $creator
 * @property User $updater
 * @property PageTypes $pageType
 */
class Page extends \common\components\classes\BackendActiveRecord{

  const PAGE_COMMON     = 1;
  const PAGE_PROPERTIES = 2;
  const PAGE_BLOG       = 3;
  const PAGE_AGENCY     = 4;
  const PAGE_ACCOUNT    = 5;


  /**
   * @inheritdoc
   */
  public static function tableName(){
    return '{{%pages}}';
  }

  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['status', 'priority', 'page_type_id', 'page_category_id', 'page_template_id', 'parent_id',
        'view_counter', 'menu_type', 'related_media_id', 'creator_id', 'created_at',
        'updater_id', 'updated_at', 'page_robot_indexing', 'show_contact_form', 'show_testimonial'], 'integer'],
      [['title'], 'required'],
      [['url_name'], 'required', 'on' => 'page-update'],
      [['h1_tag'], 'required', 'on' => 'page-with-header'],
      [['body', 'additional_text'], 'string'],
      [['url_name', 'title', 'h1_tag', 'menu_label', 'meta_keywords'], 'string', 'max' => 255],
      [['meta_description', 'video_link'], 'string', 'max' => 1024],
      [['show_contact_form', 'show_testimonial'], 'default', 'value' => self::STATUS_DRAFT],
      ['menu_label', 'required', 'when' => function($model) {return $model->menu_type === 1;}
        , 'whenClient' => "function (attribute, value) {return $('#page-menu_type').val() == 1;}"
        ,'message' => Yii::t('app','Please add menu label')],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels(){
    return [
      'id' => Yii::t('app', 'ID'),
      'status' => Yii::t('app', 'Active ?'),
      'page_robot_indexing' => Yii::t('app', 'Index by search engines ?'),
      'priority' => Yii::t('app', 'Priority'),
      'url_name' => Yii::t('app', 'Slug Name'),
      'show_contact_form' => Yii::t('app', 'Show contact form in current page'),
      'show_testimonial' => Yii::t('app', 'Show testimonial in current page'),
      'title' => Yii::t('app', 'Title'),
      'h1_tag' => Yii::t('app', 'Page header'),
      'body' => Yii::t('app', 'Body'),
      'additional_text' => Yii::t('app', 'Additional field'),
      'meta_keywords' => Yii::t('app', 'Meta Keywords'),
      'meta_description' => Yii::t('app', 'Meta Description'),
      'page_type_id' => Yii::t('app', 'Page Type'),
      'page_category_id' => Yii::t('app', 'Page Category'),
      'page_template_id' => Yii::t('app', 'Page Template'),
      'parent_id' => Yii::t('app', 'Parent Page In Navigation Menu'),
      'view_counter' => Yii::t('app', 'Views'),
      'menu_type' => Yii::t('app', 'Menu Type'),
      'video_link' => Yii::t('app', 'Video Link'),
      'related_media_id' => Yii::t('app', 'Related Media'),
      'publication_date' => Yii::t('app', 'Publication Date'),
      'creator_id' => Yii::t('app', 'Creator'),
      'created_at' => Yii::t('app', 'Created At'),
      'updater_id' => Yii::t('app', 'Updater'),
      'updated_at' => Yii::t('app', 'Updated At'),
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getImage(){
    return \common\models\MediaStorage::find()->where(['model_id' => $this->id])
      ->andWhere(['model' => 'Page'])->andWhere(['default' => 1])->one();
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getWithImages(){
    return $this->hasMany(MediaStorage::className(), ['model_id' => 'id'])
      ->andWhere(['model' => 'Page']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCreator(){
    return $this->hasOne(User::className(), ['id' => 'creator_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUpdater(){
    return $this->hasOne(User::className(), ['id' => 'updater_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getPageType(){
    return $this->hasOne(PageTypes::className(), ['id' => 'page_type_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getParent(){
    return $this->hasOne(self::className(), ['id' => 'parent_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCategory(){
    return $this->hasOne(PageCategory::className(), ['id' => 'page_category_id']);
  }

  /**
   * For all pages create url
   * @param bool $insert
   * @return bool
   */
  public function beforeSave($insert){
    if(empty($this->parent_id)) $this->parent_id = 1;

    if($this->page_template_id !== 5)
      $this->url_name = Setup::generateUniqueUrl($this);

    if (parent::beforeSave($insert)){
      return true;
    } else {
      return true;
    }
  }

  /**
   * @return array
   */
  public function menuPosition(){
    $output = [
      0 => Yii::t('app', 'Not in menu'),
      1 => Yii::t('app', 'In menu'),
      2 => Yii::t('app', 'In menu (Only Link)'),
    ];
    return $output;
  }


  public function pageTemplate(){
    return [
      self::PAGE_COMMON => 'Common Pages',
      self::PAGE_PROPERTIES => 'Properties',
      self::PAGE_BLOG => 'Blog Page',
    ];
  }

}
