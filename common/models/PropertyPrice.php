<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%property_prices}}".
 *
 * @property string $id
 * @property integer $property_id
 * @property integer $currency_id
 * @property double $value
 * @property double $equivalent_value
 *
 * @property Currencies $currency
 * @property Properties $property
 */
class PropertyPrice extends \common\components\classes\CommonActiveRecord{
  /**
   * @inheritdoc
   */
  public static function tableName(){
    return '{{%property_prices}}';
  }

  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['property_id', 'currency_id'], 'integer'],
      [['value', 'equivalent_value'], 'number'],

      [['value'], 'required'],

    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels(){
    return [
      'id' => Yii::t('common', 'ID'),
      'property_id' => Yii::t('common', 'Property'),
      'currency_id' => Yii::t('common', 'Currency'),
      'value' => Yii::t('common', 'Property Price'),
      'equivalent_value' => Yii::t('common', 'Equivalent Value'),
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCurrency(){
    return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getProperty(){
    return $this->hasOne(Properties::className(), ['id' => 'property_id']);
  }
}
