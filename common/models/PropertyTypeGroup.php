<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%property_type_groups}}".
 *
 * @property integer $id
 * @property integer $status
 * @property integer $priority
 * @property integer $creator_id
 * @property integer $created_at
 * @property integer $updater_id
 * @property integer $updated_at
 *
 * @property PropertyTypeGroupsLang[] $propertyTypeGroupsLangs
 */
class PropertyTypeGroup extends \common\components\classes\BackendActiveRecord{
  /**
   * @inheritdoc
   */
  public static function tableName()
  {
      return '{{%property_type_groups}}';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
      return [
          [['status', 'priority', 'creator_id', 'created_at', 'updater_id', 'updated_at'], 'integer']
      ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
      return [
          'id' => Yii::t('common', 'ID'),
          'status' => Yii::t('common', 'Status'),
          'priority' => Yii::t('common', 'Priority'),
          'creator_id' => Yii::t('common', 'Creator ID'),
          'created_at' => Yii::t('common', 'Created At'),
          'updater_id' => Yii::t('common', 'Updater ID'),
          'updated_at' => Yii::t('common', 'Updated At'),
      ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getPropertyTypeGroupsLangs()
  {
      return $this->hasMany(PropertyTypeGroupLang::className(), ['relation_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getChild(){
      return $this->hasMany(PropertyType::className(), ['parent_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getTranslation(){
    $code = $this->getLanguageID(\Yii::$app->language);
    return $this->hasOne(PropertyTypeGroupLang::className(), ['relation_id' => 'id'])
      ->where(['lang_id' => $code]);
  }

  public static function getPropertyList(){
    $options = [];

    $parents = self::find()->active()->joinWith('translation')->all();

    foreach($parents as $type) {
      $children = \common\models\PropertyType::find()->where(['parent_id' => $type->id])->active()->joinWith('translation')->all();
      //self::find()->where("parent_id=:parent_id", [":parent_id"=>$p->id])->all();
      $child_options = [];
      foreach($children as $child) {
        $child_options[$child->id] = $child->translation->title;
      }
      $options[$type->translation->title] = $child_options;
    }
    return $options;
  }
}
