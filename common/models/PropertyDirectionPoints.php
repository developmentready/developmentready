<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%property_direction_points}}".
 *
 * @property integer $id
 * @property integer $property_id
 * @property integer $direction_point_id
 */
class PropertyDirectionPoints extends \common\components\classes\CommonActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%property_direction_points}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['property_id', 'direction_point_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'property_id' => Yii::t('common', 'Property ID'),
            'direction_point_id' => Yii::t('common', 'Direction Point ID'),
        ];
    }
}
