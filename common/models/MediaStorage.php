<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/17/15
 * Time: 16:29
 */

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%media_storage}}".
 *
 * @property integer $id
 * @property string $model
 * @property integer $model_id
 * @property string $base_name
 * @property string $media_path
 * @property integer $size
 * @property string $type
 * @property string $alt
 * @property string $caption
 * @property string $created_at
 */
class MediaStorage extends \common\components\classes\CommonActiveRecord{
  /**
   * @inheritdoc
   */
  public static function tableName(){
    return '{{%media_storage}}';
  }

  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['model_id', 'size', 'default'], 'integer'],
      [['size', 'type', 'media_path', 'model', 'model_id'], 'required'],
      [['created_at'], 'safe'],
      [['model'], 'string', 'max' => 100],
      [['base_name', 'alt', 'caption'], 'string', 'max' => 250],
      [['media_path'], 'string', 'max' => 1024],
      [['type'], 'string', 'max' => 20]
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels(){
    return [
      'id' => Yii::t('app', 'ID'),
      'model' => Yii::t('app', 'Model'),
      'model_id' => Yii::t('app', 'Model ID'),
      'default' => Yii::t('app', 'Default image?'),
      'base_name' => Yii::t('app', 'Base Name'),
      'media_path' => Yii::t('app', 'Media Path'),
      'size' => Yii::t('app', 'Size'),
      'type' => Yii::t('app', 'Type'),
      'alt' => Yii::t('app', 'Alt'),
      'caption' => Yii::t('app', 'Caption'),
      'created_at' => Yii::t('app', 'Created At'),
    ];
  }
}