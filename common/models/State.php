<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/12/15
 * Time: 19:56
 */

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%countries}}".
 *
 * @property integer $id
 * @property integer $status
 * @property string $title
 * @property string $default
 * @property string $country_id
 * @property string $page_title
 * @property string $page_body
 * @property double $lat
 * @property double $lng
 * @property integer $creator_id
 * @property integer $created_at
 * @property integer $updater_id
 * @property integer $updated_at
 *
 * @property User $creator
 * @property User $updater
 * @property Currency[] $currency
 */
class State extends \common\components\classes\BackendActiveRecord{
  /**
   * @inheritdoc
   */
  public static function tableName(){
    return '{{%states}}';
  }

  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['status', 'country_id', 'default', 'creator_id', 'created_at', 'updater_id', 'updated_at'], 'integer'],
      [['country_id', 'title', 'lat', 'lng'], 'required'],
      [['lat', 'lng'], 'double'],
      [['page_body'], 'safe'],
      [['title', 'page_title'], 'string', 'max' => 255],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels(){
    return [
      'id' => Yii::t('common', 'ID'),
      'status' => Yii::t('common', 'Status'),
      'title' => Yii::t('common', 'Name of State'),
      'country_id' => Yii::t('common', 'Country'),
      'page_title' => Yii::t('common', 'Page Title'),
      'page_body' => Yii::t('common', 'Page Body'),
      'lat' => Yii::t('common', 'Latitude'),
      'lng' => Yii::t('common', 'Longitude'),
      'default' => Yii::t('common', 'Default state'),
      'creator_id' => Yii::t('common', 'Creator ID'),
      'created_at' => Yii::t('common', 'Created At'),
      'updater_id' => Yii::t('common', 'Updater ID'),
      'updated_at' => Yii::t('common', 'Updated At'),
    ];
  }


  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCountry(){
    return $this->hasOne(Country::className(), ['id' => 'country_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCreator(){
    return $this->hasOne(User::className(), ['id' => 'creator_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUpdater(){
    return $this->hasOne(User::className(), ['id' => 'updater_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getDefault(){
    return $this->hasMany(City::className(), ['country_id' => 'id'])
      ->andWhere(['default' => 1]);
  }
}