<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/17/15
 * Time: 02:12
 */

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%agency_agents}}".
 *
 * @property integer $id
 * @property integer $status
 * @property integer priority
 * @property string $full_name
 * @property string $about
 * @property integer $agency_id
 * @property integer $position_id
 * @property string $mobile
 * @property string $email
 * @property integer $creator_id
 * @property integer $created_at
 * @property integer $updater_id
 * @property integer $updated_at
 *
 * @property Agencies $agency
 * @property AgencyPositions $position
 */

class AgencyAgent extends \common\components\classes\BackendActiveRecord{
  /**
   * @inheritdoc
   */
  public static function tableName(){
    return '{{%agency_agents}}';
  }

  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['status', 'agency_id', 'priority', 'position_id', 'creator_id', 'created_at', 'updater_id', 'updated_at'], 'integer'],
      [['about'], 'string'],
      [['agency_id', 'full_name'], 'required'],
      [['full_name'], 'string', 'max' => 100],
      [['mobile', 'email'], 'string', 'max' => 50]
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels(){
    return [
      'id' => Yii::t('app', 'ID'),
      'status' => Yii::t('app', 'Active ?'),
      'full_name' => Yii::t('app', 'Full Name'),
      'priority' => Yii::t('app', 'Priority in Agency view (More that 100 will not be shown)'),
      'about' => Yii::t('app', 'About'),
      'agency_id' => Yii::t('app', 'Agency Name'),
      'position_id' => Yii::t('app', 'Position Name'),
      'mobile' => Yii::t('app', 'Mobile'),
      'email' => Yii::t('app', 'Email'),
      'creator_id' => Yii::t('app', 'Creator'),
      'created_at' => Yii::t('app', 'Created At'),
      'updater_id' => Yii::t('app', 'Updater'),
      'updated_at' => Yii::t('app', 'Updated At'),
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getAgency(){
    return $this->hasOne(Agency::className(), ['id' => 'agency_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getPosition(){
    return $this->hasOne(AgencyPosition::className(), ['id' => 'position_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getImage(){
    return $this->hasOne(MediaStorage::className(), ['model_id' => 'id'])
      ->andOnCondition(['model' => 'AgencyAgent']);
  }
}