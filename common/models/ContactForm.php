<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%contact_messages}}".
 *
 * @property integer $id
 * @property string $slug
 * @property string $name
 * @property string $email
 * @property string $subject
 * @property string $phone_number
 * @property string $message
 * @property string $created_at
 */
class ContactForm extends \common\components\classes\CommonActiveRecord{
  #public $verifyCode;
  public $subjects = [];
  public $states = [];
  public $check;

  /**
   * @inheritdoc
   */
  public static function tableName(){
    return '{{%contact_messages}}';
  }

  /**
   * @inheritdoc
   */
  public function rules(){

    return [
      //[['reCaptcha'], \himiklab\yii2\recaptcha\ReCaptchaValidator::className(), 'secret' => '6LcbuCcTAAAAAHygZzgjNssqfNeLNSuzGD7mMAVo'],

      [['name', 'email', 'subjects'], 'required', 'on' => ['send_message']],
      [['name', 'email', 'states[]'], 'required', 'on' => ['subscription']],
      //[['states'], 'each', 'rule' => ['integer', 'required'], 'on' => ['subscription']],
      [['name', 'message', 'email', 'phone_number'], 'required', 'on' => ['testimonial']],

      //[['subject'], 'required', 'on' => ['testimonial'], 'message' => 'Company name can\'t be blank.'],

      //['phone_number', 'match', 'pattern' =>'/\(\d{3}\)\-.\d.-\d{4}/'],
      ['phone_number', 'match', 'pattern' =>'/.[0-9 ]{1,20}$/', 'message' => 'Please enter valid phone number (only numbers, space and "+" allowed).'],
      // ['x', 'each', 'rule' => ['integer']],

      //['states', 'each', 'rule' => ['integer']],
      ['subjects', 'each', 'rule' => ['in', 'range' => [1, 2, 3, 4, 5]]],

      ['email', 'emailUniqueness', 'on' => ['subscription']],
      ['email', 'email'],

      [['slug'], 'string', 'max' => 1024],

      [['contact_type'], 'integer'],
      //[['message'], 'string', 'min' => 20],
      [['created_at', 'subjects', 'message'], 'safe'],
      [['phone_number'], 'string', 'max' => 50],
      [['name', 'email', 'subject','check'], 'string', 'max' => 250],
      ['email', 'email'],
      //['verifyCode', 'captcha'],
    ];
  }

  public function emailUniqueness($attribute, $params){
    $look = self::find()->where('contact_type = 1 AND email = :email',['email' => $this->$attribute]);
    // add custom validation
    if ($look->count() > 0){
      $cookie = new \yii\web\Cookie([
        'name' => 'subscribed',
        'value' => 'yes',
        'expire' => time() + (60*60*24*360),
      ]);

      \Yii::$app->response->cookies->add($cookie);
      $this->addError($attribute, 'You have been already subscribed, thank you.');
    }
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels(){
    return [
      'id' => Yii::t('frontend', 'ID'),
      'name' => Yii::t('frontend', 'Name'),
      'slug' => Yii::t('frontend', 'Agency\\Property url'),
      'states' => Yii::t('frontend', 'State you interested in'),
      'email' => Yii::t('frontend', 'Email'),
      'contact_type' => Yii::t('frontend', 'Type of contact'),
      'subject' => Yii::t('frontend', 'Subject'),
      'phone_number' => Yii::t('frontend', 'Phone Number'),
      'message' => Yii::t('common', 'Message'),
      'created_at' => Yii::t('common', 'Created At'),
      #'verifyCode' => Yii::t('frontend', 'Verification Code'),
    ];
  }

  /**
   * Sends an email to the specified email address using the information collected by this model.
   * @param  object  $model the target email address
   * @return boolean whether the model passes validation
   */
  public function contact($model){
    if ($this->validate()) {
      return Yii::$app->mailer->compose('contact_form', ['model' => $model])
        ->setTo(Yii::$app->params['bccReceipent'])
        ->setFrom(Yii::$app->params['robotEmail'])
        ->setSubject("Contacting from Permit Ready '{$model->slug}' Page")
        ->setTextBody($this->message)
        ->send();
    } else {
      return false;
    }
  }

  /**
   * Sends an email to the specified email address using the information collected by this model.
   * @param  string  $email the target email address
   * @param  string  $model contact form model
   * @return boolean whether the model passes validation
   */
  public function contactAgency($email, $model){
    if ($this->validate()) {
      return Yii::$app->mailer->compose('agency_message', ['model' => $model])
        ->setTo($email)
        ->setFrom(Yii::$app->params['robotEmail'])
        ->setReplyTo([$this->email => $this->name])
        ->setBcc(Yii::$app->params['bccReceipent'])
        ->setSubject('Email from Permit Ready Page')
        ->setTextBody($this->message)
        ->send();
    } else {
      return false;
    }
  }

  /**
   * Send bulk message
   * @param $model
   * @param $mail_list
   * @param string $subject
   * @param string $theme
   */
  public function sendBulkNotificationMessage($model, $mail_list, $subject = 'New message from Permit Ready Web Page.', $theme = 'property_message'){

    foreach ($mail_list as $email){
      $messages[] =
        Yii::$app->mailer->compose($theme, ['model' => $model])
          ->setFrom(Yii::$app->params['robotEmail'])
          ->setSubject(Yii::t('frontend', "{app-name} | {$subject}", [
            'app-name'=>Yii::$app->name]))
          ->setTo(trim($email))
          ->setBcc(Yii::$app->params['bccReceipent']);
      /*Yii::$app->mailer->compose($theme, ['model' => $model])
          ->setTo(trim($email))
          ->setFrom(Yii::$app->params['robotEmail'])
          ->setBcc(Yii::$app->params['bccReceipent'])
          ->setSubject(Yii::t('frontend', "{app-name} | {$subject}", [
              'app-name'=>Yii::$app->name]))
          ->send();*/
    }
    Yii::$app->mailer->sendMultiple($messages);
  }

  /**
   * Sends an email to the specified email address using the information collected by this model.
   * @param  string  $email the target email address
   * @param  string  $model contact form model
   * @return boolean whether the model passes validation
   */
  public function subscriptionInfoMail($model){
    if ($this->validate()) {
      return Yii::$app->mailer->compose('subscription_message', ['model' => $model])
        ->setTo(Yii::$app->params['bccReceipent'])
        ->setFrom(Yii::$app->params['robotEmail'])
        ->setReplyTo([$this->email => $this->name])
        ->setSubject('You have new subscriber.')
        ->setTextBody($this->message)
        ->send();
    } else {
      return false;
    }
  }

  /**
   * @return array
   */
  public function getSubjects(){
    return[
      '1' => 'Price Guide',
      '2' => 'Obtain a copy of plans & permit',
      '3' => 'Obtain the property information memorandum',
      '4' => 'Obtain the contract of sale',
      '5' => 'Other',
    ];
  }


  public function afterSubscription($fullname, $email, $states = []){
    //$states = [0 => 'VIC'];
    //$email = 'rashad@jameshammon.com.au';
    //$fullname = 'Rashad Aliyev';

    $MailChimp = new \common\components\helpers\MailChimp('8e49da49cb381f88db535ef87783b88e-us9');
    $interests = [];

    if(empty($states)){
      $interests['848302bd1b'] = true;
    } else {
      foreach($states as $v){
        switch ($v) {
          case 'All states':
            $interests['848302bd1b'] = true; break;
          case 'ACT':
            $interests['5899ab9143'] = true; break;
          case 'NSW':
            $interests['38c2230dd3'] = true; break;
          case 'NT':
            $interests['bb34aeb0e8'] = true; break;
          case 'QLD':
            $interests['afef3ac27c'] = true; break;
          case 'SA':
            $interests['c1ac922b55'] = true; break;
          case 'TAS':
            $interests['2fe5c5da40'] = true; break;
          case 'VIC':
            $interests['67d82b2b7d'] = true; break;
          case 'WA':
            $interests['fe57fdfa9a'] = true; break;
          default:
            $interests .= '';
        }
      }
    }

    $result = $MailChimp->post('lists/8836feb3ee/members', [
      'email_address'     => "'{$email}'",
      'status'            => 'subscribed',
      'merge_fields'      => ['FNAME'=> "'{$fullname}'"],
      'interests'         => $interests
    ]);

    return $result;

    if(empty($result['id'])) return false;

    return true;

    # Interests
    # Group '560481'
    # 848302bd1b / '40181' - All states
    # 5899ab9143 / '40185' - ACT
    # 38c2230dd3 / '40189' - NSW
    # bb34aeb0e8 / '40193' - NT
    # afef3ac27c / '40197' - QLD
    # c1ac922b55 / '40201' - SA
    # 2fe5c5da40 / '40205' - TAS
    # 67d82b2b7d / '40209' - VIC
    # fe57fdfa9a / '40213' - WA

  }

}