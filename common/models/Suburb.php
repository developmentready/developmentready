<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/12/15
 * Time: 19:25
 */

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%suburbs}}".
 *
 * @property integer $id
 * @property integer $status
 * @property integer $post_code
 * @property string  $title
 * @property integer $state_id
 * @property integer $city_id
 * @property double  $lat
 * @property double  $lng
 * @property integer $creator_id
 * @property integer $created_at
 * @property integer $updater_id
 * @property integer $updated_at
 *
 * @property Addresses[] $addresses
 * @property Countries $country
 * @property User $creator
 * @property User $updater
 * @property CitiesLang[] $citiesLangs
 */
class Suburb extends \common\components\classes\BackendActiveRecord{
  /**
   * @inheritdoc
   */
  public static function tableName(){
    return '{{%suburbs}}';
  }

  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['status', 'state_id', 'is_city', 'belongs_to_city', 'creator_id', 'created_at', 'updater_id', 'updated_at', 'post_code'], 'integer'],
      [['post_code', 'title', 'state_id'],'required'],
      [['lat', 'lng'], 'double'],
      [['title'], 'string', 'max' => 255],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels(){
    return [
      'id' => Yii::t('common', 'ID'),
      'status' => Yii::t('common', 'Active?'),
      'state_id' => Yii::t('common', 'Parent State'),
      'is_city' => Yii::t('common', 'Is city center ?'),
      'default_city_of_state' => Yii::t('common', 'Is default city of the state ?'),
      'belongs_to_city' => Yii::t('common', 'Parent City'),
      'title' => Yii::t('common', 'Name of the City'),
      'post_code' => Yii::t('common', 'Post Code'),
      'lat' => Yii::t('common', 'Latitude'),
      'lng' => Yii::t('common', 'Longitude'),
      'creator_id' => Yii::t('common', 'Creator'),
      'created_at' => Yii::t('common', 'Created At'),
      'updater_id' => Yii::t('common', 'Updater'),
      'updated_at' => Yii::t('common', 'Updated At'),
    ];
  }

  public function getProperty(){
    return $this->hasOne(PropertySuburb::className(), ['suburb_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getAddresses(){
    return $this->hasMany(Address::className(), ['suburb_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getParentCity(){
    return $this->hasOne(self::className(), ['id' => 'belongs_to_city']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getState(){
    return $this->hasOne(State::className(), ['id' => 'state_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCreator(){
    return $this->hasOne(User::className(), ['id' => 'creator_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUpdater(){
    return $this->hasOne(User::className(), ['id' => 'updater_id']);
  }

}