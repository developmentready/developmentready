<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%document_types_lang}}".
 *
 * @property integer $id
 * @property integer $relation_id
 * @property integer $lang_id
 * @property string $title
 *
 * @property DocumentTypes $relation
 * @property Languages $lang
 */
class DocumentTypeLang extends \common\components\classes\CommonActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%document_types_lang}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['relation_id', 'lang_id'], 'integer'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'relation_id' => Yii::t('common', 'Relation ID'),
            'lang_id' => Yii::t('common', 'Lang ID'),
            'title' => Yii::t('common', 'Title'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function DocumentType()
    {
        return $this->hasOne(DocumentType::className(), ['id' => 'relation_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang(){
        return $this->hasOne(Language::className(), ['id' => 'lang_id']);
    }
}
