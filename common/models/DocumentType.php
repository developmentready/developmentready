<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%document_types}}".
 *
 * @property integer $id
 * @property integer $status
 * @property integer $priority
 * @property integer $creator_id
 * @property integer $created_at
 * @property integer $updater_id
 * @property integer $updated_at
 *
 * @property DocumentTypesLang[] $documentTypesLangs
 * @property Properties[] $properties
 */
class DocumentType extends \common\components\classes\BackendActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%document_types}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'priority', 'creator_id', 'created_at', 'updater_id', 'updated_at'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'status' => Yii::t('common', 'Status'),
            'priority' => Yii::t('common', 'Priority'),
            'creator_id' => Yii::t('common', 'Creator ID'),
            'created_at' => Yii::t('common', 'Created At'),
            'updater_id' => Yii::t('common', 'Updater ID'),
            'updated_at' => Yii::t('common', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentTypesLangs(){
        return $this->hasMany(DocumentTypeLang::className(), ['relation_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasMany(Property::className(), ['document_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslation(){
        $code = $this->getLanguageID(\Yii::$app->language);
        return $this->hasOne(DocumentTypeLang::className(), ['relation_id' => 'id'])
          ->where(['lang_id' => $code]);
    }
}
