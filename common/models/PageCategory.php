<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/20/15
 * Time: 21:53
 */

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%page_categories}}".
 *
 * @property integer $id
 * @property integer $status
 * @property string $title
 * @property integer $creator_id
 * @property integer $created_at
 * @property integer $updater_id
 * @property integer $updated_at
 */
class PageCategory extends \common\components\classes\BackendActiveRecord{
  /**
   * @inheritdoc
   */
  public static function tableName(){
    return '{{%page_categories}}';
  }

  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['status', 'creator_id', 'created_at', 'updater_id', 'updated_at'], 'integer'],
      [['title'], 'string', 'max' => 255]
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels(){
    return [
      'id' => Yii::t('app', 'ID'),
      'status' => Yii::t('app', 'Active ?'),
      'title' => Yii::t('app', 'Title'),
      'creator_id' => Yii::t('app', 'Creator'),
      'created_at' => Yii::t('app', 'Created At'),
      'updater_id' => Yii::t('app', 'Updater'),
      'updated_at' => Yii::t('app', 'Updated At'),
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getPages(){
    return $this->hasMany(Page::className(), ['page_category_id' => 'id']);
  }
}