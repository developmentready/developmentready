<?php

namespace common\models;

use frontend\models\search\PropertySearch;
use Yii;
use yii\helpers\ArrayHelper;
use common\models\PropertyImages;
use common\models\PropertyUser;
use common\models\PropertyAgents;

/**
 * This is the model class for table "{{%properties}}".
 *
 * @property integer $id
 * @property integer $priority
 * @property integer $status
 * @property integer $page_robot_indexing
 * @property string  $description
 * @property string  $slug
 * @property integer $price_type_id
 * @property integer $show_agency_banner
 * @property string  $price_in_numbers
 * @property string  $eoi_description
 * @property double  $square
 * @property integer $square_measure_id
 * @property integer $contract_type_id
 * @property integer $under_offer
 * @property string  $contract_details
 * @property integer $suburb_id
 * @property string  $location
 * @property string  $meta_keywords
 * @property string  $meta_description
 * @property double  $lat
 * @property double  $lng
 * @property integer $view_count
 * @property integer $sold
 * @property integer $accept_terms
 * @property integer $publication_date
 * @property integer $featured_till
 * @property integer $creator_id
 * @property string  $created_at
 * @property integer $updater_id
 * @property integer $updated_at
 *
 * @property \common\models\Favorite $favorites
 * @property \common\models\User $user
 * @property \common\models\PropertyImages $propertyImages
 * @property \common\models\PropertyPrice $propertyPrices
 * @property \common\models\PropertyStats $stats
 * @property \common\models\PropertyReport $reports
 * @property Property $reportCalls
 * @property Property $reportViews
 * @property Property $reportMessages
 * @property Property $report
 */
class Property extends \common\components\classes\BackendActiveRecord{

  public $agencies = [];
  public $agents = [];
  public $use_watermark;

  /**
   * @inheritdoc
   */
  public static function tableName(){
    return '{{%properties}}';
  }

  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['priority', 'status', 'price_type_id', 'square_measure_id', 'page_robot_indexing',
        'contract_type_id', 'suburb_id', 'view_count', 'sold', 'accept_terms', 'creator_id',
        'updater_id', 'updated_at', 'use_watermark', 'show_agency_banner', 'under_offer', 'square_min'], 'integer'],
      [['description'], 'string'],
      [['location', 'price_type_id', 'contract_type_id', 'suburb_id'], 'required'],
      [['square', 'lat', 'lng'], 'number'],

      # do'not index pages in SEO
      [['page_robot_indexing'], 'default', 'value' => 0],
      [['under_offer'], 'default', 'value' => 0],

      [['created_at', 'publication_date'], 'safe'],
      [['price_in_numbers'], 'string', 'min' => 3, 'max' => 30, 'message' => 'Minimum number can\'t be less than $10.000 '],
      [['contract_details', 'location', 'eoi_description'], 'string', 'max' => 255],
      [['meta_description', 'meta_keywords', 'slug'], 'string', 'max' => 1024],
      ['price_in_numbers', 'required', 'when' => function($model) {return $model->price_type_id === 6;}
        , 'whenClient' => "function (attribute, value) {return $('#property-price_type_id').val() == 6;}"
        ,'message' => Yii::t('app','Please enter amount of property')],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels(){
    return [
      'id' => Yii::t('app', 'ID'),
      'priority' => Yii::t('app', 'Priority'),
      'slug' => Yii::t('app', 'Link Name'),
      'status' => Yii::t('app', 'Active ?'),
      'use_watermark' => Yii::t('app', 'Add watermark on images?'),
      'page_robot_indexing' => Yii::t('app', 'Index page on search engines ?'),
      'description' => Yii::t('app', 'About Property'),
      'price_type_id' => Yii::t('app', 'Price Type'),
      'price_in_numbers' => Yii::t('app', 'Price In USD'),
      'eoi_description' => Yii::t('app', 'EOI comments'),
      'show_agency_banner' => Yii::t('app', 'Show\\hide agency banner in property'),
      'square' => Yii::t('app', 'Area (square max.)'),
      'square_min' => Yii::t('app', 'Area (square min.)'),
      'square_measure_id' => Yii::t('app', 'Area Measure'),
      'contract_type_id' => Yii::t('app', 'Permit Status'),
      'contract_details' => Yii::t('app', 'Permit Details'),
      'suburb_id' => Yii::t('app', 'Suburb'),
      'meta_keywords' => Yii::t('app', 'Meta Keywords'),
      'meta_description' => Yii::t('app', 'Meta Description'),
      'location' => Yii::t('app', 'Location details'),
      'lat' => Yii::t('app', 'Latitude'),
      'lng' => Yii::t('app', 'Longitude'),
      'view_count' => Yii::t('app', 'Number Of Views'),
      'sold' => Yii::t('app', 'Sold ?'),
      'accept_terms' => Yii::t('app', 'Accept Terms'),
      'publication_date' => Yii::t('app', 'Publication date'),
      'featured_till' => Yii::t('app', 'Featured Till'),
      'creator_id' => Yii::t('app', 'Creator'),
      'created_at' => Yii::t('app', 'Created At'),
      'updater_id' => Yii::t('app', 'Updater'),
      'updated_at' => Yii::t('app', 'Updated At'),
    ];
  }


  public function beforeSave($insert){
    if (parent::beforeSave($insert)) {
      $this->slug = \common\components\helpers\Setup::generatePropertyUniqueUrl($this);
      return true;
    } else {
      return false;
    }
  }

  /**
   * @param bool $insert
   * @param array $changedAttributes
   */
  public function afterSave($insert, $changedAttributes){
    parent::afterSave($insert, $changedAttributes);

    if($insert){
      # Some action related only for new entry
      Yii::$app->commandBus->handle(new \common\commands\command\AddToTimelineCommand([
        'category' => 'property',
        'event' => 'newProperty',
        'data' => [
          'propertyId' => $this->id,
          'slug' => $this->slug,
          'created_at' => $this->created_at
        ]
      ]));
    } else {
      PropertyUser::deleteAll(['property_id' => $this->id]);
      PropertyAgents::deleteAll(['property_id' => $this->id]);
    }

    if(array_key_exists('agents', \Yii::$app->request->post('Property'))){
      if(!empty($agents = \Yii::$app->request->post('Property')["agents"])){
        foreach($agents as $agent){

          $id = (int)$agent;
          $model = new PropertyAgents();
          $model->property_id = $this->id; $model->agent_id = $id;
          $model->save();
        }
      }
    }

    if(empty($this->publication_date)) $this->publication_date = $this->updated_at;

    if(array_key_exists('agencies',\Yii::$app->request->post('Property'))){
      if(!empty($agencies = \Yii::$app->request->post('Property')["agencies"])){
        foreach($agencies as $agency){

          $id = (int)$agency;
          $model = new PropertyUser();
          $model->property_id = $this->id; $model->user_id = $id;
          $model->save();
        }

      }
    } else {
      $model = new PropertyUser();
      $model->property_id = $this->id;

      if($insert){
        $model->user_id = \Yii::$app->user->identity->id;
      }


      $model->save();
    }

    $image = Yii::$app->request->post('Property')["fileAttribute"];
    if(!empty($image)) $this->saveDefaultImage($image);

  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getAreaType(){
    return $this->hasOne(AreaType::className(), ['id' => 'square_measure_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getDefaultImage(){
    return $this->hasOne(PropertyImages::className(), ['property_id' => 'id'])
      ->where(['default' => '1']);
  }

  /**
   * Return all properties registered in some default state
   * @return \yii\db\ActiveQuery
   */
  public function getDefaultStateProperties(){
    return $this->hasMany(State::className(), ['id' => 'state_id'])
      ->viaTable('{{%suburbs}}', ['id' => 'suburb_id'])->where('{{%states}}.default = 1');
  }


  /**
   * @return \yii\db\ActiveQuery
   */
  public function getImages(){
    return $this->hasMany(PropertyImages::className(), ['property_id' => 'id'])->orderBy('sort_order');
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getImage(){
    return $this->hasOne(PropertyImages::className(), ['property_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getPropertyTypes(){
    return $this->hasMany(PropertyType::className(), ['id' => 'property_type_id'])
      ->viaTable('{{%property_types_assn}}', ['property_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getPropertyUser(){
    return $this->hasOne(User::className(), ['id' => 'user_id']);
  }

  # get agency name based on
  public function getPropertyUsers(){
    return $this->hasMany(User::className(), ['id' => 'user_id'])
      ->viaTable('{{%property_user_assn}}', ['property_id' => 'id']);
  }

  # get agency name based on
  public function getPropertyAgencies(){
    return $this->hasMany(User::className(), ['id' => 'user_id'])
      ->viaTable('{{%property_user_assn}}', ['property_id' => 'id'])
      ->andWhere(['user_type' => 1]);
  }

  # get agency name based on
  public function getPropertyAgents(){
    return $this->hasMany(AgencyAgent::className(), ['id' => 'agent_id'])
      ->viaTable('{{%property_agents_assn}}', ['property_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getPropertyPrice(){
    return $this->hasOne(PriceType::className(), ['id' => 'price_type_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getReport(){
    return $this->hasOne(PropertyReport::className(), ['property_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getReports(){
    return $this->hasMany(PropertyReport::className(), ['property_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getReportCalls() {
    return $this->hasOne(PropertyReport::className(), ['property_id' => 'id'])
      ->andWhere(['reference' => 'call_count'])->sum('counter');
  }


  /**
   * @return \yii\db\ActiveQuery
   */
  public function getReportViews(){
    return $this->hasOne(PropertyReport::className(), ['property_id' => 'id'])
      ->andWhere(['reference' => 'view_count'])->sum('counter');
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getReportMessages(){
    return $this->hasOne(PropertyReport::className(), ['property_id' => 'id'])
      ->andWhere(['reference' => 'message_count'])->sum('counter');
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getStats(){
    return $this->hasOne(PropertyStats::className(), ['property_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getSuburb(){
    return $this->hasOne(Suburb::className(), ['id' => 'suburb_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getSuburbs(){
    return $this->hasMany(Suburb::className(), ['id' => 'suburb_id']);
  }


  /**
   * Return all properties registered in state.
   * @return \yii\db\ActiveQuery
   */
  public function getStateProperties(){
    return $this->hasMany(State::className(), ['id' => 'state_id'])
      ->viaTable('{{%suburbs}}', ['id' => 'suburb_id']);
  }

  /**
   * Return all properties registered in state.
   * @return \yii\db\ActiveQuery
   */
  public static function getStatePropertiesById($id){
    $query = Property::find();
    $query->select('{{%suburbs}}.id, {{%suburbs}}.state_id, {{%properties}}.* ')
      ->from('{{%properties}}')
      ->join('INNER JOIN','{{%suburbs}}', '{{%suburbs}}.id = {{%properties}}.suburb_id')
      ->where('{{%properties}}.status = 1 AND  {{%suburbs}}.state_id = '.$id." ")
      ->orderBy('sold ASC, {{%properties}}.updated_at DESC')
      ->limit(12);

    return $query;
  }

  /**
   * @param $state_id
   * @return \common\components\classes\CommonQuery|\yii\db\ActiveQuery
   */
  public static function getStateFeaturedPropertiesById($state_id){
    $query = Property::find();
    $query->select('{{%suburbs}}.id, {{%suburbs}}.state_id, {{%properties}}.* ')
      ->from('{{%properties}}')
      ->join('INNER JOIN','{{%suburbs}}', '{{%suburbs}}.id = {{%properties}}.suburb_id')
      ->where('{{%properties}}.status = 1 AND  {{%suburbs}}.state_id = '.$state_id." AND {{%properties}}.featured_till > ".time()." AND sold = 0")
      ->orderBy('{{%properties}}.publication_date DESC, {{%properties}}.updated_at DESC')
      ->limit(12);

    return $query;
  }


  /**
   * @param $state_id
   * @return \common\components\classes\CommonQuery|\yii\db\ActiveQuery
   */
  public static function getStateLatestPropertiesById($state_id){
    $query = Property::find();
    $query->select('{{%suburbs}}.id, {{%suburbs}}.state_id, {{%properties}}.* ')
      ->from('{{%properties}}')
      ->join('INNER JOIN','{{%suburbs}}', '{{%suburbs}}.id = {{%properties}}.suburb_id')
      ->where('{{%properties}}.status = 1 AND  {{%suburbs}}.state_id = '.$state_id." AND sold = 0")
      ->orderBy('{{%properties}}.publication_date DESC, {{%properties}}.id DESC')
      ->limit(12);

    return $query;
  }

  /***********************
   *  Personal methods
  *************************/

  /**
   * @param $state_id
   * @return array|\yii\db\ActiveRecord[]
   */
  public static function getStatesProperties($state_id){
    $properties = self::find()->select("suburb_id, MIN(suburb_id)")->where(['state_id' => $state_id])->activeProperty()->joinWith('suburbs')
      ->groupBy('suburb_id')->orderBy('{{%suburbs}}.title')->all();
    return $properties;
  }

  /**
   * @return array|\yii\db\ActiveRecord[]
   */
  public static function getSoldPropertiesSuburbs(){
    $properties = self::find()->select("suburb_id, COUNT(suburb_id)")
      ->where(['sold' => 1, '{{%properties}}.status' => 1])->joinWith('suburbs')
      ->groupBy('suburb_id')->orderBy('{{%suburbs}}.title')->all();
    return $properties;
  }

  /**
   * @return array|\yii\db\ActiveRecord[]
   */
  public static function getActivePropertiesSuburbs(){
    $properties = self::find()->select("suburb_id, COUNT(suburb_id)")
      ->where(['sold' => 0, '{{%properties}}.status' => 1])->joinWith('suburbs')
      ->groupBy('suburb_id')->orderBy('{{%suburbs}}.title')->all();
    return $properties;
  }

  /**
   * @return array|\yii\db\ActiveRecord[]
   */
  public static function getActivePropertiesStateSuburbs($state_id){
    $properties = self::find()->select("suburb_id, COUNT(suburb_id)")
      ->where(['sold' => 0, '{{%properties}}.status' => 1])->andWhere(['state_id' => $state_id])
      ->joinWith('suburbs')->groupBy('suburb_id')->orderBy('{{%suburbs}}.title')->all();
    return $properties;
  }


  /**
   * @return array
   */
  public static function getSoldPropertiesStates(){
    $select  = "SELECT pr_states.id, pr_states.title, ";
    $select .= "pr_suburbs.state_id, pr_suburbs.state_id, ";
    $select .= "pr_properties.id, suburb_id, sold, pr_properties.status ";
    $from    = "FROM pr_states ";
    $join    = "LEFT JOIN pr_suburbs ON pr_suburbs.state_id = pr_states.id ";
    $join   .= "LEFT JOIN pr_properties ON suburb_id = pr_suburbs.id ";
    $where   = "WHERE pr_properties.status = 1 AND sold = 1 ";
    $group_by= "GROUP BY pr_states.id ";

    $sql = $select.$from.$join.$where.$group_by;

    return Yii::$app->db->createCommand($sql)->queryAll();
  }

  /**
   * @return array|\yii\db\ActiveRecord[]
   */
  public static function getLastPublishedProperties(){
    return self::find()->where(['status' => 1, 'sold' => 0])->limit(12)
      ->orderBy(['publication_date' => SORT_DESC, 'id' => SORT_DESC])->all();
  }

  /**
   * @param int $default_state
   * @return array|\yii\db\ActiveRecord[]
   */
  public static function getStateRelatedFeaturedProperties($default_state = 7) {

    if(\Yii::$app->getRequest()->getCookies()->has('userState'))
      $user_state = \Yii::$app->getRequest()->getCookies()->getValue('userState');

    else if(\Yii::$app->session->has('state'))
        $user_state = \Yii::$app->session->get('state');

    else
      $user_state  = \common\components\helpers\PermitReadyHelpers::convertStateToId();

    if(self::getStateFeaturedPropertiesById($user_state)->count() > 0 )
      return self::getStateFeaturedPropertiesById($user_state)->all();
    else
      return self::getStateFeaturedPropertiesById($default_state)->all();
  }

  /**
   * @param int $default_state
   * @return array|\yii\db\ActiveRecord[]
   */
  public static function getStateRelatedLatestProperties($default_state = 7){

    if(\Yii::$app->getRequest()->getCookies()->has('userState'))
      $user_state = \Yii::$app->getRequest()->getCookies()->getValue('userState');

    else if(\Yii::$app->session->has('state'))
      $user_state = \Yii::$app->session->get('state');

    else
      $user_state  = \common\components\helpers\PermitReadyHelpers::convertStateToId();


    if(self::getStateLatestPropertiesById($user_state)->count() > 0 )
      return self::getStateLatestPropertiesById($user_state)->all();
    else
      return self::getStateLatestPropertiesById($default_state)->all();
  }


  /**
   * Return 3 images, default image first
   * @param int $limit
   * @return array|\yii\db\ActiveRecord[]
   */
  public function imagesWithLimit($limit = 3){
    return PropertyImages::find()->where(['property_id' => $this->id])->limit($limit)
      ->orderBy(['default' => SORT_DESC])->all();
  }

  /**
   * Return array with permit statuses
   * @return array
   */
  public function permitStatus(){
    return [
      '1' => 'Permit Approved',
      '0' => 'Permit Potential',
    ];
  }

  /**
   * @return array
   */
  public function soldStatus(){
    return [
      '0' => 'Not Sold',
      '1' => 'Sold',
    ];
  }

  /**
  * Set default image for property. Add agency watermark to it if it's have
  * @param $image
  * @param int $img_width
  * @param string $bg_color
  * @param int $img_height
  * @return bool
  */
  public function saveDefaultImage($image, $img_width = 800, $bg_color = "#d8d8d8", $img_height = 600){
    $new_dir = \Yii::getAlias('@storage')."/properties/{$this->id}";

    if(!is_dir($new_dir)){
      if(!mkdir($new_dir, 0777)) die('Failed to create folders...'); chmod($new_dir, 0777);

      $content = "Silence is power!";
      $fp = fopen($new_dir . "/index.html", "wb"); fwrite($fp, $content); fclose($fp);
    }

    $model = PropertyImages::find()->where(['property_id' => $this->id])->andWhere(['default' => 1])->one();

    if(empty($model)) $model = new PropertyImages();
    else {
      $old_file = \Yii::getAlias('@storage')."/".$model->file_name;
      if(is_file($old_file)) unlink($old_file);
    }

    if(empty($image['path'])) return false;

    $file_name = basename($image['path']);
    $image_path = \Yii::getAlias('@storage')."/source/".$image['path'];
    $new_path = $new_dir."/{$file_name}";
    \gregwar\image\Image::open($image_path)->scaleResize($img_width, $img_height, $bg_color)->save($new_path, 'jpg', 90);

    if(!empty($users = $this->propertyAgencies) && $this->use_watermark == 1){
      $this->_addWaterMark($users, $new_path);
    }

    $this->_addImageThumbnail($new_path);

    $model->property_id = $this->id;
    $model->default = 1;
    $model->file_name = $file_name;

    if($model->save()) {
      $file_storage = \common\models\FileStorageItem::find()->where(['path' => $image['path']])->one();
      if($file_storage->delete()) {
        if(is_file($image_path)) unlink($image_path);
      }
    } else {
      return false;
    }

    if(Yii::$app->session->has('_uploadedFiles')){
      Yii::$app->session->remove('_uploadedFiles');
    }

    return true;
  }

  /**
   * Save uploaded files
   * @param $images
   * @param $id
   * @return bool
   */
  public function saveUploadedPropertyImages($images, $id){
    $new_dir = \Yii::getAlias('@storage')."/properties/{$id}";

    if(!is_dir($new_dir)){
      if(!mkdir($new_dir, 0777)) die('Failed to create folders...'); chmod($new_dir, 0777);

      $content = "Silence is power!";
      $fp = fopen($new_dir . "/index.html", "wb"); fwrite($fp, $content); fclose($fp);
    }

    if(!empty($images)){
      foreach($images as $item){
        $model = new PropertyImages();
        $file_name = explode("/", $item['path']);
        $file_name = $file_name[1];
        $image_path = \Yii::getAlias('@storage')."/source/".$item['path'];
        $new_path = $new_dir."/{$file_name}";
        \gregwar\image\Image::open($image_path)->scaleResize(800, 600, "#d8d8d8")->save($new_path, 'jpg', 90);
        $model->property_id = $id;
        $model->file_name = $file_name;

        if(!empty($users = $this->propertyAgencies) && $this->use_watermark == 1){
          $this->_addWaterMark($users, $new_path);
        }

        if(!$model->save()){ return false; };
      }
    }

    if(Yii::$app->session->has('_uploadedFiles')){
      Yii::$app->session->remove('_uploadedFiles');
    }
    return true;
  }

  /**
   * @param $model_id
   * @param $reference_name
   */
  public static function updatePropertyReports($model_id, $reference_name){
    $property = self::findOne($model_id);

    if(empty($reports = $property->_checkTodayReport( $reference_name, $model_id ))){
      $stats = new \common\models\PropertyReport();
      $stats->property_id = $model_id;
      $stats->reference = $reference_name;
      if($stats->save()) return;
    }
      else
        $reports->updateCounters(['counter' => 1]);
  }

  /**
   * @param $model_id
   * @param $reference_name
   */
  public static function updateStatesReports( $model_id, $reference_name ) {
    $property = self::findOne($model_id);

    if(empty($reports = $property->_checkTodayReport( $reference_name, $property->suburb->state->id ))){
      $stats = new \common\models\PropertyReport();
      $stats->property_id = $property->suburb->state->id;
      $stats->reference = $reference_name;
      if($stats->save()) return;
    }
    else
      $reports->updateCounters(['counter' => 1]);
  }


  /**
   * @return bool
   */
  public function updatePropertyImagesWatermark(){
    $images = $this->images;

    foreach($images as $image){
      $file = Yii::getAlias('@storage')."/properties/{$this->id}/".$image->file_name;
      if(!empty($users = $this->propertyAgencies) && is_file($file)){
        $counter = 0;
        foreach($users as $user){
          if(!empty($logo = $user->agency->logo_thumbnail)){ //&& $user->agency->featured_aceny
            $watermark = \Yii::getAlias('@storageUrl').$logo;
            \yii\imagine\Image::watermark($file, $watermark, [0, $counter])->save($file);
            $counter = $counter + 40;
          }
        }
      }
    }
  }

  /**
   * @param null $user_id
   * @return bool
   */
  public function isUserPropertyOwner($user_id = null){
    foreach($this->propertyUsers as $user){
      if($user->id == $user_id && $this->creator_id == $user_id)
        return true;
    }
    return false;
  }

  /**
   * @param null $user_id
   * @return bool
   */
  public function isEditedByAdmin($user_id = null){
    foreach($this->propertyUsers as $user){
      if($user->id == $user_id && $this->updater_id == $user_id)
        return false;
    }
    return true;
  }

  /**
   * @param null $user_id
   * @return bool
   */
  public function isUserPropertyCoOwner($user_id = null){
    foreach($this->propertyUsers as $user){
      if($user->id == $user_id)
        return true;
    }
    return false;
  }

  /**
   * User has to be signed in for this action
   * @return \yii\db\ActiveQuery
   */
  public function getFavorited(){
    return $this->hasOne(Favorite::className(), ['property_id' => 'id'])
      ->andWhere(['deleted' => 0, 'user_id' => \Yii::$app->user->identity->id]);
  }


  /**
   * Check if current user choose as favorite exact property
   * @param $property_id
   * @return bool
   */
  public function isFavorite($property_id){
    $model = \common\models\Favorite::find()
      ->where(['property_id' => $property_id, 'user_id' => Yii::$app->user->identity->id])
      ->one();
    if(!$model) return false;
    else return true;
  }

  /***********************
   *  Private methods
   *************************/

  /**
   * @param $users
   * @param $file
   */
  private function _addWaterMark($users, $file){
    $storage = \Yii::getAlias('@storage'); $counter = 0;
    $multi_users = count($users) > 1 ? true : false ;
    foreach($users as $user){
      if($multi_users){
        if(!empty($logo = $user->agency->logo_thumbnail)){
          $watermark = $storage."/web/agency/watermarks/half/".basename($logo);
          if(!is_file($watermark)){
            \gregwar\image\Image::open($storage.$logo)->crop(0, 0, 400, 40)->save($watermark);
          }
          if($counter == 0)
            \yii\imagine\Image::watermark($file , $watermark, [0, 0])->save($file);
          elseif($counter == 1)
            \yii\imagine\Image::watermark($file , $watermark, [400, 0])->save($file);
        }
      } else {
        if(!empty($logo = $user->agency->logo_thumbnail)
          //&& $user->agency->featured_aceny
        ){
          $watermark = \Yii::getAlias('@storageUrl').$logo;
          \yii\imagine\Image::watermark($file , $watermark, [0, 0])->save($file);
        }
      }
      $counter++;
    }
  }

  /**
   * @param $image_path
   * @return bool
   */
  private function _addImageThumbnail($image_path){
    $thumb_path = dirname($image_path)."/thumb";
    $file_name = basename($image_path);

    if(!is_dir($thumb_path)){
      if(!mkdir($thumb_path, 0777)) die('Failed to create folders...');chmod($thumb_path, 0777);

      $content = "Not allowed!";
      $fp = fopen($thumb_path . "/index.html", "wb"); fwrite($fp, $content); fclose($fp);
    }

    if(is_dir($thumb_path))
      \yii\imagine\Image::thumbnail($image_path, 320, 240)
        ->save(Yii::getAlias($thumb_path."/".$file_name, ['quality' => 71]));
    else
      return false;
  }

  /**
   * @param $attribute
   * @param $id
   * @return array|null|\yii\db\ActiveRecord
   */
  private function _checkTodayReport($attribute, $id){
    return \common\models\PropertyReport::find()->where([ 'property_id' => $id ])
      ->andWhere(" `recorded_at` >= CURRENT_DATE
                  AND `recorded_at` <  CURRENT_DATE + INTERVAL 1 DAY
                  AND `reference` = '".$attribute."' ")
      ->one();
  }

  /**
   * @param $model_id
   * @param $attribute_name
   */
  public static function updatePropertyStats($model_id, $attribute_name){
    $property = self::findOne($model_id);
    if(empty($stats = $property->stats)){
      $stats = new \common\models\PropertyStats();
      $stats->property_id = $model_id;
      $stats->$attribute_name = 1;
      $stats->save();
    } else
      $stats->updateCounters([$attribute_name => 1]);
  }

}