<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/26/15
 * Time: 13:27
 */
namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%property_agents_assn}}".
 *
 * @property integer $id
 * @property integer $property_id
 * @property integer $agent_id
 */
class PropertyAgents extends \common\components\classes\CommonActiveRecord{
  /**
   * @inheritdoc
   */
  public static function tableName(){
    return '{{%property_agents_assn}}';
  }

  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['property_id', 'agent_id'], 'required'],
      [['property_id', 'agent_id'], 'integer']
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels(){
    return [
      'id' => Yii::t('app', 'ID'),
      'property_id' => Yii::t('app', 'Property ID'),
      'agent_id' => Yii::t('app', 'Agent ID'),
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getAgent(){
    return $this->hasOne(AgencyAgent::className(), ['id' => 'agent_id']);
  }
}