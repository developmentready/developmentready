<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%page_types}}".
 *
 * @property integer $id
 * @property integer $status
 * @property integer $creator_id
 * @property integer $created_at
 * @property integer $updater_id
 * @property integer $updated_at
 *
 * @property User $creator
 * @property User $updater
 * @property PageTypesLang[] $pageTypesLangs
 */
class PageType extends \common\components\classes\BackendActiveRecord{
  
  # Below is method of relation method 
  public $pageTypesLangs;
  
  /**
   * @inheritdoc
   */
  public static function tableName(){
    return '{{%page_types}}';
  }

  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['status', 'creator_id', 'created_at', 'updater_id', 'updated_at'], 'integer'],
      [['pageTypesLangs'], 'safe']
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels(){
    return [
      'id' => Yii::t('common', 'ID'),
      'status' => Yii::t('common', 'Status'),
      'creator_id' => Yii::t('common', 'Creator'),
      'created_at' => Yii::t('common', 'Created At'),
      'updater_id' => Yii::t('common', 'Updater'),
      'updated_at' => Yii::t('common', 'Updated At'),
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCreator(){
    return $this->hasOne(User::className(), ['id' => 'creator_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUpdater(){
    return $this->hasOne(User::className(), ['id' => 'updater_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getPageTypesLangs(){
    return $this->hasMany(PageTypesLang::className(), ['page_type_id' => 'id']);
  }
  
  
  public function getLangtile($typeID, $lang = 'ru'){
    $langCode = $this->getLanguageID($lang);
    $select  = ' SELECT pt.id, ';
    $select .= ' ptl.id, ptl.lang_id, ptl.title as title, ptl.page_type_id ';
    $from    = ' FROM {{%page_types}} AS pt';
    $join    = ' JOIN {{%page_types_lang}} AS ptl ON pt.id = ptl.page_type_id';
    $where   = " WHERE pt.id = ". $typeID ." ";
    $and     = " AND ptl.lang_id = '". $langCode ."' ";
    
    $sql = $select.$from.$join.$where.$and;
    $connection = Yii::$app->db;
    $query = $connection->createCommand($sql)->queryAll();
    
    $string = $query;#= \common\components\Setup::truncate($query['content'], 300, "....") ;
    
    return $string;
  }
  
}
