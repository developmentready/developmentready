<?php

namespace common\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "{{%property_direction_points}}".
 *
 * @property integer $id
 * @property integer $property_id
 * @property integer $call_count
 * @property integer $message_count
 *
 * @var \common\models\PropertyReport $report
 */
class PropertyReport extends \common\components\classes\CommonActiveRecord{
  /**
   * @inheritdoc
   */
  public static function tableName(){
    return '{{%property_reports}}';
  }

  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['property_id', 'counter'], 'integer'],
      [['property_id', 'reference'], 'required'],

      [['reference'], 'string', 'max' => 250]
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels(){
    return [
      'id' => Yii::t('common', 'ID'),
      'property_id' => Yii::t('common', 'Property'),
      'counter' => Yii::t('common', 'Counter'),
      'reference' => Yii::t('common', 'Reference'),
      'recorded_at' => Yii::t('common', 'Date of record'),
    ];
  }

  /**
   * @param $property_id
   * @param $reference
   * @param int $period_in_days
   * @return array
   */
  public function propertyReportForReference($property_id, $reference, $period_in_days = 30){
    $query = new Query;
    $query->select('*')
      ->from('{{%property_reports}}')
      ->where("property_id = :property_id AND reference = :reference AND date(recorded_at) >= date(now()-interval :days day)",
          ['reference' => $reference, 'property_id' => $property_id, 'days' => $period_in_days])
      ->limit($period_in_days)
      ->orderBy(['recorded_at' => SORT_DESC]);
    // build and execute the query
    // $rows = $query->all();
    // alternatively, you can create DB command and execute it
    $command = $query->createCommand();


    # SELECT * FROM `pr_property_reports`
    # WHERE property_id = :property_id
    #   AND reference = :reference
    #   AND `recorded_at` < CURRENT_DATE - INTERVAL :period_in_days DAY
    # ORDER BY `recorded_at` DESC LIMIT 30

    // show the SQL statement
    //echo $command->sql;

    return $rows = $command->queryAll();
  }

  /**
   * @param $property_id
   * @param $reference
   * @param int $date
   * @return array
   */
  public function propertyReportForDate($property_id, $reference , $date){
    $query = new Query;
    $query->select('*')
      ->from('{{%property_reports}}')
      ->where("property_id = :property_id AND reference = :reference AND date(recorded_at) = :date", [
          'reference' => $reference, 'property_id' => $property_id, 'date' => $date
      ])
      ->limit(1);

    // alternatively, you can create DB command and execute it
    $command = $query->createCommand();

    return $rows = $command->queryAll();
  }

  public function propertyReportForDateRange($property_id, $reference , $start_date, $end_date){
    $query = new Query;
    $query->select('*')
      ->from('{{%property_reports}}')
      ->where("property_id = :property_id AND reference = :reference AND date(recorded_at) BETWEEN min_date AND max_date", [
        'reference' => $reference, 'property_id' => $property_id, 'min_date' => $start_date, 'max_date' => $end_date
      ])
      ->limit(1);

    // alternatively, you can create DB command and execute it
    $command = $query->createCommand();

    return $rows = $command->queryAll();
  }

  /**
   * @param $model
   * @param $period_in_days
   * @return array|\yii\db\ActiveRecord[]
   */
  public function showPropertyMessages($model, $period_in_days){
    return \common\models\ContactForm::find()
        ->where('contact_type = 3 AND slug = :slug AND date(created_at) >= date(now()-interval :days day) ',
          ['slug' => $model->slug, 'days' => $period_in_days])->all();
  }

  /**
   * @param $start_date
   * @param $end_date
   * @param $model
   * @return array|\yii\db\ActiveRecord[]
   */
  public function showPropertyMessagesForRange($start_date, $end_date, $model){
    return \common\models\ContactForm::find()
      ->where('contact_type = 3 AND slug = :slug AND date(created_at) BETWEEN date(:min) AND date(:max) ',
        ['slug' => $model->slug, 'min' => $start_date, 'max' => $end_date])->all();
  }
}