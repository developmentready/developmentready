<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%favorites}}".
 *
 * @property string $id
 * @property integer $property_id
 * @property integer $user_id
 * @property integer $deleted
 *
 * @property Properties $property
 * @property User $user
 */
class Favorite extends \common\components\classes\CommonActiveRecord{
  /**
   * @inheritdoc
   */
  public static function tableName(){
    return '{{%favorites}}';
  }

  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['property_id', 'user_id', 'deleted'], 'integer']
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels(){
    return [
      'id' => Yii::t('app', 'ID'),
      'property_id' => Yii::t('app', 'Property'),
      'user_id' => Yii::t('app', 'User'),
      'deleted' => Yii::t('app', 'Deleted?'),
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getProperty(){
    return $this->hasOne(Property::className(), ['id' => 'property_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUser(){
    return $this->hasOne(User::className(), ['id' => 'user_id']);
  }
}
