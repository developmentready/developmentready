<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%property_images}}".
 *
 * @property integer $id
 * @property integer $default
 * @property integer $property_id
 * @property string $file_name
 * @property string $created_at
 *
 * @property Property $property
 */
class PropertyImages extends \common\components\classes\CommonActiveRecord{
  /**
   * @inheritdoc
   */
  public static function tableName(){
    return '{{%property_images}}';
  }

  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['default', 'property_id'], 'integer'],
      [['created_at'], 'safe'],
      [['file_name'], 'string', 'max' => 1024]
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels(){
    return [
      'id' => Yii::t('app', 'ID'),
      'default' => Yii::t('app', 'Default'),
      'property_id' => Yii::t('app', 'Property ID'),
      'file_name' => Yii::t('app', 'File Name'),
      'created_at' => Yii::t('app', 'Created At'),
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getProperty(){
    return $this->hasOne(Property::className(), ['id' => 'property_id']);
  }
}
