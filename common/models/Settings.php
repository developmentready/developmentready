<?php

namespace common\models;

use Yii;
use common\models\SettingsLang;

/**
 * This is the model class for table "{{%settings}}".
 *
 * @property integer $id
 * @property string $name
 * @property double $numeric_value
 * @property string $text_value
 * @property string $description
 * @property integer $creator_id
 * @property integer $created_at
 * @property integer $updater_id
 * @property integer $updated_at
 *
 * @property User $creator
 * @property User $updater
 */
class Settings extends \common\components\classes\BackendActiveRecord{
  /**
   * @inheritdoc
   */
  public static function tableName(){
    return '{{%settings}}';
  }

  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['numeric_value'], 'number'],
      [['creator_id', 'created_at', 'updater_id', 'updated_at'], 'integer'],
      [['name'], 'string', 'max' => 250],
      [['name'], 'unique'],
      [['text_value', 'description'], 'string']
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels(){
    return [
      'id' => Yii::t('common', 'ID'),
      'name' => Yii::t('common', 'Key'),
      'numeric_value' => Yii::t('common', 'Numeric Value'),
      'text_value' => Yii::t('common', 'Value'),
      'description' => Yii::t('common', 'Comment'),
      'creator_id' => Yii::t('common', 'Creator '),
      'created_at' => Yii::t('common', 'Created At'),
      'updater_id' => Yii::t('common', 'Updater'),
      'updated_at' => Yii::t('common', 'Updated At'),
    ];
  }

  public function findKeyword($name){
    $model = self::find()->where(['name' => $name])->one()->text_value;

    if(!$model) return false;

    return $model;
  }
}
