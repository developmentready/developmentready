<?php
/**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 7/11/15
 * Time: 16:10
 */

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%cities_lang}}".
 *
 * @property integer $id
 * @property integer $relation_id
 * @property integer $lang_id
 * @property string  $title
 *
 * @property Period $relation
 * @property Languages $lang
 */
class PeriodLang extends \common\components\classes\CommonActiveRecord{
  /**
   * @inheritdoc
   */
  public static function tableName(){
    return '{{%periods_lang}}';
  }

  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['relation_id', 'lang_id'], 'integer'],
      [['title'], 'string', 'max' => 255]
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels(){
    return [
      'id' => Yii::t('common', 'ID'),
      'relation_id' => Yii::t('common', 'Relation'),
      'lang_id' => Yii::t('common', 'Language'),
      'title' => Yii::t('common', 'Title'),
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getPeriod(){
    return $this->hasOne(Period::className(), ['id' => 'relation_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getLang(){
    return $this->hasOne(Language::className(), ['id' => 'lang_id']);
  }
}