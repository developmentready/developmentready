<?php
/**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 7/11/15
 * Time: 17:47
 */

namespace common\actions;

use yii;

class TitleUpdateAction extends \yii\base\Action{
  public $model;

  public function run($id){
    $main_model = $this->findModel($id);

    if ($main_model->load(Yii::$app->request->post())){
      if($main_model->save()){
        Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-success'],
          'body'=>Yii::t('backend', 'All data was successfully saved')]);
        return $this->controller->redirect(['index']);
      } else {
        Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-danger'],
          'body'=>Yii::t('backend', 'Error occurred during saving all data')]);
      }
      return $this->controller->refresh();
    } else {
      return $this->controller->render('//layouts/action/title-update', [
        'model' => $main_model,
      ]);
    }
  }

  /**
   * Finds the Model model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Model the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id){
    $main_model = $this->model->getModel('main_model');
    $class = $main_model::className();
    if (($model = $class::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }


}