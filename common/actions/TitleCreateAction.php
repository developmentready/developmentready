<?php
/**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 7/11/15
 * Time: 13:27
 */
namespace common\actions;

use yii;

class TitleCreateAction extends \yii\base\Action{
  public $model;

  public function run(){
    $main_model = $this->model->getModel('main_model');

    if($main_model->load(Yii::$app->request->post())){
      if($main_model->save()){
          Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-success'],
            'body'=>Yii::t('backend', 'New data successfully saved.')]);
        } else {
        Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-danger'],
          'body'=>Yii::t('backend', 'Error occurred during saving all data')]);
        }
      return $this->controller->refresh();
    } else {
      return $this->controller->render('//layouts/action/title-create', [
        'model' => $main_model,
      ]);
    }
  }
}