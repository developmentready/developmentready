<?php
/**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 7/11/15
 * Time: 17:01
 */

namespace common\actions;

use yii;
use backend\models\search\TitleSearch;

class TitleIndexAction extends \yii\base\Action{
  public $model;

  public function run(){
    $newSearchModel = new TitleSearch();
    $dataProvider = $newSearchModel->search($this->model, \Yii::$app->request->queryParams);

    return $this->controller->render('//layouts/action/title-index', [
      'searchModel' => $newSearchModel,
      'dataProvider' => $dataProvider,
    ]);
  }
}