<?php
/**
 * Created by PhpStorm.
 * User: rashad
 * Date: 12/18/15
 * Time: 00:05
 */

return [
  'all' => [
      'class' => 'yii\web\AssetBundle',
      'basePath' => '@frontend/assets/pready',
      'baseUrl' => '@frontendUrl/assets',
      'css' => ['/production/app.min.css'],
      'js' => ['/production/app.min.js'],
    ],
    'A' => ['css' => [

            ], 'js' => [

            ], 'depends' => ['all']
    ],
];


