<?php
return [
  'class'=>'codemix\localeurls\UrlManager',
  'enablePrettyUrl'=>true,
  'showScriptName'=>false,
  // List all supported languages here
  // Make sure, you include your app's default language.
  'languages' => ['en'],
  'rules'=> [
    '/' => 'site/index',
    'page/index' => 'site/index',
    'my-properties' => 'manage/index',
    //'page/about' => 'page/about',

    //exemptions
    'sold' => 'page/sold', 'about' => 'page/about', 'advertise' => 'page/advertise',
    'off-market-properties' => 'page/off-market-properties',

    //Property
    'properties' => 'property/index',
    'my-favorites' => 'property/my-favorites',
    ['pattern'=>'page/sold', 'route'=>'property/sold'],
    ['pattern'=>'properties/<slug>', 'route'=>'property/view'],
    'state/<state:\w+>' => 'property/state',

    // Account
    'my-account'  => 'account/index',
    'my-account2'  => 'account/index2',
    'property/id/<id:\d+>' => 'account/view-in-site',
    'edit/<id:\d+>' => 'account/update-property',
    'delete/<id:\d+>' => 'account/delete-property',

    // Report
    //'report'=>'report/index',
    //'report/<id:\d+>'=>'report/view',

    // Agencies
    ['pattern'=>'agencies/<slug>', 'route'=>'agency/view'],

    // Pages
    'page/testimonials' => 'page/testimonials',
    ['pattern'=>'page/<slug>', 'route'=>'page/view'],

    // Blog
    'blog' => 'blog/index',
    //['pattern'=>'blog/<slug>', 'route'=>'blog/view'],
    ['pattern'=>'<slug>', 'route'=>'blog/view'],

    // Articles
    ['pattern'=>'article/index', 'route'=>'article/index'],
    ['pattern'=>'article/attachment-download', 'route'=>'article/attachment-download'],
    ['pattern'=>'article/<slug>', 'route'=>'article/view'],

    #User
    'user/logout' => 'user/sign-in/logout',
    'user/login' => 'user/sign-in/login',
    'user/signup' => 'user/sign-in/signup',
    'user/reset-password' => 'user/sign-in/request-password-reset',
    'user/profile' => 'user/default/index',

    // Api
    ['class' => 'yii\rest\UrlRule', 'controller' => 'api/v1/article', 'only' => ['index', 'view', 'options']],
    ['class' => 'yii\rest\UrlRule', 'controller' => 'api/v1/user', 'only' => ['index', 'view', 'options']]
  ],
];
