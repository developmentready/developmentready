<?php
$config = [
  'homeUrl'=>Yii::getAlias('@frontendUrl'),
  'controllerNamespace' => 'frontend\controllers',
  'defaultRoute' => 'site/index',
  'language'=>'en',
  'modules' => [
    'user' => [
      'class' => 'frontend\modules\user\Module'
    ],

    'api' => [
      'class' => 'frontend\modules\api\Module',
      'modules' => [
        'v1' => 'frontend\modules\api\v1\Module'
      ]
    ],

    'gridview' =>  [
      'class' => '\kartik\grid\Module',
      // enter optional module parameters below - only if you need to
      // use your own export download action or custom translation
      // message source
      'downloadAction' => 'gridview/export/download',
      // 'i18n' => []
    ],
  ],
  'components' => [
    'authClientCollection' => [
      'class' => 'yii\authclient\Collection',
      'clients' => [
        'facebook' => [
          'class' => 'yii\authclient\clients\Facebook',
          'clientId' => getenv('FB_CLIENT_ID'),
          'clientSecret' => getenv('FB_CLIENT_SECRET')
        ],
      ],
    ],
    'assetManager' => [
      'bundles' => false,
    ],
	/*'reCaptcha' => [
	 'name' => 'reCaptcha',
	 'class' => 'himiklab\yii2\recaptcha\ReCaptcha',
	 //'siteKey' => '6LcbuCcTAAAAAMuoUiY7HbaaBb1Vw6lVS_Hr0hUu',
	 //'secret' => '6LcbuCcTAAAAAHygZzgjNssqfNeLNSuzGD7mMAVo',*/
	//],
    'errorHandler' => [
      'errorAction' => 'site/error'
    ],
    'request' => [
      'cookieValidationKey' => getenv('FRONTEND_COOKIE_VALIDATION_KEY')
    ],
    'user' => [
      'class'=>'yii\web\User',
      'identityClass' => 'common\models\User',
      'loginUrl'=>['/user/sign-in/login'],
      'enableAutoLogin' => false,
      'as afterLogin' => 'common\behaviors\LoginTimestampBehavior'
    ]
  ]
];

if (YII_ENV_DEV) {
  $config['modules']['gii'] = [
    'class'=>'yii\gii\Module',
    'generators'=>[
      'crud'=>[
        'class'=>'yii\gii\generators\crud\Generator',
        'messageCategory'=>'frontend'
      ]
    ]
  ];
}

if (YII_ENV_PROD) {
  // Maintenance mode
  $config['bootstrap'] = ['maintenance'];
  $config['components']['maintenance'] = [
    'class' => 'common\components\maintenance\Maintenance',
    'enabled' => function ($app) {
      return $app->keyStorage->get('frontend.maintenance') === 'enabled';
    }
  ];
}

return $config;
