<?php
namespace frontend\modules\user\models;

use common\models\User;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model{

  const AGENT_USER  = 1;   # Agent as a person
  const COMMON_USER = 2;  # All users will be registered
  const COMP_USER   = 3;   # Agencies as a company
  const ADMIN_USER  = 4;   # Web page administrative users

  public $username;
  public $email;
  public $user_type;
  public $password;
  public $name;

  /**
   * @inheritdoc
   */
  public function rules(){
    return [

      ['email', 'filter', 'filter' => 'trim'],
      ['email', 'required'],
      ['name', 'string', 'min' => 2, 'max' => 255],
      ['email', 'email'],
      ['email', 'unique',
        'targetClass'=> '\common\models\User',
        'message' => Yii::t('frontend', 'This email address has already been taken.')
      ],

      ['name', 'required'],
      ['name', 'string', 'min' => 3],

      ['user_type', 'required'],
      ['user_type', 'integer'],

      ['password', 'required'],
      ['password', 'string', 'min' => 4],
    ];
  }

  public function attributeLabels(){
    return [
      'username'=>Yii::t('frontend', 'Username'),
      'name' => Yii::t('frontend', 'Name'),
      'email'=>Yii::t('frontend', 'E-mail'),
      'password'=>Yii::t('frontend', 'Password'),
    ];
  }

  /**
   * Signs user up.
   *
   * @return User|null the saved model or null if saving fails
   */
  public function signup($from_property = false){
    if ($this->validate()) {
      $user = new User();
      $user->email = $this->email;
      $user->name = $this->name;
      $user->user_type = $this->user_type;
      if($from_property)
        $this->password = substr(Yii::$app->getSecurity()->generateRandomString(), 2, 8);
      $user->setPassword($this->password);
      if($user->save()){
        if($user->user_type == 1){
          Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-success'],
            'body'=>Yii::t('app', 'Thanks for registration.
          Admin of app will check and approve your status so you can add new properties.')]);
        } elseif($user->user_type == 2){
          Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-success'],
            'body'=>Yii::t('app', 'Thank you for registration.')]);
        }
      }
      $user->afterSignup();
      return $user;
    }

    return null;
  }

  public function getUserTypes(){
    return [
      self::AGENT_USER => Yii::t('common',  'Agency'),
      self::COMMON_USER => Yii::t('common', 'Personal')
    ];
  }
}
