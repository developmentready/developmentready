<?php
/**
 * Created by PhpStorm.
 * User: rashad
 * Date: 3/1/16
 * Time: 16:02
 */

namespace frontend\components;

use yii\base\Exception;
use yii\helpers\ArrayHelper;

class GeoLocation extends \yii\base\Component{

  /**
   *
   * link to service
   * @var string
   */
  protected $_service = 'http://www.geoplugin.net/php.gp?ip={IP}&base_currency={CURRENCY}';
  /**
   *
   * reponse data received
   * @var ArrayHelper
   */
  protected $_data;
  /**
   *
   * Base currency. CurrencyConversion property will be set based on this
   * property and the returned by the request to the service
   * @var string
   * @link http://www.geoplugin.com/webservices/php#php_currency_converter
   * @link http://www.geoplugin.com/iso4217
   */
  protected $_currency = 'USD';
  /**
   *
   * Constructor
   */
  public function __construct(){
    $this->_data = new ArrayHelper();

    return parent::__construct();
  }

  /**
   *
   * Locates IP information
   * @param string $ip address. If null, it will locate the IP of request
   * @return bool
   */
  public function locate($ip = null) {

    // Some IP exaomples of AU
    //VIC    -  202.86.212.97
    //QLD  - 110.23.26.96  /  27.33.6.182
    //WA    -  14.203.130.116
    //TAS   - 14.2.128.0
    //SA 	   - 14.203.62.150
    //NSW  -  14.203.193.223
    //ACT   -  14.202.198.7

    //$ip = '27.33.6.182';

    if(null === $ip) $ip = \Yii::$app->request->userIP;

    $host = str_replace('{IP}', $ip, $this->_service);
    $host = str_replace('{CURRENCY}', $this->_currency, $host );

    $response = $this->fetch($host);

    if(!is_null($response) && is_array($response)){
      return $response;
    }

    return true;
  }

  /**
   *
   * Converts an amount to the located currency by using the
   * located currency converter value. This function can only be
   * used after a call to locate function has been executed.
   * @param float | integer $amount
   * @param integer $float number of decimals
   * @param boolean $symbol to display the currency symbol or not (true by default)
   *
   * @return bool|float|string
   */
  public function currencyConvert($amount, $float=2, $symbol=true) {

    if( null === $this->getCurrencyConverter() || !is_numeric($amount) )
      return false;

    $converted = round( ($amount * $this->getCurrencyConverter()), $float );
    if ( $symbol === true ) {
      if($this->getCurrencySymbol() === 'USD')
        $converted   = $this->getCurrencySymbol().$converted;
      else $converted .= $this->getCurrencySymbol();
    }

    return $converted;
  }

  /**
   *
   * Set base property
   * @param string $currency
   * @link http://www.geoplugin.com/iso4217
   */
  public function setBaseCurrency( $currency = 'USD' ){
    $this->_currency = $currency;
  }

  /**
   *
   * Returns base currency
   */
  public function getBaseCurrency(){
    return $this->_currency;
  }

  /**
   *
   * Returns located IP
   */
  public function getIp(){
    return $this->_data->itemAt('ip');
  }

  /**
   *
   * Returns located City
   */
  public function getCity(){
    return $this->_data->itemAt('geoplugin_city');
  }
  /**
   *
   * Returns located region
   */
  public function getRegion(){
    return $this->_data->itemAt('geoplugin_region');
  }
  /**
   *
   * Returns located area code
   */
  public function getAreaCode(){
    return $this->_data->itemAt('geoplugin_areaCode');
  }
  /**
   *
   * Returns located DMA code
   */
  public function getDma(){
    return $this->_data->itemAt('geoplugin_dmaCode');
  }
  /**
   *
   * Returns located area code
   */
  public function getCountryCode(){
    return $this->_data->itemAt('geoplugin_countryCode');
  }
  /**
   *
   * Returns located country name
   */
  public function getCountryName(){
    return $this->_data->itemAt('geoplugin_countryName');
  }
  /**
   *
   * Returns located continent code
   */
  public function getContinentCode(){
    return $this->_data->itemAt('geoplugin_continentCode');
  }
  /**
   *
   * Returns located latitude
   */
  public function getLatitude(){
    return $this->_data->itemAt('geoplugin_latitude');
  }
  /**
   *
   * Returns located longitude
   */
  public function getLongitude(){
    return $this->_data->itemAt('geoplugin_longitude');
  }
  /**
   *
   * Returns located currency code
   */
  public function getCurrencyCode(){
    return $this->_data->itemAt('geoplugin_currencyCode');
  }
  /**
   *
   * Returns located currency symbol
   */
  public function getCurrencySymbol(){
    return $this->_data->itemAt('geoplugin_currencySymbol');
  }
  /**
   *
   * Returns located currency converter
   */
  public function getCurrencyConverter(){
    return $this->_data->itemAt('geoplugin_currencyConverter');
  }

  /**
   *
   * Fetches a URI and returns the contents of the call
   * EHttpClient could also be used
   *
   * @param string $host
   * @see http://www.yiiframework.com/extension/ehttpclient/
   *
   * @return mixed
   */
  protected function fetch($host) {

    $response = null;
    $result = false;

    if (function_exists('curl_init') ) {

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $host);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_USERAGENT, 'sambua');
      $response = curl_exec($ch);
      curl_close ($ch);

    } else if ( ini_get('allow_url_fopen') ) {

      $response = file_get_contents($host, 'r');
    }

    //var_dump(function_exists('file_get_contents'));

    try {

      $result = @unserialize($response);

    } catch(\yii\base\Exception $e) {

      echo 'Caught exception: ',  $e->getMessage(), "\n";
    }

    if( $result ) {
      return $result;
    } else {
      return [
        'geoplugin_request' => "202.86.212.97",
        'geoplugin_status' => 200,
        'geoplugin_credit' => "Some of the returned data includes GeoLite data created by MaxMind, available from <a href=\'http://www.maxmind.com\'>http://www.maxmind.com</a>.",
        'geoplugin_city' => "Melbourne",
        'geoplugin_region' => "Victoria",
        'geoplugin_areaCode' => "0",
        'geoplugin_dmaCode' => "0",
        'geoplugin_countryCode' => "AU",
        'geoplugin_countryName' => "Australia",
        'geoplugin_continentCode' => "OC",
        'geoplugin_latitude' =>  "-37.814",
        'geoplugin_longitude' => "144.9633",
        'geoplugin_regionCode' => "07",
        'geoplugin_regionName' => "Victoria",
        'geoplugin_currencyCode' => "AUD",
        'geoplugin_currencySymbol' => "&#36;",
        'geoplugin_currencySymbol_UTF8' => "$",
        'geoplugin_currencyConverter' => "1.3054"
      ];
    }
  }


}