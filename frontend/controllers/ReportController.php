<?php
namespace frontend\controllers;

use Symfony\Component\HttpKernel\Tests\Exception\HttpExceptionTest;
use Yii;
use mPDF;

class ReportController extends \common\components\classes\FrontendController
{
	public $layout = "//account/main";

	/**
	 * @inheritdoc
	 */
	public function actions(){
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			]
		];
	}

	function actionIndex(){
		return 'index';
	}

	public function actionView($id){
		$property = \common\models\Property::findOne($id);

		$propertyId = (int)$property['id'];
		$userId = (int)\Yii::$app->user->id;
		$result = \common\models\PropertyUser::find()->where("property_id = {$propertyId} AND user_id = {$userId}")->one();
		if(!$result or \Yii::$app->user->isGuest){
			thrownew \yii\web\HttpException(404, 'Property Not Found');
		}

		if(!$property->report){
			return $this->render('no-data');
		}

		$report = new \backend\models\Report();
		if($report->load(Yii::$app->request->get())){
			$start_date = strtotime($report->stat_start_date);
			$end_date = strtotime($report->stat_end_date);
			$report_for_last_days = 'Report From:' . $report->stat_start_date . ", Till:" . $report->stat_end_date;
			$contacts = $property->report->showPropertyMessagesForRange($start_date, $end_date, $property);

			if($end_date < $start_date || empty($end_date) || empty($start_date)){
				$report_for_last_days .= ' Wrong date range selected!. 0';
				$data_array = ['date' => [0 => date('M d', time())], 'views' => [0], 'calls' => [0], 'messages' => [0]];
			}else{
				$days = round(abs($end_date - $start_date) / 60 / 60 / 24 + 1, 0);
				$report_for_last_days .= ". {$days} ";
				$data_array = $this->_getDateRangeDataForChart($start_date, $end_date, $property);
			}
		}else{
			$report_for_last_days = $this->_getReportPeriodDays($property->publication_date);
			$data_array = $this->_getDataForChart($report_for_last_days, $property);
			$contacts = $property->report->showPropertyMessages($property, $report_for_last_days);
		}


		return $this->render('view', [
			'model' => $property,
			'report' => $report,
			'data_array' => $data_array,
			'contacts' => $contacts,
			'report_for_last_days' => $report_for_last_days,
		]);
	}

	/**
	 * @param $id
	 * @return string
	 * @throws \MpdfException
	 */
	public function actionExport($id){
		$this->layout = "//report/main";
		$property = \common\models\Property::findOne($id);

		$propertyId = (int)$property['id'];
		$userId = (int)\Yii::$app->user->id;
		$result = \common\models\PropertyUser::find()->where("property_id = {$propertyId} AND user_id = {$userId}")->one();
		if(!$result or \Yii::$app->user->isGuest){
			thrownew \yii\web\HttpException(404, 'Property Not Found');
		}

		if(!$property->report){
			return $this->render('no-data');
		}

		$report = new \backend\models\Report();

		if($report->load(Yii::$app->request->get())){
			$start_date = strtotime($report->stat_start_date);
			$end_date = strtotime($report->stat_end_date);
			$report_for_last_days = 'Report From:' . $report->stat_start_date . ", Till:" . $report->stat_end_date;
			$contacts = $property->report->showPropertyMessagesForRange($report->stat_start_date, $report->stat_end_date, $property);

			if($end_date < $start_date || empty($end_date) || empty($start_date)){
				$report_for_last_days .= ' Wrong date range selected!. 0';
				$data_array = ['date' => [0 => date('M d', time())], 'views' => [0], 'calls' => [0], 'messages' => [0]];
			}else{
				$days = round(abs($end_date - $start_date) / 60 / 60 / 24 + 1, 0);
				$report_for_last_days .= ". {$days} ";
				$data_array = $this->_getDateRangeDataForChart($start_date, $end_date, $property);
			}
		}else{
			$report_for_last_days = $this->_getReportPeriodDays($property->publication_date);
			$data_array = $this->_getDataForChart($report_for_last_days, $property);
			$contacts = $property->report->showPropertyMessages($property, $report_for_last_days);
		}

		$mpdf = new mPDF();
		// This must be set to allow mPDF to parse table data
		$mpdf->useGraphs = true;
		$mpdf->WriteHTML(
			$this->renderPartial('pdf', [
				'model' => $property,
				'data_array' => $data_array,
				'contacts' => $contacts,
				'report_for_last_days' => $report_for_last_days,
			])
		);
		$mpdf->Output();
	}

	/**
	 * @param $id
	 * @return string
	 * @throws \MpdfException
	 */
	/*public function actionSaveExport($id){
		$this->layout = "//report/main";
		$property = \common\models\Property::findOne($id);

		if(!$property->report){
			return $this->render('no-data');
		}

		$report = new \backend\models\Report();

		if($report->load(Yii::$app->request->get())){
			$start_date = strtotime($report->stat_start_date);
			$end_date = strtotime($report->stat_end_date);
			$report_for_last_days = 'Report From:' . $report->stat_start_date . ", Till:" . $report->stat_end_date;
			$contacts = $property->report->showPropertyMessagesForRange($report->stat_start_date, $report->stat_end_date, $property);

			if($end_date < $start_date || empty($end_date) || empty($start_date)){
				$report_for_last_days .= ' Wrong date range selected!. 0';
				$data_array = ['date' => [0 => date('M d', time())], 'views' => [0], 'calls' => [0], 'messages' => [0]];
			}else{
				$days = round(abs($end_date - $start_date) / 60 / 60 / 24 + 1, 0);
				$report_for_last_days .= ". {$days} ";
				$data_array = $this->_getDateRangeDataForChart($start_date, $end_date, $property);
			}
		}else{
			$report_for_last_days = $this->_getReportPeriodDays($property->publication_date);
			$data_array = $this->_getDataForChart($report_for_last_days, $property);
			$contacts = $property->report->showPropertyMessages($property, $report_for_last_days);
		}

		$mpdf = new mPDF();
		// This must be set to allow mPDF to parse table data
		$mpdf->useGraphs = true;
		$mpdf->WriteHTML(
			$this->render('pdf', [
				'model' => $property,
				'data_array' => $data_array,
				'contacts' => $contacts,
				'report_for_last_days' => $report_for_last_days,
			])
		);
		$mpdf->Output('D');
	}*/


	/**
	 * @param $id
	 * @return \yii\web\Response
	 * @throws \MpdfException
	 */
	/*public function actionSend($id){
		$property = \common\models\Property::findOne($id);

		$users = $property->propertyUsers;

		$report = new \backend\models\Report();

		if($report->load(Yii::$app->request->get())){
			$start_date = strtotime($report->stat_start_date);
			$end_date = strtotime($report->stat_end_date);
			$report_for_last_days = 'Report From:' . $report->stat_start_date . ", Till:" . $report->stat_end_date;
			$contacts = $property->report->showPropertyMessagesForRange($report->stat_start_date, $report->stat_end_date, $property);

			if($end_date < $start_date || empty($end_date) || empty($start_date)){
				$report_for_last_days .= ' Wrong date range selected!. 0';
				$data_array = ['date' => [0 => date('M d', time())], 'views' => [0], 'calls' => [0], 'messages' => [0]];
			}else{
				$days = round(abs($end_date - $start_date) / 60 / 60 / 24 + 1, 0);
				$report_for_last_days .= ". {$days} ";
				$data_array = $this->_getDateRangeDataForChart($start_date, $end_date, $property);
			}
		}else{
			$report_for_last_days = $this->_getReportPeriodDays($property->publication_date);
			$data_array = $this->_getDataForChart($report_for_last_days, $property);
			$contacts = $property->report->showPropertyMessages($property, $report_for_last_days);
		}

		$mpdf = new mPDF();
		$mpdf->WriteHTML($this->renderPartial('pdf', [
			'model' => $property,
			'data_array' => $data_array,
			'contacts' => $property->$contacts,
			'report_for_last_days' => $report_for_last_days,
		]));

		foreach($users as $user):
			$message = Yii::$app->mailer->compose('agency_message', ['model' => $property])
				->setFrom([Yii::$app->params['robotEmail'] => Yii::$app->name . ' robot'])
				->setTo($user->email)
				->setSubject('Permit ready monthly report for ' . $property->location);

			$filename = "Report-Permit-Ready-" . date("d-m-Y_H-i", time()) . ".pdf"; //Your Filename with local date and time

			$content = $mpdf->Output("", 'S');

			// Create attachment on-the-fly
			$message->attachContent($content,
				['fileName' => "{$filename}.pdf", 'contentType' => 'application/pdf']);

			if($message->send()){
				Yii::$app->session->setFlash('success', 'Attachment successfully sent to receipent e-mail.');
			}else{
				Yii::$app->session->setFlash('error', 'Some error occur during sending report to your mailbox.');
			}
		endforeach;

		return $this->redirect(\yii\helpers\Url::to(['view', 'id' => $id]));
	}*/

	/**
	 * @param $publication_date
	 * @return bool|string
	 */
	private function _getReportPeriodDays($publication_date){
		$report_for_last_days = 60 * 60 * 24 * 30; // 30 days
		if((time() - $publication_date) < $report_for_last_days)
			$report_for_last_days = time() - $publication_date;

		return date("j", $report_for_last_days);
	}

	/**
	 * @param $last_n_days
	 * @param $model
	 * @return mixed
	 */
	private function _getDataForChart($last_n_days, $model){
		$output['date'] = $output['views'] = $output['calls'] = $output['messages'] = [];
		$keywords = ['views' => 'view_count', 'calls' => 'call_count', 'messages' => 'message_count'];

		for($i = $last_n_days; $i > 0; $i--){
			$date = date('Y-m-d', strtotime("-{$i} days"));
			array_push($output['date'], date('d-M', strtotime("-{$i} days")));

			foreach($keywords as $k => $v){
				if(!empty($data = $model->report->propertyReportForDate($model->id, $v, $date))){
					array_push($output[$k], $data[0]['counter']);
				}else{
					array_push($output[$k], 0);
				}
			}
		}
		return $output;
	}

	/**
	 * @param $start_date
	 * @param $end_date
	 * @param $model
	 * @return mixed
	 */
	private function _getDateRangeDataForChart($start_date, $end_date, $model){
		$output['date'] = $output['views'] = $output['calls'] = $output['messages'] = [];
		$keywords = ['views' => 'view_count', 'calls' => 'call_count', 'messages' => 'message_count'];

		if($start_date == $end_date){
			$date = date('Y-m-d', $end_date);
			array_push($output['date'], date('d-M', $end_date));
			foreach($keywords as $k => $v){
				if(!empty($data = $model->report->propertyReportForDate($model->id, $v, $date))){
					array_push($output[$k], $data[0]['counter']);
				}else{
					array_push($output[$k], 0);
				}
			}
		}else{
			for($i = $start_date; $i <= $end_date; $i += 86400){
				$date = date('Y-m-d', $i);
				array_push($output['date'], date('d-M', $i));
				foreach($keywords as $k => $v){
					if(!empty($data = $model->report->propertyReportForDate($model->id, $v, $date))){
						array_push($output[$k], $data[0]['counter']);
					}else{
						array_push($output[$k], 0);
					}
				}
			}
		}

		return $output;
	}
}