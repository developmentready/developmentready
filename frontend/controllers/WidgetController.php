<?php
namespace frontend\controllers;

use Yii;
use common\models\Property;


/**
 * Site controller
 */
class WidgetController extends \common\components\classes\FrontendController{

  public $layout = "//widget";

  /**
   * @return string
   */
  public function actionBanner(){
    $featured_properties = Property::find()->active()->notSold()->featuredProperties()
      ->orderBy(['publication_date' => SORT_DESC])->all();

    return $this->render('banner', ['featured_properties' => $featured_properties,]);
  }
}
