<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/23/15
 * Time: 11:01
 */

namespace frontend\controllers;

use Yii;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use common\models\Page;
use yii\data\Pagination;


class BlogController extends Controller{

  public function actionView($slug){
    $page = Page::find();

    $model = $page->where(['url_name'=>$slug, 'page_type_id' => 2])->active()->one();

    if(!$model){
      throw new NotFoundHttpException(Yii::t('frontend', 'Page not found'));
    }

    $related_blog = $page->where('id != :model_id AND page_type_id = :page_type',
      ['model_id' => $model->id, 'page_type' => 2])
      ->active()->orderBy(['updated_at' => SORT_DESC])->limit(3)->all();

    $model->updateCounters(['view_counter' => 1]);

    return $this->render('view', [
      'model'=>$model, 'related_blog' => $related_blog]);
  }

  public function actionIndex(){
    $model  =  Page::findOne(5);

    $query   =  Page::find()->where(['page_type_id' => 2]);

    $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 12]);

    $blog = $query->offset($pages->offset)
      ->active()->orderBy(['publication_date' => SORT_DESC])->limit($pages->limit)->all();

    return $this->render('index', [
      'model' => $model,
      'blog'  => $blog,
      'pages' => $pages
    ]);
  }

}