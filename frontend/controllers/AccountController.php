<?php
/**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 7/29/15
 * Time: 20:00
 */

namespace frontend\controllers;

use common\models\PropertyImages;
use Yii;
use common\models\Property;
use yii\web\NotFoundHttpException;
use frontend\models\search\PropertySearch;
use yii\data\Pagination;
use common\models\MediaStorage;
use common\models\Agency;
use common\models\AgencyAgent;
use common\models\AgencyPosition;
use yii\helpers\Url;

class AccountController extends \common\components\classes\FrontendController{

  public $layout = "//account/main";

  ###########  PROPERTY  ###########

  /**
   * Index
   * @return string
   */
  public function actionIndex(){
    $searchModel = new PropertySearch();

    $query = $searchModel->searchAccount(Yii::$app->request->queryParams);

    $pages = new Pagination(['pageSize' => 10, 'totalCount' => $query->getTotalCount()]);

    $models = $query->getModels();

    return $this->render('index', [
      'models'        => $models,
      'dataProvider'  => $query,
      'searchModel'   => $searchModel,
      'pages'         => $pages
    ]);
  }

  public function actionIndex2(){
    $searchModel = new PropertySearch();

    $query = $searchModel->searchAccount2(Yii::$app->request->queryParams);

    $pages = new Pagination(['pageSize' => 10, 'totalCount' => $query->getTotalCount()]);

    $models = $query->getModels();

    return $this->render('index', [
        'models'        => $models,
        'dataProvider'  => $query,
        'searchModel'   => $searchModel,
        'pages'         => $pages
    ]);
  }

  /**
   * Redirect to main page property view
   * @param $slug
   * @return \yii\web\Response
   */
  public function actionViewInSite($slug){
    return $this->redirect(Url::to(["/properties/{$slug}"]));
  }


  /**
   * Add new property
   * @return string|\yii\web\Response
   */
  public function actionNewProperty(){
    if (\Yii::$app->user->identity->status == 1):
      $model = new Property();

      if ($model->load(Yii::$app->request->post())) {
        # if property set as featured till for some date it will be set.
        $model->status = 0;

        if($model->save()){
          Property::setLinks($model, 'propertyTypes', Yii::$app->request->post('Property')["propertyTypes"]);

          $images = Yii::$app->request->post('Property')["fileAttributes"];
          if(!empty($images) && (count($images) > 1)) $model->saveUploadedPropertyImages($images, $model->id);

          return $this->redirect(['index']);
        }
        echo "Couldn't save";
      } else {
        return $this->render('property/create', [
          'model' => $model,
        ]);
      }

    else:
      Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-danger'],
        'body'=>Yii::t('app', 'Sorry but you can\'t create new property while you are not approved by admin.')]);
      return $this->redirect('index');
    endif;
  }

  /**
   * Update Property
   * @param $id
   * @return string|\yii\web\Response
   * @throws NotFoundHttpException
   */
  public function actionUpdateProperty($id){

    $model = $this->findModel($id);
    $user_id = \Yii::$app->user->identity->id;
    //!$model->isEditedByAdmin($user_id)
    if($model->isUserPropertyCoOwner($user_id) && (!empty($model))):
      if ($model->load(Yii::$app->request->post())) {

        if($model->save()){
          Property::setLinks($model,'propertyTypes', Yii::$app->request->post('Property')["propertyTypes"]);

          $images = Yii::$app->request->post('Property')["fileAttributes"];
          //if(!empty($images) && (count($images) > 1)) $model->saveUploadedPropertyImages($images, $model->id);
          if(!empty($images)) $model->saveUploadedPropertyImages($images, $model->id);

          //+dorian
          if(\Yii::$app->request->post('sort_img')){
            $i = 1;
            foreach(\Yii::$app->request->post('sort_img') as $item){
              $fName = pathinfo($item['url']);
              $img = $fName['filename'] . '.' . $fName['extension'];
              $obj = PropertyImages::find()->where(['file_name' => $img, 'property_id' => $model->id])->one();
              $obj->sort_order = $i;
              $obj->save();
              $i++;
            }
          }
          //-dorian

          return $this->redirect(['index']);
        }
        $this->render('update', ['model' => $model]);
      } else {
        return $this->render('property/update', [
          'model' => $model,
        ]);
      }
    else:
      Yii::$app->getSession()->setFlash('alert', [
        'body'=>Yii::t('common', 'You don\'t have access for this action.'),
        'options'=>['class'=>'alert-danger']]);
      return $this->goHome();
    endif;
  }

  /**
   *
   * Delete Property
   * @param $id
   * @return \yii\web\Response
   * @throws NotFoundHttpException
   */
  public function actionDeleteProperty($id){
    $model = $this->findModel($id);
    $user_id = \Yii::$app->user->identity->id;

    if($model->isUserPropertyOwner($user_id) && (!empty($model))):
      if($model->delete()){
        $images_folder = Yii::getAlias('@storage')."/properties/{$id}";
        if(is_dir($images_folder)){
          #!!!! Don't play with this method, you can remove all folders
          \common\components\helpers\Setup::deleteDir($images_folder) ;
        }
        Yii::$app->getSession()->setFlash('alert', [
          'body'=>Yii::t('common', 'Your property was successfully deleted from listing.'),
          'options'=>['class'=>'alert-success']
        ]);
        return $this->redirect(['/my-account']);
      }
      else
        Yii::$app->getSession()->setFlash('alert', [
          'body'=>Yii::t('common', 'Sorry some error occurred during deleting your property listing.'),
          'options'=>['class'=>'alert-danger']]);
      return $this->redirect(['index']);
    else:
      Yii::$app->getSession()->setFlash('alert', [
        'body'=>Yii::t('common', 'You don\'t have access for this action'),
        'options'=>['class'=>'alert-danger']]);
      return $this->goHome();
    endif;
  }

  /**
   * @param $id
   * @return \yii\web\Response
   */
  public function actionDeletePropertyImage($id){
    $model = \common\models\PropertyImages::findOne($id);
    if(!empty($model)){
      $property_id = $model->property_id;
      $file = \Yii::getAlias('@storage')."/properties/".$property_id."/".$model->file_name;
      if(is_file($file)) unlink ($file);
      $model->delete();
    } else{

    }
    return $this->redirect(Url::to(['update-property', 'id' => $property_id]));
  }

  ###########  /PROPERTY  ###########

  /**
   * Updates an existing Agency model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionAgency(){
    if (\Yii::$app->user->identity->status == 1):
      $model = $this->findAgency();

      if ($model->load(Yii::$app->request->post()) && $model->save()){

        $image = Yii::$app->request->post('Agency', '')["fileAttributes"];

        if(!empty($image)) {
          $model->createAgencyWatermark($image, false);
          $model->saveSingleImage($image, $model->id, 'Agency', 320, $model->color, 320);
        }

        return $this->redirect(['/account/index', 'id' => $model->id]);
      } else {
        return $this->render('agency/update', [
          'model' => $model,
        ]);
      }

    else:
      Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-danger'],
        'body'=>Yii::t('app', 'Sorry but you aren\'t allowed for this operation while you are not approved by admin.')]);
      return $this->redirect('index');
    endif;
  }

  ###########  AGENTS  ###########

  /**
   * Lists all AgencyAgent models.
   * @return mixed
   */
  public function actionAgents(){
    $searchModel = new \frontend\models\search\AgencyAgentSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('agents/index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Creates a new AgencyAgent model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionNewAgent(){
    $model = new AgencyAgent();

    if ($model->load(Yii::$app->request->post()) && $model->save()) {

      $image = Yii::$app->request->post('AgencyAgent', '')["fileAttributes"];

      if(!empty($image)) {
        $model->saveSingleImage($image, $model->id, 'AgencyAgent', 150, '#ffffff', 150);
      }

      return $this->redirect(['agents']);
    } else {
      return $this->render('agents/create', [
        'model' => $model,
        'agencies' => Agency::find()->active()->all(),
        'positions' => AgencyPosition::find()->active()->all(),
      ]);
    }
  }

  /**
   * Updates an existing AgencyAgent model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdateAgent($id){
    $model = $this->findAgent($id);

    if ($model->load(Yii::$app->request->post()) && $model->save()) {

      $image = Yii::$app->request->post('AgencyAgent', '')["fileAttributes"];

      if(!empty($image)) {
        $model->saveSingleImage($image, $model->id, 'AgencyAgent', 150, '#ffffff', 150);
      }

      return $this->redirect(['agents']);
    } else {
      return $this->render('agents/update', [
        'model' => $model,
        'agencies' => Agency::find()->active()->all(),
        'positions' => AgencyPosition::find()->active()->all(),
      ]);
    }
  }

  /**
   * Deletes an existing AgencyAgent model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDeleteAgent($id){
    $this->findAgent($id)->delete();

    return $this->redirect(['agents']);
  }


  public function actionSingleImageDelete($id){
    $model = MediaStorage::findOne($id);

    if(!empty($model)){
      $return_id = $model->model_id;
      $path = \Yii::getAlias('@storage').$model->media_path;
      $model->delete();
      if(is_file($path)) unlink ($path);
      Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-success'], 'body'=>Yii::t('app', 'All data was successfully deleted')]);
      if(Yii::$app->request->referrer !== Null)
        return $this->redirect(Yii::$app->request->referrer);
      else
        return $this->redirect([$this->id."/update/?id={$return_id}"]);
    } else {
      Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-danger'], 'body'=>Yii::t('app', 'Something went wrong')]);
      if(Yii::$app->request->referrer !== Null)
        return $this->redirect(Yii::$app->request->referrer);
      else
        $this->redirect([$this->id."/index"]);
    }
  }

  ###########  /AGENTS  ###########

  /**
   *
   * @return string|\yii\web\Response
   */
  public function actionProfile(){
    $user_id = Yii::$app->user->identity->id;
    $model = \common\models\User::findOne($user_id);
    $profile = \common\models\UserProfile::findOne($user_id); # ->where(['user_id' => $user_id]);

    if(Yii::$app->request->post()){
      if($profile->load(Yii::$app->request->post()) && $profile->save()){
        Yii::$app->getSession()->setFlash('alert', [
          'body'=>Yii::t('common', 'Your profile information was successfully updated.'),
          'options'=>['class'=>'alert-success']
        ]);
      } else {
        Yii::$app->getSession()->setFlash('alert', [
          'body'=>Yii::t('common', 'Sorry some error occurred during editing your profile information.'),
          'options'=>['class'=>'alert-danger']]);
      }
      return $this->refresh();
    }

    return $this->render('profile',[
      'model' =>$model,
      'profile' => $profile,
    ]);
  }

  /**
   * Finds the PropertyType model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Amenity the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id){
    if (($model = Property::findOne($id)) !== null){
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
   * Finds the Agency model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @return Agency the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findAgency(){
    if (($model = Agency::find()->where(['user_id' => Yii::$app->user->identity->id])->one()) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
   * Finds the AgencyAgent model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return AgencyAgent the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findAgent($id){
    if (($model = AgencyAgent::find()
        ->where(['id' => $id, 'agency_id' => Yii::$app->user->identity->agency->id])
        ->one()) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

}