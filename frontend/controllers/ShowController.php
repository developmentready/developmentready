<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/11/15
 * Time: 21:11
 */

namespace frontend\controllers;

use Yii;

use yii\web\Controller;
use yii\web\NotFoundHttpException;


class ShowController extends Controller{

  public function actionIndex(){
    return $this->render('index', ['title' => 'Links Page for Permit ready']);
  }

  public function actionHome(){
    return $this->render('home', ['title' => 'Permit Ready | Home Page']);
  }

  public function actionProperties(){
    return $this->render('listings', ['title' => 'Permit Ready | Home Page']);
  }

  public function actionAgencies(){
    return $this->render('agencies', ['title' => 'Permit Ready | Agencies List Page']);
  }

  public function actionBlog(){
    return $this->render('blog', ['title' => 'Permit Ready | Blog Page']);
  }

  public function actionSingleBlog(){
    return $this->render('single-blog', ['title' => 'Permit Ready | Single Blog Page']);
  }

  public function actionAbout(){
    return $this->render('about', ['title' => 'Permit Ready | About Page']);
  }

  public function actionSingleProperty(){
    return $this->render('single-property', ['title' => 'Permit Ready | Single Property Page']);
  }

  public function actionWhy(){
    return $this->render('why', ['title' => 'Permit Ready | Why We Page']);
  }

}