<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/29/15
 * Time: 16:51
 */

namespace frontend\controllers;

use Yii;
use common\models\FileStorageItem;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FileStorageController implements the CRUD actions for FileStorageItem model.
 */
class FileStorageController extends Controller{
  public function behaviors(){
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
          'upload-delete' => ['delete']
        ]
      ]
    ];
  }


  public function actions(){
    return [
      'upload' => [
        'class' => 'trntv\filekit\actions\UploadAction',
        'deleteRoute' => 'upload-delete'
      ],
      'upload-delete' => [
        'class' => 'trntv\filekit\actions\DeleteAction'
      ],
      'upload-imperavi' => [
        'class' => 'trntv\filekit\actions\UploadAction',
        'fileparam' => 'file',
        'responseUrlParam'=> 'filelink',
        'multiple' => false,
        'disableCsrf' => true
      ]
    ];
  }

  /**
   * Finds the FileStorageItem model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return FileStorageItem the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id){
    if (($model = FileStorageItem::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }
}
