<?php
/**
 * Created by PhpStorm.
 * User: zein
 * Date: 7/4/14
 * Time: 2:01 PM
 */

namespace frontend\controllers;

use Yii;
use common\models\Page;
use yii\web\NotFoundHttpException;
use yii\data\Pagination;
use common\models\ContactForm;

class PageController extends \common\components\classes\FrontendController{

  public function actionView($slug){
    $model = Page::find()->where(['url_name'=>$slug, 'status'=>Page::STATUS_PUBLISHED])->one();
    $testimonials = Page::find()->where(['page_type_id' => 3])->joinWith('withImages')->active()->all();
    $testimonial = new ContactForm();
    $testimonial->scenario = 'testimonial';

    if($testimonial->load(Yii::$app->request->post())){
      # contact type for testimonial
      $testimonial->contact_type = 2;
      $testimonial->slug = $slug;
//      $new_model = new Page();
//      $new_model->title = $testimonial->name;
//      $new_model->body = $testimonial->message;
//      $new_model->page_type_id = 3;
//      $new_model->status = 0;
//      $new_model->save()
      if($testimonial->save()){
        $testimonial->contact($testimonial);
        Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-success'],
          'body'=>Yii::t('app', 'Thank you, your message has been successfully submitted.')]);

        $testimonial = new ContactForm();

      } else {
        Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-danger'],
          'body'=>Yii::t('app', 'Error occurred during sending data')]);
      }
    }

    if(!$model){
      throw new NotFoundHttpException(Yii::t('frontend', 'Page not found'));
    }

    return $this->render('view', [
      'model'=>$model,
      'testimonials' => $testimonials,
      #Testimonial form records
      'testimonial' => $testimonial,
    ]);
  }
}