<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/23/15
 * Time: 01:14
 */

namespace frontend\controllers;

use Yii;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use common\models\Property;
use common\models\Agency;
use yii\data\Pagination;


class AgencyController extends Controller{


  public function actionIndex($id){
    $model = Agency::find()->active()->makeSort()->all();

    return $this->render('index' ,['']);
  }

  /**
   * View separate property in new page based on provided id
   * @param $slug
   * @return string
   * @throws NotFoundHttpException
   */
  public function actionView($slug){
    $model = Agency::find()->where(['slug'=>$slug])->featuredAgency()->one();

    $message_model = new \common\models\ContactForm();
    $message_model->scenario = 'send_message';

    if (!$model) {
      throw new NotFoundHttpException(Yii::t('frontend', 'Page not found'));
    }

    $query = Property::find()->joinWith('propertyAgencies')
      ->where(['user_id' => $model->user->id, '{{%properties.status}}' => 1])->orderBy(['sold' => SORT_ASC, '{{%properties}}.created_at' => SORT_DESC]);

    $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 6]);

    $properties = $query->offset($pages->offset)->limit($pages->limit)->all();

    return $this->render('view', [
      'model'=>$model,
      'pages' => $pages,
      'property' => new Property(),
      'properties' => $properties,
      'featured_properties' => Property::find()->active()->notSold()->featuredProperties()
        ->orderBy(['updated_at' => SORT_DESC])->all(),
      'message_model' => $message_model,
    ]);
  }
}
