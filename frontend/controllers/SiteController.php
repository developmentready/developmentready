<?php
namespace frontend\controllers;


use Yii;
use yii\base\Model;
use common\models\Page;
use common\models\Property;
use common\models\State;
use common\models\ContactForm;
use common\models\User;
use frontend\models\search\PropertySearch;
use common\models\Agency;
use yii\bootstrap\ActiveForm;
use yii\helpers\Json;
use yii\web\Response;



/**
 * Site controller
 */
class SiteController extends \common\components\classes\FrontendController{

//  public function behaviors() {
//    return [
//      [
//        'class' => 'yii\filters\HttpCache',
//        'only' => ['index'],
//        'cacheControlHeader' => 'Cache-Control: public, max-age=3600',
//      ],
//    ];
//  }

  /**
   * @inheritdoc
   */
  public function actions(){
    return [
      'error' => [
        'class' => 'yii\web\ErrorAction'
      ],
      'captcha' => [
        'class' => 'yii\captcha\CaptchaAction',
        'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null
      ],
      'set-locale'=>[
        'class'=>'common\actions\SetLocaleAction',
        'locales'=>array_keys(Yii::$app->params['availableLocales'])
      ]
    ];
  }

  /**
   * @return string
   */
  public function actionIndex() {
    $site = Page::findOne(2)->attributes;

    $featured_agencies = \common\models\Agency::find()->featuredAgency()->limit(6)->orderBy(['priority' => SORT_ASC ])->all();
    //$featured_agencies = (new Agency)->getFeatured($state['id'],$limit = 6);

    $blog = Page::find()->where(['page_type_id' => 2])->active()->orderBy(['publication_date' => SORT_DESC])
      ->limit(4)->all();

    $about = Page::findOne(20);

    $testimonials = Page::find()->where(['page_type_id' => 6])->active()->makeSort()->all();

    $cookies = Yii::$app->request->cookies;
    if($cookies->getValue('userState') !== null){
      $state = State::findOne($cookies->getValue('userState'))->attributes;
    }else{
      $state = State::find()->where('`default` = 1')->active()->one()->attributes;
      if(!$state) $state = State::findOne(7);
    }

    /*$state = State::find()->where('`default` = 1')->active()->one();
    if(!$state) $state = State::findOne(7);*/

    $map_properties = Property::find()->joinWith('suburbs')
      ->where("state_id = :id", ['id' => $state['id']])->activeProperty()->limit(50)->all();

    return $this->render('index', [
      'site'                => $site,
      'featured_properties' => Property::getStateRelatedFeaturedProperties(),
      'latest_properties'   => Property::getStateRelatedLatestProperties(),
      'testimonials'        => $testimonials,
      'featured_agencies'   => $featured_agencies,
      'blog'                => $blog,
      'about'               => $about,
      'state'               => $state,
      'map_properties'      => $map_properties,
    ]);
  }

  /**
   * @return string|\yii\web\Response
   */
  /*
  public function actionContacts($id, $type){
    $model = new ContactForm();
    $response = [];
    $success_message = "<p class='text-center text-success flash-text'>Thank you for contacting us. We will respond to you as soon as possible.</p>";
    $error_message = "<p class='text-center text-danger'>Error occurred sending message.</p>";
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

    if ($model->load(Yii::$app->request->post()) and $model->validate()){

      if(array_key_exists('subjects', \Yii::$app->request->post('ContactForm'))){
        if(!empty($subjects = \Yii::$app->request->post('ContactForm')["subjects"])){
          $output = ''; $counter = 1;
          foreach($subjects as $subject){
            if($subject){
              if(is_array($subject)){

                if(isset($subject[$id][0]))
                  $subject_id = (int)$subject[$id][0];
                else
                  $subject_id = 0;

                if($subject_id > 0)
                  $key = $subject_id;
                else
                  $key = "";

              } elseif(strlen($subject) > 0){
                $key = (int)$subject;
              }
            }

            if(!empty($key)){
              $output .= "{$counter}) ".$model->getSubjects()[$key].". ";
              $counter++;
            }
          }
          $model->subject = rtrim($output, ",");
        }
      }

      if($type == 1) {
        #message received from property page
        $model->contact_type = 3;
        $property = Property::findOne($id);
        if(!$property) return $error_message;
        $mail_list = [];

        $property->updatePropertyStats($property->id, 'message_count');
        $property->updatePropertyReports($property->id, 'message_count');

        if(!empty($agencies = $property->propertyAgents)){
          foreach($agencies as $agent){
            if(!empty($agent->email)){
              array_push($mail_list, $agent->email);
            }
          }
        } else {
          array_push($mail_list, 'no-agent@permitready.com.au');
        }

        $model->slug = $property->slug;
        //$model->sendBulkNotificationMessage($model, $mail_list);

        if(!$model->save(false)) {
          return $response['message'] = $error_message;
        }

        $response['message'] = $success_message;
        $response['property_id'] = $property->id;
        $response['model_id'] = $model->id;

        return $response;

      } elseif ($type == 2){
        #message received from agency page
        $model->contact_type = 4;
        $agency = Agency::findOne($id);
        if(!$agency) return $error_message;
        $model->slug = $agency->slug;
        $model->contactAgency($agency->user->email, $model);
      }

      if($model->save(false)){
        echo $success_message;
        return;
      }
    }
    echo $error_message;
    return;
  }*/

  /**
   * @param $id
   * @param $model
   * @return string|void
   */

  public function actionContacts($id, $type){
    $model = new ContactForm();
    $response = [];
    $success_message = "<p class='text-center text-success flash-text'>Thank you for contacting us. We will respond to you as soon as possible.</p>";
    $error_message = "<p class='text-center text-danger'>Error occurred sending message.</p>";

    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

    if ($model->load(Yii::$app->request->post())){

      // check captcha
      if($model->check != Yii::$app->session->getFlash('captcha')){
        echo $error_message;
        return;
      }

      if(array_key_exists('subjects', \Yii::$app->request->post('ContactForm'))){
        if(!empty($subjects = \Yii::$app->request->post('ContactForm')["subjects"])){
          $output = ''; $counter = 1;
          foreach($subjects as $subject){
            if($subject){
              if(is_array($subject)){

                if(isset($subject[$id][0]))
                  $subject_id = (int)$subject[$id][0];
                else
                  $subject_id = 0;

                if($subject_id > 0)
                  $key = $subject_id;
                else
                  $key = "";

              } elseif(strlen($subject) > 0){
                $key = (int)$subject;
              }
            }

            if(!empty($key)){
              $output .= "{$counter}) ".$model->getSubjects()[$key].". ";
              $counter++;
            }
          }
          $model->subject = rtrim($output, ",");
        }
      }

      if($type == 1) {
        #message received from property page
        $model->contact_type = 3;
        $property = Property::findOne($id);
        if(!$property) return $error_message;
        $mail_list = [];

        $property->updatePropertyStats($property->id, 'message_count');
        $property->updatePropertyReports($property->id, 'message_count');

        if(!empty($agencies = $property->propertyAgents)){
          foreach($agencies as $agent){
            if(!empty($agent->email)){
              array_push($mail_list, $agent->email);
            }
          }
        } else {
          array_push($mail_list, 'no-agent@permitready.com.au');
        }

        $model->slug = $property->slug;
        //$model->sendBulkNotificationMessage($model, $mail_list);

        if(!$model->save(false)) {
          return $response['message'] = $error_message;
        }

        $response['message'] = $success_message;
        $response['property_id'] = $property->id;
        $response['model_id'] = $model->id;

        return $response;

      } elseif ($type == 2){
        #message received from agency page
        $model->contact_type = 4;
        $agency = Agency::findOne($id);
        if(!$agency) return $error_message;
        $model->slug = $agency->slug;
        $model->contactAgency($agency->user->email, $model);
      }

      if($model->save(false)){
        echo $success_message;
        return;
      }
    }
    echo $error_message;
    return;
  }

  public function actionSendMessageToAgencies($id, $model) {
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    $model = ContactForm::findOne($model);
    $property = Property::findOne($id);
    $error_message = "<p class='text-center text-danger'>Error occurred sending message.</p>";

    if(!$property || !$model) return $error_message;

    $mail_list = [];

    if(!empty($agencies = $property->propertyAgents)) {
      foreach($agencies as $agent) {
        if(!empty($agent->email)){
          array_push($mail_list, $agent->email);
        }
      }
    } else {
      array_push($mail_list, 'no-agent@permitready.com.au');
    }

    $model->slug = $property->slug;
    $model->sendBulkNotificationMessage($model, $mail_list);
    echo "Success";
    return;
  }

  /**
   * @return string
   */
  public function actionSubscription() {
    $model = new ContactForm();
    $success_message =
      "<p class='text-center flash-text'>
        Thanks, you will now be kept up-to-date with the latest development opportunities within the states you selected.
      </p>";
    $error_message = "<p class='text-center text-warning'>Your e-mail have been already registered. Thanks!</p>";

    //var_dump(Yii::$app->request->post());die;
    if ($model->load(Yii::$app->request->post())){

      $output = 'Interested states: ';
      if(array_key_exists('states', \Yii::$app->request->post('ContactForm'))){
        if(!empty($states = \Yii::$app->request->post('ContactForm')["states"])){
          $counter = 1;
          foreach($states as $state){
            // mysql_real_escape_string Not supports by PHP7
            //$name = mysql_real_escape_string($state);
            $output .= "{$counter}) {$state} ";
            $counter++;
          }
        }
      } else {
        $output .= 'No state selected';
      }
      #message received from agency page
      $model->contact_type = 1;
      $model->subject = $output;
      $model->subscriptionInfoMail($model);
    }
    //var_dump(222);die;

    if($model->save()){
      $cookie = new \yii\web\Cookie([
        'name' => 'subscribed',
        'value' => 'yes',
        'expire' => time() + (60*60*24*360),
      ]);

      Yii::$app->response->cookies->add($cookie);

      $model->afterSubscription($model->name, $model->email, $states);

      echo $success_message;
      return;
    }

    echo $error_message;
    return;
  }


  /**
   * Set cookie for 4 days if user just closed subscription page
   */
  public function actionCloseSubscription(){
    $cookie = new \yii\web\Cookie([
      'name' => 'subscribed',
      'value' => 'yes',
      'expire' => time() + (60*60*24*10),
    ]);

    Yii::$app->response->cookies->add($cookie);
  }

  /**
   * @param $state
   * @return string
   */
  public function actionChangeState($state) {

    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

    $state_title = \common\models\State::findOne($state)->title;

    $cookie = new \yii\web\Cookie([
      'name' => 'userState',
      'value' => $state,
      'expire' => time() + (60*60*24*180), // for 180 days
    ]);

    Yii::$app->response->cookies->add($cookie);

    $cookie2 = new \yii\web\Cookie([
      'name' => 'userStateTitle',
      'value' => $state_title,
      'expire' => time() + (60*60*24*180), // for 180 days
    ]);

    Yii::$app->response->cookies->add($cookie2);

    return $state_title;
  }


  /**
   * If user press this button, it will save information for one year
   */
  public function actionAlreadySubscribed(){
    $cookie = new \yii\web\Cookie([
      'name' => 'subscribed',
      'value' => 'yes',
      'expire' => time() + (60*60*24*365),
    ]);

    Yii::$app->response->cookies->add($cookie);
  }

  /**
   * @return array
   */
  public function actionValidateForm(){
    $model = new ContactForm();
    $request = \Yii::$app->getRequest();
    if ($request->isPost && $model->load($request->post())) {
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      return ActiveForm::validate($model);
    }
  }

  /**
   * Will return list of option items based on selected country
   * @param $id
   * @return string
   */
  public function actionLists($id) {
    $id = (int)$id;

    $defaultCountryId = Yii::$app->request->cookies->getValue('country_id', 15);
    $defaultCountry = Country::findOne($defaultCountryId);

    $defaultCityId = $defaultCountry->defaultCity['0']->id;


    $countOfCities = City::find()->where(['country_id' => $id])->active()->count();
    $defaultCity = City::find($defaultCityId)->joinWith('translation')->one();

    $cities = City::find()->where(['country_id' => $id])->active()->joinWith('translation')->orderBy('title ASC')->all();

    if($id === $defaultCountryId && $countOfCities > 0)
      $output = "<option value='".$defaultCity->id."'>".$defaultCity->translation->title."</option>";
    else
      $output = '<option value="1">'.Yii::t('common','Not in the list').'</option>';

    if($countOfCities>0){
      foreach($cities as $item){
        $output .= "<option value='".$item->id."'>".$item->translation->title."</option>";
      }
    }

    return $output;
  }

  /**
   * @return string
   */
  public function actionSitemapGenerator(){
    $time = time() + (60 * 60 * 24 * 30);
    $home_url = Yii::getAlias('@frontendUrl');
    $pages = [
      ['link' => '', 'priority' => '1.0', 'frequency' => 'weekly'],
    ];
    $page_output = '';
    foreach($pages as $item){
      $page_output .= "\n<url>\n\t<loc>".$home_url."/{$item['link']}</loc>\n\t<changefreq>{$item['frequency']}</changefreq>\n\t<priority>{$item['priority']}</priority>\n</url>";
    }

    $blog = Page::find()->where("status = 1 AND page_robot_indexing = 1 AND page_type_id = 2")->orderBy(['publication_date' => SORT_DESC, 'id' => SORT_ASC])->all();
    $common_pages = Page::find()->where("status = 1 AND page_robot_indexing = 1 AND url_name <> 'index' AND page_type_id = 39")->orderBy(['priority' => SORT_ASC, 'id' => SORT_ASC])->all();

    $properties = Property::find()->where("status=1 AND page_robot_indexing = 1")->orderBy(['priority' => SORT_ASC, 'publication_date' => SORT_DESC ])->all();

    $output = '';

    foreach($common_pages as $item){
      if(strpos($item->url_name, "/")){
        $url = $home_url."/{$item->url_name}";
      } else {
        $url = $home_url."/page/{$item->url_name}";
      }
      $output .= "\n<url>\n\t<loc>".$url."</loc>\n\t<changefreq>monthly</changefreq>\n\t<priority>0.8</priority>\n</url>";
    }

    foreach($blog as $item){
      $output .= "\n<url>\n\t<loc>".$home_url."/".$item->url_name."</loc>\n\t<changefreq>monthly</changefreq>\n\t<priority>0.8</priority>\n</url>";
    }

    foreach ($properties as $item){
      $output .= "\n<url>\n\t<loc>".$home_url."/properties/{$item->slug}</loc>\n\t<changefreq>monthly</changefreq>\n\t<priority>0.6</priority>\n</url>";
    }

    $content = "<?xml version='1.0' encoding='UTF-8'?>\n<urlset xmlns='http://www.sitemaps.org/schemas/sitemap/0.9'>".$page_output. $output. "\n</urlset>";
    $myFile = Yii::getAlias("@frontend/web/sitemap.xml");
    $fh = fopen($myFile, 'w+') or die("can't open file");
    fwrite($fh, $content);
    fclose($fh);
    return "Sitemap Successfully Generated";
  }

  /**
   * @return string
   */
  public function actionTestimonials(){
    $model = Page::findOne(28);
    $testimonials = Page::find()->where(['page_type_id' => 6])->active()->makeSort()->all();
    $agencies = \common\models\Agency::find()->joinWith('media')->featuredAgency()->all();

    return $this->render('testimonials', [
      'model'=>$model,
      'testimonials' => $testimonials,
      'agencies' => $agencies
    ]);

  }


  /**
   * @return string
   */
  public function actionTest() {
    $site = Page::findOne(2)->attributes;

    $cookies = Yii::$app->request->cookies;
    if($cookies->getValue('userState') !== null){
      $state = State::findOne($cookies->getValue('userState'))->attributes;
    }else{
      $state = State::find()->where('`default` = 1')->active()->one()->attributes;
      if(!$state) $state = State::findOne(7)->attributes;
    }

      //$featured_agencies = \common\models\Agency::find()->featuredAgency()->limit(6)->orderBy(['priority' => SORT_ASC ])->all();
    $featured_agencies = (new Agency)->getFeatured($state['id'],$limit = 6);



    $blog = Page::find()->where(['page_type_id' => 2])->active()->orderBy(['publication_date' => SORT_DESC])
        ->limit(4)->all();

    $about = Page::findOne(20);

    $testimonials = Page::find()->where(['page_type_id' => 6])->active()->makeSort()->all();

    /*$state = State::find()->where('`default` = 1')->active()->one();
    if(!$state) $state = State::findOne(7);*/

    $map_properties = Property::find()->joinWith('suburbs')
        ->where("state_id = :id", ['id' => $state['id']])->activeProperty()->limit(50)->all();

    return $this->render('index', [
        'site'                => $site,
        'featured_properties' => Property::getStateRelatedFeaturedProperties(),
        'latest_properties'   => Property::getStateRelatedLatestProperties(),
        'testimonials'        => $testimonials,
        'featured_agencies'   => $featured_agencies,
        'blog'                => $blog,
        'about'               => $about,
        'state'               => $state,
        'map_properties'      => $map_properties,
    ]);
  }

  public function actionSetCaptcha(){
    if(Yii::$app->request->isAjax) ;
    $str = Yii::$app->request->post('data');
    Yii::$app->session->setFlash('captcha', $str);
  }

}
