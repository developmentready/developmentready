<?php
/**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 7/22/15
 * Time: 07:39
 */

namespace frontend\controllers;

use common\models\AgencyAgent;
use Yii;

use common\models\ContactForm;
use common\models\Property;
use frontend\models\search\PropertySearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\data\Pagination;
use common\models\Suburb;
use common\models\State;
use common\models\Page;

class PropertyController extends Controller{

  /**
   * @return string
   * @throws NotFoundHttpException
   */
  public function actionIndex(){
    $model = Page::findOne(4);

    if (!$model) {
      throw new NotFoundHttpException(\Yii::t('frontend', 'Page not found'));
    }

    $search_model = new PropertySearch();
    $query = $search_model->search(Yii::$app->request->queryParams);

    $pages = new Pagination(['pageSize' => 12, 'totalCount' => $query->getTotalCount()]);

    $message_model = new ContactForm();
    $message_model->scenario = 'send_message';

    $state = State::find()->where('`default` = 1')->active()->one();
    if(!$state) $state = State::findOne(7);

    $map_properties = Property::find()->joinWith('suburbs')
      ->where("state_id = :id", ['id' => $state->id])->activeProperty()->limit(50)->all();

    return $this->render('index', [
      'model' => $model,
      'pages' => $pages,
      'featured_properties' => Property::getStateRelatedFeaturedProperties(),
      'state'     => $state,
      'map_properties' => $map_properties,
      'message_model' => $message_model,
      'properties' => $query->getModels(),
      'grouped_properties' => Property::getActivePropertiesSuburbs(),
      'search_model' => $search_model,
    ]);
  }


  /**
   * @return string
   * @throws NotFoundHttpException
   */
  public function actionSold(){
    $model = Page::findOne(25);
    $search_model = new PropertySearch();

    if (!$model) {
      throw new NotFoundHttpException(Yii::t('frontend', 'Page not found'));
    }

    $query = $search_model->searchSoldProperties(Yii::$app->request->queryParams);

    $pages = new Pagination(['pageSize' => 12, 'totalCount' => $query->getTotalCount()]);

    $message_model = new ContactForm();
    $message_model->scenario = 'send_message';

    $properties = $query->getModels();

    $state = State::find()->where('`default` = 1')->active()->one();
    if(!$state) $state = State::findOne(7);

    $map_properties = Property::find()->joinWith('suburbs')
      ->where("{{%properties}}.status = 1 AND state_id = :id AND sold = :status", ['id' => $state->id, 'status' => 1])->limit(50)->all();

    return $this->render('sold', [
      'model' => $model,
      'grouped_properties' => Property::getSoldPropertiesSuburbs(),
      'search_model' => $search_model,
      'pages' => $pages,
      'featured_properties' => Property::getStateRelatedFeaturedProperties(),
      'properties' => $properties,
      'state'     => $state,
      'map_properties' => $map_properties,
      'message_model' => $message_model,
    ]);
  }

  /**
   * View separate property in new page based on provided id
   * @param $slug
   * @return string
   * @throws NotFoundHttpException
   */
  public function actionView($slug){
    $model = Property::find()->where(['slug'=>$slug])->active()->one();

    $message_model = new ContactForm();
    $message_model->scenario = 'send_message';

    if (!$model) {
      throw new NotFoundHttpException(Yii::t('frontend', 'Page not found'));
    }

    if(!Yii::$app->user->isGuest) {
      if(count($model->favorited) > 0) $favorite = true;
      else $favorite = false;
    } else {
      $favorite = false;
    }

    $model->updateCounters(['view_count' => 1]);
    $model->updatePropertyReports($model->id, 'view_count');
    $model->updateStatesReports($model->id, 'state_count');

    return $this->render('view', [
      'model'=>$model,
      'featured_properties' => Property::getStateRelatedFeaturedProperties(),
      'message_model' => $message_model,
      'favorite' => $favorite,
    ]);
  }


  /**
   * @param $state
   * @return string
   * @throws NotFoundHttpException
   */
  public function actionState($state){
    $model = Page::findOne(4);
    $state = strtoupper($state);
    $state_model = State::find()->where(['title' => $state])->one();

    if (!$model || !$state_model) {
      throw new NotFoundHttpException(Yii::t('frontend', 'Page not found'));
    }

    $search_model = new PropertySearch();
    $query = $search_model->searchStatePropertiesById(Yii::$app->request->queryParams, $state_model->id);

    $pages = new Pagination(['pageSize' => 12, 'totalCount' => $query->getTotalCount()]);

    $message_model = new ContactForm();
    $message_model->scenario = 'send_message';

    return $this->render('state', [
      'model' => $model,
      'pages' => $pages,
      'featured_properties' => Property::getStateRelatedFeaturedProperties(),
      'state'     => $state_model,
      'message_model' => $message_model,
      'properties' => $query->getModels(),
      'search_model' => $search_model,
      'grouped_properties' => Property::getActivePropertiesStateSuburbs($state_model->id),
    ]);
  }

  /**
   * @return string
   * @throws NotFoundHttpException
   */
  public function actionSearch(){
    $model = Page::findOne(4);
    $search_model = new PropertySearch();

    if (!$model) {
      throw new NotFoundHttpException(Yii::t('frontend', 'Page not found'));
    }
    
    $query = $search_model->search(Yii::$app->request->queryParams);

    $pages = new Pagination(['pageSize' => 12, 'totalCount' => $query->getTotalCount()]);

    $properties = $query->getModels();

    $message_model = new ContactForm();
    $message_model->scenario = 'send_message';

    //+dorian
    $cookies = Yii::$app->request->cookies;
    if($cookies->getValue('userState') !== null){
      $state = State::findOne($cookies->getValue('userState'));
    }else{
      $state = State::find()->where('`default` = 1')->active()->one();
      if(!$state) $state = State::findOne(7);
    }

    /*$state = State::find()->where('`default` = 1')->active()->one();
    if(!$state) $state = State::findOne(7);*/
    //-dorian

    if(Yii::$app->request->queryParams)
      $map_properties = $properties;
    else
      $map_properties = Property::find()->joinWith('suburbs')
        ->where("state_id = :id", ['id' => $state->id])->activeProperty()->limit(50)->all();


    return $this->render('index-wo-filter', [
      'model'       => $model,
      'featured_properties' => Property::getStateRelatedFeaturedProperties(),
      'properties'  => $properties,
      'pages'       => $pages,
      'state'       => $state,
      'map_properties' => $map_properties,
      'message_model' => $message_model,
    ]);
  }

  /**
   * Show all properties that customer add to his favorite lis
   * @return string|\yii\web\Response
   */
  public function actionMyFavorites(){
    if(!\Yii::$app->user->isGuest):
      $model = Page::findOne(4);

      $search_model = new PropertySearch();

      $message_model = new ContactForm();
      $message_model->scenario = 'send_message';

      $query = $search_model->searchFavorites();
      $pages = new Pagination(['pageSize' => 12, 'totalCount' => $query->getTotalCount()]);
      $properties = $query->getModels();

      $state = State::find()->where('`default` = 1')->active()->one();
      if(!$state) $state = State::findOne(7);

      $map_properties = Property::find()->joinWith('suburbs')
        ->where("state_id = :id", ['id' => $state->id])->activeProperty()->limit(50)->all();

      return $this->render('index-wo-filter', [
        'model'  => $model,
        'featured_properties' => Property::getStateRelatedFeaturedProperties(),
        'properties' => $properties,
        'pages' => $pages,
        'state'     => $state,
        'map_properties' => $map_properties,
        'message_model' => $message_model,
      ]);
    else:
      return $this->goHome();
    endif;
  }

  /**
   * @param $id
   * @return string
   */
  public function actionMapData($id){
    $state = State::findOne($id);
    $search_model = new PropertySearch();

    if(!$state)
      $state = \common\models\State::find()->where('`default` = 1')->active()->one();

    $properties = Property::find()->joinWith('suburbs')
      ->where("state_id = :id", ['id' => $id])->activeProperty()->limit(50)->all();

    return $this->renderPartial("//layouts/parts/_map", [
      'state' => $state,
      'properties' => $properties,
    ]);
  }

  /**
   * @return string|\yii\web\Response
   */
  public function actionSearchMapData(){
    $model = Page::findOne(4);
    if(!empty($param = \Yii::$app->request->get('PropertySearch')) && is_array($param)){
      if(array_key_exists('state', $param)){
        $state_id = $param['state'];
      } else {
        $state_id = 7;
      }
    }
    else {
      $state_id = 7;
    }

    $state = State::findOne($state_id);

    $search_model = new PropertySearch();

    $query = $search_model->searchInMap(Yii::$app->request->get('PropertySearch'));

    if (empty($query)) {
      return $this->redirect('/property/search');
    }

    $message_model = new ContactForm();
    $message_model->scenario = 'send_message';

    $pages = new Pagination(['pageSize' => 12, 'totalCount' => $query->getTotalCount()]);

    $properties =  $query->getModels();

    $map_properties = Property::find()->joinWith('suburbs')
      ->where("state_id = :id", ['id' => $state->id])->activeProperty()->limit(50)->all();

    return $this->render('index-wo-filter', [
      'model'  => $model,
      'map_properties' => $map_properties,
      'pages' => $pages,
      'featured_properties' => Property::getStateRelatedFeaturedProperties($state_id),
      'properties' => $properties,
      'state'     => $state,
      'message_model' => $message_model,
    ]);
  }


  /**
   * Congratulation page for user
   *
   * @param $id
   * @return string
   * @throws NotFoundHttpException
   */
  public function actionCongratulation($id){
    $model = Property::findOne($id);

    if (!$model) {
      throw new NotFoundHttpException(Yii::t('frontend', 'Page not found'));
    } else {
      return $this->render('congratulation', [
        'model' => $model,
      ]);
    }
  }

  /**
   * Return all aviable suburs list
   * @param null $q
   * @return array
   */
  public function actionSuburbList($q = null){
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    $out = ['results' => ['id' => '', 'title' => '', 'state' => '']];
    if (!is_null($q)) {
      $query = new \yii\db\Query;
      $query->select('{{%suburbs}}.id, {{%suburbs}}.title AS text, {{%states}}.title AS state, post_code ')
        ->from('{{%suburbs}}')
        ->join('INNER JOIN','{{%states}}', 'state_id = {{%states}}.id')
        //->where(['like', '{{%states}}.title', $q])
        ->where(['like', '{{%suburbs}}.title', $q])
        ->orWhere(['like', 'post_code', $q])
        ->limit(20);
      $command = $query->createCommand();
      $data = $command->queryAll();
      $out['results'] = array_values($data);
    }
    return $out;
  }


  /**
   * Return suburb list for special map location
   * @param integer $state
   * @param null $q
   * @return array
   */
  public function actionStateSuburbsList($q = null, $state = 7){
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    $out = ['results' => ['id' => '', 'title' => '', 'state' => '']];
    if (!is_null($q)) {
      $query = new \yii\db\Query;
      $query->select('{{%suburbs}}.id, {{%suburbs}}.title AS text, {{%states}}.title AS state, post_code ')
        ->from('{{%suburbs}}')
        ->join('RIGHT JOIN','{{%states}}', 'state_id = {{%states}}.id')
        ->where(['like', '{{%suburbs}}.title', $q])
        ->andWhere(['state_id' => $state])
        ->orWhere(['like', 'post_code', $q])
        ->limit(20);
      $command = $query->createCommand();
      $data = $command->queryAll();
      $out['results'] = array_values($data);
    }
    return $out;
  }

  /**
   *
   * @param null $id
   * @return array|bool
   */
  public function actionStateProperties($id){
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    if(is_int($id) && !is_null($id)) {
      $query = Property::find()->select('{{%properties}}.id as prop_id,
      suburb_id, state_id, {{%properties}}.lat, {{%properties}}.lng, {{%properties}}.description,
      {{%suburbs}}.title as suburb, {{%suburbs}}.post_code, {{%states}}.title as state')->joinWith('stateProperties')
        ->activeProperty()->andWhere("{{%states}}.id = {$id}")->limit(50);
    } else{
      return false;
    }

    $command = $query->createCommand();
    $data = $command->queryAll();

    return array_values($data);
  }


  /**
   * @param $id
   * @param null $m
   * @return string
   * @throws NotFoundHttpException
   */
  public function actionAgentInfo($id, $m = null){
    $id = (int)$id;
    $model_id = (int)$m;
    if(!empty($id) && !empty($model_id)):
      # calling static method updatePropertyStats() from Property Model
      $property = Property::findOne($model_id);

      Property::findOne($model_id)->updatePropertyStats($model_id, 'call_count');
      $property->updatePropertyReports($model_id, 'call_count');

      $agent = AgencyAgent::findOne($id);

      if (!$agent) {
        throw new NotFoundHttpException(Yii::t('frontend', 'Page not found'));
      } else {
        return "<a href='tel:$agent->mobile'><em>{$agent->mobile}</em></a>";
      }
    else:
      return Yii::t('common', 'No info');
    endif;
  }

  /**
   * @param $id
   * @return string
   * @throws NotFoundHttpException
   */
  public function actionSingleAgentInfo($id){
    $id = (int)$id;
    if(!empty($id)):

      $agent = AgencyAgent::findOne($id);

      if (!$agent) {
        throw new NotFoundHttpException(Yii::t('frontend', 'Page not found'));
      } else {
        return "<a href='tel:$agent->mobile'>{$agent->mobile}</a>";
      }
    else:
      return Yii::t('common', 'No info');
    endif;
  }

  /**
   * Add property to user's favorit's list
   * @param $id
   * @return string
   */
  public function actionFavorite($id){
    $id = (int)$id;
    if(!\Yii::$app->user->isGuest):
      $user_id = \Yii::$app->user->identity->id;

      $favorite = \common\models\Favorite::find()->where(['user_id' =>  $user_id, 'property_id' => $id])->one();

      if(!empty($favorite)){
        if($favorite->deleted == 0){ $favorite->deleted = 1; $favorite->save(false); $add = false;}
        else { $favorite->deleted = 0; $favorite->save(false); $add = true; }
        $favorite->save(false);
      } else {
        $model = new \common\models\Favorite();
        $model->property_id = $id; $model->user_id = $user_id;
        $model->save(); $add = true;
      }

      if($add){
        $output =  "<i class='fa fa-heart fa-left'></i>";
        $output .= "<span class='social-name'>".Yii::t('frontend', 'Remove')."</span>";
      } else {
        $output =  "<i class='fa fa-heart-o fa-left'></i>";
        $output .= "<span class='social-name'>".Yii::t('common', 'Add to Favorites')."</span>";
      }
      return $output;
    endif;

    return "You don't have access for this action";
  }


  /**
   * @param $id
   * @return \yii\web\Response
   */
  public function actionDeleteImage($id){
    $model = \common\models\PropertyImages::findOne($id);
    if(!empty($model)){
      $property_id = $model->property_id;
      $file = \Yii::getAlias('@storage')."/properties/".$property_id."/".$model->file_name;
      if(is_file($file)) unlink ($file);
      $model->delete();
    } else{

    }
    return $this->redirect(Url::to(['update', 'id' => $property_id]));
  }

  /**
   * @param $model_id
   * @param $id
   * @return string
   */
  public function actionAgentsList($model_id, $id){
    $output = '';
    if(!empty($id) && !is_null($id)){
      $users = \common\models\User::find()->joinWith('agency')->where("{{%user}}.id IN ({$id}) ")->all();
      foreach($users as $item){
        if(!empty($item)){
          $output .= "<p class='text-success'>".$item->agency->title." agents: </p>";
          if(!empty($agents = $item->agency->agencyAgent)){
            foreach($agents as $agent){
              $checker = '';
              if($model_id > 0){
                $is_user_checked = \common\models\PropertyAgents::find()
                  ->where(['property_id' => $model_id, 'agent_id' => $agent->id])->one();
                $checker = empty($is_user_checked) ? "" : "checked";
              }
              $output .= '<div class="col-sm-6"><label class="checkbox">';
              $output .= '<input type="checkbox" '.$checker.' name="Property[agents][]" value="'.$agent->id.'"> '.$agent->full_name.'';
              $output .= '</label></div>';
            }
          } else {
            $output .= "No agents";
          }
          $output .= "<hr class='col-xs-12' />";
        }
      }
      return $output;
    } else {
      return 'no agency';
    }
  }
}