<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/29/15
 * Time: 23:31
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ColorPickerAssests extends AssetBundle{

  public $sourcePath = '@frontend/assets/pready';

  public $css = [

  ];

  public $js = [

  ];

}
