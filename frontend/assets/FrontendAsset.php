<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class FrontendAsset extends AssetBundle{

    public $sourcePath = '@frontend/assets/pready';

    public $css = [
//      //'plugins/bootstrap/css/bootstrap.min.css',
//      'plugins/font-awesome/css/font-awesome.min.css',
//      'plugins/bootstrap-select/css/bootstrap-select.min.css',
//      //'plugins/select/select2.min.css',
//      'plugins/owl-carousel/assets/owl.carousel.css',
//      'plugins/pgw-slideshow/pgwslideshow.min.css',
      'production/css/app.min.css'
    ];

    public $js = [
      'production/js/app.min.js',
//      'plugins/bootstrap/js/bootstrap.min.js',
//      'plugins/bootstrap-select/js/bootstrap-select.min.js',
//      //'plugins/select/select2.full.min.js',
//      'plugins/owl-carousel/owl.carousel.min.js',
//      'plugins/pgw-slideshow/pgwslideshow.min.js',
//      'js/jq-smooth-scroll.min.js',
//      'js/script.js',
    ];

    public $depends = [
      //'yii\web\YiiAsset',
      //'yii\bootstrap\BootstrapAsset',
      'common\assets\Html5shiv',
    ];

    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
}
