// Create HTML5 elements for IE                     
document.createElement("article");
document.createElement("section");

// Wait for window load
$(window).load(function(){
  // Animate loader off screen
  $(".se-pre-con").fadeOut("slow");
});

$(document).ready(function(){ 
	justifyContent();
  calculateHeight();

  $('section[data-type="background"]').each(function(){
    var $bgobj = $(this); // assigning the object

    $(window).scroll(function(){
      var yPos = -($(window).scrollTop() / $bgobj.data('speed')); 
      // Put together our final background position
      var coords = '50% '+ yPos + 'px';
      // Move the background
      $bgobj.css({ backgroundPosition: coords });
    }); 
  });
});

$('.image-overlay').hover(function (){
  var parent_el = $(this).parent('.property-thumbnail-link');
  parent_el.find(".agency-name, .property-image-details").fadeOut('slow');
},function () {
  var parnt_el = $(this).parent('.property-thumbnail-link');
  parnt_el.find(".agency-name, .property-image-details").fadeIn("slow");
});

$('.blog-image-overlay').hover(function (){
  var parent_el = $(this).parent('.overflow-image-block');
  parent_el.find(".blog-image-title").fadeOut('slow');
},function () {
  var parnt_el = $(this).parent('.overflow-image-block');
  parnt_el.find(".blog-image-title").fadeIn("slow");
});


$( window ).resize(function(){
  fitThubmnailHeight();
  calculateHeight();
  if($("body").width() > 799 ){
  } else {
  }
});

function calculateHeight(){
  var parentElement = $('.height-3-2, .height-3-1').parent();
  var height_3_1 = parentElement.height() / 3;
  var height_3_2 = parentElement.height() / 3 * 2;
  parentElement.find('.height-3-2').height(height_3_2);
  parentElement.find('.height-3-1').height(height_3_1);
}


//$(function(){
//  $('a[href*=#]:not([href=#])').click(function() {
//    if(!$(this).hasClass("no-effect")){
//      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
//        var target = $(this.hash);
//        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
//        if (target.length) {
//          $('html, body').animate({
//            scrollTop: target.offset().top
//          }, 1000);
//          return false;
//        }
//      }
//    }
//  });
//});

function fitThubmnailHeight(){
  var thumbnail_height = jQuery('.property-thumbnail').height();
  jQuery('.property-thumbnail div.image-overlay').css('height', thumbnail_height);
}

function justifyContent(){
	// calculating margin-left to align it to center;
	var width = $('.justified').width();
	$('.justified').css('margin-left', '-' + (width / 2)+'px');
}
function equalHeights(element1, element2){
var height;
  if (element1.outerHeight() > element2.outerHeight()){
    height = element1.outerHeight();
    element2.css('height', height);
  } else {
    height = element2.outerHeight();
    element1.css('height', height);
  }
}