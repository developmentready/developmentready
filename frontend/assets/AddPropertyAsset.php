<?php
/**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 7/20/15
 * Time: 12:53
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AddPropertyAsset extends AssetBundle{
  #   public $basePath = '@webroot';
  #   public $baseUrl = '@web';
  public $sourcePath = '@frontend/assets/das';

  public $css = [
    'libraries/dropzone/dropzone.min.css',
  ];

  public $js = [
    'libraries/dropzone/dropzone.min.js',
  ];

}
