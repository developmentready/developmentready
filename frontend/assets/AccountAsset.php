<?php
/**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 7/28/15
 * Time: 12:04
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AccountAsset extends AssetBundle{

  public $sourcePath = '@frontend/assets/pready';

  public $css = [
    'plugins/font-awesome/css/font-awesome.min.css',
    'plugins/bootstrap-fileinput/css/fileinput.min.css',
//    'plugins/owl-carousel/assets/owl.carousel.css',
    'plugins/color-picker/color-picker.min.css',
    'css/account.min.css',
  ];

  public $js = [
    //'js/jquery.js',
    'plugins/jquery-transit/jquery.transit.min.js',
    'plugins/bootstrap/assets/javascripts/bootstrap/transition.min.js',
    'plugins/bootstrap/assets/javascripts/bootstrap/collapse.min.js',
    'plugins/bootstrap-fileinput/js/fileinput.min.js',
    'plugins/autosize/jquery.autosize.min.js',
    'plugins/isotope/dist/isotope.pkgd.min.js',
//    'plugins/owl-carousel/owl.carousel.min.js',
    'plugins/jquery.scrollTo/jquery.scrollTo.min.js',
    'plugins/color-picker/color-picker.min.js',
    'js/account.min.js',
//    'js/map.min.js',
  ];

  //public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
}
