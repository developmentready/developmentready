<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/26/15
 * Time: 23:48
 */
?>

Hello Dear Agency,

You have message from <a href="http://permitready.com.au">Permit Ready</a> web site.

<p><b>Subject:</b> <?= $model->subject ?></p>
<br/>
<p><b>Message:</b></p>
<p>
  <?= $model->message ?>
</p>


Contacts of sender:
Name: <?= $model->name ?><br/>
Email: <?= $model->email ?><br/>
Phone: <?= $model->phone_number ?><br/>
