<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/26/15
 * Time: 23:49
 */

$link = \Yii::getAlias('@frontendUrl')."/properties/".$model->slug;

?>

<div style="background: #020202; height: 125px; margin-bottom: 40px">
  <a href="http://permitready.com.au/" style="width: 180px; display: block; margin: 0 auto; position: relative; padding-top: 10px">
    <img src="http://permitready.com.au/logo.png">
  </a>
</div>

<p>You have a message from "Permit Ready" <a href="http://permitready.com.au">http://permitready.com.au</a>.</p>

<p>Link to the property:
<a href="<?= $link ?>"><?= $link ?></a>
</p>

<hr/>
<p><em>Sender details:</em></p>
Name: <?= $model->name ?><br/>
Email: <?= $model->email ?><br/>
Phone: <?= $model->phone_number ?><br/>

<p><b>Subject:</b> <?= $model->subject ?></p>
<p><b>Message:</b></p>
<p>
  <?= $model->message ?>
</p>

<hr/>
<p>Best Regards,</p>
<p>Permit Ready <a href="http://permitready.com.au">http://permitready.com.au</a></p>