<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/23/15
 * Time: 11:17
 */

use common\components\helpers\Setup;
use yii\helpers\Url;

$this->title = $model->title;

$this->registerMetaTag(['name' => 'keywords', 'content' => $model->meta_keywords]);
$this->registerMetaTag(['name' => 'description', 'content' => $model->meta_description]);
$this->registerMetaTag(['name' => 'robots', 'content' => 'noindex,follow']);

$storage = \Yii::getAlias('@storageUrl');

?>

<!-- ALL BLOG LIST SECTION
---------------------------------------------------------------------->
<section id="page-blog-section">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-left"><?= $model->h1_tag ?></h1>
      </div>
    </div>
    <div class="row">
      <?php foreach($blog as $item): ?>
        <div class="col-sm-4">
          <div class="blog-page-review-block">
            <a class="thumbnail" href="<?= Url::to(["/{$item->url_name}"]) ?>">
              <div class="overflow-image-block">
                <?php if($media = $item->image): ?>
                  <img class="overflow-image" src="<?= $storage.$media->media_path ?>" />
                <?php endif; ?>
                <div class="blog-image-title">
                  <h5 class="text-center"><?= $item->h1_tag ?></h5>
                </div>
                <div class="image-overlay blog-image-overlay">
                  <div class="col-sm-12 blog-overlay-top">
                    <div class="pull-left"><i class="fa fa-calendar"></i> <span> <?= date('d F Y', $item->publication_date) ?> </span></div>
                    <!--<div class="pull-right text-right"><i class="fa fa-eye"></i> <span> <?= $item->view_counter ?> view</span></div>-->
                  </div>
                  <div class="col-sm-12">
                    <h5 class="text-center"><?= $item->h1_tag ?></h5>
                  </div>
                </div>
              </div>
            </a>
            <p class="blog-review-text">
              <?= strip_tags(Setup::truncate($item->body,150, "...")) ?>
            </p>
          </div>
        </div>
      <?php endforeach; ?>
    </div>
  </div><!--/.container-->
</section><!--section ends-->

<!-- PAGINATION SECTION
---------------------------------------------------------------------->

<?= $this->render("//layouts/parts/_pagination", ['pages' => $pages]) ?>
