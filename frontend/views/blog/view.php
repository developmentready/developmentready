<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/23/15
 * Time: 11:17
 */

use yii\widgets\LinkPager;
use common\components\helpers\Setup;
use yii\helpers\Url;
use yii\helpers\Html;

$this->title = $model->title;

$this->registerMetaTag(['name' => 'keywords', 'content' => $model->meta_keywords]);
$this->registerMetaTag(['name' => 'description', 'content' => $model->meta_description]);

$storage = \Yii::getAlias('@storageUrl');

$featured_image = !empty($media = $model->image) ? $storage.$media->media_path : false ;

$this->registerMetaTag(['property' => 'og:url', 'content' => Yii::$app->request->absoluteUrl]);
$this->registerMetaTag(['property' => 'og:type', 'content' => 'website' ]);
$this->registerMetaTag(['property' => 'og:title', 'content' => $this->title ]);
$this->registerMetaTag(['property' => 'og:description', 'content' => $model->meta_description ]);
$this->registerMetaTag(['property' => 'og:image', 'content' => $featured_image ]);



?>

<!-- SINGLE BLOG SECTION
---------------------------------------------------------------------->
<section id="single-blog-section">
  <div class="container">
    <div class="row">
      <div class="col-sm-9">
        <div class="row">
          <div class="col-sm-10 col-sm-offset-1">
            <h1 class="text-center page-header"><?= $model->h1_tag ?></h1>
            <h5 class="text-center">Posted by Permit Ready on <?= date('M d, Y', $model->created_at) ?></h5>
          </div><!--/.col-->
        </div><!--/.row-->
        <div class="row">
          <?= $this->render('//layouts/parts/_share_links', ['url' => Yii::$app->request->absoluteUrl]); ?>
        </div>
        <div class="row">
          <div class="col-sm-7">
            <?php if($featured_image): ?>
              <div class="overflow-image-block">
                <img class="overflow-image" src="<?= $featured_image ?>"/>
              </div>
            <?php endif; ?>
          </div><!--/.col-->
          <div class="blog-content-text">
            <?= $model->body ?>
          </div>
          <hr class="col-xs-12" />
          <?php if(!empty($quot = $model->additional_text)): ?>
          <div class="col-sm-12">
            <div class="blog-quote">
              <p class="text-center"><?= $quot ?></p>
            </div>
          </div>
          <?php endif; ?>
          <div class="col-sm-12">
            <p class="source-file">
              Source: <br/>
              <?php $source = "http://".$model->video_link; ?>
              <?= Html::a($source, $source) ?>
            </p>
          </div>
        </div><!--/.row-->
      </div>
      <aside class="col-sm-3">
        <?php if(!empty($related_blog)): ?>
          <?php foreach($related_blog as $blog): ?>
            <?php $url = Url::to(["/{$blog->url_name}"]); ?>
            <div class="blog-widget-block">
              <div class="blog-widget-header text-center">
                <h4><a href="<?= $url ?>"><?= Setup::truncate($blog->h1_tag, 100, "...") ?></a></h4>
              </div>
              <div class="blog-widget-image">
                <a href="<?= $url ?>">
                  <?php $image = !empty($blog_media = $blog->image) ? $blog_media->media_path : '/properties/no-image.png' ?>
                  <div class="overflow-image-block">
                    <img src="<?= $storage.$image ?>"/>
                  </div>
                </a>
              </div>
            </div><!-- /.blog-widget-block -->
          <?php endforeach; ?>
        <?php endif; ?>
      </aside>
    </div>
  </div><!--/.container-->
</section><!--section ends-->
