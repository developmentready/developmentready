<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/29/15
 * Time: 22:45
 */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\AgencyAgent */
/* @var $form yii\bootstrap\ActiveForm */
?>
<?= Html::cssFile('@frontendUrl/assets/production/css/upload.min.css') ?>
<div class="agency-agent-form">

  <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>

  <?= Html::activeHiddenInput($model, 'agency_id', ['value' => Yii::$app->user->identity->agency->id]) ?>

  <?php echo $form->errorSummary($model); ?>

  <div class="col-sm-12">
    <?= $this->render("//layouts/account/_upload_images", ['form' => $form, 'model' => $model, 'max_image_count' => 1]) ?>
  </div>
  <?php if(!$model->isNewRecord && !empty($image_model = $model->image)):

    $media_url = \Yii::getAlias('@storageUrl').$image_model->media_path;
    ?>
    <div class="col-sm-12">
      <div class="thumbnail">
        <img src="<?= $media_url ?>" />
        <a class="btn btn-danger full-width" href="<?= Url::to(['single-image-delete', 'id' => $image_model->id]) ?>">Delete Image</a>
      </div>
    </div>
  <?php endif; ?>

  <div class="row">
    <div class="col-sm-6 col-md-4">
      <?php echo $form->field($model, 'full_name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-sm-6 col-md-4">
      <?php echo $form->field($model, 'mobile')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-sm-6 col-md-4">
      <?php echo $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-6">
      <?php echo $form->field($model, 'position_id')
        ->dropDownList(ArrayHelper::map($positions, 'id', 'title'),['prompt' => 'Select Agent Position']) ?>
    </div>
    <div class="col-sm-6">
      <?php echo $form->field($model, 'about')->textarea(['rows' => 4]) ?>
    </div>
  </div>

  <div class="form-group">
    <div class="row">
      <div class="col-xs-4 col-lg-2">
        <?php echo $form->field($model, 'status')->checkbox() ?>
      </div>
      <div class="col-xs-8 col-lg-10">
        <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
      </div>
    </div>
  </div>

  <?php ActiveForm::end(); ?>

</div>
<?= Html::jsFile('@frontendUrl/assets/production/js/upload.min.js') ?>
<?= Html::jsFile('@frontendUrl/assets/production/js/jquery-ui.min.js') ?>