<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/30/15
 * Time: 00:28
 */


use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\AgencyAgentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Agency Agents');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="admin-content-main-inner">
  <div class="container-fluid">

    <p>
      <?php echo Html::a(Yii::t('app', 'Add Agent'), ['new-agent'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo GridView::widget([
      'dataProvider' => $dataProvider,
      'filterModel' => $searchModel,
      'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
          'attribute'=>'status',
          'format'   => 'html',
          'value'    => function($data){ return $data->status ? '<i class="fa fa-check-square" style="color:#5CB85C"></i>' : '<i class="fa fa-close" style="color: #A94442"></i>';},
          'filter'=> ['0' => 'Deactive' , '1' => 'Active'],
        ],
        'full_name',
        [
          'attribute'=>'position_id',
          'value'  => function($data){ return !empty($position = $data->position) ? $position->title : 'No position selected';},
          'filter'=> \yii\helpers\ArrayHelper::map(\common\models\AgencyPosition::find()->active()->all(), 'id' , 'title'),
        ],
        'mobile',
        'email:email',
        'updated_at:date',

        [
          'class' => 'yii\grid\ActionColumn',
          'template' => '{update-agent}{delete-agent}',
          'buttons' => [
            'update-agent' => function ($url, $model) {
              return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                'title' => Yii::t('app', 'Update Agent Info'),
              ]);
            },
            'delete-agent' => function ($url, $model) {
              return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                'title' => Yii::t('app', 'Delete Agent'),
              ]);
            },
          ],
          'urlCreator' => function ($action, $model, $key, $index) {
            if ($action === 'update-agent') {
              $url ='/account/update-agent?id='.$model->id;
              return $url;
            }
            if ($action === 'delete-agent') {
              $url ='/account/delete-agent?id='.$model->id;
              return $url;
            }
          }
        ],
      ],
    ]); ?>
  </div>
</div>