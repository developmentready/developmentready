<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/29/15
 * Time: 22:45
 */
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AgencyAgent */

$this->title = Yii::t('app', 'Update ') . ' ' . $model->full_name;
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="admin-content-main-inner">
  <div class="container-fluid">

    <?php echo $this->render('_form', [
      'model' => $model,
      'agencies' => $agencies,
      'positions' => $positions
    ]) ?>

  </div>
</div>