<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/29/15
 * Time: 22:44
 */

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AgencyAgent */

$this->title = Yii::t('app', 'New Agent');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'All Agents'), 'url' => ['agents']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-content-main-inner">
  <div class="container-fluid">

    <?php echo $this->render('_form', [
      'model' => $model,
      'agencies' => $agencies,
      'positions' => $positions
    ]) ?>

  </div>
</div>