<?php
/**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 8/23/15
 * Time: 14:46
 */

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \frontend\modules\user\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title =  Yii::t('frontend', 'Request password reset');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="admin-content-main-inner">
  <div class="container-fluid">
    <div class="admin-content">
      <div class="admin-content-image-text">
        <h1><?php echo Html::encode($this->title) ?></h1>
      </div><!-- /.admin-content-image-text -->

      <div class="admin-content-image-call-to-action">
        <i class="fa fa-long-arrow-left"></i>
      </div><!-- /.admin-content-image-call-to-action -->

    </div><!-- /.admin-content -->

    <div class="admin-sidebar-secondary">
      <div class="admin-sidebar-secondary-inner">
        <div class="admin-sidebar-secondary-inner-top">
          <div class="site-request-password-reset">
            <h1><?php echo Html::encode($this->title) ?></h1>
            <div class="row">
              <div class="col-lg-12">
                <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
                <?php echo $form->field($model, 'email') ?>
                <div class="form-group">
                  <?php echo Html::submitButton(Yii::t('frontend','Send'), ['class' => 'btn brand-green btn-primary']) ?>
                </div>
                <?php ActiveForm::end(); ?>
              </div>
            </div>
          </div>
        </div><!-- /.admin-sidebar-secondary-inner-top -->

        <div class="admin-sidebar-secondary-inner-bottom">
          <div class="admin-sidebar-footer">
            <div class="admin-sidebar-info">
            </div><!-- /.admin-landing -->
          </div><!-- /.admin-landing-content-footer -->
        </div><!-- /.admin-sidebar-secondary-inner-bottom -->
      </div><!-- /.admin-sidebar-secondary-inner -->
    </div><!-- /.admin-sidebar-secondary -->
  </div>
</div>