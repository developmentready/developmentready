<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/29/15
 * Time: 22:35
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Agency */

$this->title = 'Update Agency: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="admin-content-main-inner">
  <div class="container-fluid">

  <?php echo $this->render('_form', [
    'model' => $model,
  ]) ?>
  </div>
</div>