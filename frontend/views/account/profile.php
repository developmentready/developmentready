<?php
/**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 8/23/15
 * Time: 14:23
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

$this->title = Yii::t('common', 'Profile information');

?>
<div class="admin-content-main-inner">
  <div class="container-fluid">
    <h1 class="page-header"><?= $this->title ?></h1>

    <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

    <div class="row">
      <div class="col-sm-12 col-md-6">
        <p><strong><?= Yii::t('common','Hello') ?>, <?= $model->name ?></strong> (<?= $model->email ?>)</p>
        <p><strong><?= Yii::t('common','You have been registered as') ?></strong> : <?= $model->user_type == 1 ? Yii::t('common',  'Agent') : Yii::t('common', 'Personal') ?></p>
        <hr/>
<!--        <a href="--><?php # echo Url::to(['/user/sign-in/reset-password']) ?><!--" class="btn"> --><?php # echo Yii::t('common', 'Change Password') ?><!--</a>-->
      </div>
      <div class="col-sm-12 col-md-6">
      </div>
    </div>
    <?php ActiveForm::end(); ?>
  </div>
</div>