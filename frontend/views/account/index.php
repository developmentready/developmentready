<?php /**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 7/30/15
 * Time: 10:10
 */

use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
#use yii\grid\GridView;

$this->title = Yii::t('common','My Properties');

?>
<div class="admin-content-main-inner">
  <div class="container-fluid">
    <h1 class="page-header"><?= $this->title ?></h1>

    <?php if(Yii::$app->user->identity->status !== 1): ?>
      <p class="bg-danger"> As you are not approved by admin, some features including creating new property not accessible.</p>
    <?php else: ?>
    <p class="bg-primary">
      <a target="_blank" href="<?= \yii\helpers\Url::to(['/account/new-property']) ?>" class="btn btn-xs brand-green">  <?= Yii::t('common', 'Add New Property') ?> </a>
    </p>

    <?php endif; ?>

    <?php
      $gridColumns=[
        'id',
        [
          'attribute'=>'status',
          'format' => 'html',
          'value' => function($data){ return $data->status == 1 ? '<i class="fa fa-check-square" style="color:#5CB85C"></i>' : '<i class="fa fa-close" style="color: #A94442"></i>';},
          'filter'=> ['0' => Yii::t('common','Deactivated') , '1' => Yii::t('common','Active'), '3' => Yii::t('common','Expired')],
        ],
        [
          'label' => 'Address',
          'format' => 'raw',
          'value' => function($data){
            return $data->location;
          }
        ],
        [
          'label'   => 'Property Image',
          'format'  => 'html',
          'value'   => function($data){

            if(!empty($file = $data->defaultImage)) $image = $file->file_name;
            elseif(!empty($file = $data->image)) $image = $file->file_name;
            $id = $data->id ;
            $out = empty($image) ? \Yii::getAlias('@storageUrl')."/properties/no-image.png" : \Yii::getAlias('@storageUrl')."/properties/".$id."/".$image ;
            return Html::img($out, ['width' => '70px']);

          },
        ],
        [
          'label' => Yii::t('common','Contract type'),
          'attribute' => 'contract_type_id',
          'value' => function($data){ return $data->permitStatus()[$data->contract_type_id];},
          'filter'=> function($data){ return ArrayHelper::map($data->permitStatus(), 'id', 'translation.title');},
        ],
        [
          'attribute'=> 'sold',
          'value'    => function($data){return $data->soldStatus()[$data->sold];},
          'filter'   => ['0' => 'No', '1' => 'Yes'],
        ],
        'updated_at:date',
        [
          'class' => 'yii\grid\ActionColumn',
          'template' => '{update-property}{view-in-site}{delete-property}{view-stats}',
          'buttons' => [
            'update-property' => function ($data){
              return Html::a('<i class="fa fa-pencil"></i> ', $data, [], [
                'title' => Yii::t('common', 'Update'),
              ]);
            },
            'delete-property' => function ($data){
              return Html::a(' <i class="fa fa-trash"></i> ', $data, [
                'title' => Yii::t('common', 'Delete'), 'data-confirm' => Yii::t('yii', 'Are you sure you want to delete selected item?'),
              ]);
            },
            'view-in-site' => function ($data){
              return Html::a(' <i class="fa fa-eye"></i> ', $data, [
                'title' => Yii::t('common', 'View Property In Site'), 'target' => '_blank',
              ]);
            },
            'view-stats' => function ($data){
              return Html::a(' <i class="glyphicon glyphicon-stats"></i> ', $data, [
                  'title' => Yii::t('common', 'View Stats'),
              ]);
            },
          ],
          'urlCreator' => function ($action, $model, $key, $index) {
            if ($action === 'view-in-site') {
              $url ='/properties/'.$model->slug;
              return $url;
            }
            if ($action === 'update-property') {
              $url = Url::to(['update-property', 'id' => $model->id]);
              return $url;
            }
            if ($action === 'delete-property') {
              $url = Url::to(['delete-property', 'id' => $model->id]);
              return $url;
            }
            if ($action === 'view-stats') {
              $url = Url::to(['report/view', 'id' => $model->id]);
              return $url;
            }
          }
        ]
      ];
    ?>

    <?=
       GridView::widget([
        'dataProvider'=> $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => $gridColumns,
        'filterRowOptions'=>['class'=>'kartik-sheet-style'],
        'pjax'=>true,
        'pjaxSettings'=>[
          'neverTimeout'=>true,
          'beforeGrid'=>Yii::t('common', 'You can sort properties where header is link.'),
          #'afterGrid'=>'My fancy content after.',
        ],
        'responsive'=>true,
        'hover'=>true,
        // set your toolbar
        'toolbar'=> [
             ['content'=>
               Html::button('<i class="glyphicon glyphicon-plus"></i>', ['type'=>'button', 'title'=>Yii::t('kvgrid', 'Add Book'), 'class'=>'btn btn-success', 'onclick'=>'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' '.
               Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['grid-demo'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=>Yii::t('kvgrid', 'Reset Grid')])
             ],
             '{export}',
             '{toggleData}',
        ],
        // set export properties
        'export'=> false,
      ]);
    ?>

    <div class="center">

    </div><!-- /.center -->
  </div>
</div>