<?php
/**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 8/22/15
 * Time: 18:43
 */

$this->title = Yii::t('common','Edit Property with ID').": ".$model->id;

?>

<div class="admin-content-main-inner">
  <div class="container-fluid">

    <h1 class="page-header"><?= $this->title ?></h1>

    <?php if($model->sold == 1): ?>
      <p class="bg-danger">
        <?= Yii::t('common', 'Your advertisement marked as sold.') ?>
      </p>
    <?php endif; ?>

    <?php echo $this->render('_form', ['model' => $model,]) ?>

  </div>
</div>