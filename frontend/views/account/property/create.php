<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/27/15
 * Time: 22:10
 */

$this->title = 'Add new property';

?>

<div class="admin-content-main-inner">
  <div class="container-fluid">

    <h1 class="page-header"><?= $this->title ?></h1>

    <?php if($model->sold == 1): ?>
      <p class="bg-danger">
        <?= Yii::t('common', 'Your advertisement is not visible for common users, as it was marked as sold.') ?>
      </p>
    <?php endif; ?>

    <?php echo $this->render('_form', ['model' => $model,]) ?>

  </div>
</div>
