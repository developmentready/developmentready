<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/27/15
 * Time: 20:49
 */


use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\web\JsExpression;


/* @var $this yii\web\View */
/* @var $model common\models\Property */
/* @var $form yii\bootstrap\ActiveForm */

$model_id = $model->isNewRecord ? 0 : $model->id ;
$keys = [];
$partner_id = '';

$user_id = \Yii::$app->user->identity->id;

if(\Yii::$app->user->can('agencies'))
  $is_agency = true;
else
  $is_agency = false;

if(!$model->isNewRecord){
  $agencies = \common\models\PropertyUser::find()->select('user_id')->where(['property_id' => $model->id])->all();
  if($agencies){
    foreach ($agencies as $user) {
      if($user->user_id != $user_id){
        array_push($keys, $user->user_id);
        $partner_id = $user->user_id;
      }
    }
  }
} else {
  $model->status = true;
}


?>

<?= Html::cssFile('@frontendUrl/assets/production/css/editor.min.css') ?>
<?= Html::cssFile('@frontendUrl/assets/production/css/upload.min.css') ?>

  <div class="property-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
      <?php if(!$model->isNewRecord): ?>
        <div class="col-sm-2">
          <?php echo $form->field($model, 'sold')->checkbox() ?>
        </div>
      <?php endif; ?>
      <div class="col-sm-2">
        <?php echo $form->field($model, 'under_offer')->checkbox() ?>
      </div>
      <div class="col-sm-5">
        <?php echo $form->field($model, 'contract_type_id')->inline(true)->radioList(
            $model->permitStatus()) ?>
      </div>
      <div id="permit-details"  class="col-sm-5">
        <?php echo $form->field($model, 'contract_details')->textInput(['maxlength' => true]) ?>
      </div>
      <!--    <div class="col-sm-5">-->
      <!--      --><?php //echo $form->field($model, 'ext_id')->textInput(['maxlength' => true]) ?>
      <!--    </div>-->
    </div>
    <hr class="col-xs-12" />
    <!-- Default Image -->
    <div class="row">
      <div class="col-xs-7">
        <?= $this->render("//layouts/account/_upload_default_image", ['form' => $form, 'model' => $model]) ?>
      </div>

      <div class="col-xs-5">
        <?php if($default_image = $model->defaultImage): ?>
          <div class="thumbnail">
            <img src="<?= \Yii::getAlias('@storageUrl')."/properties/{$model->id}/".$default_image->file_name ?>" />
          </div>
        <?php endif; ?>
      </div>
    </div>

    <hr class="col-xs-12" />

    <div class="row">
      <div class="col-sm-9">
        <?php echo $form->field($model, 'propertyTypes')->inline(true)
            ->checkboxList(ArrayHelper::map(\common\models\PropertyType::find()->active()->all(),
                'id', 'title')) ?>
      </div>
    </div>

    <hr class="col-xs-12" />

    <input type="hidden" name="Property[agencies][]" value="<?= $user_id ?>">
    <?php if($is_agency): ?>
      <div class="row">
        <div class="col-xs-12">
          <p class="text-success"><strong>You can add one additional partner with several agents</strong></p>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-6">
          <?=
          Select2::widget([
              'id' => 'property-agencies',
              'name' => 'Property[agencies][]',
              'data' => ArrayHelper::map(\common\models\Agency::find()->active()->all(),
                  'user_id', 'title'),
              'value' => $keys,
              'options' => [
                  'placeholder' => 'Select agency ...', 'multiple' => true
              ],
              'pluginOptions' => [
                  'maximumSelectionLength' => 1,
              ],
              'pluginEvents' => [
                  "change" => "function(){
            $.ajax({
              data : 'id=' + $('#property-agencies').val(),
              url: '/property/agents-list?model_id=$model_id',
              success: function (data) {
                $('#agents_list').html(data);
              },
            })
          }"
              ],
          ]); ?>
        </div>

        <div class="col-xs-6">
          <div id="agents_list">
            <?php $output=''; ?>
            <?php
            if(!empty($partner_id) && !is_null($partner_id)){
              $users = \common\models\User::find()->joinWith('agency')->where("{{%user}}.id IN ({$partner_id}) ")->all();
              foreach($users as $item){
                if(!empty($item) && $item->user_type == 1){
                  $output .= "<p class='text-success'>".$item->agency->title." agents: </p>";
                  if(!empty($agents = $item->agency->agencyAgent)){
                    foreach($agents as $agent){
                      $checker = '';
                      if($model->id > 0){
                        $is_user_checked = \common\models\PropertyAgents::find()
                            ->where(['property_id' => $model_id, 'agent_id' => $agent->id])->one();
                        $checker = empty($is_user_checked) ? "" : "checked";
                      }
                      $output .= '<div class="col-sm-6"><label class="checkbox">';
                      $output .= '<input type="checkbox" '.$checker.' name="Property[agents][]" value="'.$agent->id.'"> '.$agent->full_name.'';
                      $output .= '</label></div>';
                    }
                    $output .= "<hr class='col-xs-12' />";
                  } else {
                    $output .= "No agents";
                  }
                }
              }
            }
            ?>
            <?= $output ?>
          </div>
        </div>
      </div>
      <hr class="col-xs-12" />
      <?php $output=''; ?>
      <?php
      $user = \common\models\User::findOne($user_id);
      if(!empty($user) && $user->user_type == 1){
        $output .= "<p class='text-success'>Our agents: </p>";
        if(!empty($agents = $user->agency->agencyAgent)){
          foreach($agents as $agent){
            $checker = '';
            if($model->id > 0){
              $is_user_checked = \common\models\PropertyAgents::find()
                  ->where(['property_id' => $model_id, 'agent_id' => $agent->id])->one();
              $checker = empty($is_user_checked) ? "" : "checked";
            }
            $output .= '<div class="col-sm-6"><label class="checkbox">';
            $output .= '<input type="checkbox" '.$checker.' name="Property[agents][]" value="'.$agent->id.'"> '.$agent->full_name.'';
            $output .= '</label></div>';
          }
          $output .= "<hr class='col-xs-12' />";
        } else {
          $output .= "No agents";
        }
      }
      ?>
      <?= $output ?>
      <hr class="col-xs-12" />
    <?php endif; ?>

    <div class="row">
      <div class="col-sm-3 col-xs-6">
        <?php echo $form->field($model, 'price_type_id')->dropDownList(
            ArrayHelper::map(\common\models\PriceType::find()->active()->all(), 'id', 'title'),
            ['prompt' => 'Please select']) ?>
      </div>
      <div class="input-wrapper col-sm-3 col-xs-6">
        <div id="price-in-number" >
          <label>Price in numbers: </label>
          <div class="input-group">
            <span class="input-group-addon">$</span>
            <?php echo $form->field($model, 'price_in_numbers',['template' => '{input}'])
                ->textInput() ?>
          </div>
        </div>
        <div id="eoi-description">
          <?= $form->field($model, 'eoi_description')->textInput() ?>
        </div>
      </div>
      <div id="" class="col-sm-6">
        <div class="row">
          <div class="col-xs-7">
            <?php echo $form->field($model, 'square')->textInput() ?>
            <?php echo $form->field($model, 'square_min')->textInput() ?>
          </div>
          <div class="col-xs-5">
            <?php echo $form->field($model, 'square_measure_id')
                ->dropDownList(ArrayHelper::map(\common\models\AreaType::find()->active()->all(),
                    'id', 'title'),['prompt' => 'Please select'])?>
          </div>
        </div>
      </div>
    </div>

    <hr class="col-xs-12" />

    <?= $this->render("//layouts/account/_text_editor", [ 'form' => $form, 'model' => $model, 'attribute' => 'description']) ?>

    <hr class="col-xs-12" />
    <div class="row">
      <div class="col-sm-12">
        <?= $this->render("//layouts/account/_upload_images", ['form' => $form, 'model' => $model, 'max_image_count' => 10]) ?>
      </div>
      <?php if(!empty($model->images)): ?>
        <div id="sortable">
          <?php $image_url = Yii::getAlias('@storageUrl')."/properties/{$model->id}" ?>
          <?php foreach($model->images as $item): ?>
            <div class="col-sm-3" data-id="<?=$item->id;?>">
              <div class="thumbnail">
                <img src="<?= $image_url."/".$item->file_name ?>"/>
                <div class="caption text-center">
                  <p><a href="<?= Url::to(['delete-property-image', 'id' => $item->id]) ?>" class="btn btn-danger">delete image</a></p>
                </div>
              </div>
            </div>
          <?php endforeach; ?>
        </div>
      <?php endif; ?>
    </div>

    <div class="row">
      <div class="col-sm-5">
        <?php $suburb = empty($suburb_model = \common\models\Suburb::findOne($model->suburb_id)) ? '' : $suburb_model->title."(".$suburb_model->post_code.")"; ?>
        <?=
        // Normal select with ActiveForm & model
        $form->field($model, 'suburb_id')->widget(Select2::classname(), [
            'initValueText' => $suburb,
            'options' => ['placeholder' => 'Select a suburb ...'],
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'ajax' => [
                    'url' => Url::to(['/property/suburb-list']),
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                ],
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function(suburb) { if(!suburb.post_code) suburb.post_code = "Post Code"; return suburb.text + " : " + suburb.post_code ; }'),
                'templateSelection' => new JsExpression('function (suburb) { if(!suburb.post_code) suburb.post_code = "or post code"; return suburb.text + " : " + suburb.post_code; }'),
            ],
        ]);
        ?>
      </div>
      <div class="col-sm-7">
        <?php echo $form->field($model, 'location')->textInput(['maxlength' => true]) ?>
      </div>
    </div>

    <hr class="col-xs-12"/>
    <?= $this->render("//layouts/account/_map_position", ['form' => $form, 'model' => $model]) ?>

    <hr class="col-xs-12"/>

    <div class="form-group">
      <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success brand-green' : 'btn btn-primary brand-green']) ?>
    </div>
    <div id="img_sort_mass"></div>
    <?php ActiveForm::end(); ?>
  </div>

<?= Html::jsFile('@frontendUrl/assets/production/js/editor.min.js') ?>
<?= Html::jsFile('@frontendUrl/assets/production/js/upload.min.js') ?>
<?= Html::jsFile('@frontendUrl/assets/production/js/jquery-ui.min.js') ?>
<script>
  $( function() {
    $( "#sortable" ).sortable({
      stop:function(){
        genereteImgSort();
      }
    });
    $( "#sortable" ).disableSelection();
  } );
  function genereteImgSort(){
    $('#img_sort_mass').html('');
    $('#sortable>div').each(function(index){
      var img_url = $(this).find('img').attr('src');
      var img_id = $(this).attr('data-id');
      $('#img_sort_mass').append('<input type="hidden" name="sort_img['+index+'][url]" value="'+img_url+'">');
      $('#img_sort_mass').append('<input type="hidden" name="sort_img['+index+'][id]" value="'+img_id+'">');
    });
  }
</script>
<style>
  body{
    overflow-x: inherit;
  }
</style>