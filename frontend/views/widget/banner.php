<?php
/**
 * Created by PhpStorm.
 * User: rashad@jameshammon.com.au
 * Date: 1/4/16
 * Time: 10:49
 */

$storage = \Yii::getAlias('@storageUrl');

$widget_width = 336;

$this->registerMetaTag(['name' => 'robots', 'content' => 'noindex,follow']);

?>
<script>

  var widget_width = <?= !empty($widget_width) ? 336 : 0 ?>

  /* Mini carousel script */
  function Carousel(settings){
    'use strict';
    settings = settings || {};
    this.carousel = document.querySelector(settings.carousel || '.carousel');
    this.slides = this.carousel.querySelectorAll('ul li');
    this.delay = settings.delay || 2.5;
    this.autoplay = settings.autoplay === undefined ? true : settings.autoplay;

    this.slides_total = this.slides.length;
    this.current_slide = -1;

    if (this.autoplay) {
      this.play();
    }
  }

  Carousel.prototype.next = function () {
    'use strict';
    for (var s = 0; s < this.slides.length; s += 1) {
      this.slides[s].style.display = 'none';
    }
    this.current_slide = (this.current_slide + 1) % this.slides.length;
    this.slides[this.current_slide].style.display = 'block';
  };

  Carousel.prototype.prev = function () {
    'use strict';
    for (var s = 0; s < this.slides.length; s += 1) {
      this.slides[s].style.display = 'none';
    }
    this.current_slide = Math.abs(this.current_slide - 1) % this.slides.length;
    this.slides[this.current_slide].style.display = 'block';
  };

  Carousel.prototype.play = function () {
    'use strict';
    this.next();
    var that = this;
    if (this.autoplay) {
      this.interval = setTimeout(function () {
        that.play();
      }, this.delay * 1000);
    }
  };

  Carousel.prototype.stop = function () {
    'use strict';
    if (this.interval) {
      clearInterval(this.interval);
    }
  };

</script>
<style>
  .carousel, .carousel img, .banner-overlay {height:280px; width:336px;margin: 0 auto;}
  /*#banner-overlay{position: relative; margin: 0 auto;height: 300px;display: inline-block; float: left}*/
  .banner-overlay{ background: rgba(0,0,0,0.8);color: #fff; height: 50px; width: 336px; position: absolute;bottom: 0;}
  .banner-right-overlay img{   width: 75px;height: 35px;padding-top: 5px;}
  .banner-left-overlay{width: 70%;float: left;}
  .banner-left-overlay p{font-size: 0.9em;color: #fff; font-family: Arial, Helvetica, sans-serif; letter-spacing: 0.1em; padding: 0 0 0 10px; margin: 5px 0;}
  .banner-right-overlay{width: 30%; display: inline-block;}
  .carousel ul {margin: 0;padding: 0;list-style-type: none;}
  .carousel ul li {display: none;text-align: center;}
  .property-state{position: absolute; top: 200px; color: #fff; z-index: 5;left: 10px;}
</style>

<div class="carousel">
  <ul>
    <?php foreach($featured_properties as $property): ?>
      <?php if(!empty($media = $property->defaultImage)): ?>
        <li>
          <a target="_blank" href="<?= Yii::getAlias('@frontendUrl') ?>/properties/<?= $property->slug ?>">
            <img src="<?= $storage."/properties/{$property->id}/$media->file_name" ?>"/>
          </a>
          <p class="property-state"><?= $property->suburb->title ?>, <?= $property->suburb->state->title ?></p>
        </li>
      <?php endif; ?>
    <?php endforeach; ?>
  </ul>
  <div id="banner-overlay">
    <div class="banner-overlay">
      <div class="banner-left-overlay">
        <p>Development properties</p>
      </div>
      <div class="banner-right-overlay">
        <img src="http://www.permitready.com.au/logo.png" alt="Permit Ready Australia logo"/>
      </div>
    </div>
  </div>
</div>
<script>
  new Carousel({carousel: '.carousel',delay: 2.5, autoplay: true});
</script>