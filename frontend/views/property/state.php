<?php
/**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 7/22/15
 * Time: 07:51
 *
 *
 * @var \common\models\State $state
 */


//$this->title =  $state->title." state properties";
switch($state->title){
  case 'QLD': $this->title = 'Find QLD development sites with DA approval | Permit Ready'; break;
  case 'NSW': $this->title = 'Best NSW development sites with DA approval | Permit Ready'; break;
  case 'VIC': $this->title = 'Find VIC development sites with permits | Permit Ready'; break;
  default: $this->title =  $state->title." state properties";
}

$this->registerMetaTag(['name' => 'keywords', 'content' => $model->meta_keywords]);
$this->registerMetaTag(['name' => 'description', 'content' => $model->meta_description]);

$is_front_page = false;

if( !empty(\Yii::$app->request->queryParams['page']) && ( (int)\Yii::$app->request->queryParams['page'] != 1 ) ) {
  $this->registerMetaTag(['name' => 'robots', 'content' => 'noindex,follow']);
} else {
  $is_front_page = true;
}


$user = (\Yii::$app->user->isGuest) ? false : \Yii::$app->user->identity->id;

$storage = \Yii::getAlias('@storageUrl');

$js = <<< 'SCRIPT'
  jQuery(document).ready(function(){
    $('#property-carousel').owlCarousel({
      loop:true, margin:10, nav:true, navText: ['',''], dots : false,
      responsive:{
        0:{items:1},
        600:{items:2},
        1000:{items:3}
      }
    });
  });
SCRIPT;
$this->registerJs($js);


?>
<!-- Page Header Section
---------------------------------------------------------------------->
<section id="property-details-head-section"  data-type="background" data-speed="10">
  <div id="property-details-head-cover"></div>
  <div id="property-details-head-container">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 text-center">
          <div id="header-search-box-container">
            <?= \frontend\widgets\SearchBar::widget(); ?>
          </div><!-- /#header-search-box-container -->
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container -->
  </div><!-- /#home-page-header-container -->
</section>

<!-- PROPERTY PAGE HEADER
---------------------------------------------------------------------->
<section id>
  <div class="container">
    <div class="row">
      <div class='col-sm-5'>
        <?php if(empty($title = $state->page_title)): ?>
          <h1 class="page-header pull-left"><span><?= $state->title ?> </span> Properties </h1>
        <?php else: ?>
          <h1 class="page-header pull-left"><?= $title ?> </h1>
        <?php endif; ?>
      </div>
      <div class="col-sm-7">
        <?= $this->render("//layouts/parts/_filter", ['properties' => $grouped_properties, 'search_model' => $search_model]) ?>
      </div>
    </div>
  </div>
</section>


<!-- SECTION NAME
---------------------------------------------------------------------->
<section id="property-list-section">
  <div class="container">
    <div class="row">
      <?= $this->render("//layouts/parts/_properties_line", ['properties' => $properties, 'storage' => $storage, 'message_model' => $message_model]) ?>
    </div><!--/.row-->
  </div><!--/.container-->
</section><!--section ends-->

<!-- PAGINATION SECTION
---------------------------------------------------------------------->
<?= $this->render("//layouts/parts/_pagination", ['pages' => $pages]) ?>


<?php if($is_front_page): ?>
  <!-- Separator
   ---------------------------------------------------------------------->
  <hr class="section-separator"/>
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <?php if(!empty($body = $state->page_body)): ?>
          <p><?= $body ?></p>
          <hr/>
        <?php else: ?>
          <p>
            Below are a list of <?= $state->title ?> Development sites for sale as well
            as properties which have been sold in the past. The land for sale has either plans
            and permits for development or development potential.
          </p>
          <hr/>
        <?php endif; ?>
      </div>
    </div>
  </div>
<?php endif; ?>

<!-- Separator
---------------------------------------------------------------------->
<hr class="section-separator"/>

<!-- FEATURED PROPERTIES
 ---------------------------------------------------------------------->
<section id="featured-properties">
  <div class="container">
    <div class="row">
      <h2 class="page-header text-left col-xs-12"><span>Featured</span> Development Sites</h2>
    </div><!--/.row-->
    <div id="property-carousel">
      <?= $this->render("//layouts/parts/_properties_carousel", ['properties' => $featured_properties, 'storage' => $storage]) ?>
    </div> <!--/#property-carousel-->
  </div><!--/.container-->
</section><!--section ends-->
