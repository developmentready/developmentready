<?php
/**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 8/28/15
 * Time: 15:53
 */

$this->title =  \Yii::t('common', 'Congratulations! Your ad is now will be listed in our system')." $model->id ";

use yii\helpers\Url;

$this->registerMetaTag(['name' => 'robots', 'content' => 'noindex,follow']);

$current_link = Yii::$app->homeUrl.Url::to(['/property/view', 'id' => $model->id]);

?>
<?php if($model->status == 1): ?>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : <?= Yii::getAlias('@fbKey') ?>,
      xfbml      : true,
      version    : 'v2.4'
    });
  };

  (function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
</script>
<?php endif; ?>
<div class="container">
  <div class="row">
    <div class="content col-sm-12">
      <h1 class="text-center"><?= \Yii::t('common', 'Congratulations! Your ad is now will be listed in our system') ?>,</h1>
      <h4 class="text-center"><?= \Yii::t('common', 'If you want your advertise will be reached more people, you can share your ad.') ?></h4>
      <?php if($model->status == 1): ?>
      <ul class="clearfix sharing-buttons text-center"><br/>
        <li>
          <a class="facebook" href="" onclick="javascript:FB.ui({ method: 'share', href: '<?= $current_link ?>',}, function(response){});return false;">
            <i class="fa fa-facebook fa-left"></i>
            <span class="social-name">Facebook</span>
          </a>
        </li>
        <li>
          &nbsp;
        </li>
        <li>
          <input id="share-link" type="text" class="share-link" value="<?= $current_link ?>"/>
          <a id="share-link-btn" class="btn btn-default"><?= Yii::t('common','Copy Link') ?></a>
        </li>
      </ul>
      <?php else: ?>
        <p class="text-center">
          <?= Yii::t('common', 'If your ad will be approved, your ad link will be <strong>"{link}"</strong>', ['link' => $current_link]) ?>
        </p>
      <?php endif; ?>

    </div>
  </div>
</div>
