<?php
/**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 7/22/15
 * Time: 18:29
 *
 * @var $model common\models\Property
 */

use yii\helpers\Url;
use common\components\helpers\Setup;

$this->title = strip_tags($model->location);


$this->registerMetaTag(['name' => 'keywords', 'content' => $model->meta_keywords]);
$this->registerMetaTag(['name' => 'description', 'content' => $model->meta_description]);
$this->registerMetaTag(['name' => 'dcterms.created', 'content' => date("c", $model->created_at)]);

# For socials
$storage = \Yii::getAlias('@storageUrl');

$featured_image = !empty($main_image = $model->defaultImage) ? $storage."/properties/{$model->id}/"
  .$main_image->file_name : $storage.'/properties/no-image.png';
$this->registerMetaTag(['property' => 'og:url', 'content' => Yii::$app->request->absoluteUrl]);
$this->registerMetaTag(['property' => 'og:type', 'content' => 'website' ]);
$this->registerMetaTag(['property' => 'og:title', 'content' => $this->title ]);
$this->registerMetaTag(['property' => 'og:description', 'content' => $model->meta_description ]);
$this->registerMetaTag(['property' => 'og:image', 'content' => $featured_image ]);

//$this->registerJsFile('https://www.google.com/recaptcha/api.js?hl=en', ['position' => \yii\web\View::POS_HEAD]);
//$this->registerMetaTag(['name' => 'expires', 'content' => date("r", $model->advertised_till)]);
if(!$model->page_robot_indexing)
  $this->registerMetaTag(['name' => 'robots', 'content' => 'noindex,follow']);
?>

<script>
  var property_lat = <?= empty($model->lat) ? -37.810102 : $model->lat ?>;
  var property_lng = <?= empty($model->lng) ? 144.964748 : $model->lng ?>;
  var property_name = '<?= $model->location ?>';
</script>

<?= $this->render("//layouts/parts/_message_to_agency", ['model' => $message_model, 'message_type' => 1, 'id' => $model->id]) ?>

<!-- Page Header Section
---------------------------------------------------------------------->
<section id="property-details-head-section"  data-type="background" data-speed="10">
  <div id="property-details-head-cover"></div>
  <div id="property-details-head-container">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 text-center">
          <div id="header-search-box-container">
            <?= \frontend\widgets\SearchBar::widget(); ?>
          </div><!-- /#header-search-box-container -->
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container -->
  </div><!-- /#home-page-header-container -->
</section>

<!-- SINGLE PROPERTY MAIN SECTION
---------------------------------------------------------------------->
<section id="single-property-section">
  <div class="container">
    <div class="row width-50">
      <h1 class="text-center page-header">
        <?= $this->title ?>
      </h1>
    </div><!--/.row-->
    <div class="row">
      <div class="col-sm-12 col-md-5">
        <a id="print-button" href="javascript:window.print()" class="btn btn-default">
          <span class="glyphicon glyphicon-print text-info"></span> Print
        </a>
      </div>
      <div class="col-sm-12 col-md-7">
        <?= $this->render('//layouts/parts/_share_links', ['url' => Yii::$app->request->absoluteUrl]); ?>
      </div>
    </div>
    <?php if(!empty($users = $model->propertyAgencies) && $model->show_agency_banner): ?>
    <div class="row">
    <?php foreach($users as $user): ?>
      <div class="property-agency-banner-block col-xs-<?= count($users) == 1 ? 12 : 6 ?>">
      <?php $agency = $user->agency; ?>
        <?php if(!empty($agency)): ?>
            <?php if(!empty($media = $agency->image)): ?>
              <div class="text-center agency-logo-block">
                <div class="item property-thumbnail" style="background-color:<?= !empty($color = $agency->color) ? $color : "#fff"  ?>;">
                  <a style="color: <?= !empty($font_color = $agency->secondary_color) ? $font_color : "#000"  ?> <?= $agency->featured_aceny ? "" : '; cursor: text' ?> " <?= $agency->featured_aceny ? "href='".Url::to(["/agencies/{$agency->slug}"])."'" : '' ?>>
                    <div class="overflow-image-block" >
                      <img src="<?= $storage.$media->media_path ?>" alt="<?= $agency->title ?>"/>
                    </div>
                  </a>
                </div>
              </div>
            <?php endif; ?>
        <?php endif; ?>
      </div><!-- /.property-agency-banner-block-->
      <?php endforeach; ?>
    </div><!-- /.row-->
    <?php endif; ?>

    <div class="row">
      <div class="col-sm-7">
        <div id="property-in-tabs">
          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#photos" class="no-effect" aria-controls="photos" role="tab" data-toggle="tab">Photos</a></li>
            <li role="presentation" id="show-map"><a href="#map-tab" class="no-effect" aria-controls="map-tab" role="tab" data-toggle="tab">Map</a></li>
            <li role="presentation" id="show-street-view"><a href="#street-view-tab" class="no-effect" aria-controls="street-view-tab" role="tab" data-toggle="tab">Street view</a></li>
          </ul>
          <!-- Tab panes -->
          <div class="tab-content">
            <?php if($model->sold): ?><div class="ribbon"><span>SOLD</span></div> <?php endif; ?>
            <?php if($model->under_offer): ?><div class="ribbon ribbon-green"><span>Under Offer</span></div><?php endif; ?>
            <div role="tabpanel" class="tab-pane active" id="photos">
              <ul class="pgwSlideshow">
                <?php if($model->image): ?>
                  <?php foreach($model->images as $item): ?>
                    <?php if( $item->default !== 1 ): ?>
                      <li>
                        <img src="<?= $storage."/properties/{$model->id}/{$item->file_name}" ?>" alt="image for property <?= $this->title ?>" />
                      </li>
                    <?php endif; ?>
                  <?php endforeach; ?>
                <?php endif; ?>
              </ul>
            </div>
            <div role="tabpanel" class="tab-pane" id="map-tab">
              <div id="propety-in-map"></div>
            </div>
            <div role="tabpanel" class="tab-pane" id="street-view-tab">
              <div id="propety-street-view"></div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xs-1" style="min-height: 25px"></div>
      <aside id="side-bar-block" class="col-sm-4">
        <div class="side-bar-block">
          <div class="side-information-title text-left">
            <h5>Property details</h5>
          </div>
          <div class="side-information-content">
            <ul>
              <li>
                <div class="row">
                  <div class="col-xs-12">
                    <strong>Id:</strong> #<?= $model->id ?>
                  </div>
                </div>
              </li>
              <li><strong>Price:</strong> <?= $model->propertyPrice->id == 6 ? '$ '.$model->price_in_numbers : ($model->propertyPrice->id == 2 ? $model->propertyPrice->title." ".$model->eoi_description : $model->propertyPrice->title) ?></li>
              <li><strong>Contract:</strong> <?= $model->sold ? 'Sold' : 'Sale' ?></li>
              <li><strong>Location:</strong> <?= $this->title ?></li>
              <li><strong>Area:</strong>
                <?php if($model->square_min){
                  echo 'min - ' . $model->square_min;
                  echo !empty($square = $model->square) ? ', max - ' . $square : '';
                }else{
                  echo !empty($square = $model->square) ? $square : '';
                }?>
                <?= !empty($measure = $model->areaType) ? $measure->title : ''?></li>
              <li><strong>Type:</strong> <?php foreach($model->propertyTypes as $item){ echo $item->title. ", "; } ?> </li>
              <?php if(isset($model->contract_type_id)): ?>
                <li><strong><?= $model->permitStatus()[$model->contract_type_id] ?></strong>  <?= !empty($details = $model->contract_details) ? "<a>{$details}</a>" : '' ?> </li>
              <?php endif; ?>
              <li class="text-center">
                <a rel="nofollow" class="favorite <?= ($favorite) ? 'active' : '' ?>" onclick='
                <?php if(Yii::$app->user->isGuest): ?>
                  javascript:alert("<?= Yii::t('common', 'This feature available only for registered users, please register') ?>");
                <?php else: ?>
                  $.post("<?= Yii::$app->urlManager->createUrl('property/favorite?id=').$model->id ?>", function( data ) {$("span#favorite-content").html(data);});
                <?php endif; ?>
                  '>
              <span id="favorite-content">
                <i class="fa <?= ($favorite) ? 'fa-heart' : 'fa-heart-o' ?> fa-left"></i>
                <span class="social-name"><?= ($favorite) ? Yii::t('frontend', 'Remove') : Yii::t('common', 'Add to Favorites') ?></span>
              </span>
                </a>
              </li>
            </ul>
          </div>
        </div><!-- /.side-bar-block -->

        <?php if(!empty($users = $model->propertyAgencies)): ?>
          <?php foreach($users as $user): ?>
            <?php $agency = $user->agency; ?>
            <?php if(!empty($agency)): ?>
              <div class="side-bar-block side-bar-agency-name">
                <div class="side-information-title text-left">
                  <h5>Agency information</h5>
                </div>
                <div class="side-information-content">
                  <ul>
                    <?php if(!empty($media = $agency->image)): ?>
                    <li>
                      <div class="text-center agency-logo-block">
                        <div class="item property-thumbnail" style="background-color:<?= !empty($color = $agency->color) ? $color : "#fff"  ?>;">
                          <a style="color: <?= !empty($font_color = $agency->secondary_color) ? $font_color : "#000"  ?> <?= $agency->featured_aceny ? "" : '; cursor: text' ?> " <?= $agency->featured_aceny ? "href='".Url::to(["/agencies/{$agency->slug}"])."'" : '' ?>>
                            <div class="overflow-image-block" >
                              <img src="<?= $storage.$media->media_path ?>"/>
                            </div>
                          </a>
                        </div>
                      </div>
                    </li>
                    <?php endif; ?>
                    <li>
                      <strong><?= $agency->title ?></strong>
                    </li>
                    <?php # if(!empty($agency_address = $agency->address)): ?>
                      <li><strong>Address:</strong> <br/>
                        <?= $agency->address ?: '&nbsp;' ?>
                      </li>
                    <?php # endif; ?>
                    <?php if(!empty($agents = $model->propertyAgents)): ?>
                      <?php foreach($agents as $agent): ?>
                        <?php if($agent->agency_id == $agency->id): ?>
                          <li class="text-center call-to-agent">
                            <strong><?= $agent->full_name ?></strong> <br/>
                            <?php $image = empty($media = $agent->image) ? '/web/agency/anonymous-agent.png' : $media->media_path ; ?>
                            <img class="img-responsive" src="<?= $storage.$image ?>"/>

                            <a id="show-contact-btn-<?= $agent->id ?>" rel="nofollow" class="btn btn-default"
                               onclick="$.post('<?= Yii::$app->urlManager->createUrl('property/agent-info?id='.$agent->id."&m=".$model->id) ?>',
                                 function(data) {$('#show-contact-btn-<?= $agent->id ?>').html(data);});"
                              >
                              <i class="fa fa-phone"></i> Call Agent
                            </a>
                          </li>
                        <?php endif; ?>
                      <?php endforeach; ?>
                    <?php endif; ?>
                  </ul>
                  <div class="text-center">
                    <a data-toggle="modal" rel="nofollow" data-id="<?= $model->id ?>" data-agency-id="<?= $user->id ?>" data-target="#message-agency" class="btn brand-green btn-view-details agency-message">Message Agent</a>
                  </div>
                </div>
              </div><!-- /.side-bar-agency-name -->
            <?php endif; ?>
          <?php endforeach; ?>
        <?php endif; ?>
      </aside>
      <div class="">
        <hr class="section-secondary-separator"/>
        <div id="property-description">
          <?= $model->description ?>
        </div>
        <br/>
      </div>
    </div>
  </div><!--/.container-->
</section><!--section ends-->

<!-- Separator
---------------------------------------------------------------------->
<hr class="section-separator"/>

<!-- FEATURED PROPERTIES
 ---------------------------------------------------------------------->
<section id="featured-properties">
  <div class="container">
    <div class="row">
      <h2 class="page-header text-left col-xs-12"><span>Featured</span> Development Sites</h2>
    </div><!--/.row-->
    <div id="property-carousel">
      <?= $this->render("//layouts/parts/_properties_carousel", ['properties' => $featured_properties, 'storage' => $storage]) ?>
    </div> <!--/#property-carousel-->
  </div><!--/.container-->
</section><!--section ends-->

