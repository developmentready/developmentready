<?php
/**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 7/22/15
 * Time: 07:51
 */

use yii\widgets\Pjax;
use common\components\helpers\Setup;
use yii\helpers\Url;

$this->title = $model->title;

$this->registerMetaTag(['name' => 'keywords', 'content' => $model->meta_keywords]);
$this->registerMetaTag(['name' => 'description', 'content' => $model->meta_description]);
$this->registerMetaTag(['name' => 'robots', 'content' => 'noindex,follow']);

$user = (\Yii::$app->user->isGuest) ? false : \Yii::$app->user->identity->id;

$storage = \Yii::getAlias('@storageUrl');

$js = <<< 'SCRIPT'
  jQuery(document).ready(function(){
    $('#property-carousel').owlCarousel({
      loop:true, margin:10, nav:true, navText: ['',''], dots : false,
      responsive:{
        0:{items:1},
        600:{items:2},
        1000:{items:3}
      }
    });

    $('#map-block').addClass('hide-map');
  });

  google.maps.event.trigger(map_object, 'resize');
SCRIPT;
$this->registerJs($js);

?>

<!-- PROPERTY PAGE HEADER
---------------------------------------------------------------------->
<section id>
  <div class="container">
    <div class="row">
      <div class="col-sm-5">
        <h1 class="page-header pull-left"><span>Sold properties</span> list</h1>
      </div>
      <div class="col-sm-7">
        <?= $this->render("//layouts/parts/_filter", ['properties' => $grouped_properties, 'search_model' => $search_model]) ?>
      </div>
    </div>
  </div>
</section>

<!-- MAP SECTION
---------------------------------------------------------------------->
<?= $this->render("//layouts/parts/_map", ['state' => $state, 'properties' => $map_properties]) ?>

<!-- SECTION NAME
---------------------------------------------------------------------->
<section id="property-list-section">
  <div class="container">
    <div class="row">
      <?= $this->render("//layouts/parts/_properties_line", ['properties' => $properties, 'storage' => $storage, 'message_model' => $message_model]) ?>
    </div><!--/.row-->
  </div><!--/.container-->
</section><!--section ends-->

<!-- PAGINATION SECTION
---------------------------------------------------------------------->
<?= $this->render("//layouts/parts/_pagination", ['pages' => $pages]) ?>


<!-- Separator
---------------------------------------------------------------------->
<hr class="section-separator"/>

<!-- FATURED PROPERTIES
 ---------------------------------------------------------------------->
<section id="featured-properties">
  <div class="container">
    <div class="row">
      <h2 class="page-header text-left col-xs-12"><span>Featured</span> Development Sites</h2>
    </div><!--/.row-->
    <div id="property-carousel">
      <?= $this->render("//layouts/parts/_properties_carousel", ['properties' => $featured_properties, 'storage' => $storage]) ?>
    </div> <!--/#property-carousel-->
  </div><!--/.container-->
</section><!--section ends-->
