<?php
/**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 7/22/15
 * Time: 07:51
 */

use yii\widgets\Pjax;
use common\components\helpers\Setup;
use yii\helpers\Url;

$this->title = $model->title;

$this->registerMetaTag(['name' => 'keywords', 'content' => $model->meta_keywords]);
$this->registerMetaTag(['name' => 'description', 'content' => $model->meta_description]);
$this->registerMetaTag(['name' => 'robots', 'content' => 'noindex,follow']);

$user = (\Yii::$app->user->isGuest) ? false : \Yii::$app->user->identity->id;

$storage = \Yii::getAlias('@storageUrl');

$js = <<< 'SCRIPT'
  jQuery(document).ready(function(){
    $('#property-carousel').owlCarousel({
      loop:true, margin:10, nav:true, navText: ['',''], dots : false,
      responsive:{
        0:{items:1},
        600:{items:2},
        1000:{items:3}
      }
    });
    google.maps.event.addListener(map_object, 'idle', function() {});

    $('#map-block').css('display', 'block');
    $('#map-block').css('margin-bottom', '30px');
  });
SCRIPT;
$this->registerJs($js);

?>

<!-- Page Header Section
---------------------------------------------------------------------->
<!--<section id="page-header-section" data-type="background" data-speed="10">
  <div id="page-header-page-cover"></div>
  <div id="page-header-container">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 text-center">
          <div id="header-search-box-container">
            <?/*= \frontend\widgets\SearchBar::widget(); */?>
          </div><!-- /#header-search-box-container -->
        <!--</div>--><!-- /.col -->
      <!--</div>--><!-- /.row -->
   <!--</div>--><!-- /.container -->
  <!--</div>--><!-- /#home-page-header-container-->
<!--</section>-->

<!-- PROPERTY PAGE HEADER
---------------------------------------------------------------------->
<section id>
  <div class="container">
    <div class="row">
      <div class='col-sm-12'>
        <h1 class="page-header pull-left"><span>Properties</span> list</h1>
      </div>
    </div>
  </div>
</section>

<!-- MAP SECTION
---------------------------------------------------------------------->
<?= $this->render("//layouts/parts/_map", ['state' => $state, 'properties' => $map_properties]) ?>

<section id="">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header pull-left"><span>Latest</span> listings from around Australia</h1>
      </div>
    </div>
  </div>
</section>

<!-- SECTION NAME
---------------------------------------------------------------------->
<section id="property-list-section">
  <div class="container">
    <div class="row">
      <?= $this->render("//layouts/parts/_properties_line", ['properties' => $properties, 'storage' => $storage, 'message_model' => $message_model]) ?>
    </div><!--/.row-->
  </div><!--/.container-->
</section><!--section ends-->

<!-- PAGINATION SECTION
---------------------------------------------------------------------->
<?= $this->render("//layouts/parts/_pagination", ['pages' => $pages]) ?>

<!-- Separator
---------------------------------------------------------------------->
<hr class="section-separator"/>

<!-- FEATURED PROPERTIES
 ---------------------------------------------------------------------->
<section id="featured-properties">
  <div class="container">
    <div class="row">
      <h2 class="page-header text-left col-xs-12"><span>Featured</span> Development Sites</h2>
    </div><!--/.row-->
    <div id="property-carousel">
      <?= $this->render("//layouts/parts/_properties_carousel", ['properties' => $featured_properties, 'storage' => $storage]) ?>
    </div> <!--/#property-carousel-->
  </div><!--/.container-->
</section><!--section ends-->
