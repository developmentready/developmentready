<?php
/**
 * @var $this \yii\web\View
 * @var $model \common\models\Page
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$setting = new common\models\Settings;

$this->title = $model->title;

$this->registerMetaTag(['name' => 'keywords', 'content' => $model->meta_keywords]);
$this->registerMetaTag(['name' => 'description', 'content' => $model->meta_description]);

$storage = \Yii::getAlias('@storageUrl');
if(!empty($media = $model->image)){$image = $media->media_path;};


?>

<script>
  var video = '<?= !empty($model->video_link) ? $model->video_link : 'false' ?>';
  var video_image = '<?= !empty($image) ? $storage.$image : $storage.'/properties/no-image.png' ?>';
</script>

<!-- COMMON PAGE SECTION
---------------------------------------------------------------------->
<section id="about-section">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header"><?= $model->h1_tag ?></h1>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-5">
        <?php if(!empty($model->video_link)): ?>
          <div id="about-video"></div>
          <div id="thumbnail_container" class="thumbnail_container"><img class="thumbnail" id="video-thumbnail" /></div>
          <a class="ci start-video"></a>
        <?php elseif(!empty($image)): ?>
          <div id="thumbnail_container" class="thumbnail_container">
            <img class="thumbnail" src="<?= $storage.$image ?>" />
          </div>
        <?php endif; ?>
      </div>
      <div class="col-sm-7">
        <?= $model->body ?>
      </div>
    </div>
  </div><!--/.container-->
</section><!--section ends-->

<!-- Separator
---------------------------------------------------------------------->
<hr class="section-separator"/>

<!-- TESTIMONIALS SECTION NAME
---------------------------------------------------------------------->
<?php if(!empty($testimonials)): ?>
<section id="testimonials-section">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h2 class="page-header">Testimonials</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div id="testimonial-carousel">
          <?php foreach($testimonials as $item): ?>
            <div class="carousel-item">
              <div class="row">
                <div class="col-sm-3 testimonial-logo-block">
                  <div class="testimonial-logo">
                    <?php if($t_image = $item->image): ?>
                      <img src="<?= $storage.$t_image->media_path ?>" />
                    <?php endif; ?>
                  </div>
                </div>
                <div class="col-sm-9 testimonial-text-block">
                  <?= $item->body ?>
                  <h5 class="text-center"><strong><?= $item->title ?></strong></h5>
                </div>
              </div>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div><!--/.row-->
  </div><!--/.container-->
</section><!--section ends-->
<?php endif; ?>

<hr class="col-xs-12" />

<!-- SECTION NAME
---------------------------------------------------------------------->
<section id="send-testimonial-section">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h2 class="page-header text-center">Leave Your Testimonial</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-8 col-sm-offset-2">
        <?php $form = ActiveForm::begin(); ?>

        <?php echo $form->errorSummary($testimonial); ?>

          <div class="row form-group">
            <div class="col-sm-6">
              <?php echo $form->field($testimonial, 'name')->textInput(['placeholder' => 'Name *'])->label(false) ?>
            </div>
            <div class="col-sm-6">
              <?php echo $form->field($testimonial, 'email')->textInput(['type' => 'email', 'placeholder' => 'Email *'])->label(false) ?>
            </div>
          </div>
          <div class="row form-group">
            <div class="col-sm-12">
              <?php echo $form->field($testimonial, 'message')->textarea(
                ['rows' => 4, 'placeholder' => 'Message *']
              )->label(false) ?>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-4 col-sm-offset-4 text-center">
              <?= Html::submitButton('Done', ['class' => 'btn brand-green btn-read-more']) ?>
            </div>
          </div>
        <?php ActiveForm::end(); ?>
      </div>
    </div><!--/.row-->
  </div><!--/.container-->
</section><!--section ends-->

<!-- THREE CONTACTS SECTION
---------------------------------------------------------------------->
<section id="three-contacts">
  <div class="container full-width">
    <div class="row">
      <div class="col-sm-4 three-contacts-map three-contacts-block text-center">
        <div class="three-contacts-content">
          <div class="col-xs-5 text-right">
            <span class="ci ci-map-location"></span>
          </div>
          <div class="col-xs-7 text-left">
            <h5>Address</h5>
            <p>
              <?= !empty($address = $setting->findKeyword('inner-page-address')) ? $address : '' ?>
            </p>
          </div>
        </div>
      </div>
      <div class="col-sm-4 three-contacts-call three-contacts-block text-center">
        <div class="three-contacts-content">
          <div class="col-xs-5 text-right">
            <span class="ci ci-call-us"></span>
          </div>
          <div class="col-xs-7 text-left">
            <h5>Call Us</h5>
            <p><?= !empty($phone = $setting->findKeyword('inner-page-phone-numbers')) ? $phone : '' ?></p>
          </div>
        </div>
      </div>
      <div class="col-sm-4 three-contacts-email three-contacts-block text-center">
        <div class="three-contacts-content">
          <div class="col-xs-5 text-right">
            <span class="ci ci-envelope"></span>
          </div>
          <div class="col-xs-7 text-left">
            <h5>Email</h5>
            <p><?= !empty($email = $setting->findKeyword('e_mail')) ? $email : '' ?></p>
          </div>
        </div>
      </div>
    </div><!--/.row-->
  </div>
</section><!--section ends-->
