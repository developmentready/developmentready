<?php /**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 7/28/15
 * Time: 13:03
 */ ?>

<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \frontend\modules\user\models\LoginForm */

$this->title = 'Login page | Welcome in '.Yii::$app->name.' control panel';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="admin-content-main-inner">
  <div class="container-fluid">
    <div class="admin-content">
      <div class="admin-content-image-text">
        <h1><?php echo Html::encode($this->title) ?></h1>
      </div><!-- /.admin-content-image-text -->

      <div class="admin-content-image-call-to-action">
        <i class="fa fa-long-arrow-left"></i>
      </div><!-- /.admin-content-image-call-to-action -->

    </div><!-- /.admin-content -->

    <div class="admin-sidebar-secondary">
      <div class="admin-sidebar-secondary-inner">
        <div class="admin-sidebar-secondary-inner-top">
          <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
            <div class="form-group">
              <?php echo $form->field($model, 'identity', ['template' => "{input}\n{error}"])
                ->textInput(['type' => 'email','class' => 'form-control', 'placeholder' => Yii::t('frontend','E-mail')]) ?>
            </div><!-- /.form-group -->
            <div class="form-group">
              <?php echo $form->field($model, 'password', ['template' => "{input}\n{error}"])
                ->passwordInput(['class' => 'form-control', 'placeholder' => Yii::t('frontend','Password')]) ?>
            </div><!-- /.form-group -->
            <?php echo $form->field($model, 'rememberMe')->checkbox() ?>

            <?php echo Html::submitButton(Yii::t('frontend', 'Login'), ['class' => 'btn brand-green btn-xl pull-right', 'name' => 'login-button', 'type' => 'submit']) ?>

          <?php ActiveForm::end(); ?>
          <br/>
          <p><?php echo Yii::t('frontend', 'If you forgot your password you can reset it <a href="{link}">here</a>', [
              'link'=>yii\helpers\Url::to(['sign-in/request-password-reset'])
            ]) ?></p>
        </div><!-- /.admin-sidebar-secondary-inner-top -->

        <?php # yii\authclient\widgets\AuthChoice::widget(['baseAuthUrl' => ['/user/sign-in/oauth']]) ?>

        <div class="admin-sidebar-secondary-inner-bottom">
          <div class="admin-sidebar-footer">
            <div class="admin-sidebar-info">
              <h4><?php echo Html::a(Yii::t('frontend', 'Need an account? Sign up.'), ['signup']) ?></h4>
            </div><!-- /.admin-landing -->
          </div><!-- /.admin-landing-content-footer -->
        </div><!-- /.admin-sidebar-secondary-inner-bottom -->
      </div><!-- /.admin-sidebar-secondary-inner -->
    </div><!-- /.admin-sidebar-secondary -->
 </div>
</div>
