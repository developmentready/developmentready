<?php  /**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 7/28/15
 * Time: 12:36
 */ ?>

<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \frontend\modules\user\models\LoginForm */

$this->title = 'SignUp | Welcome to get account page in'.Yii::$app->name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-content-main-inner">
  <div class="container-fluid">
    <div class="admin-content">
      <div class="admin-content-image-text">
        <h1><?php echo Html::encode($this->title) ?></h1>
        <h2>Please click on login to continue.</h2>
      </div><!-- /.admin-content-image-text -->

      <div class="admin-content-image-call-to-action">
        <i class="fa fa-long-arrow-left"></i>
        <br>
        <span>"Login" to see what's here.</span>
      </div><!-- /.admin-content-image-call-to-action -->

    </div><!-- /.admin-content -->

    <div class="admin-sidebar-secondary">
      <div class="admin-sidebar-secondary-inner">
        <div class="admin-sidebar-secondary-inner-top">
          <h1 class="text-center"><?= Yii::$app->name ?></h1>
          <h2 class="text-center">SignUp for more features.</h2>
          <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
          <hr class="col-xs-12" />
          <div class="form-group">
            <?php echo $form->field($model, 'user_type', ['template' => "{input} {error}"])
              ->dropDownList($model->getUserTypes(), ['prompt'=>Yii::t('common','User Type')]) ?>
          </div><!-- /.form-group -->
          <div class="form-group">
            <?php echo $form->field($model, 'name', ['template' => "{input} {error}"])
              ->textInput(['class' => 'form-control', 'placeholder' => Yii::t('frontend','Name')]) ?>
          </div><!-- /.form-group -->
          <div class="form-group">
            <?php echo $form->field($model, 'email', ['template' => "{input} {error}"])
              ->textInput(['type' => 'email','class' => 'form-control', 'placeholder' => 'E-mail']) ?>
          </div><!-- /.form-group -->
          <div class="form-group">
            <?php echo $form->field($model, 'password', ['template' => "{input} {error}"])
              ->passwordInput(['class' => 'form-control', 'placeholder' => Yii::t('frontend','Password')]) ?>
          </div><!-- /.form-group -->
          <?php echo Html::submitButton(Yii::t('common', 'Register'), ['class' => 'btn btn-xl brand-green pull-right', 'name' => 'signup-button']) ?>
          <div class="form-group">
            <?php # yii\authclient\widgets\AuthChoice::widget(['baseAuthUrl' => ['/user/sign-in/oauth']]) ?>
          </div>
          <?php ActiveForm::end(); ?>
        </div><!-- /.admin-sidebar-secondary-inner-top -->

        <?php # yii\authclient\widgets\AuthChoice::widget([
        #'baseAuthUrl' => ['/user/sign-in/oauth']
        #]) ?>
        <div class="admin-sidebar-secondary-inner-bottom">
          <div class="admin-sidebar-footer">
            <div class="admin-sidebar-info">
              <h4><?php echo Html::a(Yii::t('common', 'Already member? Sign in.'), ['login']) ?></h4>
            </div><!-- /.admin-landing -->
          </div><!-- /.admin-landing-content-footer -->
        </div><!-- /.admin-sidebar-secondary-inner-bottom -->
      </div><!-- /.admin-sidebar-secondary-inner -->
    </div><!-- /.admin-sidebar-secondary -->
  </div>
</div>