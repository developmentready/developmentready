<?php
/**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 9/13/15
 * Time: 12:35
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \frontend\modules\user\models\LoginForm */

$this->title = Yii::t('frontend', 'Request password reset');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="admin-content-main-inner">
  <div class="container-fluid">
    <div class="admin-content">
      <div class="admin-content-image-text">
        <h1></h1>
      </div><!-- /.admin-content-image-text -->
    </div><!-- /.admin-content -->

    <div class="admin-sidebar-secondary">
      <div class="admin-sidebar-secondary-inner">
        <div class="admin-sidebar-secondary-inner-top">
          <h1><?php echo Html::encode($this->title) ?></h1>
          <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
          <div class="form-group">
            <?php echo $form->field($model, 'email', ['template' => "{input} {error}"])
              ->textInput(['type' => 'email','class' => 'form-control', 'placeholder' => 'E-mail']) ?>
          </div><!-- /.form-group -->
          <?php echo Html::submitButton(Yii::t('common', 'Send a Request'), ['class' => 'btn btn-xl brand-green  pull-right', 'name' => 'signup-button']) ?>
          <div class="form-group">
            <?php # yii\authclient\widgets\AuthChoice::widget(['baseAuthUrl' => ['/user/sign-in/oauth']]) ?>
          </div>
          <?php ActiveForm::end(); ?>
        </div><!-- /.admin-sidebar-secondary-inner-top -->

        <?php # yii\authclient\widgets\AuthChoice::widget([
        #'baseAuthUrl' => ['/user/sign-in/oauth']
        #]) ?>
        <div class="admin-sidebar-secondary-inner-bottom">
          <div class="admin-sidebar-footer">
            <div class="admin-sidebar-info">
              <h4><?php echo Html::a(Yii::t('frontend', 'Login'), ['login']) ?></h4>
            </div><!-- /.admin-landing -->
          </div><!-- /.admin-landing-content-footer -->
        </div><!-- /.admin-sidebar-secondary-inner-bottom -->
      </div><!-- /.admin-sidebar-secondary-inner -->
    </div><!-- /.admin-sidebar-secondary -->
  </div>
</div>