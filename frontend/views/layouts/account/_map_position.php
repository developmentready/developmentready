<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/27/15
 * Time: 20:54
 */

?>
<style>
  #map>div{
    min-height: 450px;
  }
</style>
<br/>
<div class="row">
  <div class="col-xs-10 col-xs-offset-1">
    <input id="pac-input" class="form-control" type="text" onkeydown="if (event.keyCode == 13) {return false;}" placeholder='Please start typing address'>
    <div id="map" style="min-height: 450px"></div>
  </div>
</div>
<br/>
<div class="row">
  <div class="col-xs-10 col-xs-offset-1">
    <div class="row">
      <div class="col-xs-6">
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
          <?= $form->field($model, 'lat', ['template' => '{input}'])->textInput(['placeholder' => Yii::t('common', 'Latitude'), 'class' => 'form-control', 'id' => 'input-latitude'])->label(false); ?>
        </div><!-- /.form-group -->
      </div><!-- /.col-* -->
      <div class="col-xs-6">
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
          <?= $form->field($model, 'lng', ['template' => '{input}'])->textInput(['placeholder' => Yii::t('common', 'Longitude'), 'class' => 'form-control', 'id' => 'input-longitude'])->label(false); ?>
        </div><!-- /.form-group -->
      </div><!-- /.col-* -->
    </div>
  </div>
</div>

<br/>

<script>
  var prop_lat = <?= empty($lat = $model->lat) ? '-33.9' : $lat?>;
  var prop_long = <?= empty($lat = $model->lng) ? '151.2' : $lat?>;

  function initAutocomplete(){
    var latlng = new google.maps.LatLng(prop_lat, prop_long);

    var map = new google.maps.Map(document.getElementById('map'), {
      center: latlng, zoom: 8,
      scrollwheel: false,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    // Create the search box and link it to the UI element.
    var input = document.getElementById('pac-input');
    //var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);

    var infowindow = new google.maps.InfoWindow();
    var marker = new google.maps.Marker({
      position: latlng, title: 'Point To Select',
      map: map, draggable: true, anchorPoint: new google.maps.Point(0, -29)
    });

    google.maps.event.addListener(marker, "mouseup", function(event) {
      document.getElementById("input-latitude").value = this.position.lat();
      document.getElementById("input-longitude").value = this.position.lng();
    });

    google.maps.event.addListener(autocomplete, 'place_changed', function(){
      infowindow.close();
      marker.setVisible(false);
      var place = autocomplete.getPlace();
      if (!place.geometry){
        return;
      }

      // If the place has a geometry, then present it on a map.
      if (place.geometry.viewport) {
        map.fitBounds(place.geometry.viewport);
      } else {
        map.setCenter(place.geometry.location);
        map.setZoom(8);
      }
      marker.setIcon(/** @type {google.maps.Icon} */({
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(35, 35)
      }));
      marker.setPosition(place.geometry.location);
      marker.setVisible(true);

      $('#input-latitude').val(place.geometry.location.lat());
      $('#input-longitude').val(place.geometry.location.lng());

      var address = '';
      if (place.address_components) {
        address = [
          (place.address_components[0] && place.address_components[0].short_name || ''),
          (place.address_components[1] && place.address_components[1].short_name || ''),
          (place.address_components[2] && place.address_components[2].short_name || '')
        ].join(' ');
      }

      infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
      infowindow.open(map, marker);
    });
    //addMarker(latlng, map);
  }


  //$('a').on('click', function(e) {e.preventDefault();});

</script>
<script src="http://maps.googleapis.com/maps/api/js?callback=initAutocomplete&libraries=places" type="text/javascript"></script>