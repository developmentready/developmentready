<?php /**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 7/28/15
 * Time: 12:02
 */ ?>

<?php
/* @var $this \yii\web\View */
/* @var $content string */

$setting = new common\models\Settings;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

//\frontend\assets\YiiAsset::register($this);
//\frontend\assets\AccountAsset::register($this);

?>
<?php $this->beginPage()?>
<!DOCTYPE html>
<html lang="<?php echo Yii::$app->language ?>">
<head>
  <meta charset="<?php echo Yii::$app->charset ?>"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
  <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Roboto:400,300,500,700&amp;subset=latin,latin-ext">
  <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Montserrat:400,700">
  <link rel="icon" href="/favicon.png" type="image/png">
  <meta name="robots" content="noindex, nofollow">

  <title><?php echo Html::encode($this->title) ?></title>


  <?php $this->head() ?>

  <?php echo Html::csrfMetaTags() ?>
  <?= Html::cssFile('@frontendUrl/assets/production/css/account.min.css') ?>
  <?= Html::jsFile('@frontendUrl/assets/production/js/account.min.js') ?>
  <style>
    .admin-sidebar-secondary .help-block{ position: static; }
    select{border: 1px solid #555555; padding-left: 15px;}
    #switcher > .form-group{position: relative; bottom: 12px; left: 5px}
  </style>
  <script> var prop_lat = 40.378349; var prop_long = 49.855431;</script>
</head>
<body class="open">
  <?php $this->beginBody() ?>
  <div class="admin-landing-image-source"></div>
  <div class="admin-landing-image-cover"></div>

  <div class="admin-wrapper">
    <?= $this->render('//layouts/account/_sidebar', ['setting' => $setting]) ?>

    <div class="admin-content-inner">
      <div class="admin-content-header">
        <?= $this->render('//layouts/account/_header', ['setting' => $setting]) ?>
      </div>

      <div class="admin-content-main">
            <?php if(Yii::$app->session->hasFlash('alert')):?>
              <?= \yii\bootstrap\Alert::widget([
                'body'=>ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'body'),
                'options'=>ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'options'),
              ])?>
            <?php endif; ?>
            <?php echo $content ?>
      </div>
      <div class="admin-content-footer">
        <?= $this->render('//layouts/account/_footer', ['setting' => $setting]) ?>
      </div>
    </div>
  </div>
  <?php $this->endBody() ?>
<script>
  // prevent default enter
  $('input').bind('keypress keydown keyup', function(e){
    if(e.keyCode == 13) { e.preventDefault(); }
  });
</script>
</body>
</html>
<?php $this->endPage() ?>

