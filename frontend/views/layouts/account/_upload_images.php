<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/19/15
 * Time: 23:39
 */

use trntv\filekit\widget\Upload;


echo $form->field($model, 'fileAttributes')->widget(
  Upload::className(),
  [
    'url' => ['/file-storage/upload'],
    'sortable' => true,
    'maxFileSize' => 10 * 1024 * 1024, // 10 MiB
    'maxNumberOfFiles' => $max_image_count
  ])->label('Add new images');
