<?php
/**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 9/13/15
 * Time: 12:29
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('common', 'Reset Password');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-content-main-inner">
  <div class="container-fluid">
    <div class="admin-content">
      <div class="admin-content-image-text">
        <h1><?php echo Html::encode($this->title) ?></h1>
        <h2>Please click on login to continue.</h2>
      </div><!-- /.admin-content-image-text -->

      <div class="admin-content-image-call-to-action">
        <i class="fa fa-long-arrow-left"></i>
        <br>
      </div><!-- /.admin-content-image-call-to-action -->

    </div><!-- /.admin-content -->

    <div class="admin-sidebar-secondary">
      <div class="admin-sidebar-secondary-inner">
        <div class="admin-sidebar-secondary-inner-top">
          <h1><?= Yii::t('frontend', 'Reset password') ?></h1>
          <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>
          <div class="form-group">
            <?php echo $form->field($model, 'password', ['template' => "{input} {error}"])
              ->passwordInput(['class' => 'form-control', 'placeholder' => Yii::t('frontend','Reset password')]) ?>
          </div><!-- /.form-group -->
          <?php echo Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-xl pull-right', 'name' => 'signup-button']) ?>
          <div class="form-group">
            <?php # yii\authclient\widgets\AuthChoice::widget(['baseAuthUrl' => ['/user/sign-in/oauth']]) ?>
          </div>
          <?php ActiveForm::end(); ?>
        </div><!-- /.admin-sidebar-secondary-inner-top -->
      </div><!-- /.admin-sidebar-secondary-inner -->
    </div><!-- /.admin-sidebar-secondary -->
  </div>
</div>