<?php /**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 7/28/15
 * Time: 12:37
 */


use yii\helpers\Url;

?>

<div class="admin-navigation">
  <div class="admin-navigation-inner">
    <?php if(!Yii::$app->user->isGuest): ?>
    <nav>
        <ul class="menu">
        <li class="avatar">
          <?= \frontend\widgets\UserInfo::widget() ?>
        </li>
        <li class="">
          <a href="<?= Url::to(['/account/index']) ?>"><strong><i class="fa fa-building"></i></strong> <span><?= Yii::t('common','My Properties') ?></span></a>
        </li>
        <li class="">
          <a href="<?= Url::to(['/account/new-property']) ?>"><strong><i class="fa fa-plus"></i></strong> <span><?= Yii::t('common', 'Add New Property') ?></span></a>
        </li>
        <li class="">
          <a href="<?= Url::to(['/property/my-favorites']) ?>"><strong><i class="fa fa-heart"></i></strong> <span><?= Yii::t('common', 'Favorites') ?></span></a>
        </li>
        <?php if(Yii::$app->user->can('agencies') && (Yii::$app->user->identity->user_type == 1)): ?>
          <li class="">
            <a href="<?= Url::to(['/account/agency']) ?>"><strong><i class="fa fa-pencil"></i></strong> <span><?= Yii::t('common', 'Agency info') ?></span></a>
          </li>
          <li class="">
            <a href="<?= Url::to(['/account/agents']) ?>"><strong><i class="fa fa-user"></i></strong> <span><?= Yii::t('common', 'Agents') ?></span></a>
          </li>
        <?php endif; ?>
        <?php if(Yii::$app->user->can('user') && (Yii::$app->user->identity->user_type == 2)): ?>
          <li class="">
              <a href="<?= Url::to(['/account/profile']) ?>"><strong><i class="fa fa-user"></i></strong> <span><?= Yii::t('common', 'Profile information') ?></span></a>
          </li>
        <?php endif; ?>
        <li>
          <a href="<?= Url::to(['/user/sign-in/logout']) ?>"><strong><i class="fa fa-sign-out"></i></strong> <span><?= Yii::t('common', 'Logout') ?></span></a>
        </li>
      </ul>
      </nav>
    <?php endif; ?>
    <div class="layer"></div>
  </div><!-- /.admin-navigation-inner -->
</div><!-- /.admin-navigation -->

