<?php /**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 7/28/15
 * Time: 12:38
 */ ?>

<div class="admin-content-footer-inner">
  <div class="container-fluid">
    <div class="admin-content-footer-left">
    </div><!-- /.admin-content-footer-left -->

    <div class="admin-content-footer-right">
      &copy; 2015 <a href="<?= Yii::getAlias('@frontendUrl') ?>"><?= Yii::$app->name ?></a>
    </div><!-- /.admin-content-footer-right -->
  </div><!-- /.container-fluid -->
</div><!-- /.admin-content-footer-inner -->
