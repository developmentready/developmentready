<?php /**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 8/17/15
 * Time: 10:37
 */

use yii\helpers\Url;

?>

<div class="admin-content-header-inner">
  <div class="container-fluid">
    <div class="admin-content-header-logo">
      <a href="<?= Yii::getAlias('@frontendUrl') ?>"> <?= Yii::$app->name ?> </a>
    </div><!-- /admin-content-header-logo -->

    <div class="admin-content-header-menu">
      <ul class="admin-content-header-menu-inner collapse">
        <li><a href="<?= Url::to(['/property/index']) ?>"><?= Yii::t('common','All Properties') ?></a></li>
        <?php if(!Yii::$app->user->isGuest): ?>
          <li><a href="<?= Url::to(['/user/sign-in/logout']) ?>"><?= Yii::t('common', 'Logout') ?></a></li>
        <?php endif; ?>
      </ul>
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".admin-content-header-menu-inner">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div><!-- /.admin-content-header-menu  -->
  </div><!-- /.container-fluid -->
</div><!-- /.admin-content-header-inner -->

