<?php
/**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 8/23/15
 * Time: 21:21
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<script>
  country_id = <?= $default_country_id ?>
//  var url = urlWithFilters();
//  $.pjax({url: url, container: ''});
</script>

<?php $form = ActiveForm::begin(['method' => 'get', 'action' => Url::current()]); ?>
<h3 class="page-header"> <?= Yii::t('common', 'Filter') ?>
  <a id="btn-advanced-search" class="pull-right"> + <?= Yii::t('common', 'Advanced Search') ?></a>
  <a id="filter-close-button" class="pull-right text-right"><i class="fa fa-times fa-2x"></i></a>
</h3>
<div class="row">
  <div id="first" class="col-sm-4 col-md-2">
    <?= $form->field($searchModel, 'country_id')->dropDownList(ArrayHelper::map($countries, 'id', 'translation.name'),['prompt' => Yii::t('common', 'Country'), 'id' => 'country_id',
      'onchange'=>'$.post( "'.Yii::$app->urlManager->createUrl('site/lists?id=').'"+$(this).val(), function( data ) {$("select#address-city_id").html( data ).selectpicker("refresh");});',
      'inputTemplate' => '{{select}}'])->label(false);
    ?>
  </div><!-- /.col-* -->
  <div class="col-sm-4 col-md-2">
    <?= $form->field($searchModel, 'city_id')->dropDownList(ArrayHelper::map($cities, 'id', 'translation.title'), ['prompt'=>Yii::t('common', 'City'), 'id' => 'address-city_id',])->label(false) ?>
  </div><!-- /.col-* -->

  <div class="col-sm-4 col-md-2">
    <?= $form->field($searchModel, 'contract_type_id')->dropDownList(ArrayHelper::map($contract_types, 'id', 'translation.title'), ['prompt' => Yii::t('common', 'Contract Type')])->label(false) ?>
  </div><!-- /.col-* -->

  <div class="col-sm-4 col-md-2">
    <?= $form->field($searchModel, 'property_type_id')->dropDownList($property_types, ['prompt'=>Yii::t('common','Property Type')])->label(false) ?>
  </div><!-- /.col-* -->
  <div class="col-sm-4 col-md-2">
    <?= $form->field($searchModel, 'sort')->dropDownList($sort_by, ['prompt'=>Yii::t('common', 'Sort By')])->label(false) ?>
  </div><!-- /.col-* -->
  <div class="col-sm-4 col-md-2">
    <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-sm form-control']) ?>
  </div><!-- /.col-* -->
</div><!-- /.row -->
<div id="additional-search">
  <div  class="row">
    <div class="col-sm-4 col-md-3">
      <?= $form->field($searchModel, 'min_rooms')->textInput(['placeholder' => Yii::t('common', 'Min. Rooms'), 'class' => 'form-control'])->label(false); ?>
    </div>
    <div class="col-sm-4 col-md-3">
      <?= $form->field($searchModel, 'min_price')->textInput(['placeholder' => Yii::t('common', 'Min. Price'), 'class' => 'form-control'])->label(false); ?>
    </div>
    <div class="col-sm-4 col-md-3">
      <?= $form->field($searchModel, 'max_price')->textInput(['placeholder' => Yii::t('common', 'Max. Price'), 'class' => 'form-control'])->label(false); ?>
    </div>
    <div class="col-sm-4 col-md-3">
      <?= $form->field($searchModel, 'min_square')->textInput(['placeholder' => Yii::t('common', 'Min. Square'), 'class' => 'form-control'])->label(false); ?>
    </div>
  </div>
  <div class="row">
    <div id='propertysearch-direction_points' class="col-sm-12"></div>
    <?php
//    Html::activeCheckboxList($searchModel, "direction_points", ArrayHelper::map($directions, 'id','translation.title'),[
//      'itemOptions' => [
//        'labelOptions' => ['class' => 'checkbox-inline'],
//      ]]) ?>
  </div>
</div>
<?php ActiveForm::end(); ?>
