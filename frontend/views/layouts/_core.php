<?php

header ("Cache-Control: public, max-age=3600");

use yii\helpers\Html;
/* @var $this \yii\web\View */
/* @var $content string */

$setting = new common\models\Settings;

use yii\helpers\ArrayHelper;
use yii\bootstrap\Modal;

//\frontend\assets\YiiAsset::register($this);
//\frontend\assets\FrontendAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?php echo Yii::$app->language ?>">
<head>
  <meta charset="<?php echo Yii::$app->charset ?>"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
  <title><?php echo Html::encode($this->title) ?></title>
  <meta name="dcterms.rightsHolder" content="Permit Ready Company"/>
  <link rel="icon" href="/favicon.png" type="image/png">

  <style>
    .no-js #loader {display: none;}
    .js #loader { display: block; position: absolute; left: 100px; top: 0; }
    .se-pre-con {
      position: fixed;left: 0px;top: 0px;width: 100%;height: 100%;z-index: 9999;
      background: url("/img/loader/Preloader_7.gif") center no-repeat #fff;
    }
    #home{
      background:url(/assets/production/img/slider/mod_homepage_compressed_banner.jpg)50% 0 fixed;
      min-height:640px;position:relative; background-repeat: no-repeat;
    }
    .pjax-loading {
      opacity: 0.5;
      background: #ffffff url('/img/loader/Preloader_7.gif') center center no-repeat !important;
    }
    .pjax-loading  * {
      background: transparent !important;
    }
    #pop-up-subscription{visibility: hidden;}
  </style>
  <?php $this->head() ?>
  <?php echo Html::csrfMetaTags() ?>
  <?= Html::cssFile('@frontendUrl/assets/production/css/front-required.min.css?v='.filemtime(Yii::getAlias('@frontend/web/assets/production/css/app.min.css'))) ?>
  <?= Html::jsFile('@frontendUrl/assets/production/js/front-required.min.js?v='.filemtime(Yii::getAlias('@frontend/web/assets/production/js/front-required.min.js'))) ?>
  <script>
    if($(window).width()>900){
      $(document).ready(function(){setTimeout(function () { $(".se-pre-con").fadeOut("slow");})});
    }else{
      $(window).load(function(){$(".se-pre-con").fadeOut("slow");});
    }
  </script>
  <meta name="google-site-verification" content="sVycYI_TmvX__Ep3gZ61wUvSVgt6gj-ge-OhlLoLQI8" />
</head>
<body class="<?= \Yii::$app->controller->id ?> <?= \Yii::$app->controller->id ?>-<?= \Yii::$app->controller->action->id ?>">
<div class="se-pre-con"></div>
<div id="fullpage">
  <div id="wrap" ><!--container block starts-->
    <?php $this->beginBody() ?>

    <?= $setting->findKeyword('google-tag-manager') ? $setting->findKeyword('google-tag-manager') : ''; ?>
    <?= $setting->findKeyword('google-adsec') ? $setting->findKeyword('google-adsec') : ''; ?>

    <?php //echo $this->render('//layouts/_header', ['setting' => $setting]) ?>

    <?php
      $action_name = \Yii::$app->controller->action->id;
      $controller_name = \Yii::$app->controller->id;
    if($controller_name == 'site' && $action_name == 'test'){
      echo $this->render('//layouts/_header_test', ['setting' => $setting]);
    }else{
      echo $this->render('//layouts/_header', ['setting' => $setting]);
    }
    ?>

    <div id="content" data-name="<?=$controller_name?>"><!--content block starts-->

      <?php if(Yii::$app->session->hasFlash('alert')):?>
        <?= \yii\bootstrap\Alert::widget([
          'body'=>ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'body'),
          'options'=>ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'options'),
        ])?>
      <?php endif; ?>

      <?php echo $content ?>

    </div><!--content block ends-->
  </div><!--warp block ends-->

  <?= $this->render('//layouts/_footer', ['setting' => $setting]) ?>
  <div id="pop-up-subscription">
    <?= $this->render('//layouts/parts/_subscription_pop_up') ?>
  </div>
</div><!-- /#fullpage-->
<?= Html::cssFile('@frontendUrl/assets/production/css/app.min.css?v='.filemtime(Yii::getAlias('@frontend/web/assets/production/css/app.min.css'))) ?>
<?= Html::jsFile('@frontendUrl/assets/production/js/app.min.js?v='.filemtime(Yii::getAlias('@frontend/web/assets/production/js/app.min.js'))) ?>

<?php $this->endBody() ?>
<!-- Home Page scripts -->
<?php if(Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'index'): ?>
  <?= $this->render('//layouts/inline-scripts/_home') ?>
<?php endif; ?>

<!-- Agencies Page scripts -->
<?php if(Yii::$app->controller->id == 'agency' && Yii::$app->controller->action->id == 'view'): ?>
  <?= $this->render('//layouts/inline-scripts/_agencies') ?>
<?php endif; ?>

<!-- Home Page scripts -->
<?php if(Yii::$app->controller->id == 'page'): ?>
  <?= $this->render('//layouts/inline-scripts/_page') ?>
<?php endif; ?>

<!-- Home Page scripts -->
<?php if(Yii::$app->controller->id == 'property' && Yii::$app->controller->action->id == 'view'): ?>
  <?= $this->render('//layouts/inline-scripts/_single-property') ?>
<?php endif; ?>


</body>
</html>
<?php $this->endPage() ?>