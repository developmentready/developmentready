<?php
/**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 8/23/15
 * Time: 19:03
 */

use common\components\helpers\Setup;
use yii\helpers\Url;

?>

<script>
  var markers = new Array();
  var colors = ['orange', 'blue'] //, 'cyan', 'pink', 'deep-purple', 'teal', 'indigo', 'green', 'light-green', 'amber', 'yellow', 'deep-orange', 'brown', 'grey'];

  <?php foreach($properties as $item): ?>
  <?php if(!empty($item->address->latitude) && !empty($item->address->longitude)): ?>
  var description = "<?= preg_replace("/\r\n|\r|\n/",' ', Setup::truncate($item->description, 40,"...")) ?>";
  var color =
    <?php if($item->contract_type_id == 1): ?>
    colors[0];
  <?php elseif($item->contract_type_id == 2): ?>
  colors[1];
  <?php endif; ?>

  markers.push({
    latitude: <?= $item->address->latitude ?>,
    longitude: <?= $item->address->longitude ?>,
    marker_content: '<div class="marker ' + color + '"><img src="<?= Yii::getAlias('@storageUrl') ?>/img/house.png" alt="<?= $item->contractType->translation->title." - ".$item->id ?>"></div>',
    content:
    '<div class="infobox ' + color + ' ">' +
      '<a class="infobox-image" target="_blank" href="<?= Url::to(['/property/view', 'id' => $item->id]) ?>">' +
        '<img src="<?= Yii::getAlias('@storageUrl') ?>/properties/<?= $item->id ?>/thumb/<?= $item->defaultImage['name'] ?>" alt="<?= $item->propertyType->translation->title ." / ". $item->contractType->translation->title." - ".$item->id?>" />' +
      '</a>' +
      '<div class="infobox-content">' +
        '<a target="_blank" href="<?= Url::to(['/property/view', 'id' => $item->id]) ?>">' +
          '<div class="infobox-content-title">' +
            '<?= $item->contractType->translation->title ?>' +
          '</div>' +
          '<div class="infobox-content-price"><?= $item->price->currency_id == 1 ? 'AZN' : '<i class="fa fa-dollar"></i>' ?> <?= $item->price->value ?> <?= $item->contractType->id == 1 ? ", ".$item->period->translation->title : ''?></div>' +
          "<div class='infobox-content-body'>"+ description +"</div>" +
        '</a>' +
    '</div>' +

    '<div class="infobox-contact">' +
      '<div class="infobox-contact-title">' +
        '<a href="#"><?= !empty($item->user_id) ? $item->user->name : '' ?></a>' +
      '</div>' +
      '<div class="infobox-contact-body"><?= $item->address->address ?>' +
        //'<i class="fa fa-phone"></i></div>' +
        '<a href="#" class="close"><i class="fa fa-close"></i></a>' +
      '</div>' +
    '</div>'
  });


  <?php endif; ?>
  <?php endforeach; ?>
</script>
