<?php
/**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 7/15/15
 * Time: 15:31
 */
?>

<div id="directions" class="col-sm-12" style="padding: 5px 0; margin-bottom: 10px;">
  <div id="block-header" class="col-sm-12">
    <h2 class="col-sm-10 pull-left"><?= Yii::t('common','Direction Points') ?></h2>
    <p id="close-button" class="col-sm-2 pull-righ text-right"><i class="fa fa-times"></i></p>
  </div>
  <div id="block-content" class="col-sm-12"></div>
</div>
