<?php
/**
 * Created in PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 7/10/15
 * Time: 20:39
 *
 * @var $this \yii\web\View
 */
use yii\helpers\Url;

# We define this variable in case if we want to show simple header w/o

$cookies = Yii::$app->request->cookies;

$js = <<<'SCRIPT'

 $('#location-of-customer').click(function() {
    $('#locaiton-sub-menu').toggle('slow');
 });

SCRIPT;

$this->registerJs($js, $this::POS_READY);

?>
<header class="header main-page"> <!-- header container starts -->
  <div class="container">
    <div class="row">
      <div class="col-xs-3 main-logo-block">
        <a href="/">
          <img class="main-logo" src="/logo.png">
        </a>
      </div>
      <div id="header-list-menu" >
        <?= \frontend\widgets\Navigation::widget() ?>
      </div><!-- /.navbar-collapse -->
      <div id="register-block" class="col-xs-9">
        <ul class="list-unstyled pull-right">
          <li id="desktop-login-view">
            <?php if(\Yii::$app->user->isGuest): ?>
          <li><a href='/user/login'><span class="glyphicon glyphicon-edit"></span>Login |</a><a href='/user/signup'> Register</a></li>
          <?php else: ?>
            <li><a href='/account/index'><span class="glyphicon glyphicon-user"></span>My Account</a></li>
            <li><a href='/user/logout'><span class="glyphicon glyphicon-log-out"></span>Log Out</a></li>
          <?php endif; ?>
          </li>
          <li>
            <a id="location-of-customer">
              <span class="glyphicon glyphicon-map-marker"></span><span id="current-location-name"><?= ($cookies->has('userStateTitle') && $cookies->has('userState') ) ? $cookies->getValue('userStateTitle') : 'Location' ?></span><span class="glyphicon glyphicon-play"></span>
            </a>
            <ul id="locaiton-sub-menu" class="list-unstyled">
              <li>
                <a rel="nofollow" onclick='
                    $.post("<?= Url::to(['/site/change-state', 'state' => 7]) ?>", function( data ) {
                    $("span#current-location-name").html(data); location.href = "/"; });
                    '> VIC </a>
              </li>
              <li>
                <a rel="nofollow" onclick='
                    $.post("<?= Url::to(['/site/change-state', 'state' => 4]) ?>", function( data ) {
                    $("span#current-location-name").html(data);  location.href = "/"; });
                    '> QLD </a>
              </li>
              <li>
                <a rel="nofollow" onclick='
                    $.post("<?= Url::to(['/site/change-state', 'state' => 3]) ?>", function( data ) {
                    $("span#current-location-name").html(data);  location.href = "/";});
                    '> NSW </a>
              </li>
            </ul>
          </li>
          <li>
            <div id="header-navigation-block">
              <nav class="navbar navbar-default">
                <div class="container-fluid">
                  <!-- Brand and toggle get grouped for better mobile display -->
                  <div class="navbar-header">
                    <button id="show-menu" type="button" class="navbar-toggle closed">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar last-icon"></span>
                      &nbsp; Menu
                    </button>
                    <button id="close-menu" type="button" class="navbar-close">
                      <span>&times;</span> &nbsp; Menu
                    </button>

                  </div>
                  <!-- Collect the nav links, forms, and other content for toggling -->
                </div><!-- /.container-fluid -->
              </nav>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</header> <!-- header container ends -->