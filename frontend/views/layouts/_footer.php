<?php
/**
 * Created by PhpStorm.
 * Created by : Rashad Aliyev (RAliyev@avagr.com)
 * Date: 7/10/15
 * Time: 20:44
 */

use yii\helpers\Url;



?>


<footer id="footer">
  <div class="container">
    <div id="footer-header" class="row">
      <h3 class="col-sm-12"><span>Get</span> in touch</h3>
    </div>
  </div>
  <hr/>
  <div id="footer-content" class="container">
    <div class="row">
      <div id="address-in-footer" class="col-sm-4">
        <h5>Contact Us</h5>
        <ul>
          <?php if(!empty($phone = $setting->findKeyword('phone_number'))): ?>
            <li>Phone: <a href="tel:<?= $phone ?>"><?= $phone ?></a></li>
          <?php endif; ?>
          <?php if(!empty($email = $setting->findKeyword('e_mail'))): ?>
            <li>E-mail: <a href="mailto:<?= $email ?>"><?= $email ?></a></li>
          <?php endif; ?>
          <?php /*if(!empty($skype = $setting->findKeyword('skype'))): ?>
            <li>Skype: <a href="skype:<?= $skype ?>?call"><?= $skype ?></a></li>
          <?php endif; */?>
          <li class="footer-social-icons">
            <?php if(!empty($twitter = $setting->findKeyword('Twitter'))): ?>
              <a href="<?= $twitter ?>" class="twitter" target="_blank"></a>
            <?php endif; ?>
            <?php if(!empty($linkedin = $setting->findKeyword('Linkedin'))): ?>
              <a href="<?=$linkedin?>" class="linkedin" target="_blank"></a>
            <?php endif; ?>
            <?php if(!empty($facebook = $setting->findKeyword('Facebook'))): ?>
              <a href="<?=$facebook ?>" class="facebook" target="_blank"></a>
            <?php endif; ?>
          </li>
        </ul>
      </div>
      <div class="col-sm-4">
        <h5>Register for email alerts</h5>
        <?= \frontend\widgets\Subscription::widget(); ?>
      </div>
      <div class="col-sm-4">
        <div id="twetter-footer-feed">
          <a class="twitter-timeline" href="https://twitter.com/permitready" data-widget-id="663592721085554688"
             data-chrome="noheader nofooter noborders transparent"
             data-theme="dark" data-link-color="#b5985a">Tweets by @permitready</a>
          <script data-cfasync="true" >!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 text-left">
          <a href="/page/about">About Us</a>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-6 text-left">
          <p style="color: #fff; padding-top: 20px; margin-bottom: -20px; text-align: left;" class="text-right small"><a href="http://www.permitready.com.au/page/privacy-policy" target="_blank">Privacy Policy</a></p>
        </div>
        <div class="col-xs-6">
          <p style="color: #fff; padding-top: 20px; margin-bottom: -20px" class="text-right small">Digital by <a href="http://jameshammon.com.au" target="_blank">James Hammon & Co</a></p>
        </div>
      </div>
    </div>
  </div>
</footer><!--footer block ends-->

