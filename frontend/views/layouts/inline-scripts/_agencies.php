<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/11/15
 * Time: 22:00
 */
?>

<script>
  jQuery(document).ready(function(){
    jQuery('.selectpicker').selectpicker();
    $("#type-region-name, #type-region-name-in-home").select2({
      placeholder: "Enter Suburb, Street or Post Code",
      multiple: true,
      closeOnSelect: true,
      ajax: {
        url: "https://api.github.com/search/repositories",
        dataType: 'json',
        delay: 250,
        data: function(params){
          return {
            q: params.term, // search term
            page: params.page
          };
        },
        processResults: function (data, page) {
          // parse the results into the format expected by Select2.
          // since we are using custom formatting functions we do not need to
          // alter the remote JSON data
          return {
            results: data.items
          };
        },
        cache: true
      },
      escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
      minimumInputLength: 1,
      //templateResult: formatRepo, // omitted for brevity, see the source of this page
      //templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
    });
  });

  function initialize(){
    var myLatLng = {lat: agency_lat, lng: agency_lng};
    // Create a map object and specify the DOM element for display.
    var map = new google.maps.Map(document.getElementById('agency-map'), {center: myLatLng,scrollwheel: false,zoom: 12 });
    // Create a marker and set its position.
    var marker = new google.maps.Marker({map: map,position: myLatLng, title: agency_name});
  }

</script>
<script src="http://maps.googleapis.com/maps/api/js?callback=initialize" type="text/javascript"></script>
