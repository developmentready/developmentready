<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/11/15
 * Time: 21:59
 */
?>


<script>

  //$('#map-block').addClass('hide-map');

  $('#testimonials-carousel').owlCarousel({
    margin:10, nav:false, dots : true,
    responsive: false,
    items: 1,
    startPosition: 3, autoplay: true
  });

  $('#open-map').click(function(){
    $("#map-block").delay(200).slideToggle(function(){
      var center = map_object.getCenter();
      google.maps.event.trigger(map_object, "resize");
      map_object.setCenter(center);
    });
    if($(this).hasClass('closed')){
      $(this).removeClass('closed').addClass('open');
      $("#featured-properties, #featured-properties h2.page-header").css('margin-top', '30px');
    } else {
      $(this).addClass('closed').removeClass('open');
      $("#featured-properties, #featured-properties h2.page-header").css('margin-top', '0');
    }
  });

  // youtube script
  // https://developers.google.com/youtube/player_parameters?hl=en
  var tag = document.createElement('script');
  tag.src = "//www.youtube.com/player_api";
  var firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

  var player;

  function onYouTubePlayerAPIReady(){
    player = new YT.Player('about-video', {
      width: '100%',
      videoId: youtube_code,  // youtube video id
      playerVars: {'autoplay': 0, 'rel': 0,'showinfo': 0, origin: ''},
      events: {'onStateChange': onPlayerStateChange}
    });
  }
  var p = document.getElementById ("about-video");
  $(p).hide();

  var t = document.getElementById ("video-thumbnail");
  //t.src = "http://img.youtube.com/vi/OWsyrnOBsJs/0.jpg";
  t.src = video_image;
  onPlayerStateChange = function (event) {
    if (event.data == YT.PlayerState.ENDED) {
      $('.start-video').fadeIn('normal');
    }
  }

  $(document).on('click', '.start-video', function () {
    $(this).hide();$("#about-video").show();
    $("#thumbnail_container").hide();
    if(player){
      if(typeof player.playVideo == 'function'){
        var fn = function(){ player.playVideo(); }
        setTimeout(fn, 1000);
      }
    }
  });
</script>

