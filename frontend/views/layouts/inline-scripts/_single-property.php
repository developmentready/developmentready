<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/12/15
 * Time: 10:50
 */
?>

<script>

  $('#property-in-tabs a').click(function (e) {
    e.preventDefault();
    $(this).tab('show')
  });

  $(document).ready(function() {
    $('.pgwSlideshow').pgwSlideshow({
      displayControls : false,
      //maxHeight : 800,
    });
  });

  var map_object;
  var street_view_object;
  function initialize(){
    var myLatLng = {lat: property_lat, lng: property_lng};
    // Create a map object and specify the DOM element for display.
    var map = new google.maps.Map(document.getElementById('propety-in-map'), {center: myLatLng,scrollwheel: false,zoom: 14 });
    // Create a marker and set its position.
    var marker = new google.maps.Marker({map: map,position: myLatLng, title: property_name});
    map_object = map;

    var fenway = {lat: property_lat, lng: property_lng};
    var street = new google.maps.Map(document.getElementById('propety-street-view'), {center: fenway,zoom: 14});
    var panorama = new google.maps.StreetViewPanorama(document.getElementById('propety-street-view'), {
      position: fenway,
      pov: {heading: 34,pitch: 10}
    });
    street.setStreetView(panorama);
    street_view_object = street;
  }

  $('#show-map a').click(function (e) {
    var center = map_object.getCenter();
    google.maps.event.trigger(map_object, "resize");
    map_object.setCenter(center);
  });

  $('#show-street-view a').click(function (e) {
    var center = street_view_object.getCenter();
    google.maps.event.trigger(street_view_object, "resize");
    street_view_object.setCenter(center);
  });

</script>
<script src="http://maps.googleapis.com/maps/api/js?callback=initialize" type="text/javascript"></script>
