<?php
/**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 11/11/15
 * Time: 22:01
 */

?>

<script>
  $(document).ready(function(){
    $('#testimonial-carousel').owlCarousel({loop:true, nav:false,items: 1, margin: 30, autoHeight:true})
  });

if(video){
  // youtube script
  // https://developers.google.com/youtube/player_parameters?hl=en
  var tag = document.createElement('script');
  tag.src = "//www.youtube.com/iframe_api";
  var firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

  var player;
  onYouTubeIframeAPIReady = function () {
    player = new YT.Player('about-video', {
      width: '100%',
      videoId: video,  // youtube video id
      playerVars: {
        'autoplay': 0,
        'rel': 0,
        'showinfo': 0
      },
      events: {
        'onStateChange': onPlayerStateChange
      }
    });
  }
  var p = document.getElementById ("about-video");
  $(p).hide();
  var t = document.getElementById ("video-thumbnail");
  //t.src = "http://img.youtube.com/vi/OWsyrnOBsJs/0.jpg";
  t.src = video_image;
  onPlayerStateChange = function (event) {
    if (event.data == YT.PlayerState.ENDED) {$('.start-video').fadeIn('normal');}
  }

  $(document).on('click', '.start-video', function () {
    $(this).hide();$("#about-video").show();
    $("#thumbnail_container").hide();
    player.playVideo();
  });
}
</script>
