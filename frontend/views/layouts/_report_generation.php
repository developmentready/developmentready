<?php
/**
 * Created by PhpStorm.
 * User: rashad
 * Date: 4/6/16
 * Time: 00:26
 */

use yii\widgets\ActiveForm;
use \trntv\yii\datetimepicker\DatetimepickerWidget;
use yii\helpers\Html;
use yii\helpers\Url;
//$this->registerJsFile('/assets/production/js/jquery.min.js');

?>
<?php ActiveForm::begin([
  'method' => 'get',
  'action' => Url::current(['Report' => null]),
]) ?>
  <div class="col-xs-4">
    <?= DatetimepickerWidget::widget([
      'model' => $searchModel,
      'attribute' => 'stat_start_date',
      'phpDatetimeFormat' => 'yyyy-MM-dd',
      'clientOptions' => [
        'minDate' => new \yii\web\JsExpression('new Date("2015-01-01")'),
        'allowInputToggle' => false,
        'sideBySide' => true,
        'widgetPositioning' => [
          'horizontal' => 'auto',
          'vertical' => 'auto'
        ]
      ]
    ]); ?>
  </div>
  <div class="col-xs-4">
    <?= DatetimepickerWidget::widget([
      'model' => $searchModel,
      'attribute' => 'stat_end_date',
      'phpDatetimeFormat' => 'yyyy-MM-dd',
      'clientOptions' => [
        'minDate' => new \yii\web\JsExpression('new Date("2015-01-01")'),
        'allowInputToggle' => false,
        'sideBySide' => true,
        'widgetPositioning' => [
          'horizontal' => 'auto',
          'vertical' => 'auto'
        ]
      ]
    ]); ?>
  </div>
  <div class="col-xs-4">
    <?= Html::submitButton('Generate for period', ['class' => 'btn btn-primary','style'=>'background-color: #3c8dbc']) ?>
  </div>
<?php ActiveForm::end() ?>
