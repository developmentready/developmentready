<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/27/15
 * Time: 11:20
 */

//\frontend\assets\SliderAsset::register($this);

$js = <<< 'SCRIPT'

SCRIPT;
$this->registerJs($js);

$storage = \Yii::getAlias('@storageUrl');

$js = <<< 'SCRIPT'
  jQuery(document).ready(function($) {
    var gallery = $('#thumbs').galleriffic({
      delay:                     5000, // in milliseconds
      numThumbs:                 6, // The number of thumbnails to show page
      preloadAhead:              -1, // Set to -1 to preload all images
      enableTopPager:            false,
      enableBottomPager:         false,
      maxPagesToShow:            7,  // The maximum number of pages to display in either the top or bottom pager
      imageContainerSel:         '#slideshow', // The CSS selector for the element within which the main slideshow image should be rendered
      controlsContainerSel:      '', // The CSS selector for the element within which the slideshow controls should be rendered
      captionContainerSel:       '', // The CSS selector for the element within which the captions should be rendered
      loadingContainerSel:       '', // The CSS selector for the element within which should be shown when an image is loading
      renderSSControls:          false, // Specifies whether the slideshow's Play and Pause links should be rendered
      renderNavControls:         false, // Specifies whether the slideshow's Next and Previous links should be rendered
      playLinkText:              'Play',
      pauseLinkText:             'Pause',
      prevLinkText:              'Previous',
      nextLinkText:              'Next',
      nextPageLinkText:          'Next &rsaquo;',
      prevPageLinkText:          '&lsaquo; Prev',
      enableHistory:             false, // Specifies whether the url's hash and the browser's history cache should update when the current slideshow image changes
      enableKeyboardNavigation:  false, // Specifies whether keyboard navigation is enabled
      autoStart:                 true, // Specifies whether the slideshow should be playing or paused when the page first loads
      syncTransitions:           false, // Specifies whether the out and in transitions occur simultaneously or distinctly
      defaultTransitionDuration: 1000, // If using the default transitions, specifies the duration of the transitions
      onSlideChange:             undefined, // accepts a delegate like such: function(prevIndex, nextIndex) { ... }
      onTransitionOut:           undefined, // accepts a delegate like such: function(slide, caption, isSync, callback) { ... }
      onTransitionIn:            undefined, // accepts a delegate like such: function(slide, caption, isSync) { ... }
      onPageTransitionOut:       undefined, // accepts a delegate like such: function(callback) { ... }
      onPageTransitionIn:        undefined, // accepts a delegate like such: function() { ... }
      onImageAdded:              undefined, // accepts a delegate like such: function(imageData, $li) { ... }
      onImageRemoved:            undefined  // accepts a delegate like such: function(imageData, $li) { ... }
    });
  });
SCRIPT;
$this->registerJs($js);

?>

<div id="loading"></div>
<div id="slideshow"></div>
<!--<div id="caption"></div>-->
<div id="thumbs">
  <ul id="logo-slider" class="thumbs noscript">
    <?php foreach($agencies as $agency): ?>
      <?php if(!empty($media = $agency->image)): ?>
      <li>
        <a class="thumb" name="optionalCustomIdentifier" href="<?= $storage.$media->media_path ?>" title="your image title">
          <img data-src="<?= $storage.$media->media_path ?>" alt="Permit ready partner <?= $agency->title ?>" />
        </a>
        <span id="project-path" style="display:none;"><?= "{$agency->slug}" ?></span>
      </li>
      <?php endif; ?>
    <?php endforeach; ?>
  </ul>
</div>
