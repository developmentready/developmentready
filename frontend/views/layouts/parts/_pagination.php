<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/23/15
 * Time: 20:21
 */

use yii\widgets\LinkPager;

?>

<section id="pagination-section">
  <div class="container">
    <div class="row">
      <div id="pagination-block" class="col-sm-12 text-center">
        <?= LinkPager::widget([
          'pagination' => $pages,
        ]); ?>
      </div>
    </div><!--/.row-->
  </div>
</section><!--section ends-->

