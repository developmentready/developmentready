<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/23/15
 * Time: 14:29
 *
 * @var $property common\models\Property
 */

use yii\helpers\Url;

$js = <<< 'SCRIPT'
  $(document).ready(function(){
    $('.message-agent').on('click', function(){
      var parent = $(this).closest('.properties-review-details');
      parent.children('.message-to-agency-block').show('slow');
      $(this).hide('slow');
    });

    $('.close-message').on('click', function(){
      var parent = $(this).closest('.properties-review-details');
      parent.children('.message-to-agency-block').hide('slow');
      parent.find('.message-agent').show('slow');
    });
  });
SCRIPT;
$this->registerJs($js);
$captcha = Yii::$app->security->generateRandomString();
?>
<?php $banner = 1; $banner_counter = 4; ?>
<?php if(!empty($properties)): ?>
  <?php foreach($properties as $property): ?>
    <?php $url = Url::to(["/properties/{$property->slug}"]); ?>
    <div class="col-sm-12">
      <div class="properties-review-details">
        <div class="col-sm-12 listed-property-details-line">
          <h3 class="text-center">
            <?= $property->location ?>
          </h3>
        </div>
        <?php if(true): ?>
          <div class="row">
            <div class="col-sm-7">
              <div class="row">
                <a href="<?= $url ?>">
                  <?php $count = 2; ?>
                  <?php if(!empty($images = $property->imagesWithLimit())): ?>
                    <?php $counter = 1; $count = count($images)?>
                    <?php foreach( $images as $image ): ?>
                      <?= $counter == 1 ? '<div class="col-xs-7">' : ($counter == 2 ? '<div class="col-xs-5 right-thumbnail">' : '') ?>
                      <div class="thumbnail">
                        <div class="overflow-image-block <?= $counter == 1 ? 'default-image' : 'image-thumb' ?>">
                          <?php if($property->sold): ?><div class="ribbon"><span>SOLD</span></div> <?php endif; ?>
                          <?php if($property->under_offer): ?><div class="ribbon ribbon-green"><span>Under Offer</span></div><?php endif; ?>
                          <img class="overflow-image" data-src="<?= $storage."/properties/{$property->id}/$image->file_name" ?>">
                        </div>
                      </div>
                      <?= $counter == 1 ? '</div>' : ($counter == 3 ? '</div>' : '') ?>
                      <?php $counter++; ?>
                    <?php endforeach; ?>
                  <?php else: ?>
                    <?php $count = 3; ?>
                    <?php for($counter = 1; $counter < 2; $counter++): ?>
                      <?= $counter == 1 ? '<div class="col-xs-7">' : ($counter == 2 ? '<div class="col-xs-4 right-thumbnail">' : '') ?>
                      <div class="thumbnail">
                        <div class="overflow-image-block <?= $counter == 1 ? 'default-image' : 'image-thumb' ?>">
                          <img class="overflow-image" src="<?= $storage."/properties/no-image.png" ?>">
                        </div>
                      </div>
                      <?= $counter == 1 ? '</div>' : ($counter == 3 ? '</div>' : '') ?>
                    <?php endfor; ?>
                  <?php endif; ?>
                  <?= $count == 2 ? '</div>' : '' ?>
                </a><!-- /a -->
              </div><!-- /.row -->
            </div><!-- /.col-sm-7 -->
            <div class="col-sm-5">
              <ul>
                <li>
                  <?php if(isset($property->contract_type_id)): ?>
                    <strong><?= $property->permitStatus()[$property->contract_type_id] ?></strong>
                  <?php endif; ?>
                  <span><?= $property->contract_details ?></span>
                </li>
                <li>
                  <div class="row">
                    <div class="col-xs-12">
                      <strong>Id:</strong> #<?= $property->id ?>
                    </div>
                  </div>
                </li>
                <li><strong>Price:</strong> Contact Agent </li>
                <li><strong>Contract:</strong> <?= $property->sold ? 'Sold' : 'Sale' ?> </li>
                <li><strong>Location:</strong> <?= $property->suburb->title.", ".$property->suburb->state->title." ".$property->suburb->post_code ?></li>
                <li><strong>Area:</strong>
                  <?php if($property->square_min){
                          echo 'min - ' . $property->square_min;
                          echo !empty($square = $property->square) ? ', max - ' . $square : '';
                        }else{
                          echo !empty($square = $property->square) ? $square : '';
                        }?>

                  <?= !empty($measure = $property->areaType) ? $measure->title : '' ?></li>
                <li><strong>Type:</strong> <?php $output = ''; foreach($property->propertyTypes as $item){ $output .= $item->title.", ";} $output = rtrim($output, ","); ?> <?= $output ?></li>
                <li><strong><?= $property->propertyPrice->id == 6 ? '$ '.$property->price_in_numbers : $property->propertyPrice->title ?></strong></li>
                <li>
                  <div class="row">
                    <div class="col-xs-6 text-center"><a href="<?= $url ?>" class="btn btn-primary uppercase">View Details</a></div>
                    <div class="col-xs-6 text-center"><p class="btn brand-green uppercase message-agent">Message Agent</p></div>
                  </div>
                </li>
              </ul>
            </div>
          </div>
          <div class="message-to-agency-block">
            <?= $this->render("//layouts/parts/_message_to_agency_inline", ['model' => $message_model, 'message_type' => 1, 'id' => $property->id,'captcha'=>$captcha]) ?>
          </div>
        <?php endif; ?>
      </div>
    </div><!-- /.sol-sm -->
    <?php if($banner % 3 == 0 and Yii::$app->controller->id != 'agency'): ?>
      </div> <!-- /.row -->
      <div class="row text-center">
        <div class="inner-banner-wrapper google-long-middle-banner">
          <!-- /36639447/Inner_leaderboard -->
          <div id='div-gpt-ad-1456820027687-<?= $banner_counter ?>' style='height:90px; width:728px;'>
            <script type='text/javascript'>
              googletag.cmd.push(function() { googletag.display('div-gpt-ad-1456820027687-<?= $banner_counter ?>'); });
            </script>
          </div>
        </div><!-- /.inner-banner-wrapper-->
      </div>
      <div class="row">
      <?php $banner_counter++; ?>
    <?php endif; ?>
    <?php $banner++ ?>
  <?php endforeach; ?>
<?php else: ?>
  <h4 class="text-uppercase col-sm-12 text-danger">
    Sorry, but no properties to show.
  </h4>
<?php endif; ?>
        <style>
          .checkbox_line{
            font-size: 24px;
            color: #555;
            display: inline-block;
            /*float: left;*/
            margin-right: 10px;
            cursor: pointer;
            /*cursor: pointer;*/
          }
        </style>
        <script>
          // Запрет на нажатие кнопки без заполнения каптчи
          $('form #submit').on('click',function(){
            var field = $(this).parents('form').find('#checked');
            if(field.hasClass('glyphicon-unchecked')){
              alert('Captcha is not complete');
              return false;
            }
          });

          $('.checkbox_line').on('click', function () {
            $(this).children('#checked').removeClass('glyphicon-unchecked').addClass('glyphicon-check');
            var str = $(this).parents('form').find('#contactform-check').val();
            $.ajax({
              url: '/site/set-captcha',
              data: {data: str},
              type: 'POST',
              success: function(res){
              }
            });
          });
        </script>


