<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/26/15
 * Time: 20:49
 *
 * @var $this \yii\web\View
 * @var $model \common\models\ContactForm
 */

use yii\helpers\Html;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use himiklab\yii2\recaptcha\ReCaptcha;

$js = <<< 'SCRIPT'
  jQuery(document).on('pjax:send', function() {
    jQuery('#contact-form').hide('slow');
    jQuery(".modal-footer .checkbox").hide('slow');
    jQuery(".modal-footer #submit").hide('slow');
    jQuery('#loading').html('<h2 id="text-success page-header">Sending, please wait....</h2>');
  }).on('pjax:complete', function(e, res) {
    jQuery('#loading').remove(); var messageBlock = jQuery('#contact-box > div');
    var response = jQuery.parseJSON( res.responseText );
    var id = response.property_id;
    var model = response.model_id;
    messageBlock.html(response.message);
    jQuery('.modal-footer').remove();
    sendEmails(id, model);
  });
SCRIPT;

$jsEnd = <<<'SCRIPT1'

function sendEmails(id, model) {
  $.ajax({ url:'/site/send-message-to-agencies?id=' + id + '&model=' + model, type:'POST', dataType: 'json',
    success: function( json ) {
      console.log(json);
    }
  });
}

SCRIPT1;

$this->registerJs($jsEnd, $this::POS_END);
$this->registerJs($js);

?>
<script>var property_id = <?= $id ?></script>

<div id="message-agency" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Message to agency</h4>
      </div>
      <div id="contact-box" class="modal-body">
        <div id="loading" class="text-center text-success"></div>
        <?php Pjax::begin([
          'clientOptions' => [
            'method' => 'POST',
            'container' => 'x1',
          ],
          'timeout' => 1000, 'enablePushState' => false,
        ]); ?>
        <?php $form = ActiveForm::begin([
          'id' => 'contact-form',
          //'enableAjaxValidation' => true,
          'action' => \yii\helpers\Url::to(['/site/contacts', 'id' => $id, 'type' => $message_type]),
          'options' => ['data-pjax' => '#x1' ]
        ]); ?>
        <div class="form-group">
          <div class="row">
            <div class="col-sm-4">
              <?= $form->field($model, 'name')->textInput(['placeholder' => \Yii::t('frontend','Name')." *", 'class' => 'form-control'])->label(false) ?>
            </div>
            <div class="col-sm-4">
              <?= $form->field($model, 'email')->textInput(['placeholder' => \Yii::t('frontend','Email')." *", 'class' => 'form-control'])->label(false) ?>
            </div>
            <div class="col-sm-4">
              <?= $form->field($model, 'phone_number')->textInput(['placeholder' => \Yii::t('frontend','Phone Number'), 'class' => 'form-control'])->label(false) ?>
            </div>
          </div>
        </div><!-- /.form-group -->
        <div class="form-group">
          <?=
          $form->field($model, 'subjects[]')->widget(Select2::className(),[
            'data' => $model->getSubjects(),
            'options' => ['placeholder' => 'Please select subject * ', 'multiple' => true],
          ])->label(false);
          ?>
        </div>
        <div class="form-group">
          <?= $form->field($model, 'message')->textarea(['placeholder' => \Yii::t('frontend','Body'), 'class' => 'form-control', 'rows' => 5])->label(false) ?>
          <?= $form->field($model, 'check')->hiddenInput(['value'=>Yii::$app->security->generateRandomString()])->label(false) ?>
        </div><!-- /.form-group -->
      </div>
      <div class="modal-footer" style="padding:10px;">
        <div class="checkbox">
          <span id="checked" class="glyphicon glyphicon-unchecked"></span> <span class="pointer">I'm not a robot</span>
        </div>
        <button type="button" class="btn btn-default" rel="nofollow" data-dismiss="modal">Close</button>
        <?= Html::submitButton(\Yii::t('frontend','Send Message'), ['id'=>'submit', 'rel'=>'nofollow' ,'class' => 'btn brand-green btn-view-details']) ?>
      </div>
      <?php ActiveForm::end(); ?>
      <?php Pjax::end(); ?>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<style>
  .checkbox{
    font-size: 24px;
    color: #555;
    display: block;
    /*float: left;*/
    margin-right: 5px;
  }
  .checkbox span{
    cursor: pointer;
  }
</style>
<?php
$js2 = <<<'SCRIPT2'
  // Запрет на нажатие кнопки без заполнения каптчи - _message_to_agency
  $('#message-agency #submit').on('click',function(){
    var field = $('.checkbox #checked');
    if(field.hasClass('glyphicon-unchecked')){
      alert('Captcha is not complete');
      return false;
    }
  });

  $('#message-agency .checkbox').on('click', function () {
    $(this).children('#checked').removeClass('glyphicon-unchecked').addClass('glyphicon-check');
    var str = $('#contactform-check').val();
    $.ajax({
      url: '/site/set-captcha',
      data: {data: str},
      type: 'POST',
      success: function(res){
      }
    });
  });
SCRIPT2;
$this->registerJs($js2);
?>
