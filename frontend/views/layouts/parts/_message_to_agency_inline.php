<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/26/15
 * Time: 20:49
 *
 * @var $this \yii\web\View
 */

use yii\helpers\Html;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

$js = <<< 'SCRIPT'
$(document)
  .on('submit', '[id|="contact-form"][data-pjax]', function(event) {
    jQuery(this).find('.form-group').hide('slow');
    jQuery(this).find('.checkbox_line').hide('slow');
    jQuery(this).find(".message-to-agency-block #submit").hide('slow');
    jQuery(this).find('#loading').html('<h2 id="text-success page-header text-center">Sending, please wait....</h2>');
    jQuery(this).find(".send-message").hide('slow');
  }).on('pjax:complete', function(e, res) {
    jQuery('#loading').remove(); var messageBlock = jQuery('.message-to-agency-block > div');
    var response = jQuery.parseJSON( res.responseText );
    var id = response.property_id;
    var model = response.model_id;
    messageBlock.html(response.message);
    sendEmails(id, model);
  });
SCRIPT;

$jsEnd = <<<'SCRIPT1'

function sendEmails(id, model) {
  $.ajax({ url:'/site/send-message-to-agencies?id=' + id + '&model=' + model, type:'POST', dataType: 'json',
    success: function( json ) {
      console.log(json);
    }
  });
}

SCRIPT1;

$this->registerJs($js);
$this->registerJs($jsEnd, $this::POS_END);

?>

<?php Pjax::begin([
  'clientOptions' => [
    'method' => 'POST',
    'container' => 'x1',
  ],
  'timeout' => 1000, 'enablePushState' => false,
]); ?>
  <?php $form = ActiveForm::begin([
    'id' => 'contact-form-'.$id,
    'action' => \yii\helpers\Url::to(['/site/contacts', 'id' => $id, 'type' => $message_type]),
    'options' => ['data-pjax' => '#x1' ]
  ]); ?>
    <div class="row">
      <div class="col-sm-12">
        <h4 class="text-center uppercase">Message to agency</h4>
      </div>
      <div id="loading" class="text-center text-success"></div>
      <div class="col-sm-6">
        <div class="form-group">
          <?= $form->field($model, 'name')->textInput(['placeholder' => \Yii::t('frontend','Name')." *", 'class' => 'form-control'])->label(false) ?>
        </div>
        <div class="form-group">
          <?= $form->field($model, 'email')->textInput(['placeholder' => \Yii::t('frontend','Email')." *", 'class' => 'form-control'])->label(false) ?>
        </div>
        <div class="form-group">
          <?= $form->field($model, 'phone_number')->textInput(['placeholder' => \Yii::t('frontend','Phone Number')." *", 'class' => 'form-control'])->label(false) ?>
        </div><!-- /.form-group -->
        <div class="form-group">
          <?=
          $form->field($model, 'subjects[]['.$id.']')->widget(Select2::className(),[
            'data' => $model->getSubjects(),
            'options' => ['placeholder' => 'Please select subject * ', 'multiple' => true],
          ])->label(false);
          ?>
        </div><!-- /.form-group -->
      </div><!-- /.col -->
      <div class="col-sm-6">
        <div class="form-group">
          <?= $form->field($model, 'message')->textarea(['placeholder' => \Yii::t('frontend','Body'), 'class' => 'form-control', 'rows' => 9])->label(false) ?>
          <?= $form->field($model, 'check')->hiddenInput(['value'=>$captcha])->label(false) ?>

        </div><!-- /.form-group -->
      </div>
      <div class="col-sm-12 text-right">
        <div class="checkbox_line">
          <span id="checked" class="glyphicon glyphicon-unchecked"></span> I'm not a robot
        </div>
        <?= Html::submitButton(\Yii::t('frontend','Send Message'), ['id'=>'submit', 'rel'=>'nofollow' ,'class' => 'btn brand-green uppercase send-message']) ?>
        <button type="button" class="btn btn-danger close-message  uppercase" rel="nofollow" data-dismiss="modal">Close</button>
      </div><!-- /.col -->
    </div><!-- /.row -->
  <?php ActiveForm::end(); ?>
<?php Pjax::end(); ?>