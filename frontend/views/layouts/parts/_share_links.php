<?php

/**
 * Created by PhpStorm.
 * User: rashad
 * Date: 5/16/16
 * Time: 10:53
 */


?>
<div class="row text-right ">
  <ul id="socials-in-content" class="col-xs-12 list-unstyled">
    <li class="share-text-li">
      <span class="share-text">Share on: </span>
    </li>
    <li class="share-fb-li">
      <!-- Load Facebook SDK for JavaScript -->
      <div id="fb-root"></div>
      <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6&appId=1714531232134789";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

      <!-- Your share button code -->
      <div class="fb-share-button" data-href="<?= $url ?>" data-layout="button" data-mobile-iframe="true"></div>
    </li>
    <li class="share-linkedin-li">
      <script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: en_US</script>
      <script type="IN/Share" data-url="<?= $url ?>"></script
    </li>
    <li class="share-tweetter-li">
      <a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
      <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
    </li>
    <li class="share-email-li">
      <a class="ci ci-send-email" title="Send e-mail"
        href="mailto:info@permitready.com.au?body=View this property here: <?= $url ?>"></a>
    </li>
  </ul>
</div>