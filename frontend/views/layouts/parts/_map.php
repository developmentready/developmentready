<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/25/15
 * Time: 00:00
 */

use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

$js = <<< 'SCRIPT'

$("#checkAll").change(function(){
  $("#maps-suburb-block .checkbox input:checkbox").prop('checked', $(this).prop("checked"));
});

$( document ).ajaxComplete(function() {
  $("#checkAll").change(function(){
     $("#maps-suburb-block .checkbox input:checkbox").prop('checked', $(this).prop("checked"));
  });
});

google.maps.event.addDomListener(window, 'load', function(){
	var currCenter = map_object.getCenter();

	setTimeout(function(){
		$('#searching-map-block').css('width', '99.5%'); 
		google.maps.event.trigger(map_object, 'resize');
		map_object.setCenter(currCenter);
	}, 100);
});

SCRIPT;
$this->registerJs($js);

$storage = \Yii::getAlias('@storageUrl');

?>

<?php if( \Yii::$app->controller->id == 'property' &&  \Yii::$app->controller->action->id == 'map-data'): ?>
  <meta name="robots" content="noindex,follow">
<?php endif; ?>

<script src="http://maps.googleapis.com/maps/api/js" type="text/javascript"></script>
<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDIHohGtPy4y-WrVz-0PjiRToggENa5cxg" type="text/javascript"></script>-->
<script>var map_object;</script>

<section id="map-block-section">
  <div class="container">
    <div id="map-block">
      <div class="row">
        <div class="col-sm-12 text-center">
          <ul class="regions-list-block">
            <?php foreach(\common\models\State::find()->active()->where(['country_id' => 13])->all() as $item ): ?>
              <li class="<?= $item->id == $state['id'] ? 'active' : '' ?>" >
                <a rel="nofollow" href="<?= Url::to(["/state/".strtolower($item->title)]) ?>"><?= $item->title ?></a>
              </li>
            <?php endforeach; ?>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <div id="searching-map-block" style="height:100%; width:100%;">
            <div id="searching-map"></div>
          </div>
        </div>
        <div class="col-sm-6">
          <?php $form = ActiveForm::begin([
            'id' => 'search-in-map',
            'method' => 'GET',
            'action' => ["property/search-map-data"],
            //'options' => ['data-pjax' => true ]
          ]); ?>

          <input type="hidden" name="PropertySearch[state]" value="<?= $state['id'] ?>">
          <?=
          // Multiple select without model
          Select2::widget([
            'id' => 'map-select',
            'initValueText' => '',
            'name' => 'PropertySearch[suburb_ids][]',
            'options' => ['multiple' => true,
              'placeholder' => 'Select a suburb ...', 'id' =>'type-region-name-in-home'],
            'pluginOptions' => [
              'allowClear' => true,
              'minimumInputLength' => 3,
              'ajax' => [
                'url' => Url::to(['/property/state-suburbs-list', 'state' => $state['id']]),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
              ],
              'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
              'templateResult' => new JsExpression('function(suburb) { if(suburb.post_code == 0) suburb.post_code = ""; return suburb.text + ", "+ suburb.state +" " + suburb.post_code ; }'),
              'templateSelection' => new JsExpression('function (suburb) { if(suburb.post_code == 0) suburb.post_code = ""; return suburb.text + " " + suburb.post_code; }'),
            ],
          ]);
          ?>

          <div class="row">
            <div class="col-xs-12">
              <p>Or manually select suburbs</p>
            </div>
          </div>
          <div id="maps-suburb-block" class="suburbs-block">
            <div class="col-xs-12" style="border-bottom: 1px solid #020202; padding-left: 20px">
              <input id="checkAll" type="checkbox" /> <label for="checkAll" class="btn btn-link" >Check All</label>
            </div>
            <?php $properties_list = \common\models\Property::getStatesProperties($state['id'])?>
            <?php if($properties_list): ?>
              <?php foreach( $properties_list as $property): ?>
                <?php $id = $property->suburb->id; ?>
                <div class="checkbox col-xs-6">
                  <input id="suburb-<?= $id ?>" name="PropertySearch[suburb_ids][]" value="<?= $id ?>" type="checkbox" />
                  <label for="suburb-<?= $id ?>"><?= $property->suburb->title ?></label>
                </div>
              <?php endforeach; ?>
            <?php else: ?>
              <p class="text-warning text-uppercase">No properties for this state</p>
            <?php endif; ?>
          </div><!-- /.suburbs-block -->
          <div class="">
            <div class="checkbox-block pull-left">
              <input id="checkbox14" type="checkbox" name="PropertySearch[contract_type_ids][]" value="1" checked/>
              <label for="checkbox14">Permit Approved</label>
            </div>
            <div class="checkbox-block pull-right">
              <input id="checkbox15" type="checkbox" name="PropertySearch[contract_type_ids][]" value="0" checked/>
              <label for="checkbox15">Permit Potential</label>
            </div>
          </div><!-- /.row -->

          <div class="row">
            <div class="col-xs-12">
              <?= Html::submitButton('Search Now', ['class' => 'btn brand-green search-property-btn']) ?>
            </div>
          </div><!-- /.row -->
          <?php ActiveForm::end(); ?>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /#map-block -->
  </div>
  <script>
    var map_lat = <?= empty($def_lat = $state['lat']) ? -37.812000 : $def_lat ?>;
    var map_lng = <?= empty($def_lng = $state['lng']) ? 144.963203 : $def_lng ?>;
    var properties_object = '';
    var properties = [];
    var infoWindowContent = [];

    <?php foreach($properties as $item): ?>

    <?php if(!empty($lat = $item->lat) && !empty($item->lng)): ?>
    properties.push([
      '<?= $item->location ?>' , <?= $item->lat ?>, <?= $item->lng ?>, <?= $item->id ?>
    ]);

    infoWindowContent.push([
      "' <div class='info_content'><div class='col-xs-5'> <img src='<?= !empty($image = $item->defaultImage) ? $storage."/properties/{$item->id}/".$image->file_name : $storage."/properties/no-image.png" ?>' width='100%' /> </div><h5 class='col-xs-7'>"+ '<?= $item->suburb->title ?>' + " (" + '<?= $item->suburb->post_code ?>'
      + "), " + '<?= $item->suburb->state->title ?>' + "</h5> <p>" + '<?= $item->location ?>' + "</p>"
      + "<a href='/properties/" + '<?= $item->slug ?>' +"'> Link to property </a>"
    ]);
    <?php endif; ?>
    <?php endforeach; ?>

  </script>
  <script>
    $(document).ajaxComplete(function(){
      createMap();
    });

  </script>
</section>

<script>

  createMap();

  function createMap(){
    var map = new google.maps.Map(document.getElementById('searching-map'),{
      zoom: 7,
      scrollwheel: false,
      center: {lat: map_lat, lng: map_lng},
      //mapTypeId: google.maps.MapTypeId.SATELLITE
    });
    setMarkers(map);
    // We define this variable as a global variable which we will call from other place
    map_object = map;

    function setMarkers(map) {
      // Origins, anchor positions and coordinates of the marker increase in the X
      // direction to the right and in the Y direction down.
      var image = {
        url: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAEZklEQVRYR8WWXUxbZRjHn/ecnlLaHmjJkA8BAYeTtcQPhqMF1jOi2QcjmYkYl5i4xBvujBfswiWyuGTGhejlYnah0ZvpYlADW4gaitAWhngxCg4dCwiUUeWjPf045Xy85rAQT1nb09PN2Ms+7////53nvO9zXgT/8w9pzZ/psumtUWs7QtgBCFWABAghCAAB46xA/1B382ZCi2fWALixkbpfbngHMD4HQBSnCdnAAJ8kIrq+GrebywYkK4C/jrWVCSR8BwRuysYUI5gGUeosv+FdVFuvChDoZPYhvO0BIJ5RM1PWMYZlisSHi7/3BDLpVAFWT7UMAqCTWsL/XYtHSwc8LgSA0+kzAqx2tHUAwgOpxKKEIS4KO9YGHQk6gkiZgTA6Uzo4ei0ngJXjrR5CB86k1gKGtWgcwlwCMEhrEhASCVBWYMiDElM+IEh+JhGwv2LA06AZINjFmPkYHyIQSnq0ABsFlt/+nCSIi21j0/dk4+HmZ6sRQfaaKersk7T5oSxESSWl/d5gKoi0r+B+J9OMseBTimK8AEthtpfxzXyQymzYab9QSZt6TRSVVBZ56VTFkHdQE8Dvr7zUTefpryhFf0diK3b3r5XpNhUGIKaPvrBcbDKVKXVRXnx//5DvoiaA2ZcP9VgNhstK0XqU+8o+/MsbmU7ETHvT10XGvC7lmi0u0Vf/42SPJoAp5sU3y83GL5WiCJ/4pm5o8rVMAPPHDvcbKeq0ck0wEu95zj3VpwlgpNleX11UMKtXHC9exOuVQa4MTU3xqcz+OHEiLx9vrZI60rpbFzGGu5vrTsZ7J2k/7dbTbkL5ffqZxvA+c75JGYYArpYcGutGF0BKPp6AVjtaryIEbyv/3+BiQlDYoI+6F1J+GzIOop8dB6/UWC3dDw0ZSRqRCLikJ5CPx5xIIkOLJODzQBAuZbiEMSxuhr91eqdf1TwHZMFYq63KrKPulpjNyecq0yZQ1IJxDq9HEk3tE9NTOQHIIrej4eMaC/2unkw9atMZC5IE81uha4x35kwmXtWPkddhK6JI3Z8VFjppL6g1IcBGtyMxfr9r0r/0SAA7XWi2nauy0B/l63RquTv1hCDCQij0IeObfU9NoNoB2WCYqTYYpcLFqgL6CTVDub7MspsJgXuq1TPHqq3PCmAHotl2tqLQ/Jl5z5zfGxDleVgJs90u3+ynauFyPWsAeS54nfY7tVZLXTqVfOtYDIXnl0oPHHj9+nXxsQLIZiMt9SdLjfRgQZ4+pXeI24a1ONvh8vx2I5twTR3YNfS22bzVhRbH3ouHPHQWttjRFs/tI9mG5wTgdtqaio35t4oMhqScYCyOQ9HE80cm/Lf/UwDZfNRp76+1Fp4m0IMtJEgY7m2GvnD5/G9pCc+pA7LoJ6f96UIdMVdG0yTGAIFIJLEtcrWtnrmMV/BUcFmfgr1it+PgZQD04JKB0Xlm3H9J69Pn3IHdILezwSFJotQ+PjuRS/gjA+QaqtTl/AoeR7js8Q95q5QwY5WjqwAAAABJRU5ErkJggg==',
        // This marker is 24 pixels wide by 24 pixels high.
        size: new google.maps.Size(32, 32),
        // The origin for this image is (0, 0).
        origin: new google.maps.Point(0, 0),
        // The anchor for this image is the base of the flagpole at (0, 32).
        anchor: new google.maps.Point(0, 32)
      };
      // Shapes define the clickable region of the icon. The type defines an HTML
      // <area> element 'poly' which traces out a polygon as a series of X,Y points.
      // The final coordinate closes the poly by connecting to the first coordinate.
      var shape = {
        coords: [1, 1, 1, 20, 18, 20, 18, 1],
        type: 'poly'
      };


      // Display multiple markers on a map
      var infoWindow = new google.maps.InfoWindow(), marker, i;

      for (var i = 0; i < properties.length; i++) {
        var property = properties[i];
        var marker = new google.maps.Marker({
          position: {lat: property[1], lng: property[2]},
          map: map,icon: image,
          shape: shape,title: property[0],
          zIndex: property[3]
        });

        // Allow each marker to have an info window
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {
            infoWindow.setContent(infoWindowContent[i][0]);
            infoWindow.open(map, marker);
          }
        })(marker, i));
      }
    }
  }

</script>