<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/22/15
 * Time: 21:04
 */

use yii\helpers\Url;
use common\components\helpers\Setup;

$js = <<< 'SCRIPT'
  $('#property-carousel, #latest-carousel').owlCarousel({
    autoplay:true, autoplayHoverPause:true, autoplayTimeout:3000, nav:true, navText: ['',''], dots : false,
    //autoWidth:true, lazyLoad:true, loop:true,
    loop:true,
    responsive:{
      0:{items:1, margin:0},
      600:{items:2,margin:10},
      1000:{items:3, margin:10}
    }
  });
  
  $('#property-carousel .owl-item img').each(function(){
    var src = $(this).attr('data-src');
    $(this).attr('src',src);  
  });
  
  $('#latest-carousel .owl-item img').each(function(){
    var src = $(this).attr('data-src');
    $(this).attr('src',src);  
  });
  
SCRIPT;
$this->registerJs($js);

?>

<?php foreach($properties as $item): ?>
  <?php if(!empty($media = $item->defaultImage)): ?>
    <?php $url = Url::to(["/properties/{$item->slug}"]) ?>
    <div class="item property-thumbnail">
      <a class="property-thumbnail-link" href="<?= $url ?>">
        <div class="overflow-image-block">
          <?php if($item->sold): ?><div class="ribbon"><span>SOLD</span></div> <?php endif; ?>
          <?php if($item->under_offer): ?><div class="ribbon ribbon-green"><span>Under Offer</span></div><?php endif; ?>
          <?php $image = $storage."/properties/{$item->id}/".$media->file_name ?>
          <img data-src="<?= $image ?>" class="overflow-image" />
          <div class="property-image-details">
            <div class="pull-left col-xs-7">
              <p class="property-name"><?= Setup::truncate($item->location, 60, '...')  ?></p>
<!--              <p class="property-address">--><?php # echo $item->suburb->title.", ".$item->suburb->state->title." ".$item->suburb->post_code ?><!--</p>-->
              <div class="property-features">
                <?php foreach($types = $item->propertyTypes as $type): ?>
                  <span class="ci ci-<?= strtolower($type->title) ?>"></span>
                <?php endforeach; ?>
              </div><!-- /.property-futures -->
            </div><!-- /.col -->
            <div class="text-right col-xs-5">
              <div class="contract-type">
                <p><?= $item->propertyPrice->id == 6 ? '$ '.$item->price_in_numbers : $item->propertyPrice->title ?></p>
              </div>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div>
        <div class="image-overlay">
          <div class="text-center">

            <?php if(($item->contract_type_id == 1) || ($item->contract_type_id == 0)): ?>
              <h5 class="property-address"><?= $item->permitStatus()[$item->contract_type_id] ?></h5>
              <?= !empty($details = $item->contract_details) ? "<p>". Setup::truncate($details, 40, "...")."</p><br/>" : '' ?>
            <?php else: ?>
              <h5 class="property-address"><?= $item->location ?></h5>
            <?php endif; ?>
            <p class="property-features">
              <?php foreach($types as $type): ?>
              <span class="ci ci-<?= strtolower($type->title) ?>"></span><?= $type->title ?>
              <?php endforeach; ?>
            </p>
            <h5 class="contract-type"><?= $item->propertyPrice->id == 6 ? '$ '.$item->price_in_numbers : $item->propertyPrice->title ?></h5>
          </div>
        </div> <!-- /.overlay -->
      </a>
    </div><!-- /.property-thumbnail -->
  <?php endif; ?>
<?php endforeach; ?>
