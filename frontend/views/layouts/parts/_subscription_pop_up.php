<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/26/15
 * Time: 20:49
 */

// get the cookie collection (yii\web\CookieCollection) from the "request" component
$cookies = Yii::$app->request->cookies;

if (!$cookies->has('subscribed')) {
$js = <<< 'SCRIPT'
$(window).load(function(){
  setTimeout(function(){ $('#subscription-pop-up').modal('show'); }, 30000);
});
SCRIPT;
  $this->registerJs($js);
}
?>

<div id="subscription-pop-up" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"
          onclick="$.post('/site/close-subscription')"
          ><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center">Keep up-to-date with the latest development opportunities in your state</h4>
      </div>
      <div id="contact-box" class="modal-body">
        <?= \frontend\widgets\PopUp::widget() ?>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->