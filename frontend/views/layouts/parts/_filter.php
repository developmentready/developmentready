<?php
/**
 * Created by PhpStorm.
 * User: rashad
 * Date: 12/16/15
 * Time: 00:20
 */

use yii\helpers\Html;
use yii\helpers\Url;

$js = <<< 'SCRIPT'
  jQuery(function ($) {
    var checkList = $('.dropdown-check-list');
    checkList.on('click', 'span.anchor', function(event){
      var element = $(this).parent();
      if ( element.hasClass('visible')){
        element.removeClass('visible');
      } else {
        element.addClass('visible');
      }
    });
  });
SCRIPT;
$this->registerJs($js);


$keys = [];

if(!empty($request = \Yii::$app->request->get('PropertySearch'))){

  if(array_key_exists('sort', $request)){
    $search_model->sort = $request['sort'];
  }

  if(array_key_exists('suburb_id', $request)){
    $suburb_ids = $request['suburb_id'];

    if(!empty($suburb_ids)){
      foreach($suburb_ids as $k => $v){
        array_push($keys, $v);
      }
    }
  }

}
?>

<?= Html::beginForm(Url::current(['PropertySearch' => null, 'page' => null, 'per-page' => null]), 'get') ?>

<div id="filter-box" class="row">
  <div class="col-sm-4">
    <?= Html::activeDropDownList($search_model, 'sort',
      [1 => 'Oldest First', 0 => 'Newest First'], ['class' => 'form-control']) ?>
  </div>
  <div class="col-sm-5">
    <div id="list3" class="dropdown-check-list" tabindex="100">
      <span class="anchor">Location</span>
      <ul class="items">
        <?php foreach($properties as $property): ?>
          <?php $suburb_id = $property->suburb->id; ?>
          <li>
            <input id="suburb_<?= $suburb_id ?>" name="PropertySearch[suburb_id][]"
                   type="checkbox" value="<?= $suburb_id ?>" <?= in_array($suburb_id, $keys) ? 'checked' : '' ?> />
            <label for="suburb_<?= $suburb_id ?>"><?= $property->suburb->title ?>, <?= $property->suburb->post_code ?></label>
          </li>
        <?php endforeach; ?>
      </ul>
    </div>
  </div>
  <div class="col-sm-3">
    <?= Html::submitButton('Filter', ['class' => 'btn btn-primary full-width uppercase']) ?>
  </div>
</div>
<?= Html::endForm() ?>