<?php
/**
 * Created by PhpStorm.
 * User: rashad@jameshammon.com.au
 * Date: 1/4/16
 * Time: 10:40
 */

use yii\helpers\Html;
/* @var $this \yii\web\View */
/* @var $content string */

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?php echo Yii::$app->language ?>">
<head>
  <meta charset="<?php echo Yii::$app->charset ?>"/>
  <meta name="dcterms.rightsHolder" content="Permit Ready Company"/>
  <?php $this->head() ?>
  <?php echo Html::csrfMetaTags() ?>
</head>
<body>
<div id="fullpage">
  <div id="wrap" ><!--container block starts-->
    <?php $this->beginBody() ?>
    <div id="content"><!--content block starts-->
      <?= $content ?>
    </div><!--content block ends-->
  </div><!--warp block ends-->
</div><!-- /#fullpage-->
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
