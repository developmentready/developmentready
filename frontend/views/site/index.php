<?php
/* @var $this yii\web\View */
/* @var $site common\models\Page */
/* @var $about common\models\Page */
/* @var $featured_properties common\models\Property */
/* @var $map_properties common\models\Property */
/* @var $featured_agencies common\models\Agency */
/* @var $state common\models\State */
/* @var $testimonials common\models\Page */

use yii\helpers\Url;
use common\components\helpers\Setup;
use yii\helpers\ArrayHelper;



$this->title = $site['title'];

$this->registerMetaTag(['name' => 'keywords', 'content' => $site['meta_keywords']]);
$this->registerMetaTag(['name' => 'description', 'content' => $site['meta_description']]);
if(!$site['page_robot_indexing'])
  $this->registerMetaTag(['name' => 'robots', 'content' => 'noindex,follow']);

$storage = Yii::getAlias('@storageUrl');


$js = <<<'SCRIPT'

var isIOS = /iPad|iPhone|iPod/.test(navigator.platform);
if (isIOS) {
  var canvasVideo = new CanvasVideoPlayer({
    videoSelector: '.video',
    canvasSelector: '.canvas',
    timelineSelector: false,
    autoplay: true,
    makeLoop: true,
    pauseOnClick: false,
    audio: false
  });
} else {
  // Use HTML5 video
  $('.canvas').hide();
  //document.querySelectorAll('.canvas')[0].style.display = 'none';
}

SCRIPT;

$this->title = $site['title'];

$this->registerJs($js);
$this->registerJsFile("/assets/production/js/canvas-video-player.min.js");

?>

<script>
  var youtube_code = '<?= $about->video_link ?>' ;
  var video_image = '<?= !empty($media = $about->image) ? $storage.$media->media_path : '' ?>';
</script>
<style>

</style>

<!-- Home On Image Section
---------------------------------------------------------------------->
<?php
$js = <<< 'SCRIPT'
$(document).ready(function(){
$('video').html('<source src="/img/video/home.mp4" type="video/mp4">'+
      '<source src="/img/video/home.ogv" type="video/ogg">'+
      '<source src="/img/video/home.webm" type="video/webm">'+
      'Your browser doesn\'t support HTML5 video. Here\'s a <a href="/img/video/home.mp4">link</a> to download the video.');
});
SCRIPT;
$this->registerJs($js);
?>

<section id="home" data-type="background" data-speed="10">
  <div id="video-wrapper">
    <video class="video" muted="muted" loop="loop" autoplay="autoplay" poster="/img/video/background.jpg">
<!--      <source src="/img/video/home.mp4" type="video/mp4">-->
<!--      <source src="/img/video/home.ogv" type="video/ogg">-->
<!--      <source src="/img/video/home.webm" type="video/webm">-->
<!--      Your browser doesn't support HTML5 video. Here's a <a href="/img/video/home.mp4">link</a> to download the video.-->
    </video>
  </div>
  <div id="home-page-header-container-wrapper">
    <div id="home-page-header-container">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 text-center">
            <h3 class="slogan">Permit approved and permit potential</h3>
            <h1 class="text-center introduction-header h1">
              <?= $site['h1_tag']?>
            </h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container -->
    </div><!-- /#home-page-header-container -->
    <div id="home-page-header-bottom-container">
      <div id="header-search-box-container">
        <?= \frontend\widgets\SearchBar::widget(); ?>
      </div><!-- /#header-search-box-container -->
    </div>
  </div>
</section>

<!-- MAP SECTION
---------------------------------------------------------------------->
<section id="map-section">
  <div class="container">
    <div class="map-open-button-box-container row">
      <div class="col-sm-2 col-sm-offset-5 map-open-button-box-block">
        <p class="text-center">
          <span class="ci ci-map"></span><span class="map-text">Map</span><span id="open-map" class="ci ci-arrow-down closed"></span>
        </p>
      </div>
    </div>
  </div>
</section>

<?= $this->render("//layouts/parts/_map", ['state' => $state, 'properties' => $map_properties]) ?>

<!-- FEATURED PROPERTIES
---------------------------------------------------------------------->
<section id="featured-properties">
  <div class="container">
    <div class="row">
      <div style="margin-top: 25px;">&nbsp;</div>
      <h2 class="page-header text-left col-xs-12"><span>Featured</span> Development Sites</h2>
    </div><!--/.row-->
    <div id="property-carousel">
      <?= $this->render("//layouts/parts/_properties_carousel", ['properties' => $featured_properties, 'storage' => $storage]) ?>
    </div> <!--/#property-carousel-->
  </div><!--/.container-->
</section><!--section ends-->

<!-- AGENCIES LOGOs
---------------------------------------------------------------------->
<section id="agencies-logos">
  <div class="container">
    <div class="row">
      <h2 class="page-header text-left col-xs-12"><span>TRUSTED</span> by</h2>
    </div><!--/.row-->
    <div class="row">
      <div class="col-sm-12 col-md-8">
        <?= $this->render("//layouts/parts/_slider", ['agencies' => $featured_agencies]) ?>
        <div class="clearfix"></div>
      </div>
      <div class="col-sm-12 col-md-4">
        <!--           <script type="text/javascript" src="http://permitready.com.au/assets/widgets/widget.js?v=1.0"></script> -->
        <!--class="google-square-banner banner"-->
        <!-- /36639447/Homepage_logo_square -->
        <div id='div-gpt-ad-1456820027687-2' style='height:280px; width:336px;'>
          <script type='text/javascript'>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1456820027687-2'); });
          </script>
        </div>
      </div>
    </div><!--/.row-->
  </div><!--/.container-->
</section><!--section ends-->

<!-- Separator
---------------------------------------------------------------------->
<hr class="section-separator"/>

<!-- LATEST LISTINGS
---------------------------------------------------------------------->
<section id="latest-properties">
  <div class="container">
    <div class="row">
      <h2 class="page-header text-left col-xs-12"><span>Latest</span> Listings</h2>
    </div><!--/.row-->
    <div id="latest-carousel">
      <?= $this->render("//layouts/parts/_properties_carousel", ['properties' => $latest_properties, 'storage' => $storage]) ?>
    </div> <!--/#property-carousel-->
  </div><!--/.container-->
</section><!--section ends-->

<!-- SECTION BLOG
---------------------------------------------------------------------->
<section id="home-page-blog-section">
  <div class="container">
    <div id="home-page-blog-header" class="row">
      <div class="col-lg-8 col-md-12 pull-right text-right">
        <!-- /36639447/Homepage_Industry_News -->
        <div class="width-banner" id='div-gpt-ad-1460360910375-0' style='height:90px; width:728px;'>
          <script type='text/javascript'>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1460360910375-0'); });
          </script>
        </div>
      </div>
      <div class="col-lg-4 col-md-12">
        <h2 class="page-header"><span>industry</span> news</h2>
      </div>
    </div>
    <div id="home-page-blog-news" class="row">
      <?php foreach($blog as $item): ?>
        <?php if(!empty($image = $item->image)): ?>
          <?php $blog_url = Url::to(["/{$item->url_name}"]); $date = $item->publication_date ?>
          <div class="col-sm-6">
            <div class="home-page-blog-news-block">
              <div class="content-image col-sm-6">
                <a href="<?= $blog_url ?>">
                  <img data-src="<?= $storage.$image->media_path ?>"/>
                  <div class="content-image-overlay">
                    <dl>
                      <dt><?= date('d', $date) ?></dt>
                      <hr/>
                      <dt><?= date('M', $date) ?></dt>
                    </dl>
                  </div>
                </a>
              </div>
              <div class="content-description col-sm-6">
                <h5 class="content-header text-center"><?= $item->h1_tag ?></h5>
                <hr/>
                <p class="content-text">
                  <?= Setup::truncate($item->body, '120', '...') ?>
                </p>
                <p class="read-more-block text-right">
                  <a href="<?= $blog_url ?>" class="read-more">Read more</a>
                </p>
              </div>
            </div>
          </div><!--/.col-->
        <?php endif; ?>
      <?php endforeach; ?>
    </div><!--/.row-->
    <div class="row">
      <div class="col-sm-4 col-sm-offset-4 text-center margin-top-20">
        <a href="<?= Url::to(["/blog"]) ?>" class="btn brand-green btn-read-more">Read More</a>
      </div>
    </div>
  </div><!--/.container-->
</section><!--section ends-->

<!-- Separator
---------------------------------------------------------------------->
<hr class="section-separator"/>


<!-- ABOUT PREVIEW NAME
---------------------------------------------------------------------->
<section id="about-preview">
  <div class="container">
    <div id="about-preview" class="col-sm-10">
      <div class="row">
        <div class="col-sm-12">
          <h2 class="page-header"><?= $about->h1_tag ?></h2>
        </div>
      </div><!--/.row-->
      <div class="row">
        <div class="col-sm-12">
          <div class="row">
            <div class="col-sm-6" style="padding-left: 0">
              <div id="about-video"></div>
              <div id="thumbnail_container" class="thumbnail_container embed-responsive embed-responsive-16by9">
                <img class="thumbnail" id="video-thumbnail" />
              </div>
              <a class="ci start-video"></a>
            </div>
            <div class="col-sm-6">
              <div class="content-title">
                <div class="content-description">
                  <?= Setup::truncate($about->body, 900, '...') ?>
                </div>
                <a href="/page/about" class="btn brand-green btn-read-more">Read more</a>
              </div>
            </div>
          </div><!--/.row-->
        </div><!--/.col-->
      </div><!--/.row-->
    </div>
    <div class="col-sm-2">
      <!-- /36639447/hompage_skyscraper -->
      <div id='div-gpt-ad-1456820027687-3' style='height:600px; width:120px;'>
        <script type='text/javascript'>
          googletag.cmd.push(function() { googletag.display('div-gpt-ad-1456820027687-3'); });
        </script>
      </div>
    </div>
  </div>
</section>


<!-- TESTIMONIAL SECTION
---------------------------------------------------------------------->
<section id="testimonials">
  <div class="container">
    <div class="row">
      <h2 class="page-header text-left col-xs-12"><span>What </span>leading agents are saying</h2>
    </div><!--/.row-->
    <div id="testimonials-carousel" class="text-center">
      <?php foreach($testimonials as $testimonial): ?>
        <?php if(strlen($testimonial->body) > 5): ?>
          <div>
            <div class="review-style"><?php echo $testimonial->body ?></div>
            <p class="testimonial"><strong><?= $testimonial->title ?></strong> - <?= $testimonial->h1_tag ?></strong></p>
            <p><strong><?= $testimonial->menu_label ?></strong></p>
          </div>
        <?php endif; ?>
      <?php endforeach ?>
    </div> <!--/#property-carousel-->
  </div><!--/.container-->
</section>
<!--sections ends-->
