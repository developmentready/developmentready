<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $name;
$this->registerMetaTag(['name' => 'robots', 'content' => 'noindex,follow']);

?>


<div class="container" style="min-height: 500px;">
  <div class="content">
    <div class="not-found">
      <div class="not-found-title text-center">
        <strong>404</strong> <span><?= Yii::t('frontend', 'Page not found') ?></span>
      </div>

      <div class="not-found-content">
        <h1 class="not-found-subtitle"><?= yii::t('common', 'You have several options') ?></h1>

        <div class="row">
          <div class="col-sm-5">
            <ul class="not-found-list">
              <li><?= Yii::t('common', 'Return back to <a href="{link}">homepage</a>', ['link' => Url::to(['/'])]) ?></li>
              <li><?= Yii::t('common', 'Check our most recent <a href="{link}">properties</a>', ['link' => Url::to(['/property/index'])]) ?></li>
              <li><?= Yii::t('common', 'Contact us via our contact details, shown in footer.') ?></li>
            </ul>
          </div><!-- /.col-* -->

          <div class="col-sm-7">
          </div><!-- /.col-* -->
        </div><!-- /.row -->
      </div><!-- /.not-found-content -->
    </div>
  </div><!-- /.content -->
</div><!-- /.container -->