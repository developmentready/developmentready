<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/24/15
 * Time: 01:12
 */

use yii\helpers\Url;

$this->title = $model->title;

$this->registerMetaTag(['name' => 'keywords', 'content' => $model->meta_keywords]);
$this->registerMetaTag(['name' => 'description', 'content' => $model->meta_description]);

$storage = \Yii::getAlias('@storageUrl');
if(!empty($media = $model->image)){$image = $media->media_path;};


$js = <<< 'SCRIPT'
  $('#property-carousel').owlCarousel({
    loop:true, margin:50, nav:false, navText: ['',''], dots : false,
    responsive:{0:{items:1},600:{items:3},1000:{items:5}}
  })
SCRIPT;
$this->registerJs($js);

?>

<!-- WHY WE HEADER SECTION
---------------------------------------------------------------------->
<section id="page-why-we-header-section">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-left"><?= $model->h1_tag ?></h1>
      </div>
    </div><!--/.row-->
  </div><!--/.container-->
</section><!--section ends-->

<!-- Separator
---------------------------------------------------------------------->
<hr class="section-separator"/>

<!-- Why We Body Section
---------------------------------------------------------------------->
<section id="why-we-body-section">
  <div class="container">
    <?php foreach($testimonials as $testimonial): ?>
      <div class="row">
        <div class="col-sm-12 text-center">
          <span class="ci ci-quote-orange"></span>
          <p><?= $testimonial->body ?></p>
          <h5><?= $testimonial->title ?></h5>
          <p class="small"> <?= $testimonial->h1_tag ?> <br/> <?= $testimonial->menu_label ?></p>
        </div>
      </div><!--/.row-->
    <?php endforeach; ?>
  </div><!--/.container-->
</section><!--section ends-->

<!-- Separator
---------------------------------------------------------------------->
<hr class="section-separator"/>


<!-- SECTION NAME
---------------------------------------------------------------------->
<section id="agencies">
  <div class="container">
    <div class="row">
      <h2 class="page-header col-sm-12"><span>Trusted</span> By</h2>
    </div><!--/.row-->
    <div class="row">
      <div id="property-carousel" class="trusted-logos">
        <?php foreach($agencies as $agency): ?>
          <?php if(!empty($image = $agency->image)): ?>
          <div class="logo-frame">
            <a href="<?= Url::to(["/agency/{$agency->id}"]) ?>">
              <img src="<?= $storage.$image->media_path ?>">
            </a>
          </div>
          <?php endif; ?>
        <?php endforeach; ?>
      </div>
    </div><!--/.row-->
  </div><!--/.container-->
</section><!--section ends-->
