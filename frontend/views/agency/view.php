<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/23/15
 * Time: 01:18
 */

use yii\helpers\Url;
use common\components\helpers\Setup;


$this->title = "Latest Development Sites from ".strip_tags($model->title) ." | Permit Ready";

$storage = \Yii::getAlias('@storageUrl');

if(!empty(\Yii::$app->request->queryParams['page']))
  $this->registerMetaTag(['name' => 'robots', 'content' => 'noindex,follow']);

?>

<?= $this->render("//layouts/parts/_message_to_agency", ['model' => $message_model, 'message_type' => 2, 'id' => $model->id]) ?>

<script>
  var agency_lat = <?= empty($model->lat) ? -37.810102 : $model->lat ?>;
  var agency_lng = <?= empty($model->lng) ? 144.964748 : $model->lng ?>;
  var agency_name = '<?= $this->title ?>'
</script>

<!-- SECTION AGENCY DETAILS
---------------------------------------------------------------------->
<section id="agency-details-section">
  <div class="container">
    <?php if(!$model->show_logo_single_page): ?>
      <div class="row">
        <div class="col-sm-12">
          <div class="content-title agency-name text-center"
               style="background-color:<?= !empty($color = $model->color) ? $color : "#fff"  ?>; color: <?= !empty($font_color = $model->secondary_color) ? $font_color : "#000"  ?>">
            <h1><?= $model->title ?></h1>
          </div>
        </div>
      </div>
    <?php else: ?>
      <div class="property-agency-banner-block col-xs-12" style="background-color:<?= !empty($color = $model->color) ? $color : "#fff"  ?>;">
        <?php if(!empty($media = $model->image)): ?>
          <div class="item property-thumbnail">
            <a style="color: <?= !empty($font_color = $model->secondary_color) ? $font_color : "#000"  ?> <?= $model->featured_aceny ? "" : '; cursor: text' ?> " <?= $model->featured_aceny ? "href='".Url::to(["/agencies/{$model->slug}"])."'" : '' ?>>
              <div class="overflow-image-block text-center">
                <img src="<?= $storage.$media->media_path ?>" alt="<?= $model->title ?>"/>
              </div>
            </a>
          </div>
        <?php endif; ?>
      </div><!-- /.property-agency-banner-block-->
    <?php endif; ?>
    <div class="row">
      <?= $this->render('//layouts/parts/_share_links', ['url' => Yii::$app->request->absoluteUrl]); ?>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="content-description">
          <div class="row">
            <div class="col-sm-8">
              <div class="agents-list-block">
                <div class="row">
                  <?php if(!empty($agents = $model->viewAgencyAgents())): ?>
                    <?php foreach($agents as $agent): ?>
                      <div class="col-sm-4">
                        <div class="agents-list-details">
                          <div class="overflow-image-block">
                            <?php $image = empty($media = $agent->image) ? '/web/agency/anonymous-agent.png' : $media->media_path ; ?>
                            <img class="overflow-image" src="<?= $storage.$image ?>"/>
                          </div>
                          <div class="agents-short-info">
                            <h5><?= $agent->full_name ?></h5>
                            <p> <?= (!empty($position = $agent->position)) ? $position->title : '' ?> </p>
                            <em id="show-contact-<?= $agent->id ?>">
                              <a id="show-contact-btn-<?= $agent->id ?>" rel="nofollow" class="btn btn-default"
                                 onclick="$.post('<?= Yii::$app->urlManager->createUrl('property/single-agent-info?id='.$agent->id) ?>',
                                   function(data) {$('#show-contact-<?= $agent->id ?>').html(data);});"
                                >
                                <?= Setup::truncate($agent->mobile, 10, "...") ?>
                              </a>
                            </em>
                            <br/><br/>
                            <!--<a data-toggle="modal" rel="nofollow" data-id="--><?php # echo $agent->id ?><!--" data-target="#message-agency" class="btn brand-green btn-sm">Message to --><?php # echo $agent->full_name ?><!--</a>-->
                            <!--<p>--><?php # $agent->about ?><!--</p>-->
                          </div>
                        </div>
                      </div><!-- /.col -->
                    <?php endforeach; ?>
                  <?php else: ?>
                    <h4 class="text-uppercase col-sm-12">No Agents Registered For This Agency in Our web page</h4>
                  <?php endif; ?>
                </div>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="agency-information-block">
                <div class="side-information-title text-center"><h5>Agency information</h5></div>
                <div class="side-information-content">
                  <ul>
                    <li><strong>Location:</strong> <?= $model->address ?></li>
                    <li><strong>Phone:</strong> <?= $model->phone ?> <?= !empty($model->mobile) ? ". Mobile: ".$model->mobile : '' ?></li>
                    <li><strong>Website:</strong> <a href="http://<?= $web = $model->web ?>"><?= $web ?> </a></li>
                    <?php if(!empty($email = $model->email)): ?>
                      <li><strong>Email:</strong> <a href="mailto:<?= $email ?>"><?= $email ?></a></li>
                    <?php endif; ?>
                  </ul>
                  <div class="text-center">
                    <a data-toggle="modal" rel="nofollow" data-id="<?= $model->id ?>" data-target="#message-agency" class="btn brand-green btn-view-details agency-message">Message agency</a>
                  </div>
                </div>
                <div class="agency-map-location">
                  <div id="agency-map"></div>
                </div>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="about-agency">
                <?= $model->about ?>
              </div>
            </div>
          </div>
        </div>
      </div><!--/.col-->
    </div><!--/.row-->
  </div><!--/.container-->
</section><!--section ends-->

<!-- SECTION NAME
---------------------------------------------------------------------->
<section id="property-list-section">
  <div class="container">
    <div class="row">
      <?= $this->render("//layouts/parts/_properties_line", ['properties' => $properties, 'storage' => $storage, 'message_model' => $message_model]) ?>
    </div><!--/.row-->
  </div><!--/.container-->
</section><!--section ends-->

<!-- Pagination
---------------------------------------------------------------------->
<?= $this->render("//layouts/parts/_pagination", ['pages' => $pages]) ?>

<!-- Separator
---------------------------------------------------------------------->
<hr class="section-separator"/>

<!-- FEATURED PROPERTIES
 ---------------------------------------------------------------------->
<section id="featured-properties">
  <div class="container">
    <div class="row">
      <h2 class="page-header text-left col-xs-12"><span>Featured</span> Development Sites</h2>
    </div><!--/.row-->
    <div id="property-carousel">
      <?= $this->render("//layouts/parts/_properties_carousel", ['properties' => $featured_properties, 'storage' => $storage]) ?>
    </div> <!--/#property-carousel-->
  </div><!--/.container-->
</section><!--section ends-->




