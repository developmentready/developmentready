<?php
/**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 8/17/15
 * Time: 18:31
 */

namespace frontend\models\search;

use common\models\Property;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PropertyUser;


/**
 * ArticleSearch represents the model behind the search form about `common\models\Article`.
 */
class PropertySearch extends Property{

  public $suburb;
  public $sort = 1;
  public $max_price;
  public $min_price;
  public $min_square;
  public $from_filter = false;
  public $property_types = [];
  public $suburb_ids = [];
  public $state_ids = [];
  public $contract_type_ids = [];
  public $state;
  public $sort_by_date;

  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['id', 'sort', 'state', 'sort_by_date'], 'integer'],
      [['min_square', 'min_price', 'max_price'], 'number'],

      [['suburb','contract_type_id'], 'safe'],

      ['contract_type_id', 'default', 'value' => null],

      ['property_types', function ($attribute, $params) {
        if(!is_array($this->propertyTypes)){
          $this->addError('property_types', Yii::t('common', 'Provided value, is incorrect value!'));
        }
      }],
//      ['suburb_ids', function ($attribute, $params) {
//        if(!is_array($this->suburb_ids)){
//          $this->addError('suburb_ids', Yii::t('common', 'Provided value, is incorrect value!'));
//        }
//      }],
      ['contract_type_ids', function ($attribute, $params) {
        if(!is_array($this->propertyTypes)){
          $this->addError('property_types', Yii::t('common', 'Provided value, is incorrect value!'));
        }
      }],
    ];
  }

  /**
   * @inheritdoc
   */
  public function scenarios(){
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
   * Creates data provider instance with search query applied
   * @return ActiveDataProvider
   */
  public function search($params){

    $this->sort = 0;

    if(array_key_exists('PropertySearch', $params)){
      if(is_array($params['PropertySearch'])){
        if(array_key_exists('suburb_id', $params['PropertySearch'])){
          $suburbs = $params['PropertySearch']['suburb_id'];
          if(!empty($suburbs))
            foreach($suburbs as $item){
              if($item >= 1 && $item <= 8 )
                array_push($this->state_ids, $item);
              else
                array_push($this->suburb_ids, $item);
            }
        }

        if(array_key_exists('propertyTypes', $params['PropertySearch'])){
          $types = $params['PropertySearch']['propertyTypes'];
          if(!empty($types))
            foreach($types as $item){
              array_push($this->property_types, $item);
            }
        }
        if(array_key_exists('sort', $params['PropertySearch'])){
          $this->sort = (int)$params['PropertySearch']['sort'];
        }
      }
    }

    $query = Property::find()->activeProperty();

    if(!empty($this->suburb_ids) || !empty($this->state_ids)) $query->joinWith('suburb');
    if(!empty($this->property_types)) $query->joinWith('propertyTypes');

    //$query->groupBy('{{%properties}}.id');

    if($this->sort == 1){
      $query->orderBy(['publication_date' => SORT_ASC, '{{%properties}}.created_at' => SORT_ASC, 'id' => SORT_ASC]);
    } else {
      $query->orderBy(['publication_date' => SORT_DESC, 'id' => SORT_DESC]);
    }

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => ['pageSize' => 12],
    ]);

    if (!($this->load($params) && $this->validate())){
      return $dataProvider;
    }

    if(empty($this->contract_type_id)){
        $this->contract_type_id = [0 => "1", 1 => "0" ];
    }
    $query->andFilterWhere([
      'id' => $this->id,
      'contract_type_id' => $this->contract_type_id,
    ]);

    if(!empty($this->property_types)){
      $query->andFilterWhere(['in','{{%property_types_assn}}.property_type_id', $this->property_types]);
    }

    if(!empty($this->state_ids))
      $query->andFilterWhere(['in','{{%suburbs}}.state_id', $this->state_ids]);

    if(!empty($this->suburb_ids))
      $query->andFilterWhere(['in','{{%suburbs}}.id', $this->suburb_ids]);

    return $dataProvider;
  }

  /**
   * @param $params
   * @return $this
   */
  public function searchInMap($params){


    if(!empty($params)){

      if(array_key_exists('state', $params)){
        $this->state = (int)$params['state'];
      } else {
        return false;
      }

      if(array_key_exists('suburb_ids', $params)){
        $suburbs = $params['suburb_ids'];
        if(!empty($suburbs))
          foreach($suburbs as $item){
            if($item >= 1 && $item <= 8 )
              array_push($this->state_ids, $item);
            else
              array_push($this->suburb_ids, $item);
          }
      }
      if(array_key_exists('contract_type_ids', $params)){
        $contract_types = $params['contract_type_ids'];
        if(!empty($contract_types))
          foreach($contract_types as $item){
            array_push($this->contract_type_ids, (int)$item);
          }

      }
    } else {
      return false;
    }

    $query = Property::find()->activeProperty();

    $query->joinWith('suburb');

    $query->andWhere(['{{%suburbs}}.state_id' => $this->state]);
    $query->groupBy('{{%properties}}.id');

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'sort' => [ 'defaultOrder' => ['created_at' => SORT_DESC, 'id' => SORT_DESC ]],
    ]);

    if (!($this->validate())){
      return $query;
    }

    if(!empty($this->contract_type_ids))
      $query->andFilterWhere(['in','contract_type_id', $this->contract_type_ids]);

    if(!empty($this->state_ids))
      $query->andFilterWhere(['in','{{%suburbs}}.state_id', $this->state_ids]);

    if(!empty($this->suburb_ids))
      $query->andFilterWhere(['in','{{%suburbs}}.id', $this->suburb_ids]);

    return $dataProvider;
  }


  /**
   * Return all fav
   * @return ActiveDataProvider
   */
  public function searchFavorites(){
    $favorites = \common\models\Favorite::find()->where(['user_id' => \Yii::$app->user->identity->id])->all();
    $id_array = [];

    foreach($favorites as $item){
      array_push($id_array, $item->property_id);
    }

    $query = Property::find()->where(['{{%properties}}.id' => $id_array]);

    $dataProvider = new ActiveDataProvider([ 'query' => $query,
      'pagination' => [
        'pageSize' => 10,
      ],
      'sort' => [
        'defaultOrder' => [
          'created_at' => SORT_DESC, 'id' => SORT_DESC
        ]
      ],
    ]);

    return $dataProvider;
  }

  /**
   * @param $params
   * @return ActiveDataProvider
   */
  public function searchAccount($params){
    $query = Property::find()->where(['user_id' => \Yii::$app->user->identity->id])->groupBy('{{%properties}}.id');

    $query->joinWith(['propertyUsers', 'propertyAgents']);

    $dataProvider = new ActiveDataProvider([ 'query' => $query,
      'pagination' => [
        'pageSize' => 10,
      ],
      'sort' => [
        'defaultOrder' => ['created_at' => SORT_DESC, 'id' => SORT_DESC]
      ],
    ]);

    if (!($this->load($params) && $this->validate())){
      return $dataProvider;
    }

    $query->andFilterWhere([
      'id' => $this->id,
      '{{%properties}}.status' => $this->status,
      'contract_type_id' => $this->contract_type_id,
    ]);

    if(!empty($this->suburb_id)){
      $query->andFilterWhere(['=', '{{%}}.properties', $this->suburb_id]);
    }

    return $dataProvider;
  }

  public function searchAccount2($params){
    return false;

  }


  /**
   * Not using, this was provided for group search from main page, but other way was used.
   * @param $type_id
   * @param $rent
   * @return ActiveDataProvider
   */
  public function searchByType($type_id, $rent){
    $query = Property::find()->activeProperty();

    $query->joinWith(['address', 'price']);

    if($rent){
      $query->andWhere(['contract_type_id' => 1]);
    } else{
      $query->andWhere(['contract_type_id' => 2]);
      $query->andWhere(['property_type_id' => $type_id]);
    }


    $dataProvider = new ActiveDataProvider([ 'query' => $query,
      'pagination' => [
        'pageSize' => 10,
      ],
      'sort' => [
        'defaultOrder' => [
          'created_at' => SORT_DESC, 'id' => SORT_DESC
        ]
      ],
    ]);

    return $dataProvider;
  }

  /**
   * @param $params
   * @return $this
   */
  public function searchSoldProperties($params){

    $this->sort = 0;

    if(!empty($params['PropertySearch'])){
      if(array_key_exists('suburb_id', $params['PropertySearch'])){
        $suburbs = $params['PropertySearch']['suburb_id'];
        if(!empty($suburbs))
          foreach($suburbs as $item){
            if($item >= 1 && $item <= 8 )
              array_push($this->state_ids, $item);
            else
              array_push($this->suburb_ids, $item);
          }
      }

      if(array_key_exists('sort', $params['PropertySearch'])){
        $this->sort = (int)$params['PropertySearch']['sort'];
      }
    }

    $query = Property::find()->where(['{{%properties}}.status' => 1]);

    $query->joinWith('suburb');

    $query->andWhere(['{{%properties}}.sold' => 1]);
    $query->groupBy('{{%properties}}.id');

    if($this->sort == 1){
      $query->orderBy(['created_at' => SORT_ASC, 'id' => SORT_ASC]);
    } else {
      $query->orderBy(['priority' => SORT_ASC, 'created_at' => SORT_DESC, 'id' => SORT_DESC]);
    }

    $dataProvider = new ActiveDataProvider(['query' => $query]);

    if (!($this->validate())){
      die('Could not pass validation');
      return $query;
    }

    if(!empty($this->state_ids))
      $query->andFilterWhere(['in','{{%suburbs}}.state_id', $this->state_ids]);

    if(!empty($this->suburb_ids))
      $query->andFilterWhere(['in','{{%suburbs}}.id', $this->suburb_ids]);

    return $dataProvider;
  }


  /**
   * Creates data provider instance with search query applied
   * @return ActiveDataProvider
   */
  public function searchStatePropertiesById($params, $state_id){

    $this->sort = 0;

    if(array_key_exists('PropertySearch', $params)){
      if(is_array($params['PropertySearch'])){
        if(array_key_exists('suburb_id', $params['PropertySearch'])){
          $suburbs = $params['PropertySearch']['suburb_id'];
          if(!empty($suburbs))
            foreach($suburbs as $item){
              if($item >= 1 && $item <= 8 )
                array_push($this->state_ids, $item);
              else
                array_push($this->suburb_ids, $item);
            }
        }

        if(array_key_exists('propertyTypes', $params['PropertySearch'])){
          $types = $params['PropertySearch']['propertyTypes'];
          if(!empty($types))
            foreach($types as $item){
              array_push($this->property_types, $item);
            }
        }

        if(array_key_exists('sort', $params['PropertySearch'])){
          $this->sort = (int)$params['PropertySearch']['sort'];
        }
      }
    }

    $query = Property::find()->activeProperty()->joinWith('suburb')->andWhere(['state_id' => $state_id]);

    if(!empty($this->property_types)) $query->joinWith('propertyTypes');

    if($this->sort == 1){
      $query->orderBy(['publication_date' => SORT_ASC, '{{%properties}}.created_at' => SORT_ASC, 'id' => SORT_ASC]);
    } else {
      $query->orderBy(['publication_date' => SORT_DESC, 'id' => SORT_DESC]);
    }

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => ['pageSize' => 12],
    ]);

    if (!($this->load($params) && $this->validate())){
      return $dataProvider;
    }

    $query->andFilterWhere([
      'id' => $this->id,
      'contract_type_id' => $this->contract_type_id,
    ]);

    if(!empty($this->property_types)){
      $query->andFilterWhere(['in','{{%property_types_assn}}.property_type_id', $this->property_types]);
    }

    if(!empty($this->state_ids))
      $query->andFilterWhere(['in','{{%suburbs}}.state_id', $this->state_ids]);

    if(!empty($this->suburb_ids))
      $query->andFilterWhere(['in','{{%suburbs}}.id', $this->suburb_ids]);

    return $dataProvider;
  }



}
