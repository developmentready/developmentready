<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/30/15
 * Time: 00:31
 */

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\AgencyAgent;
use common\models\Agency;

/**
 * AgencyAgentSearch represents the model behind the search form about `common\models\AgencyAgent`.
 */
class AgencyAgentSearch extends AgencyAgent{
  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['id', 'status', 'agency_id', 'position_id'], 'integer'],
      [['full_name', 'about', 'mobile', 'email'], 'safe'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function scenarios(){
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
   * Creates data provider instance with search query applied
   *
   * @param array $params
   *
   * @return ActiveDataProvider
   */
  public function search($params){
    $query = AgencyAgent::find()->where(['agency_id' => Yii::$app->user->identity->agency->id]);

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
    ]);

    if (!($this->load($params) && $this->validate())) {
      return $dataProvider;
    }

    $query->andFilterWhere([
      'id' => $this->id,
      'status' => $this->status,
      'agency_id' => $this->agency_id,
      'position_id' => $this->position_id,
      'creator_id' => $this->creator_id,
      'created_at' => $this->created_at,
      'updater_id' => $this->updater_id,
      'updated_at' => $this->updated_at,
    ]);

    $query->andFilterWhere(['like', 'full_name', $this->full_name])
      ->andFilterWhere(['like', 'about', $this->about])
      ->andFilterWhere(['like', 'mobile', $this->mobile])
      ->andFilterWhere(['like', 'email', $this->email]);

    return $dataProvider;
  }
}