<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/24/15
 * Time: 16:42
 */


namespace frontend\widgets;

use yii\base\Widget;

class SearchBar extends Widget{
  public function init(){}

  public function run(){
    return $this->render('search-property', [
      'model' => new \frontend\models\search\PropertySearch()
    ]);
  }
}
