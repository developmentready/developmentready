<?php

/* 
 * This file was created by Rashad Aliyev (RAliyev@avagr.com)
 *  on Mar 25, 2015 
 */

namespace frontend\widgets;

use common\components\helpers\MenuHelper;
use yii\base\Widget;

class Navigation extends Widget{
  public function init(){}

  public function run(){
    return $this->render('navigation/view', [
      'output' => MenuHelper::getNavigationMenu(),
    ]);
  }
}