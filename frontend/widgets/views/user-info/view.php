<?php
/**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 8/16/15
 * Time: 13:06
 */

use yii\helpers\Url;

?>

<a href="<?= Url::to(['/manage/index']) ?>">
  <?php if($model->user_type == 1 && !empty($media = $model->agency)): ?>
    <?php if(!empty($image = $media->image)): ?>
      <?php if(!empty($image->media_path)): ?>
        <img src="<?= Yii::getAlias('@storageUrl').$image->media_path ?>" alt="Agency Logo">
      <?php endif; ?>
    <?php endif; ?>
  <?php endif; ?>
  <span class="avatar-content">
      <span class="avatar-title"><?= $model->name; ?></span>
      <span class="avatar-subtitle"></span>
  </span><!-- /.avatar-content -->
</a>