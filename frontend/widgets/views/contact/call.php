<?php

/* 
 * This file was created by Rashad Aliyev (RAliyev@avagr.com)
 *  on Apr 23, 2015 
 */
 
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

?>

<div class="modal fade call-me-modal" tabindex="-1" role="dialog" aria-labelledby="call-me" aria-hidden="true">
  <div class="modal-dialog modal-xs">
    <div class="modal-content">
      <?php $form = ActiveForm::begin(['action'=> Url::to('/site/call'),'id' => 'callform']); ?>
        <div class="modal-header">
          <h5 class="modal-title"><?= Yii::t('common','Please leave your contact number') ?></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class='clearfix'></div>  
        <div class="modal-body clear-fix">
          <?= Html::activeHiddenInput($model, "contact_type", ['value' => 3]) ?>
          <?= $form->field($model, 'phone_number', ['template' => '<div class="col-sm-2" style="font-size: 1.3em">+994</div> <div class="col-sm-10">{input}</div><div class="col-sm-offset-2 col-sm-10">{hint}</div>'])
                ->hint(Yii::t('common','As example {example}',['example' => '(012)-123-4567']))
                ->widget(\yii\widgets\MaskedInput::classname(), ['mask' => '(999)-99[9]-9999',]) 
          ?>
        </div>
        <div class='clearfix'></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal"><?= Yii::t('common','Close') ?></button>
          <?= Html::submitButton(\Yii::t('frontend','Call me'), ['id'=>'submit', 'class' => 'btn btn-success']) ?>          
        </div>
      <?php ActiveForm::end(); ?>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->