<?php

/* 
 * This file was created by Rashad Aliyev (RAliyev@avagr.com)
 *  on Apr 23, 2015 
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

?>


<div id="message-agency" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Subscribe and don't miss news</h4>
      </div>
      <div id="contact-box" class="modal-body">
        <?php Pjax::begin([
          'clientOptions' => ['method' => 'POST', 'container' => 'x1'],
          'timeout' => 5000, 'enablePushState' => false,
        ]); ?>
        <?php $form = ActiveForm::begin([
          'id' => 'contact-form',
          'action' => \yii\helpers\Url::to(['/site/contacts', 'id' => $id, 'type' => $message_type]),
          'options' => ['data-pjax' => '#x1' ]
        ]); ?>
        <div class="form-group">
          <div class="row">
            <div class="col-sm-4"><?= $form->field($model, 'name')->textInput(['placeholder' => \Yii::t('frontend','Name')." *", 'class' => 'form-control'])->label(false) ?><br/></div>
            <div class="col-sm-4">
              <?= $form->field($model, 'email')->textInput(['placeholder' => \Yii::t('frontend','Email')." *", 'class' => 'form-control'])->label(false) ?><br/>
            </div>
            <div class="col-sm-4">
              <?= $form->field($model, 'phone_number')->textInput(['placeholder' => \Yii::t('frontend','Phone Number')." *", 'class' => 'form-control'])->label(false) ?><br/>
            </div>
          </div>
        </div><!-- /.form-group -->
        <div class="form-group">
          <?=
          $form->field($model, 'subjects[]')->widget(Select2::className(),[
            'data' => $model->getSubjects(),
            'options' => ['placeholder' => 'Please select subject * ', 'multiple' => true],
          ])->label(false);
          ?>
        </div>
        <div class="form-group">
          <?= $form->field($model, 'message')->textarea(['placeholder' => \Yii::t('frontend','Body')." *", 'class' => 'form-control', 'rows' => 5])->label(false) ?>
        </div><!-- /.form-group -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" rel="nofollow" data-dismiss="modal">Close</button>
        <?= Html::submitButton(\Yii::t('frontend','Send Message'), ['id'=>'submit', 'rel'=>'nofollow' ,'class' => 'btn brand-green btn-view-details']) ?>
      </div>
      <?php ActiveForm::end(); ?>
      <?php Pjax::end(); ?>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->