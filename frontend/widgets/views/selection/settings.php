<?php
/**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 9/3/15
 * Time: 00:57
 */

$my_country = $location->translation->name;
$country_id = !empty($country_id) ? $country_id : 15;
$var_city_id = isset($location->defaultCity[0]->id) ? $location->defaultCity[0]->id : 3;

?>
<script>
  var country_id = <?= $country_id ?>; var currency_id = <?= $currency_id ?>; var city_id = <?= $var_city_id ?>;
  var prop_lat = <?= $location->latitude ?>; var prop_long = <?= $location->longitude ?>; var labels = 'D';
</script>
<li>
  <div class="dropdown">
    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
      <?= $my_country ?>
      <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
      <?php foreach($countries as $item): ?>
        <li><a rel="nofollow" href="#" onclick='$.post("<?= Yii::$app->urlManager->createUrl('site/set-cookie?country_id=').$item->id ?>", function(){location.reload();});false'><?= $item->translation->name ?></a></li>
      <?php endforeach; ?>
    </ul>
  </div>
</li>
<li>
  <div class="dropdown">
    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
      <?= $default_currency->code ?>
      <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
      <?php foreach($currencies as $item): ?>
        <li><a rel="nofollow" href="#" onclick='$.post("<?= Yii::$app->urlManager->createUrl('site/set-cookie?currency_id=').$item->id ?>", function(){location.reload();});false'> <?= $item->code ?></a></li>
      <?php endforeach; ?>
    </ul>
  </div>
</li>