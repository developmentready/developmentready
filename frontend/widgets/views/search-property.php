<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/24/15
 * Time: 16:45
 */

use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\select2\Select2;
use yii\web\JsExpression;


$model = new \frontend\models\search\PropertySearch();

?>


<?php
$suburbs = [];
$contract_types = [];
$keys = [];

if(!empty($request = \Yii::$app->request->get('PropertySearch'))){

  if(array_key_exists('contract_type_id', $request)){
    $contract_type = $request['contract_type_id'];
    $model->contract_type_id = $contract_type;
  }


  if(array_key_exists('suburb_id', $request)){
    $suburb_ids = $request['suburb_id'];
    if(!empty($suburb_ids)){
      foreach($suburb_ids as $item){
        $suburb = \common\models\Suburb::findOne($item);
        $suburbs[$item] =  $suburb->title;
        array_push($keys,$item);
      }
    }
  }

  if(array_key_exists('propertyTypes', $request)){
    $contract_type_ids = $request['propertyTypes'];
    if(!empty($contract_type_ids))
      foreach($contract_type_ids as $item){
        $contract_type = \common\models\PropertyType::findOne($item);
        $contract_types[$item] = $contract_type->title;
      }
  }
}
?>


<?= Html::beginForm(['/property/search'], 'get') ?>
<div class="col-sm-8 col-sm-offset-2">
  <div class="row">
    <div class="col-xs-3 select-type">
      <div class="form-group">
        <?= Html::activeDropDownList($model, 'contract_type_id', $model->permitStatus(),
            [
                'class'    => 'form-control selectpicker text-left',
                'prompt'   => 'All Properties',
                'multiple' => 'multiple',
                'title'    => 'All Properties',
            ])?>
      </div>
    </div>
    <div class="col-xs-6 search-property">

      <?php # var_dump($suburbs); die(); ?>

      <?=
      // Multiple select without model
      Select2::widget([
        'data' => $suburbs,
        'value' => $keys,
        'name' => 'PropertySearch[suburb_id][]',
        'options' => ['multiple' => true, 'placeholder' => 'Type a suburb, state or post code  ...'],
        'pluginOptions' => [
          'allowClear' => true,
          'minimumInputLength' => 2,
          'ajax' => [
            'url' => Url::to(['/property/suburb-list']),
            'dataType' => 'json',
            'data' => new JsExpression('function(params) { return {q:params.term}; }')
          ],
          'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
          'templateResult' => new JsExpression('function(suburb) { if(suburb.post_code == 0) suburb.post_code = ""; return suburb.text + ", "+ suburb.state +" " + suburb.post_code ; }'),
          'templateSelection' => new JsExpression('function (suburb) { return suburb.text;}'),
        ],
      ]);
      ?>
    </div>
    <div class="col-xs-3 search-button">
      <?= Html::submitButton('Search Now', ['class' => 'btn btn-success brand-green']) ?>
    </div>
  </div>
</div>
<div class="header-checkbox-block col-sm-8 col-sm-offset-2 text-left">
  <?php # Html::activeCheckboxList($model, 'propertyTypes',
  # ArrayHelper::map(\common\models\PropertyType::find()->active()->all(), 'id', 'title')) ?>
  <?php foreach(\common\models\PropertyType::find()->active()->all() as $item): ?>
    <input id="<?= $item->title."-".$item->id ?>" type="checkbox" name="PropertySearch[propertyTypes][]"
           value="<?= $item->id ?>" <?= empty($contract_types[$item->id]) ? 'checked="false"' : 'checked' ?> checked/>
    <label for="<?= $item->title."-".$item->id ?>"><?= $item->title ?></label>
  <?php endforeach; ?>
</div>
<?= Html::endForm() ?>
<style>
#header-search-box-container .dropdown-menu.open,#header-search-box-container .dropdown-menu.open ul{
  background-color: #fff;
}
#video-wrapper{
  position: relative;
  top: 0;
  bottom: 0;
  width: 100%;
  height: 100%;
  min-height: 640px;
  overflow: hidden;
}
#home{
  overflow: inherit!important;
}
#header-search-box-container .dropdown-menu li a{
  position: relative;
  padding: 5px 10px 5px 35px;
}
#header-search-box-container .dropdown-menu li a .check-mark:before{
  content: '' !important;
}
#header-search-box-container .dropdown-menu li a:before{
  position: absolute;
  content: '';
  width: 19px;
  height: 16px;
  display: block;
  left: 7px;
  top: 9px;
}
#header-search-box-container .dropdown-menu li.selected a:before{
  background: url('/img/icons/multi-checked-true.png') no-repeat;
}
#header-search-box-container .dropdown-menu li a:before{
  background: url('/img/icons/multi-checked.png') no-repeat;
}

#header-search-box-container span.filter-option.pull-left, #header-search-box-container .select2-search__field{
  font-size: 16px;
}

#header-search-box-container .select2-search__field::-webkit-input-placeholder {color:#000;}
#header-search-box-container .select2-search__field::-moz-placeholder          {color:#000;}/* Firefox 19+ */
#header-search-box-container .select2-search__field:-moz-placeholder           {color:#000;}/* Firefox 18- */
#header-search-box-container .select2-search__field:-ms-input-placeholder      {color:#000;}

#header-search-box-container .header-checkbox-block{
  text-align: center;
}
.header-checkbox-block input[type=checkbox]:checked+label:before{
  content: '';
  background: url('/img/icons/checked-true.png') no-repeat;
  width: 13px;
  height: 13px;
  margin-right: 10px;
}
.header-checkbox-block input[type=checkbox]+label:before{
  content: '';
  background: url('/img/icons/checked.png') no-repeat;
  width: 13px;
  height: 13px;
  margin-right: 10px;
}

</style>