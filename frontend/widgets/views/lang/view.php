<?php
/* 
 * This file was created by Rashad Aliyev (RAliyev@avagr.com)
 *  on Mar 2, 2015 
 */

use yii\helpers\Html;
use yii\helpers\Url;

$current_url = Url::current();
$array = explode("/", $current_url);
#unset($array[0]); unset($array[1]);
if(strlen($array[1]) == 2){
  $array = array_slice($array, 2);
} else{
  $array = array_slice($array, 1);
}

$url = implode("/", $array);
?>


<ul class="header-topbar-links">
  <?php foreach ($langs as $lang):?>
    <li>
      <?= Html::a("<img src='".Yii::getAlias("@storageUrl")."/img/flags/{$lang->code}.png' alt='{$lang->title}' />" , ["/{$url}", 'language'=> $lang->code]) ?>
    </li>
  <?php endforeach;?>
</ul>
