<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 12/3/15
 * Time: 22:13
 */


use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\widgets\Pjax;

$js = <<< 'SCRIPT'
  jQuery(document).on('pjax:send', function() {
    jQuery('#subscription').hide('slow');
    jQuery("#subscription .brand-green").hide('slow');
    jQuery('#loading').html('<h2 id="text-success page-header">Sending, please wait....</h2>');
  }).on('pjax:complete', function() {
    jQuery('#loading').remove();
  });
SCRIPT;
$this->registerJs($js);


array_unshift($states , 'All states');
$subject = [];

foreach($states as $v){
  $subject[$v] = $v;
}

?>

<div id="loading" class="text-center text-success"></div>
<div id="subscription-content">

  <?php Pjax::begin([
    'clientOptions' => ['method' => 'POST', 'container' => 'x1'],
    'timeout' => 3000, 'enablePushState' => false,
  ]); ?>

    <?php $form = ActiveForm::begin([
      'id' => 'subscription',
      'action' => \yii\helpers\Url::to(['/site/subscription']),
      'options' => ['data-pjax' => "#x1" ]
    ]); ?>

    <div class="form-group">
      <?php echo $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Name *'])->label(false) ?>
    </div>
    <div class="form-group">
      <?php echo $form->field($model, 'email')->textInput(['type' => 'email' ,'maxlength' => true, 'placeholder' => 'Email *'])->label(false) ?>
    </div>
    <div class="form-group">
      <?=
      $form->field($model, 'states[]')->widget(Select2::className(),[
        'data' => $subject,
        //'pluginOptions' => ['maximumSelectionLength' => 3,],
        'options' => [
          'placeholder' => 'Please select state * ', 'multiple' => true, 'class' => 'form-control',
          'id' => 'subscription-select',
        ],
      ])->label(false);
      ?>
    </div>

    <?php echo Html::submitButton('Subscribe', ['class' => 'btn brand-green btn-read-more']) ?>
    <?php ActiveForm::end(); ?>
  <?php Pjax::end(); ?>
</div>