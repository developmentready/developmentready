<?php

/* 
 * This file was created by Rashad Aliyev (RAliyev@avagr.com)
 *  on Mar 24, 2015 
 */

namespace frontend\widgets;
use common\components\helpers\MenuHelper;
use yii\base\Widget;

class ListMenu extends Widget{
  public function init(){}

  public function run(){
    return $this->render('list-menu/view', [
      'output' => MenuHelper::getHeaderFooterMenuList(false),
    ]);
  }
}
