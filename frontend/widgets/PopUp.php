<?php

/* 
 * This file was created by Rashad Aliyev (RAliyev@avagr.com)
 *  on Apr 23, 2015 
 */

namespace frontend\widgets;
use common\models\ContactForm;

class PopUp extends \yii\bootstrap\Widget{
  public function init(){}

  public function run(){
    $model = new ContactForm();
    $states = \common\models\State::find()->select('id','title')->active()->orderBy('title')->asArray()->column();
    $model->scenario = 'subscription';
    
    return $this->render('pop-up', [
      'model' => $model,
      'states' => $states,
    ]);
  }
}