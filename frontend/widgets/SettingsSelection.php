<?php
/**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 9/3/15
 * Time: 00:56
 */

namespace frontend\widgets;

use Yii;
use common\models\Country;
use common\models\Currency;
use common\components\helpers\Setup;


class SettingsSelection extends \yii\bootstrap\Widget{
  public function init(){}

  public function run(){

    $default_country = Country::find()->where(['default_country' => 1])->one();
    $default_country_code = $default_country->code;

    $country = 'az';

    /*
        if(Yii::$app->request->cookies->getValue('country_id') == NULL){
          $ip = Setup::getUserIpAddress();
          if($ip !== '127.0.0.1'){
            try{
              $details = json_decode(file_get_contents("http://ipinfo.io/{$ip}"));
              $country = strtolower($details->country);
              if(empty($country) || (strlen($country) != 2)) $country = $details->country;
            } catch (ErrorException $e) {
              $country = $default_country_code;
            }
          } else {
            $country = $default_country_code;
          }
        }
    */

    $current_country = Country::find()->where(['code' => $country])->active()->one();

//    if(!empty($current_country)){
//      $current_country_id = $current_country->id;
//    } else {
//      $current_country_id = $default_country_code->id;
//    }

    // get the cookie collection (yii\web\CookieCollection) from the "request" component
    $cookies = Yii::$app->request->cookies;

    $current_country_id = 15;

    // get the "country" cookie value. If the cookie does not exist, return "en" as the default value.
    $country_id = $cookies->getValue('country_id', $current_country_id);
    $currency_id = $cookies->getValue('currency_id', $current_country->country_currency_id);

    return $this->render('selection/settings', [
      'countries' => Country::find()->active()->all(),
      'currencies' => Currency::find()->active()->all(),
      'location' => Country::find()->where(['{{%countries}}.id' => $country_id])->active()->one(),
      'default_currency' => Currency::findOne($currency_id),
      'country_id' => $country_id,
      'currency_id' => $currency_id,
    ]);
  }
}