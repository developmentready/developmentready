<?php

namespace frontend\widgets;
use common\models\User;

class UserInfo extends \yii\bootstrap\Widget{
  public function init(){}

  public function run(){
    return $this->render('user-info/view', [
      'model' => User::find()->where(['id' => \Yii::$app->user->id])->one()
    ]);
  }
}