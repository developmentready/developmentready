<?php

/* 
 * This file was created by Rashad Aliyev (RAliyev@avagr.com)
 *  on Apr 23, 2015 
 */

namespace frontend\widgets;
use common\models\ContactForm;

class Call extends \yii\bootstrap\Widget{
  public function init(){}

  public function run(){
    $model = new ContactForm();
    $model->scenario = 'call';
    
    return $this->render('contact/call', [
      'model' => $model,
    ]);
  }
}