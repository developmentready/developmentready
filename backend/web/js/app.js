$(function() {
  "use strict";

  //Make the dashboard widgets sortable Using jquery UI
  $(".connectedSortable").sortable({
    placeholder: "sort-highlight",
    connectWith: ".connectedSortable",
    handle: ".box-header, .nav-tabs",
    forcePlaceholderSize: true,
    zIndex: 999999
  }).disableSelection();
  $(".connectedSortable .box-header, .connectedSortable .nav-tabs-custom").css("cursor", "move");
});

$(function() {
  $('#brand-color').colorpicker();
  $('#font-color').colorpicker();
});

function showPrice(){
  if($("#property-price_type_id option:selected").val() === '6'){
    $("#price-in-number").css('display', 'block');
  } else {
    $("#price-in-number").css('display', 'none');
  };

  if($("#property-price_type_id option:selected").val() === '2'){
    $("#eoi-description").css('display', 'block');
  } else {
    $("#eoi-description").css('display', 'none');
  };
}

$(function(){
  $("#property-price_type_id").change(function(){
    showPrice();
  });
});


$(document).ready(function(){
  // During update, if selected value is correct show field
  showPrice();
});

// All decimal input fields have a class named 'number'
$('input.number').each(function () {
  $(this).keypress(function(e){
    // '46' is the keyCode for '.' , '44' for ','
    if(e.keyCode == '44' || e.charCode == '44'){
      // IE
      if(document.selection){
        // Determines the selected text. If no text selected,
        // the location of the cursor in the text is returned
        var range = document.selection.createRange();
        // Place the comma on the location of the selection,
        // and remove the data in the selection
        range.text = '.';
        // Chrome + FF
      }else if(this.selectionStart || this.selectionStart == '0'){
        // Determines the start and end of the selection.
        // If no text selected, they are the same and
        // the location of the cursor in the text is returned
        // Don't make it a jQuery obj, because selectionStart
        // and selectionEnd isn't known.
        var start = this.selectionStart;
        var end = this.selectionEnd;
        // Place the comma on the location of the selection,
        // and remove the data in the selection
        $(this).val($(this).val().substring(0, start) + '.'
          + $(this).val().substring(end, $(this).val().length));
        // Set the cursor back at the correct location in the text
        this.selectionStart = start + 1;
        this.selectionEnd = start +1;
      }else{
        // if no selection could be determined,
        // place the comma at the end.
        $(this).val($(this).val() + '.');
      }

      return false;
    }
  });
});
