<?php
/**
 * Created by PhpStorm.
 * User: rashad
 * Date: 4/6/16
 * Time: 03:05
 */

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\web\ForbiddenHttpException;

class Report extends Model{

  public $call_count;
  public $message_count;
  public $views_count;
  public $stat_start_date;
  public $stat_end_date;


  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['call_count', 'message_count', 'views_count'], 'integer'],
      [['stat_start_date', 'stat_end_date'], 'date' ,'format' => 'php:Y-m-d'],
      [['stat_start_date', 'stat_end_date'], 'safe'],
      [['stat_start_date'], 'compare', 'compareAttribute'=>'stat_end_date', 'operator'=>'<', 'skipOnEmpty'=>true],
      [['stat_end_date'], 'compare', 'compareAttribute'=>'stat_start_date', 'operator'=>'>', 'skipOnEmpty'=>true],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels(){
    return [
      'call_count' => Yii::t('app', 'Count of calls'),
      'message_count' => Yii::t('app', 'Messages count'),
      'views_count' => Yii::t('app', 'Views counter'),
      'stat_start_date' => Yii::t('app', 'Report from'),
      'stat_end_date' => Yii::t('app', 'Report till'),
    ];
  }
}