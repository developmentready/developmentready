<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Country;

/**
 * CountrySearch represents the model behind the search form about `common\models\Country`.
 */
class CountrySearch extends Country{

  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['id', 'status', 'creator_id', 'created_at', 'updater_id', 'updated_at'], 'integer'],
      [['title'], 'safe'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function scenarios(){
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
   * Creates data provider instance with search query applied
   *
   * @param array $params
   *
   * @return ActiveDataProvider
   */
  public function search($params){
    $query = Country::find();
    
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
    ]);

    $this->load($params);

    if (!($this->load($params) && $this->validate())){
      return $dataProvider;
    }

    $query->andFilterWhere([
      'status' => $this->status
    ]);

    $query->andFilterWhere(['like', 'code', $this->code])
          ->andFilterWhere(['like', 'title', $this->title]);
    return $dataProvider;
  }
}
