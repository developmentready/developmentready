<?php 

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Period;

/**
* PageSearch represents the model behind the search form about `common\models\Period`.
*/
class PeriodSearch extends Period{
  public $period;
  public $translation;

  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['id', 'status', 'priority'], 'integer'],
      [['translation'], 'safe'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function scenarios(){
  // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
   * Creates data provider instance with search query applied
   * @return ActiveDataProvider
   */
  public function search($params, $page_type_id = NULL, $except_main_page = false){
    if(is_null($page_type_id))
      $query = Period::find();
    else
      $query = Period::find()->where(['page_type_id' => $page_type_id]);

    if($except_main_page) $query = $query->andWhere('parent_id <> 1');

    $query->joinWith(['translation']);

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
    ]);

    if (!($this->load($params) && $this->validate())){
      return $dataProvider;
    }

    $query->andFilterWhere([
      'id' => $this->id,
      'status' => $this->status,
    ]);

    $query->andFilterWhere(['like', '{{%periods_lang}}.title', $this->translation]);
    return $dataProvider;
  }
}