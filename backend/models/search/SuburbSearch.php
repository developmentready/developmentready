<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/16/15
 * Time: 14:53
 */
namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Suburb;

/**
 * SuburbSearch represents the model behind the search form about `common\models\Suburb`.
 */
class SuburbSearch extends Suburb{

  public $state;

  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['id', 'status', 'state_id', 'is_city', 'belongs_to_city', 'post_code', 'creator_id', 'created_at', 'updater_id', 'updated_at'], 'integer'],
      [['title', 'state'], 'safe'],
      [['lat', 'lng'], 'number'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
   * Creates data provider instance with search query applied
   *
   * @param array $params
   *
   * @return ActiveDataProvider
   */
  public function search($params){
    $query = Suburb::find()->joinWith('state');

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
    ]);

    if (!($this->load($params) && $this->validate())) {
      return $dataProvider;
    }

    $query->andFilterWhere([
      'id' => $this->id,
      'status' => $this->status,
      'state_id' => $this->state_id,
      'is_city' => $this->is_city,
      'belongs_to_city' => $this->belongs_to_city,
      'post_code' => $this->post_code,
      'lat' => $this->lat,
      'lng' => $this->lng,
      'creator_id' => $this->creator_id,
      'created_at' => $this->created_at,
      'updater_id' => $this->updater_id,
      'updated_at' => $this->updated_at,
    ]);

    $query
      ->andFilterWhere(['like', 'title', $this->title])
      ->andFilterWhere(['like', '{{%states}}.`title`', $this->state]);

    return $dataProvider;
  }
}