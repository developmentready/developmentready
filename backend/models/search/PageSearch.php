<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Page;

/**
 * PageSearch represents the model behind the search form about `common\models\Page`.
 */
class PageSearch extends Page{
  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['id', 'status', 'priority', 'page_type_id', 'page_category_id', 'parent_id', 'view_counter', 'menu_type', 'related_media_id'], 'integer'],
      [['url_name', 'title', 'h1_tag', 'body', 'meta_keywords', 'meta_description', 'video_link'], 'safe'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function scenarios(){
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
   * Creates data provider instance with search query applied
   *
   * @param array $params
   *
   * @return ActiveDataProvider
   */
  public function search($params, $page_type_ids = NULL){
    $query = Page::find();

    if(!is_null($page_type_ids)) $query->where("`page_type_id` IN ({$page_type_ids})");

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
    ]);

    if (!($this->load($params) && $this->validate())) {
      return $dataProvider;
    }


    $query->andFilterWhere([
      'id' => $this->id,
      'status' => $this->status,
      'priority' => $this->priority,
      'page_type_id' => $this->page_type_id,
      'page_category_id' => $this->page_category_id,
      'parent_id' => $this->parent_id,
      'view_counter' => $this->view_counter,
      'menu_type' => $this->menu_type,
    ]);

    $query->andFilterWhere(['like', 'url_name', $this->url_name])
      ->andFilterWhere(['like', 'title', $this->title])
      ->andFilterWhere(['like', 'h1_tag', $this->h1_tag])
      ->andFilterWhere(['like', 'body', $this->body])
      ->andFilterWhere(['like', 'meta_keywords', $this->meta_keywords])
      ->andFilterWhere(['like', 'meta_description', $this->meta_description])
      ->andFilterWhere(['like', 'video_link', $this->video_link]);

    return $dataProvider;
  }
}
