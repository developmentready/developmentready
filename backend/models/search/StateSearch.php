<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/12/15
 * Time: 19:55
 */

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\State;

/**
 * CountrySearch represents the model behind the search form about `common\models\Country`.
 */
class StateSearch extends State{

  public $country;

  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['status', 'default'], 'integer'],
      [['title', 'country'], 'safe'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function scenarios(){
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
   * Creates data provider instance with search query applied
   *
   * @param array $params
   *
   * @return ActiveDataProvider
   */
  public function search($params){
    $query = State::find();

    $dataProvider = new ActiveDataProvider(['query' => $query,]);

    $this->load($params);

    if (!($this->load($params) && $this->validate())){
      return $dataProvider;
    }

    $query->andFilterWhere([
      'status' => $this->status,
      'default' => $this->default
    ]);

    $query->andFilterWhere(['like', 'title', $this->title])
      ->andFilterWhere(['like', '{{%countries}}.title', $this->country]);
    return $dataProvider;
  }
}