<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ContactForm;

/**
 * ContactSearch represents the model behind the search form about `common\models\ContactForm`.
 */
class ContactSearch extends ContactForm{
  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['id', 'contact_type'], 'integer'],
      [['name', 'email', 'subject', 'phone_number', 'message', 'created_at', 'slug'], 'safe'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function scenarios(){
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
   * Creates data provider instance with search query applied
   *
   * @param array $params
   *
   * @return ActiveDataProvider
   */
  public function search($params, $contact_type = 3){
    $query = ContactForm::find()->where(['contact_type' => $contact_type])
              ->orderBy([ 'created_at' => SORT_DESC ]);

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
    ]);

    $this->load($params);

    if (!$this->validate()) {
      // uncomment the following line if you do not want to any records when validation fails
      // $query->where('0=1');
      return $dataProvider;
    }

    $query->andFilterWhere([
      'id' => $this->id,
      'contact_type' => $this->contact_type,
      'created_at' => $this->created_at,
    ]);

    $query->andFilterWhere(['like', 'name', $this->name])
        ->andFilterWhere(['like', 'email', $this->email])
        ->andFilterWhere(['like', 'slug', $this->slug])
        ->andFilterWhere(['like', 'subject', $this->subject])
        ->andFilterWhere(['like', 'phone_number', $this->phone_number])
        ->andFilterWhere(['like', 'message', $this->message]);

    return $dataProvider;
  }

  /**
   * @param $params
   * @param int $contact_type
   * @return ActiveDataProvider
   */
  public function searchPropertyMessages($params, $contact_type = 3){
    $query = ContactForm::find()->where(['contact_type' => $contact_type])
                ->orderBy([ 'created_at' => SORT_DESC ]);

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
    ]);

    $this->load($params);

    if (!$this->validate()) {
      // uncomment the following line if you do not want to any records when validation fails
      // $query->where('0=1');
      return $dataProvider;
    }

    $query->andFilterWhere([
      'id' => $this->id
    ]);

    $query->andFilterWhere(['like', 'name', $this->name])
      ->andFilterWhere(['like', 'email', $this->email])
      ->andFilterWhere(['like', 'slug', $this->slug])
      ->andFilterWhere(['like', 'subject', $this->subject])
      ->andFilterWhere(['like', 'phone_number', $this->phone_number])
      ->andFilterWhere(['like', 'message', $this->message]);

    return $dataProvider;
  }

}
