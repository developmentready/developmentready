<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Settings;

/**
 * Settingsearch represents the model behind the search form about `common\models\Settings`.
 */
class SettingSearch extends Settings{
  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['id', 'creator_id', 'created_at', 'updater_id', 'updated_at'], 'integer'],
      [['name', 'text_value', 'description'], 'safe'],
      [['numeric_value'], 'number'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function scenarios(){
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
   * Creates data provider instance with search query applied
   *
   * @param array $params
   *
   * @return ActiveDataProvider
   */
  public function search($params){
    $query = Settings::find();

    $dataProvider = new ActiveDataProvider([
        'query' => $query,
    ]);

    $this->load($params);

    if (!$this->validate()) {
        // uncomment the following line if you do not want to any records when validation fails
        // $query->where('0=1');
        return $dataProvider;
    }

    $query->andFilterWhere([
        'id' => $this->id,
        'numeric_value' => $this->numeric_value,
        'creator_id' => $this->creator_id,
        'created_at' => $this->created_at,
        'updater_id' => $this->updater_id,
        'updated_at' => $this->updated_at,
    ]);

    $query->andFilterWhere(['like', 'name', $this->name])
        ->andFilterWhere(['like', 'text_value', $this->text_value])
        ->andFilterWhere(['like', 'description', $this->description]);

    return $dataProvider;
  }
}
