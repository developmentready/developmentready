<?php
/**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 7/11/15
 * Time: 20:40
 */

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\DocumentType;

/**
 * DocumentTypeSearch represents the model behind the search form about `common\models\DocumentType`.
 */
class DocumentTypeSearch extends DocumentType{
  public $documentType;
  public $translation;

  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['id', 'status', 'priority'], 'integer'],
      [['translation'], 'safe'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function scenarios(){
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
   * Creates data provider instance with search query applied
   * @return ActiveDataProvider
   */
  public function search($params, $page_type_id = NULL, $except_main_page = false){
    if(is_null($page_type_id))
      $query = DocumentType::find();
    else
      $query = DocumentType::find()->where(['page_type_id' => $page_type_id]);

    if($except_main_page) $query = $query->andWhere('parent_id <> 1');

    $query->joinWith(['translation']);

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
    ]);

    if (!($this->load($params) && $this->validate())){
      return $dataProvider;
    }

    $query->andFilterWhere([
      'id' => $this->id,
      'status' => $this->status,
    ]);

    $query->andFilterWhere(['like', '{{%document_types_lang}}.title', $this->translation]);
    return $dataProvider;
  }
}