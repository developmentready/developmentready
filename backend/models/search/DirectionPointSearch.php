<?php
/**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 7/11/15
 * Time: 20:38
 */

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\DirectionPoint;

/**
 * DirectionPointSearch represents the model behind the search form about `common\models\DirectionPoint`.
 */
class DirectionPointSearch extends DirectionPoint{
  public $directionPoint;
  public $translation;

  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['id', 'status', 'city_id', 'priority'], 'integer'],
      [['translation'], 'safe'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function scenarios(){
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
   * Creates data provider instance with search query applied
   * @return ActiveDataProvider
   */
  public function search($params){
    $query = DirectionPoint::find();

    $query->joinWith(['translation']);
    $query->joinWith(['city']);

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
    ]);

    if (!($this->load($params) && $this->validate())){
      return $dataProvider;
    }

    $query->andFilterWhere([
      'id' => $this->id,
      'status' => $this->status,
      'city_id' => $this->city_id
    ]);

    $query->andFilterWhere(['like', '{{%direction_points_lang}}.title', $this->translation]);
    return $dataProvider;
  }
}