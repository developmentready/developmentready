<?php
/**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 7/11/15
 * Time: 20:31
 */

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Amenity;

/**
 * AmenitySearch represents the model behind the search form about `common\models\Amenity`.
 */
class AmenitySearch extends Amenity{
  public $amenity;
  public $translation;

  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['id', 'status', 'priority'], 'integer'],
      [['translation'], 'safe'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function scenarios(){
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
   * Creates data provider instance with search query applied
   * @return ActiveDataProvider
   */
  public function search($params, $page_type_id = NULL, $except_main_page = false){
    if(is_null($page_type_id))
      $query = Amenity::find();
    else
      $query = Amenity::find()->where(['page_type_id' => $page_type_id]);

    if($except_main_page) $query = $query->andWhere('parent_id <> 1');

    $query->joinWith(['translation']);

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
    ]);

    if (!($this->load($params) && $this->validate())){
      return $dataProvider;
    }

    $query->andFilterWhere([
      'id' => $this->id,
      'status' => $this->status,
    ]);

    $query->andFilterWhere(['like', '{{%amenities_lang}}.title', $this->translation]);
    return $dataProvider;
  }
}