<?php

/* 
 * This file was created by Rashad Aliyev (RAliyev@avagr.com)
 *  on Jul 1, 2015 
 */

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\City;

/**
 * CountrySearch represents the model behind the search form about `common\models\Country`.
 */
class CitySearch extends City{
  public $city;
  public $translation;
  
  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['id', 'status', 'creator_id', 'country_id', 'created_at', 'updater_id', 'updated_at'], 'integer'],
      [['translation'], 'safe'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function scenarios(){
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
   * Creates data provider instance with search query applied
   *
   * @param array $params
   *
   * @return ActiveDataProvider
   */
  public function search($params){
    $query = City::find()->joinWith('translation');
    
    
    $dataProvider = new ActiveDataProvider([
      'query' => $query,
    ]);

    $this->load($params);

    if (!($this->load($params) && $this->validate())){
      return $dataProvider;
    }

    $query->andFilterWhere([
      'status' => $this->status
    ]);

    $query->andFilterWhere(['like', '{{%cities_lang}}.title', $this->translation]);
    return $dataProvider;
  }
}
