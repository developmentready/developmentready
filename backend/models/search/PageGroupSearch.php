<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PageGroup;

/**
 * PageGroupSearch represents the model behind the search form about `common\models\PageGroup`.
 */
class PageGroupSearch extends PageGroup{
  public $pageGroup;
  public $pageGroupLang;
  
  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['id', 'status', 'creator_id', 'created_at', 'updater_id', 'updated_at'], 'integer'],
      [['pageGroupLang'], 'safe'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function scenarios(){
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
   * Creates data provider instance with search query applied
   *
   * @param array $params
   *
   * @return ActiveDataProvider
   */
  public function search($params){
    $query = PageGroup::find();

    $query->joinWith(['pageGroupLang']);
    
    $dataProvider = new ActiveDataProvider([
      'query' => $query,
    ]);

    if (!($this->load($params) && $this->validate())){
      return $dataProvider;
    }

    $query->andFilterWhere([
        'id' => $this->id,
        'status' => $this->status,
        'updater_id' => $this->updater_id,
        'updated_at' => $this->updated_at,
    ]);
    
    $query->andFilterWhere(['like', '{{%page_group_lang}}.title', $this->pageGroupLang]);
            
    return $dataProvider;
  }
}
