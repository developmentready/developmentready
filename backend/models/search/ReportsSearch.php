<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Period;
use common\models\PropertyReport;

/**
 * PageSearch represents the model behind the search form about `common\models\Period`.
 */
class ReportsSearch extends PropertyReport {

  public $start_date;
  public $end_date;

  /**
   * @inheritdoc
   */
  public function rules() {
    return [
      [['property_id'], 'integer'],
      [[ 'end_date', 'start_date' ], 'safe'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function scenarios(){
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
   * Creates data provider instance with search query applied
   * @return ActiveDataProvider
   */
  public function searchStates( $params ) {
    $query = PropertyReport::find()->where(['reference' => 'state_count']);

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
    ]);

    $dataProvider->setSort(['defaultOrder' => ['recorded_at' => SORT_DESC,]]);

    if (!($this->load($params) && $this->validate())) {
      return $dataProvider;
    }

    $query->andFilterWhere([
      'id' => $this->id,
      'property_id' => $this->property_id
    ]);

    $query->andFilterWhere(['>=', 'date(({{%property_reports}}.recorded_at)', $this->start_date]);
    $query->andFilterWhere(['<=', 'date(({{%property_reports}}.recorded_at)', $this->end_date]);
    return $dataProvider;
  }
}