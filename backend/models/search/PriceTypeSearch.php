<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/19/15
 * Time: 16:31
 */

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PriceType;

/**
 * PriceTypeSearch represents the model behind the search form about `common\models\PriceType`.
 */
class PriceTypeSearch extends PriceType
{
  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['id', 'status', 'creator_id', 'created_at', 'updater_id', 'updated_at'], 'integer'],
      [['title'], 'safe'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
   * Creates data provider instance with search query applied
   *
   * @param array $params
   *
   * @return ActiveDataProvider
   */
  public function search($params)
  {
    $query = PriceType::find();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
    ]);

    if (!($this->load($params) && $this->validate())) {
      return $dataProvider;
    }

    $query->andFilterWhere([
      'id' => $this->id,
      'status' => $this->status,
      'creator_id' => $this->creator_id,
      'created_at' => $this->created_at,
      'updater_id' => $this->updater_id,
      'updated_at' => $this->updated_at,
    ]);

    $query->andFilterWhere(['like', 'title', $this->title]);

    return $dataProvider;
  }
}