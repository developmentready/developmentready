<?php
/**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 7/11/15
 * Time: 20:43
 */

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Property;
use yii\data\SqlDataProvider;
use yii\db\Query;

/**
 * PropertySearch represents the model behind the search form about `common\models\Property`.
 */
class PropertySearch extends Property{

  public $suburb;
  public $image;
  public $user;
  public $call_count;
  public $message_count;
  public $views_count;
  public $stat_start_date;
  public $stat_end_date;

  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      [['id', 'priority', 'status', 'page_robot_indexing', 'price_type_id', 'square_measure_id',
        'contract_type_id', 'suburb_id', 'view_count', 'sold', 'accept_terms', 'publication_date',
        'featured_till', 'creator_id', 'updater_id', 'updated_at', 'views_count', 'call_count', 'message_count'],
        'integer'],

      [['suburb', 'description', 'contract_details', 'location', 'created_at', 'user'], 'safe'],
      [['stat_start_date', 'stat_end_date'], 'date' ,'format' => 'php:Y-m-d'],
      [['stat_start_date', 'stat_end_date'], 'safe'],
      [['price_in_numbers', 'square', 'lat', 'lng'], 'number'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function scenarios(){
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
   * Creates data provider instance with search query applied
   *
   * @param array $params
   *
   * @return ActiveDataProvider
   */
  public function search($params){
    $query = Property::find()->orderBy(['created_at' => SORT_DESC]);

    $query->joinWith(['suburbs', 'propertyUsers']);

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
    ]);

    /**
     * Setup your sorting attributes
     * Note: This is setup before the $this->load($params)
     * statement below
     */
    $dataProvider->setSort([
      'attributes' => [
        'id',
        'location',
        'user' => [
          'asc' => ['{{%user}}.name' => SORT_ASC],
          'desc' => ['{{%user}}.name' => SORT_DESC],
          'label' => 'Agency \ User',
          'default' => SORT_ASC
        ],
        'status',
        'contract_type_id',
        'sold',
        'updated_at'
      ]
    ]);

    if (!($this->load($params) && $this->validate())) {
      return $dataProvider;
    }

    $query->andFilterWhere([
      '{{%properties}}.id' => $this->id,
      '{{%properties}}.status' => $this->status,
      'sold' => $this->sold,
      'price_type_id' => $this->price_type_id,
      'contract_type_id' => $this->contract_type_id,
      '{{%properties}}.suburb_id' => $this->suburb_id,
      'featured_till' => $this->featured_till,
    ]);

    $query->andFilterWhere(['like', 'description', $this->description])
      ->andFilterWhere(['like', '{{%suburbs}}.title', $this->suburb])
      ->andFilterWhere(['like', '{{%suburbs}}.post_code', $this->suburb])
      ->andFilterWhere(['like', '{{%user}}.name', $this->user])
      ->andFilterWhere(['like', 'contract_details', $this->contract_details])
      ->andFilterWhere(['like', 'location', $this->location]);

    return $dataProvider;
  }

  /**
   * @param $params
   * @return ActiveDataProvider
   */
  public function searchFeatured($params){
    $query = Property::find()->where('featured_till > :today AND {{%properties}}.status = 1', ['today' => time()])->orderBy(['created_at' => SORT_DESC]);

    $query->joinWith(['suburbs', 'propertyUsers']);

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
    ]);

    /**
     * Setup your sorting attributes
     * Note: This is setup before the $this->load($params)
     * statement below
     */
    $dataProvider->setSort([
      'attributes' => [
        ['updated_at' => SORT_DESC],
        'id',
        'location',
        'user' => [
          'asc' => ['{{%user}}.name' => SORT_ASC],
          'desc' => ['{{%user}}.name' => SORT_DESC],
          'label' => 'Agency \ User',
          'default' => SORT_ASC
        ],
        'status',
        'contract_type_id',
        'sold',
        'updated_at'
      ]
    ]);

    if (!($this->load($params) && $this->validate())) {
      return $dataProvider;
    }

    $query->andFilterWhere([
      '{{%properties}}.id' => $this->id,
      '{{%properties}}.status' => $this->status,
      'sold' => $this->sold,
      'price_type_id' => $this->price_type_id,
      'contract_type_id' => $this->contract_type_id,
      '{{%properties}}.suburb_id' => $this->suburb_id,
      'featured_till' => $this->featured_till,
    ]);

    $query->andFilterWhere(['like', 'description', $this->description])
      ->andFilterWhere(['like', '{{%suburbs}}.title', $this->suburb])
      ->andFilterWhere(['like', '{{%suburbs}}.post_code', $this->suburb])
      ->andFilterWhere(['like', '{{%user}}.name', $this->user])
      ->andFilterWhere(['like', 'contract_details', $this->contract_details])
      ->andFilterWhere(['like', 'location', $this->location]);

    return $dataProvider;
  }

  /**
   * Creates data provider instance with search query applied
   *
   * @param array $params
   *
   * @return ActiveDataProvider
   */
  public function searchStats($params) {
    $subQuery_views_count = (new Query)
        ->select('SUM( CASE WHEN `pr_property_reports`.`reference` = \'view_count\' THEN `pr_property_reports`.counter  ELSE 0 END ) as views_count, property_id')
        ->from(' pr_property_reports')
        ->groupBy('property_id');
    $subQuery_call_count = (new Query)
        ->select('SUM( CASE WHEN `pr_property_reports`.`reference` = \'call_count\' THEN `pr_property_reports`.counter  ELSE 0 END ) as call_count, property_id')
        ->from(' pr_property_reports')
        ->groupBy('property_id');
    $subQuery_message_count = (new Query)
        ->select('SUM( CASE WHEN `pr_property_reports`.`reference` = \'message_count\' THEN `pr_property_reports`.`counter` ELSE 0 END ) as message_count, property_id')
        ->from(' pr_property_reports')
        ->groupBy('property_id');

    $query = Property::find()
        ->select("`pr_properties`.*,
        query_views_count.views_count,
        query_call_count.call_count,
        query_message_count.message_count")
        ->leftJoin(['query_views_count' => $subQuery_views_count], 'query_views_count.property_id = pr_properties.id')
        ->leftJoin(['query_call_count' => $subQuery_call_count], 'query_call_count.property_id = pr_properties.id')
        ->leftJoin(['query_message_count' => $subQuery_message_count], 'query_message_count.property_id = pr_properties.id')
        ->join('left JOIN', 'pr_suburbs ON `pr_suburbs`.`id` = `pr_properties`.`suburb_id`')
        //->andWhere([ '{{%property_images}}.default' => 1 ])
        ->groupBy('{{%properties}}.id');

    //$query->joinWith(['suburbs', 'propertyUsers']);

    $dataProvider = new ActiveDataProvider([
        'query' => $query,
    ]);

    /**
     * Setup your sorting attributes
     * Note: This is setup before the $this->load($params)
     * statement below
     */
    $dataProvider->setSort([
        'attributes' => [ 'id', 'location', 'status', 'publication_date', 'sold', 'updated_at',
                          'views_count' => [
                              'asc' => ['views_count' => SORT_ASC ],
                              'desc' => ['views_count' => SORT_DESC],
                              'label' => 'Message Count',
                          ],
                          'call_count' => [
                              'asc' => ['call_count' => SORT_ASC ],
                              'desc' => ['call_count' => SORT_DESC],
                              'label' => 'Call Count',
                          ],
                          'message_count' => [
                              'asc' => ['message_count' => SORT_ASC],
                              'desc' => ['message_count' => SORT_DESC],
                              'label' => 'Message Count',
                          ],
        ],
        'defaultOrder' => [
            'publication_date' => SORT_DESC,
        ]

    ]);

    if (!($this->load($params) && $this->validate())) {
      return $dataProvider;
    }

    $query->andFilterWhere([
        '{{%properties}}.id' => $this->id,
        'sold' => $this->sold,
        '{{%properties}}.suburb_id' => $this->suburb_id,
        'featured_till' => $this->featured_till,
    ]);

    $query->andFilterWhere(['like', 'location', $this->location]);

    return $dataProvider;
  }

  /**
   * Creates data provider instance with search query applied
   *
   * @param array $params
   *
   * @return ActiveDataProvider
   */
  public function searchStatsSortable($params) {
    $count = Yii::$app->db->createCommand('SELECT COUNT(*) FROM pr_properties WHERE status=:status', [
      ':status' => 1
    ])->queryScalar();

    $select  =  "SELECT `pr_properties`.*, `pr_property_reports`.`reference`, `pr_property_reports`.`property_id`, ";
    $select .=  "`pr_property_reports`.`counter`, `pr_property_images`.`default`, `pr_property_images`.`property_id`, `pr_property_images`.`file_name`, ";
    $select .=  "SUM( CASE WHEN `pr_property_reports`.`reference` = 'view_count' THEN `pr_property_reports`.counter  ELSE 0 END ) as view_count, ";
    $select .=  "SUM( CASE WHEN `pr_property_reports`.`reference` = 'call_count' THEN `pr_property_reports`.counter  ELSE 0 END ) as call_count, ";
    $select .=  "SUM( CASE WHEN `pr_property_reports`.`reference` = 'message_count' THEN `pr_property_reports`.counter ELSE 0 END ) as message_count ";
    $from    =  "FROM `pr_properties` ";
    $join    =  "LEFT JOIN `pr_suburbs` ON `pr_properties`.`suburb_id` = `pr_suburbs`.`id` ";
    $join   .=  "LEFT JOIN `pr_property_images` ON `pr_properties`.`id` = `pr_property_images`.`property_id` ";
    $join   .=  "LEFT JOIN `pr_property_reports` ON `pr_properties`.`id` = `pr_property_reports`.`property_id` ";
    $where   =  "WHERE `pr_properties`.`status` =:status AND `pr_property_images`.`default` = 1 ";
    $group   =  "GROUP BY `pr_properties`.`id` ";


    $dataProvider = new SqlDataProvider([
      'sql' => $select.$from.$join.$where.$group,
      'params' => [':status' => 1],
      'totalCount' => $count,
      'sort' => [
        'attributes' => [
          'id',
          'location',
          'status',
          'publication_date',
          'sold',
          'updated_at',
          'call_count' => [
            'asc' => ['message_count' => SORT_ASC ],
            'desc' => ['message_count' => SORT_DESC],
            'label' => 'Message Count',
          ],
          'message_count' => [
            'asc' => ['message_count' => SORT_ASC],
            'desc' => ['message_count' => SORT_DESC],
            'label' => 'Message Count',
          ],
        ],
        'defaultOrder' => [
          'publication_date' => SORT_DESC,
        ]
      ],
      'pagination' => [
        'pageSize' => 20,
      ],
    ]);

    if (!($this->load($params) && $this->validate())) {
      return $dataProvider;
    }

    return $dataProvider;
  }
}