<?php
namespace backend\models;

use common\models\User;
use common\models\UserProfile;
use yii\base\Model;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Create user form
 */
class UserForm extends Model{
  public $username;
  public $name;
  public $email;
  public $password;
  public $status;
  public $roles;
  public $user_type;

  private $model;

  /**
   * @inheritdoc
   */
  public function rules(){
    return [
      ['username', 'filter', 'filter' => 'trim'],
      ['name', 'required'],
      ['username', 'unique', 'targetClass'=>'\common\models\User', 'filter' => function ($query) {
        if (!$this->getModel()->isNewRecord) {
          $query->andWhere(['not', ['id'=>$this->getModel()->id]]);
        }
      }],
      ['username', 'string', 'min' => 2, 'max' => 255],

      ['email', 'filter', 'filter' => 'trim'],
      ['email', 'required'],
      ['email', 'email'],
      ['email', 'unique', 'targetClass'=> '\common\models\User', 'filter' => function ($query) {
        if (!$this->getModel()->isNewRecord) {
          $query->andWhere(['not', ['id'=>$this->getModel()->id]]);
        }
      }],

      ['password', 'required', 'on'=>'create'],
      ['password', 'string', 'min' => 6],

      [['status'], 'boolean'],
      [['roles'], 'each',
        'rule' => ['in', 'range' => ArrayHelper::getColumn(
          Yii::$app->authManager->getRoles(),
          'name'
        )]
      ],

      ['user_type', 'required'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels(){
    return [
      'username' => Yii::t('backend', 'Username'),
      'name' => Yii::t('backend', 'Full Name'),
      'email' => Yii::t('backend', 'Email'),
      'password' => Yii::t('backend', 'Password'),
      'roles' => Yii::t('backend', 'Roles'),
      'user_type' => Yii::t('backend', 'User type'),
    ];
  }

  public function setModel($model){
    $this->username = $model->username;
    $this->email = $model->email;
    $this->status = $model->status;
    $this->user_type = $model->user_type;
    $this->name  = $model->name;
    $this->model = $model;
    $this->roles = ArrayHelper::getColumn(
      Yii::$app->authManager->getRolesByUser($model->getId()),
      'name'
    );
    return $this->model;
  }

  public function getModel(){
    if (!$this->model) {
      $this->model = new User();
    }
    return $this->model;
  }

  /**
   * Signs user up.
   *
   * @return User|null the saved model or null if saving fails
   */
  public function save(){
    if ($this->validate()) {
      $model = $this->getModel();
      $isNewRecord = $model->getIsNewRecord();
      $model->username = $this->username;
      $model->email = $this->email;
      $model->status = $this->status;
      $model->user_type = $this->user_type;
      $model->name = $this->name;
      if ($this->password) {
        $model->setPassword($this->password);
      }
      if ($model->save() && $isNewRecord) {
        $model->afterSignup();
      }
      $auth = Yii::$app->authManager;
      $auth->revokeAll($model->getId());

      if ($this->roles && is_array($this->roles)) {
        foreach ($this->roles as $role) {
          $auth->assign($auth->getRole($role), $model->getId());
        }
      }

      return !$model->hasErrors();
    }
    return null;
  }
}
