<?php

namespace backend\controllers;

use Yii;
use common\models\Page;
use backend\models\search\PageSearch;
use common\components\helpers\Setup;
use common\components\classes\BackendController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PageController implements the CRUD actions for Page model.
 */
class PageController extends BackendController{
  public function behaviors(){
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
    ];
  }

  /**
   * Lists all Page models.
   * @return mixed
   */
  public function actionIndex(){
    $searchModel = new PageSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams, "39");

    $pages = Page::find()->where(['menu_type' => 1])->andWhere("`page_type_id` IN (39)")->active()->makeSort()->all();

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
      'pages' => $pages,
    ]);
  }

  /**
   * Displays a single Page model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id){
    return $this->render('view', [
      'model' => $this->findModel($id),
    ]);
  }

  /**
   * Creates a new Page model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate(){
    $model = new Page();
    $model->scenario = 'page-with-header';

    if ($model->load(Yii::$app->request->post())) {

      if($model->menu_type == 2) $model->page_template_id = 5;

      $model->page_type_id = 39;

      if($model->save()){
        if(!empty($file = \Yii::$app->request->post('Page', '')['fileAttributes'])){

          # These methods call from CommonActiveRecord
          $model->saveSingleImage($file, $model->id);
        }

        \Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-success'],
          'body'=>Yii::t('app', 'All data was successfully saved')]);
        return $this->redirect(['view', 'id' => $model->id]);
      } else {
        \Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'danger'],
          'body'=> 'Some error occurred during saving data.']);
        return $this->render('create', ['model' => $model,
          'parent' => Page::find()->where(['page_type_id' => 39])->active()->all(),]);
      }
    } else {
      return $this->render('create', [
        'model' => $model,
        'parent' => Page::find()->where(['page_type_id' => 39])->active()->all(),
      ]);
    }
  }

  /**
   * Updates an existing Page model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id){
    $model = $this->findModel($id);
    $model->scenario = 'page-update';

    if ($model->load(Yii::$app->request->post())) {
      if($model->menu_type == 2) $model->page_template_id = 5;
      else $model->page_template_id = 0;

      if($model->save()){
        if(!empty($file = \Yii::$app->request->post('Page', '')['fileAttributes'])){

          # These methods call from CommonActiveRecord
          $model->saveSingleImage($file, $model->id);
        }

        \Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-success'],
          'body'=>Yii::t('app', 'All data was successfully saved')]);
        return $this->redirect(['view', 'id' => $model->id]);
      } else {
        \Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-danger'],
          'body'=>Yii::t('app', 'Error occurred during saving data')]);
        return $this->refresh();
      }

    } else {
      return $this->render('update', [
        'model' => $model,
        'parent' => Page::find()->where(['page_type_id' => 39])->active()->all(),
      ]);
    }
  }

  /**
   * Deletes an existing Page model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id){
    $this->findModel($id)->delete();

    return $this->redirect(['index']);
  }

  /**
   * Finds the Page model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Page the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id){
    if (($model = Page::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

}