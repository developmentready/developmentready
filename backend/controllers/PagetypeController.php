<?php

namespace backend\controllers;

use Yii;
use common\models\PageType;
use yii\data\ActiveDataProvider;
use common\components\classes\BackendController;
use common\models\Language;
use common\models\PageTypesLang;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * hhhhhhhh555555555
 * PagetypeController implements the CRUD actions for PageType model.
 */
class PagetypeController extends BackendController{
  public function behaviors(){
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
    ];
  }

  /**
   * Lists all PageType models.
   * @return mixed
   */
  public function actionIndex(){    
    $dataProvider = new ActiveDataProvider([
      'query' => PageType::find(),
    ]);

    return $this->render('index', [
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single PageType model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id){
    return $this->render('view', [
      'model' => $this->findModel($id),
    ]);
  }

  /**
   * Creates a new PageType model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate(){
    $model = new PageType();
    $lang_model = new PageTypesLang();
    $active_langs = Language::find()->where('status = 1')->all();
    
    if($model->load(Yii::$app->request->post()) && $model->save()){
      $count = count(Yii::$app->request->post('PageTypesLang', []));
      $items = [new PageTypesLang()];
      for($i = 1; $i <= $count; $i++){
        $items[] = new PageTypesLang();
      }
      if(PageTypesLang::loadMultiple($items, Yii::$app->request->post())){
        foreach($items as $item):
          $item->page_type_id = $model->id;
          if($item->save()){
            Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-success'], 'body'=>Yii::t('backend', 'All data was successfully saved')]);
          } else {
            Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-danger'], 'body'=>Yii::t('backend', 'Error occurred during saving all data')]);
          }
        endforeach;
      } else {
        Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-danger'], 'body'=>Yii::t('backend', 'Error occurred during saving language realted data')]);
      }
      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('create', [
        'model' => $model,
        'lang_model' => $lang_model,
        'active_langs' => $active_langs,
      ]);
    }
  }

  /**
   * Updates an existing PageType model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id){
    $model = $this->findModel($id);
    $lang_model = $this->getItemsToUpdate($id);
    $active_langs = Language::find()->where('status = 1')->all();
    if ($model->load(Yii::$app->request->post()) && $model->save()){
      if(PageTypesLang::loadMultiple($lang_model, Yii::$app->request->post())
          && PageTypesLang::validateMultiple($lang_model)){
        foreach($lang_model as $item):
          //if($item->updateAll(['title'=>$item->title, 'body' => $item->body], 'lang_id ='.$item->lang_id)){
          if($item->save()){
            Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-success'], 'body'=>Yii::t('backend', 'All data was successfully saved')]);
          } else {
            Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-danger'], 'body'=>Yii::t('backend', 'Error occurred during saving all data')]);
          }
        endforeach;
      } else {
        Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-danger'], 'body'=>Yii::t('backend', 'Error occurred during saving language realted data')]);
      }
      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('update', [
        'model' => $model,
        'lang_model' => $lang_model,
        'active_langs' => $active_langs,
      ]);
    }
  }

  /**
   * Deletes an existing PageType model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id){
    $this->findModel($id)->delete();
    return $this->redirect(['index']);
  }

  /**
   * Finds the PageType model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return PageType the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id){
    if (($model = PageType::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }
  
  
  // fetch the list of Item models for updating
  public function getItemsToUpdate($id){
    if (($model = PageTypesLang::find()->where(['page_type_id' => $id])->all()) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }
}
