<?php

namespace backend\controllers;

use Yii;
use common\models\ContactForm;
use backend\models\search\ContactSearch;
use common\components\classes\BackendController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ContactController implements the CRUD actions for ContactForm model.
 */
class ContactController extends BackendController{
  public function behaviors(){
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
    ];
  }

  /**
   * Lists all ContactForm models.
   * @return mixed
   */
  public function actionProperty(){
    $searchModel = new ContactSearch();
    $dataProvider = $searchModel->searchPropertyMessages(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }


  /**
   * Lists all ContactForm models.
   * @return mixed
   */
  public function actionAgency(){
    $searchModel = new ContactSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 4);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }


  /**
  * Lists all ContactForm models.
  * @return mixed
  */
  public function actionSubscribers(){
    $searchModel = new ContactSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 1);

    return $this->render('subscribers', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   *
   * @return string
   */
  public function actionMessages(){
    $searchModel = new ContactSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 2);

    return $this->render('messages', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }


  /**
   * Displays a single ContactForm model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id){
    return $this->render('view', [
      'model' => $this->findModel($id),
    ]);
  }

  /**
   * Creates a new ContactForm model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate(){
    $model = new ContactForm();

    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('create', [
        'model' => $model,
      ]);
    }
  }

  /**
   * Updates an existing ContactForm model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id){
    $model = $this->findModel($id);

    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('update', [
        'model' => $model,
      ]);
    }
  }

  /**
   * Deletes an existing ContactForm model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id){
    $this->findModel($id)->delete();

    return $this->redirect(['index']);
  }

  /**
   * Finds the ContactForm model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return ContactForm the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id){
    if (($model = ContactForm::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }
}
