<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/18/15
 * Time: 08:14
 */
namespace backend\controllers;

use Yii;
use common\models\AgencyAgent;
use common\models\Agency;
use common\models\AgencyPosition;
use backend\models\search\AgencyAgentSearch;
use common\components\classes\BackendController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AgentController implements the CRUD actions for AgencyAgent model.
 */

class AgentController extends BackendController{
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
    ];
  }

  /**
   * Lists all AgencyAgent models.
   * @return mixed
   */
  public function actionIndex(){
    $searchModel = new AgencyAgentSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single AgencyAgent model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id){
    return $this->render('view', [
      'model' => $this->findModel($id),
    ]);
  }

  /**
   * Creates a new AgencyAgent model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate(){
    $model = new AgencyAgent();

    if ($model->load(Yii::$app->request->post()) && $model->save()) {

      $image = Yii::$app->request->post('AgencyAgent', '')["fileAttributes"];

      if(!empty($image)) {
        $model->saveSingleImage($image, $model->id, 'AgencyAgent', 150, '#ffffff', 150);
      }

      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('create', [
        'model' => $model,
        'agencies' => Agency::find()->active()->all(),
        'positions' => AgencyPosition::find()->active()->all(),
      ]);
    }
  }

  /**
   * Updates an existing AgencyAgent model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id){
    $model = $this->findModel($id);

    if ($model->load(Yii::$app->request->post()) && $model->save()) {

      $image = Yii::$app->request->post('AgencyAgent', '')["fileAttributes"];

      if(!empty($image)) {
        $model->saveSingleImage($image, $model->id, 'AgencyAgent', 150, '#ffffff', 150);
      }

      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('update', [
        'model' => $model,
        'agencies' => Agency::find()->active()->all(),
        'positions' => AgencyPosition::find()->active()->all(),
      ]);
    }
  }

  /**
   * Deletes an existing AgencyAgent model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id){
    $this->findModel($id)->delete();

    return $this->redirect(['index']);
  }

  /**
   * Finds the AgencyAgent model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return AgencyAgent the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id)
  {
    if (($model = AgencyAgent::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }
}