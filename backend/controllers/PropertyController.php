<?php
/**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 8/29/15
 * Time: 14:59
 * 111
 */

namespace backend\controllers;

use Yii;
use common\models\Property;
use common\models\PropertyImages;
use yii\helpers\Url;
use backend\models\search\PropertySearch;
use yii\db\Query;
use common\components\classes\BackendController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PropertyController implements the CRUD actions for Property model.
 */
class PropertyController extends BackendController{
  public function behaviors(){
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
    ];
  }

  /**
   * Lists all Property models.
   * @return mixed
   */
  public function actionIndex(){
    $searchModel = new PropertySearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Lists all Property models.
   * @return mixed
   */
  public function actionFeatured(){
    $searchModel = new PropertySearch();
    $dataProvider = $searchModel->searchFeatured(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Lists all Property models.
   * @return mixed
   */
  public function actionStats() {
    $searchModel = new PropertySearch();
    $dataProvider = $searchModel->searchStats(Yii::$app->request->queryParams);

    return $this->render('stats', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single Property model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id) {
    return $this->render('view', [
      'model' => $this->findModel($id),
    ]);
  }

  /**
   * Creates a new Property model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate(){
    $model = new Property();

    if ($model->load(Yii::$app->request->post())) {
      # if property set as featured till for some date it will be set.
      if(!empty($till_date = Yii::$app->request->post('Property')["featured_till"])) $model->featured_till = strtotime($till_date);
      if(!empty($publication_date = Yii::$app->request->post('Property')["publication_date"])) $model->publication_date = strtotime($publication_date);

      if($model->save()){

        Property::setLinks($model,'propertyTypes', Yii::$app->request->post('Property')["propertyTypes"]);

        $images = Yii::$app->request->post('Property')["fileAttributes"];
        if(!empty($images)) $model->saveUploadedPropertyImages($images, $model->id);

        return $this->redirect(['view', 'id' => $model->id]);
      }
      echo "Couldn't save";
    } else {
      return $this->render('create', [
        'model' => $model,
      ]);
    }
  }

  /**
   * Updates an existing Property model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id){
    $model = $this->findModel($id);
    $read_script = '$( function() {
                    $( "#sortable" ).sortable({
                      stop:function(){
                        genereteImgSort();
                      }
                    });
                    $( "#sortable" ).disableSelection();
                  } );
                  function genereteImgSort(){
                    $("#img_sort_mass").html("");
                    $("#sortable>div").each(function(index){
                      var img_url = $(this).find("img").attr("src");
                      var img_id = $(this).attr("data-id");
                      $("#img_sort_mass").append("<input type=\'hidden\' name=\'sort_img["+index+"][url]\' value=\'"+img_url+"\'>");
                      $("#img_sort_mass").append("<input type=\'hidden\' name=\'sort_img["+index+"][id]\' value=\'"+img_id+"\'>");
                    });
                  }';
    if ($model->load(Yii::$app->request->post())) {

      if(!empty($till_date = Yii::$app->request->post('Property')["featured_till"])) $model->featured_till = strtotime($till_date);
      if(!empty($publication_date = Yii::$app->request->post('Property')["publication_date"])) $model->publication_date = strtotime($publication_date);


      if($model->save()){
        Property::setLinks($model,'propertyTypes', Yii::$app->request->post('Property')["propertyTypes"]);

        $images = Yii::$app->request->post('Property')["fileAttributes"];
        if(!empty($images)) $model->saveUploadedPropertyImages($images, $model->id);

        if(\Yii::$app->request->post('sort_img')){
          $i = 1;
          foreach(\Yii::$app->request->post('sort_img') as $item){
            $fName = pathinfo($item['url']);
            $img = $fName['filename'] . '.' . $fName['extension'];
            $obj = PropertyImages::find()->where(['file_name' => $img, 'property_id' => $model->id])->one();
            $obj->sort_order = $i;
            $obj->save();
            $i++;
          }
        }

        return $this->redirect(['view', 'id' => $model->id]);
      }
      $this->view->registerJs($read_script, yii\web\View::POS_READY);
      $this->render('update', ['model' => $model]);
    } else {
      $this->view->registerJs($read_script, yii\web\View::POS_READY);
      return $this->render('update', [
        'model' => $model,
      ]);
    }
  }

  /**
   * Deletes an existing Property model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id){
    $model = $this->findModel($id);
    if($model->delete()){
      $images_thumb_folder = Yii::getAlias('@storage')."/properties/thumb/{$id}";
      if(is_dir($images_thumb_folder)){
        #!!!! Don't play with this method, you can remove all folders
        \common\components\helpers\Setup::deleteDir($images_thumb_folder) ;
      }

      $images_folder = Yii::getAlias('@storage')."/properties/{$id}";

      if(is_dir($images_folder)){
        #!!!! Don't play with this method, you can remove all folders
        \common\components\helpers\Setup::deleteDir($images_folder) ;
      }
    }

    return $this->redirect(['index']);
  }

  /**
   * Finds the Property model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Property the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id){
    if (($model = Property::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
   * @param null $q
   * @return array
   */
  public function actionSuburbList($q = null){
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    $out = ['results' => ['id' => '', 'title' => '']];
    if (!is_null($q)) {
      $query = new Query;
      $query->select('id, title AS text, post_code AS post_code')->from('{{%suburbs}}')
        ->where(['like', 'title', $q])
        ->orWhere(['like', 'post_code', $q])->limit(20);
      $command = $query->createCommand();
      $data = $command->queryAll();
      $out['results'] = array_values($data);
    }
    return $out;
  }

  /**
   * @param $id
   * @return \yii\web\Response
   */
  public function actionDeleteImage($id){
    $model = \common\models\PropertyImages::findOne($id);
    if(!empty($model)){
      $property_id = $model->property_id;
      $file = \Yii::getAlias('@storage')."/properties/".$property_id."/".$model->file_name;
      if(is_file($file)) unlink ($file);
      $model->delete();
    } else {
      return $this->goHome();
    }
    return $this->redirect(Url::to(['update', 'id' => $property_id]));
  }

  /**
   * @param $model_id
   * @param $id
   * @return string
   */
  public function actionAgentsList($model_id, $id){
    $output = '';
    if(!empty($id) && !is_null($id)){
      $users = \common\models\User::find()->joinWith('agency')->where("{{%user}}.id IN ({$id}) ")->all();
      if(!empty($users)):
        foreach($users as $item){
          if(!empty($item)){
            $output .= "<p class='text-success'>".$item->agency->title." agents: </p>";
            if(!empty($agents = $item->agency->agencyAgent)){
              foreach($agents as $agent){
                $checker = '';
                if($model_id > 0){
                  $is_user_checked = \common\models\PropertyAgents::find()
                    ->where(['property_id' => $model_id, 'agent_id' => $agent->id])->one();
                  $checker = empty($is_user_checked) ? "" : "checked";
                }
                $output .= '<div class="col-sm-6"><label class="checkbox">';
                $output .= '<input type="checkbox" '.$checker.' name="Property[agents][]" value="'.$agent->id.'"> '.$agent->full_name.'';
                $output .= '</label></div>';
              }
            } else {
              $output .= "No agents";
            }
            $output .= "<hr class='col-xs-12' />";
          }
        }
      else:
        $output .= "No agents";
      endif;
      return $output;
    } else {
      return 'no agency';
    }
  }

}
