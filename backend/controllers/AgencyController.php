<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/16/15
 * Time: 15:08
 */

namespace backend\controllers;

use Yii;
use common\models\Agency;
use backend\models\search\AgencySearch;
use common\models\MediaStorage;
use common\components\classes\BackendController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AgencyController implements the CRUD actions for Agency model.
 */
class AgencyController extends BackendController{

  /**
   * Lists all Agency models.
   * @return mixed
   */
  public function actionIndex(){
    $searchModel = new AgencySearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single Agency model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id){
    return $this->render('view', [
      'model' => $this->findModel($id),
    ]);
  }

  /**
   * Creates a new Agency model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate(){
    $model = new Agency();

    if ($model->load(Yii::$app->request->post())) {

      if($model->save()){
        $image = Yii::$app->request->post('Agency', '')["fileAttributes"];

        if(!empty($image)){
          $model->createAgencyWatermark($image);
          $model->saveSingleImage($image, $model->id, 'Agency', 320, $model->color, 320);
        }

        Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-success'],
          'body'=>Yii::t('app', 'All data was successfully saved')]);

        return $this->redirect(['view', 'id' => $model->id]);
      } else{
        Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-success'],
          'body'=>Yii::t('app', 'All data was successfully saved')]);

        return $this->render('create', ['model' => $model,]);
      }

    } else {
      Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-warning'],
        'body'=> '<h4>Please don\'t forget create user in advance</h4>']);
      return $this->render('create', [
        'model' => $model,
      ]);
    }
  }

  /**
   * Updates an existing Agency model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id){
    $model = $this->findModel($id);

    if ($model->load(Yii::$app->request->post()) && $model->save()){

      $image = Yii::$app->request->post('Agency', '')["fileAttributes"];

      if(!empty($image)) {
        //var_dump($model->createAgencyWatermark($image)); die();
        $model->createAgencyWatermark($image);
        $model->saveSingleImage($image, $model->id, 'Agency', 320, $model->color, 320);
      }

      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('update', [
        'model' => $model,
      ]);
    }
  }

  /**
   * Deletes an existing Agency model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id){
    $model = $this->findModel($id);
    $user_id = $model->user_id;
    \common\models\AgencyAgent::deleteAll(['agency_id' => $model->id]);
    $model->delete();
    \common\models\User::deleteAll(['id' => $user_id]);

    return $this->redirect(['index']);
  }

  /**
   * Finds the Agency model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Agency the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id){
    if (($model = Agency::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }
}