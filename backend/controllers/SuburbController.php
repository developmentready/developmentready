<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/16/15
 * Time: 14:52
 */
namespace backend\controllers;

use Yii;
use common\models\Suburb;
use common\models\State;
use backend\models\search\SuburbSearch;
use common\components\classes\BackendController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SuburbController implements the CRUD actions for Suburb model.
 */
class SuburbController extends BackendController{
  public function behaviors(){
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
    ];
  }

  /**
   * Lists all Suburb models.
   * @return mixed
   */
  public function actionIndex(){
    $searchModel = new SuburbSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single Suburb model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id){
    return $this->render('view', [
      'model' => $this->findModel($id),
    ]);
  }

  /**
   * Creates a new Suburb model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate(){
    $model = new Suburb();

    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('create', [
        'model' => $model,
        'parent_city' => Suburb::find()->where(['is_city' => 1])->active()->orderBy(['title' => SORT_ASC])->all(),
        'states' => State::find()->active()->orderBy(['title' => SORT_ASC])->all()
      ]);
    }
  }

  /**
   * Updates an existing Suburb model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id){
    $model = $this->findModel($id);

    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('update', [
        'model' => $model,
        'parent_city' => Suburb::find()->where(['is_city' => 1])->active()->orderBy(['title' => SORT_ASC])->all(),
        'states' => State::find()->active()->orderBy(['title' => SORT_ASC])->all()
      ]);
    }
  }

  /**
   * Deletes an existing Suburb model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id){
    $this->findModel($id)->delete();

    return $this->redirect(['index']);
  }

  /**
   * Finds the Suburb model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Suburb the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id){
    if (($model = Suburb::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }
}
