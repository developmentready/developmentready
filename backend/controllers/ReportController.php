<?php
namespace backend\controllers;

use Yii;
use mPDF;
use backend\models\search\ReportsSearch;

/**
 * Site controller
 *
 * @var $property \common\models\Property
 * @var $model \common\models\PropertyReport
 */
class ReportController extends \common\components\classes\BackendController{

  /**
   * @inheritdoc
   */
  public function actions(){
    return [
      'error' => [
        'class' => 'yii\web\ErrorAction',
      ]
    ];
  }

  /**
   * @param $id
   * @return string
   */
  public function actionView($id){
    $property = \common\models\Property::findOne($id);

    if(!$property->report) {
      return $this->render('no-data');
    }

    $report = new \backend\models\Report();

    if ($report->load(Yii::$app->request->get())) {
      $start_date = strtotime($report->stat_start_date);
      $end_date = strtotime($report->stat_end_date);
      $report_for_last_days = 'Report From:'.$report->stat_start_date.", Till:".$report->stat_end_date;
      $contacts = $property->report->showPropertyMessagesForRange($start_date, $end_date, $property);

      if($end_date < $start_date || empty($end_date) || empty($start_date)) {
        $report_for_last_days .= ' Wrong date range selected!. 0';
        $data_array = ['date' => [ 0 => date('M d', time())],'views' => [0], 'calls' => [0], 'messages' => [0]];
      } else {
        $days = round(abs($end_date - $start_date)/60/60/24 + 1, 0);
        $report_for_last_days .= ". {$days} ";
        $data_array = $this->_getDateRangeDataForChart($start_date, $end_date, $property);
      }
    } else {
      $report_for_last_days = $this->_getReportPeriodDays($property->publication_date);
      $data_array = $this->_getDataForChart($report_for_last_days, $property);
      $contacts = $property->report->showPropertyMessages($property, $report_for_last_days);
    }


    return $this->render('view', [
      'model' => $property,
      'report' => $report,
      'data_array' => $data_array,
      'contacts' => $contacts,
      'report_for_last_days' => $report_for_last_days,
    ]);
  }

  /**
   * @return string
   */
  public function actionStates() {
    $searchModel = new ReportsSearch();

    $dataProvider = $searchModel->searchStates(\Yii::$app->request->queryParams);

    return $this->render('states', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider
    ]);
  }

  /**
   * @param $id
   * @return string
   * @throws \MpdfException
   */
  public function actionExport($id){
    $this->layout = "//report/main";
    $property = \common\models\Property::findOne($id);

    if(!$property->report) {
      return $this->render('no-data');
    }

    $report = new \backend\models\Report();

    if ($report->load(Yii::$app->request->get())) {
      $start_date = strtotime($report->stat_start_date);
      $end_date = strtotime($report->stat_end_date);
      $report_for_last_days = 'Report From:'.$report->stat_start_date.", Till:".$report->stat_end_date;
      $contacts = $property->report->showPropertyMessagesForRange($report->stat_start_date, $report->stat_end_date, $property);

      if($end_date < $start_date || empty($end_date) || empty($start_date)) {
        $report_for_last_days .= ' Wrong date range selected!. 0';
        $data_array = ['date' => [ 0 => date('M d', time())],'views' => [0], 'calls' => [0], 'messages' => [0]];
      } else {
        $days = round(abs($end_date - $start_date)/60/60/24 + 1, 0);
        $report_for_last_days .= ". {$days} ";
        $data_array = $this->_getDateRangeDataForChart($start_date, $end_date, $property);
      }
    } else {
      //$report_for_last_days = $this->_getReportPeriodDays($property->publication_date);
      //$data_array = $this->_getDataForChart($report_for_last_days, $property);
      //$contacts = $property->report->showPropertyMessages($property, $report_for_last_days);
      /*maxim+*/
      $report_days_period = $this->_getReportPeriodDays($property->publication_date);
      $data_array = $this->_getDataForChart($report_days_period, $property);
      $contacts = $property->report->showPropertyMessages($property, $report_days_period);
      $report_date_from = !empty($property->publication_date) ? date('Y-m-d',$property->publication_date) : date('Y-m-d',$property->created_at);
      $report_date_to = date('Y-m-d',time());
      $report_for_last_days = 'Report From:'.$report_date_from.", Till:".$report_date_to.". ".$report_days_period." ";
      /*maxim-*/
    }

    $mpdf = new mPDF();
    // This must be set to allow mPDF to parse table data
    $mpdf->useGraphs = true;
    $mpdf->WriteHTML(
      $this->renderPartial('pdf', [
        'model' => $property,
        'data_array' => $data_array,
        'contacts' => $contacts,
        'report_for_last_days' => $report_for_last_days,
      ])
    );
    $mpdf->Output();
  }

  /**
   * @param $id
   * @return string
   * @throws \MpdfException
   */
  public function actionSaveExport($id){
    $this->layout = "//report/main";
    $property = \common\models\Property::findOne($id);

    if(!$property->report) {
      return $this->render('no-data');
    }

    $report = new \backend\models\Report();

    if ($report->load(Yii::$app->request->get())) {
      $start_date = strtotime($report->stat_start_date);
      $end_date = strtotime($report->stat_end_date);
      $report_for_last_days = 'Report From:'.$report->stat_start_date.", Till:".$report->stat_end_date;
      $contacts = $property->report->showPropertyMessagesForRange($report->stat_start_date, $report->stat_end_date, $property);

      if($end_date < $start_date || empty($end_date) || empty($start_date)) {
        $report_for_last_days .= ' Wrong date range selected!. 0';
        $data_array = ['date' => [ 0 => date('M d', time())],'views' => [0], 'calls' => [0], 'messages' => [0]];
      } else {
        $days = round(abs($end_date - $start_date)/60/60/24 + 1, 0);
        $report_for_last_days .= ". {$days} ";
        $data_array = $this->_getDateRangeDataForChart($start_date, $end_date, $property);
      }
    } else {
      $report_for_last_days = $this->_getReportPeriodDays($property->publication_date);
      $data_array = $this->_getDataForChart($report_for_last_days, $property);
      $contacts = $property->report->showPropertyMessages($property, $report_for_last_days);
    }

    $mpdf=new mPDF();
    // This must be set to allow mPDF to parse table data
    $mpdf->useGraphs = true;
    $mpdf->WriteHTML(
      $this->render('pdf', [
        'model' => $property,
        'data_array' => $data_array,
        'contacts' => $contacts,
        'report_for_last_days' => $report_for_last_days,
      ])
    );
    $mpdf->Output('D');
  }


  /**
   * @param $id
   * @return \yii\web\Response
   * @throws \MpdfException
   */
  public function actionSend($id){
    $property = \common\models\Property::findOne($id);

    $users = $property->propertyUsers;

    $report = new \backend\models\Report();


    if ($report->load(Yii::$app->request->get())) {
      $start_date = strtotime($report->stat_start_date);
      $end_date = strtotime($report->stat_end_date);
      $report_for_last_days = 'Report From:'.$report->stat_start_date.", Till:".$report->stat_end_date;
      $contacts = $property->report->showPropertyMessagesForRange($report->stat_start_date, $report->stat_end_date, $property);

      if($end_date < $start_date || empty($end_date) || empty($start_date)) {
        $report_for_last_days .= ' Wrong date range selected!. 0';
        $data_array = ['date' => [ 0 => date('M d', time())],'views' => [0], 'calls' => [0], 'messages' => [0]];
      } else {
        $days = round(abs($end_date - $start_date)/60/60/24 + 1, 0);
        $report_for_last_days .= ". {$days} ";
        $data_array = $this->_getDateRangeDataForChart($start_date, $end_date, $property);
      }
    } else {
      $report_for_last_days = $this->_getReportPeriodDays($property->publication_date);

      $data_array = $this->_getDataForChart($report_for_last_days, $property);
      $contacts = $property->report->showPropertyMessages($property, $report_for_last_days);
    }

    $mpdf=new mPDF();

    $mpdf->WriteHTML($this->renderPartial('pdf',[
      'model' => $property,
      'data_array' => $data_array,
      //'contacts' => $property->$contacts,
      'contacts' => $contacts,
      'report_for_last_days' => $report_for_last_days,
    ]));




//var_dump(111);die();

    foreach($users as $user):
      //var_dump($user);die();
      $message = Yii::$app->mailer->compose('agency_message', ['model' => $property])
        ->setFrom([Yii::$app->params['robotEmail'] => Yii::$app->name . ' robot'])
        ->setTo($user->email)
        ->setSubject('Permit ready monthly report for ' . $property->location);

      $filename = "Report-Permit-Ready-".date("d-m-Y_H-i",time()).".pdf"; //Your Filename with local date and time

      $content = $mpdf->Output("", 'S');

      // Create attachment on-the-fly
      $message->attachContent($content,
        ['fileName' => "{$filename}.pdf", 'contentType' => 'application/pdf']);

      if($message->send()){
        Yii::$app->session->setFlash('success', 'Attachment successfully sent to receipent e-mail.');
      } else {
        Yii::$app->session->setFlash('error', 'Some error occur during sending report to your mailbox.');
      }
    endforeach;

    return $this->redirect(\yii\helpers\Url::to(['view' , 'id' => $id]));
  }

  /**
   * @param $publication_date
   * @return bool|string
   */
  private function _getReportPeriodDays($publication_date){
    $report_for_last_days = 60 * 60 * 24 * 30; // 30 days
    if((time() - $publication_date) < $report_for_last_days)
      $report_for_last_days = time() - $publication_date;

    return date("j", $report_for_last_days);
  }

  /**
   * @param $last_n_days
   * @param $model
   * @return mixed
   */
  private function _getDataForChart($last_n_days, $model){
    $output['date'] = $output['views'] = $output['calls'] = $output['messages'] = [];
    $keywords = ['views' => 'view_count', 'calls' => 'call_count', 'messages' => 'message_count'];

    for($i = $last_n_days; $i > 0; $i--) {
      $date = date('Y-m-d', strtotime("-{$i} days"));
      array_push($output['date'], date('d-M', strtotime("-{$i} days")));

      foreach($keywords as $k => $v) {
        if(!empty($data = $model->report->propertyReportForDate($model->id, $v, $date))){
          array_push($output[$k], $data[0]['counter']);
        } else {
          array_push($output[$k], 0);
        }
      }
    }
    return $output;
  }

  /**
   * @param $start_date
   * @param $end_date
   * @param $model
   * @return mixed
   */
  private function _getDateRangeDataForChart($start_date, $end_date, $model) {
    $output['date'] = $output['views'] = $output['calls'] = $output['messages'] = [];
    $keywords = ['views' => 'view_count', 'calls' => 'call_count', 'messages' => 'message_count'];

    if($start_date == $end_date){
      $date = date('Y-m-d', $end_date);
      array_push($output['date'], date('d-M', $end_date));
      foreach($keywords as $k => $v) {
        if(!empty($data = $model->report->propertyReportForDate($model->id, $v, $date))){
          array_push($output[$k], $data[0]['counter']);
        } else {
          array_push($output[$k], 0);
        }
      }
    }
    else {
      for($i = $start_date; $i <= $end_date; $i += 86400){
        $date = date('Y-m-d', $i);
        array_push($output['date'], date('d-M', $i));
        foreach($keywords as $k => $v) {
          if(!empty($data = $model->report->propertyReportForDate($model->id, $v, $date))){
            array_push($output[$k], $data[0]['counter']);
          } else {
            array_push($output[$k], 0);
          }
        }
      }
    }

    return $output;
  }
}