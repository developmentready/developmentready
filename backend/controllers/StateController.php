<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/12/15
 * Time: 19:53
 */


namespace backend\controllers;

use Yii;
use common\models\State;
use common\models\Country;
use backend\models\search\StateSearch;
use common\components\classes\BackendController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CountryController implements the CRUD actions for Country model.
 */
class StateController extends BackendController{
  public function behaviors(){
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
    ];
  }

  /**
   * Lists all Country models.
   * @return mixed
   */
  public function actionIndex(){
    $searchModel = new StateSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single Country model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id){
    return $this->render('view', [
      'model' => $this->findModel($id),
    ]);
  }

  /**
   * Creates a new Country model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate(){
    $model = new State();

    if ($model->load(Yii::$app->request->post())) {
      if($model->save())
        Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-success'],
          'body'=>Yii::t('app', 'All data was successfully saved')]);
      else
        Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-danger'],
          'body'=>Yii::t('app', 'Error occurred during saving data')]);
      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('create', [
        'model' => $model,
        'countries' => Country::find()->active()->all()
      ]);
    }
  }

  /**
   * Updates an existing Country model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id){
    $model = $this->findModel($id);

    if ($model->load(Yii::$app->request->post())){
      if($model->save()){
        Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-success'], 'body'=>Yii::t('backend', 'All data was successfully saved')]);
      } else {
        Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-danger'], 'body'=>Yii::t('backend', 'Error occurred during saving language realted data')]);
      }
      return $this->redirect(['index']);
    } else {
      return $this->render('update', [
        'model' => $model,
        'countries' => Country::find()->active()->all()
      ]);
    }
  }

  /**
   * Deletes an existing Country model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
//  public function actionDelete($id){
//    $this->findModel($id)->delete();
//    return $this->redirect(['index']);
//  }

  /**
   * Finds the Country model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Country the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id){
    if (($model = State::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }
}

