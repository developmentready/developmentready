<?php

namespace backend\controllers;

use Yii;
use common\models\Page;
use yii\helpers\Url;
use backend\models\search\PageSearch;
use common\components\classes\BackendController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BlogController implements the CRUD actions for Page model.
 */
class BlogController extends BackendController{
  public function behaviors(){
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
    ];
  }

  /**
   * Lists all Page models.
   * @return mixed
   */
  public function actionIndex(){
    $searchModel = new PageSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 2);

    $dataProvider->sort = ['defaultOrder' => ['publication_date' => SORT_DESC,'updated_at' => SORT_DESC]];

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single Page model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id){
    return $this->render('view', [
      'model' => $this->findModel($id),
    ]);
  }

  /**
   * Creates a new Page model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate(){
    $model = new Page();
    $model->scenario = 'page-with-header';

    if ($model->load(Yii::$app->request->post())){
      $model->page_type_id = 2;
      if(!empty($publication_date = Yii::$app->request->post('Page')["publication_date"])) $model->publication_date = strtotime($publication_date);
      if($model->save()){

        $image = Yii::$app->request->post('Page')["fileAttributes"];

        if(!empty($image)) $model->saveSingleImage($image, $model->id, 'Page', 535);

        return $this->redirect(['view', 'id' => $model->id]);

      } else{
        return $this->render('create', ['model' => $model,]);
      }

    } else {
      return $this->render('create', [
        'model' => $model,
      ]);
    }
  }

  /**
   * Updates an existing Page model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id){
    $model = $this->findModel($id);
    $model->scenario = 'page-update';
    if ($model->load(Yii::$app->request->post())) {
      if(!empty($publication_date = Yii::$app->request->post('Page')["publication_date"])) $model->publication_date = strtotime($publication_date);
      if($model->save()){
        $image = Yii::$app->request->post('Page')["fileAttributes"];

        if(!empty($image)) $model->saveSingleImage($image, $model->id, 'Page', 535);

        return $this->redirect(['view', 'id' => $model->id]);
      }
      return $this->render('update', ['model' => $model,]);
    } else {
      return $this->render('update', [
        'model' => $model,
      ]);
    }
  }

  /**
   * Deletes an existing Page model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id){
    $this->findModel($id)->delete();

    return $this->redirect(['index']);
  }

  /**
   * Finds the Page model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Page the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id){
    if (($model = Page::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }


  public function actionDeleteImage($id){
    $model = \common\models\MediaStorage::findOne($id);
    if(!empty($model)){
      $file = \Yii::getAlias('@storage').$model->media_path;
      if(is_file($file)) unlink ($file);
      $model->delete();
    } else{

    }
    return $this->redirect(Url::to(['update', 'id' => $model->model_id]));
  }
}
