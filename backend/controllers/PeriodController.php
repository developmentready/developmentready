<?php
/**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 7/11/15
 * Time: 13:56
 */

namespace backend\controllers;

use common\models\Period;
use common\models\PeriodLang;

class PeriodController extends \common\components\classes\BackendController{

  public function actions()
  {
    return [
      'index' => [
        'class' => 'common\actions\TitleIndexAction',
        'searchModel' => new \backend\models\search\PeriodSearch(),
      ],
      'create' => [
        'class' => 'common\actions\TitleCreateAction',
        'model' => new \common\base\MultiModel([
          'models' => [
            'main_model' => new Period(),
            'lang_model' => new PeriodLang(),
          ]
        ]),
      ],
      'update' => [
        'class' => 'common\actions\TitleUpdateAction',
        'model' => new \common\base\MultiModel([
          'models' => [
            'main_model' => new Period(),
            'lang_model' => new PeriodLang(),
          ]
        ]),
      ],
    ];
  }


  /**
   * Deletes an existing City model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id){
    $this->findModel($id)->delete();
    return $this->redirect(['index']);
  }

  /**
   * Finds the Period model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Period the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id){
    if (($model = Period::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }
}