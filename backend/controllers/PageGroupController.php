<?php

namespace backend\controllers;

use Yii;
use common\models\PageGroup;
use common\models\PageGroupLang;
use backend\models\search\PageGroupSearch;
use common\models\Language;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PageGroupController implements the CRUD actions for PageGroup model.
 */
class PageGroupController extends \common\components\classes\BackendController{
  public function behaviors(){
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
    ];
  }

  /**
   * Lists all PageGroup models.
   * @return mixed
   */
  public function actionIndex(){
    $searchModel = new PageGroupSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single PageGroup model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id){
    return $this->render('view', [
      'model' => $this->findModel($id),
    ]);
  }

  /**
   * Creates a new PageGroup model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate(){
    $model = new PageGroup();
    $lmodel = new PageGroupLang();

    if ($model->load(Yii::$app->request->post()) && $model->save()){
      $count = count(Yii::$app->request->post('PageGroupLang', []));

      if ($count > 0){
        for($i = 1; $i <= $count; $i++){
          $items[$i] = new PageGroupLang();
        }
      
        if(PageGroupLang::loadMultiple($items, \Yii::$app->request->post())){
          
          foreach($items as $item):
            $item->relation_id = $model->id;
            if($item->save()){
              Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-success'], 'body'=>Yii::t('backend', "All data was successfully saved")]);
            } else {
              Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-danger'], 'body'=>Yii::t('backend', 'Error occurred during saving all data')]);
            }
          endforeach;
        } else {
          Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-danger'], 'body'=>Yii::t('backend', 'Error occurred during saving language realted data')]);
        }
      }else{
        Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-danger'], 'body'=>Yii::t('backend', 'Error occurred during saving language realted data')]);
      }
      return $this->redirect(['index']);
    } else {
      return $this->render('create', [
        'model' => $model,
        'lmodel' => $lmodel,
        'alangs' => Language::find()->all(),
      ]);
    }
  }

  /**
   * Updates an existing PageGroup model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id){
    $model = $this->findModel($id);
    $lmodel = new PageGroupLang();
    $ulmodel = $this->getTranslationForUpdate($lmodel,'relation_id',$id);

    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      
      if(PageGroupLang::loadMultiple($ulmodel, Yii::$app->request->post())
          && PageGroupLang::validateMultiple($ulmodel)){
        foreach($ulmodel as $item):
          if($item->save()){
            Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-success'], 'body'=>Yii::t('backend', 'All data was successfully saved')]);
          } else {
            Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-danger'], 'body'=>Yii::t('backend', 'Error occurred during saving all data')]);
          }
        endforeach;
      } else {
        Yii::$app->getSession()->setFlash('alert', ['options'=>['class'=>'alert-danger'], 'body'=>Yii::t('backend', 'Error occurred during saving language realted data')]);
      }
      return $this->redirect(['index']);
    } else {
      return $this->render('update', [
        'model' => $model,
        'lmodel' => $ulmodel,
        'alangs' => Language::find()->all(),
      ]);
    }
  }

  /**
   * Deletes an existing PageGroup model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id)
  {
      $this->findModel($id)->delete();

      return $this->redirect(['index']);
  }

  /**
   * Finds the PageGroup model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return PageGroup the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id){
    if (($model = PageGroup::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }
}
