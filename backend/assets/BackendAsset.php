<?php
/**
 * Created by PhpStorm.
 * User: zein
 * Date: 7/3/14
 * Time: 3:14 PM
 */

namespace backend\assets;

use yii\web\AssetBundle;

class BackendAsset extends AssetBundle
{
    public $basePath = '/';
    public $baseUrl = '@backendUrl';

    public $css = [
      'css/color-picker.min.css',
      'css/style.css'
    ];
    public $js = [
      'js/color-picker.min.js',
      'js/chained.min.js',
      'js/app.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'common\assets\AdminLte',
        'common\assets\Html5shiv'
    ];
}
