<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ChartAsset extends AssetBundle{

  public $sourcePath = '@backend/assets/chartjs';

  public $css = [
  ];

  public $js = [
    'chart.min.js',
  ];

  //public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
}
