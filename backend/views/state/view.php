<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/12/15
 * Time: 20:20
 */

?>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'States'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

  <p>
    <?= Html::a(Yii::t('app', 'All States'), ['index'], ['class' => 'btn btn-info']) ?>
    <?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?= Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
      'class' => 'btn btn-danger',
      'data' => [
        'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
        'method' => 'post',
      ],
    ]) ?>
  </p>

  <?php echo DetailView::widget([
    'model' => $model,
    'attributes' => [
      'id',
      [
        'attribute' => 'status',
        'format' => 'html',
        'value' => $model->status ? '<span class="text-success">Active</span>' : '<span class="text-danger">Deactive</span>',
      ],
      [
        'attribute' => 'default',
        'format' => 'html',
        'value' => $model->default ? '<span class="text-success">Yes</span>' : '<span class="text-danger">No</span>',
      ],
      'title',
      'created_at:datetime',
      'updated_at:datetime',
    ],
  ]) ?>

</div>

