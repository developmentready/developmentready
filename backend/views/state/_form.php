<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/12/15
 * Time: 20:20
 */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Article */
/* @var $categories common\models\ArticleCategory[] */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="article-form">

  <?php $form = ActiveForm::begin(); ?>



  <div class="row">
    <div class="col-xs-6">
      <?php echo $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-xs-6">
      <?php echo $form->field($model, 'page_title')->textInput(['maxlength' => true]) ?>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12">
      <?php echo $form->field($model, 'page_body')->textarea(['rows' => 3]) ?>
    </div>
  </div>

  <?php if($model->isNewRecord) $model->country_id = 13; ?>

  <?php echo $form->field($model, 'country_id')->dropDownList(
    \yii\helpers\ArrayHelper::map(
    $countries,
    'id', 'title'
  ), ['prompt'=>'Please select country']) ?>

  <?= $this->render("//layouts/_map_position", ['form' => $form, 'model' => $model]) ?>

  <div class="form-group">
    <?php echo $form->field($model, 'status')->checkbox(['inline'=>true]) ?> <?php echo $form->field($model, 'default')->checkbox(['inline'=>true]) ?>

    <?php echo Html::submitButton(
      $model->isNewRecord ? 'Add' : 'Update',
      ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>