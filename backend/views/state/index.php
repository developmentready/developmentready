<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/12/15
 * Time: 20:19
 */

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'States';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="country-index">
  <p>
    <?= Html::a('Add new state', ['create'], ['class' => 'btn btn-success']) ?>
  </p>

  <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
      ['class' => 'yii\grid\SerialColumn'],
      [
        'attribute'=>'status',
        'format'   => 'html',
        'value'    => function($data){ return $data->status ? '<i class="fa fa-check-square" style="color:#5CB85C"></i>' : '<i class="fa fa-close" style="color: #A94442"></i>';},
        'filter'=> ['0' => 'Deactive' , '1' => 'Active'],
      ],
      [
        'attribute'=>'default',
        'format'   => 'html',
        'value'    => function($data){ return $data->default ? '<i class="fa fa-check-square" style="color:#5CB85C"></i>' : '<i class="fa fa-close" style="color: #A94442"></i>';},
        'filter'=> ['0' => 'No' , '1' => 'Yes'],
      ],
      'title',
      [
        'attribute'=>'country_id',
        'label' => 'Country',
        'format' => 'html',
        'value' => function($data){ return $data->country->title; },
        //'filter' => \yii\helpers\ArrayHelper::map($city, 'id', 'translation.title'),
      ],
      ['class' => 'yii\grid\ActionColumn',
        'template'=>'{update} {view} {delete}'],
    ],
  ]); ?>
</div>
