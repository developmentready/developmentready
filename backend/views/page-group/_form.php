<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PageGroup */
/* @var $form yii\widgets\ActiveForm */
$active_code = NULL;
?>

<div class="page-group-form">
    <?php $form = ActiveForm::begin(); ?>
    
      <div role="tabpanel col-sm-12">
          <ul class="nav nav-tabs" role="tablist">
          <?php foreach ($alangs as $lang): ?>
            <?php $lang->default === 1 ? $active_code = $lang->code : '' ?>
            <li role="presentation" class="<?= ($active_code === $lang->code) ? 'active' : '' ?>"><a href="#<?= $lang->code ?>-tab" aria-controls="<?= $lang->code ?>-tab" role="tab" data-toggle="tab"><?= strtoupper($lang->title) ?></a></li>
          <?php endforeach; ?>
          </ul>
          <br/>
          <div class="tab-content">
            <?php foreach ($alangs as $lang): ?>
            <div role="tabpanel" class="tab-pane <?= $active_code === $lang->code ? 'active' : '' ?>" id="<?= $lang->code ?>-tab">
              <?= Html::activeHiddenInput($lmodel, "[$lang->id]lang_id", ['value' => $lang->id]) ?>
              <?= $form->field($lmodel, "[$lang->id]title")->textInput() ?>
            </div>
            <?php endforeach; ?>
          </div>
        </div>
      <div class="form-group">
        <?= $this->render('@backend/views/layouts/_formFooter', ['form' => $form, 'model' => $model]) ?>
      </div>
    <?php ActiveForm::end(); ?>
</div>
