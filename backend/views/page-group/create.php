<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PageGroup */

$this->title = Yii::t('common', 'Create Page Group');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Page Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-group-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'lmodel' => $lmodel,
        'alangs' => $alangs,
        'group' => $group
    ]) ?>

</div>
