<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PageGroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common', 'Page Groups');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-group-index">

    <h1><?= Html::encode($this->title) ?></h1>
        
    <p>
      <?= Html::a(Yii::t('common', 'Create Page Group'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
          ['class' => 'yii\grid\SerialColumn'],

          [
            'attribute'=>'status',
            'format' => 'html',
            'value' => function($data){ return $data->status ? '<i class="fa fa-check-square" style="color:#5CB85C"></i>' : '<i class="fa fa-close" style="color: #A94442"></i>';},
            'filter'=> ['0' => Yii::t('common','Deactive') , '1' => Yii::t('common','Active')],
          ],
          [
            'attribute'=>'pageGroupLang',
            'label'=>Yii::t('common', 'Title'),
            'value' => 'pageGroupLang.title'
          ],
          ['attribute'=>'updateName', 'format'=>'raw'],
          'updated_at:datetime',

          ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
