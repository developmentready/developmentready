<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PageGroup */

$this->title = Yii::t('common', 'Update Page Group', [
    'modelClass' => 'Page Group',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Page Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('common', 'Update');
$active_code = NULL;
?>
<div class="page-group-update">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <div class="page-group-form">
      <?php $form = ActiveForm::begin(); ?>
      <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
        <?php foreach($alangs as $lang): ?>
          <?php $lang->default === 1 ? $active_code = $lang->code : '' ?>
          <li role="presentation" class="<?= ($active_code === $lang->code) ? 'active' : '' ?>"><a href="#<?= $lang->code ?>-tab" aria-controls="<?= $lang->code ?>-tab" role="tab" data-toggle="tab"><?= strtoupper($lang->title) ?></a></li>
        <?php endforeach; ?>
        </ul>
        <br/>
        <div class="tab-content">
          <?php foreach($alangs as $lang): ?>
          <div role="tabpanel" class="tab-pane <?= $active_code === $lang->code ? 'active' : '' ?>" id="<?= $lang->code ?>-tab">
            <?php foreach($lmodel as $k => $v): ?>
              <?php if($v->lang_id == $lang->id): ?>
                <?= $form->field($v, "[$k]title")->textInput()?>
              <?php endif; ?>
            <?php endforeach; ?>
          </div>
          <?php endforeach; ?>
        </div>
      </div>

      <div class="form-group">
          <?= $this->render('@backend/views/layouts/_formFooter', ['form' => $form, 'model' => $model]) ?>
      </div>
      <?php ActiveForm::end(); ?>
    </div>

</div>
