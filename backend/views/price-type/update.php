<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/19/15
 * Time: 16:35
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PriceType */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Price Type',
  ]) . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Price Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="price-type-update">

  <?php echo $this->render('_form', [
    'model' => $model,
  ]) ?>

</div>