<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/19/15
 * Time: 16:34
 */

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PriceTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Price Types');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="price-type-index">

  <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

  <p>
    <?php echo Html::a(Yii::t('app', 'Create {modelClass}', [
      'modelClass' => 'Price Type',
    ]), ['create'], ['class' => 'btn btn-success']) ?>
  </p>

  <?php echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
      ['class' => 'yii\grid\SerialColumn'],

      'id',
      [
        'attribute'=>'status',
        'format'   => 'html',
        'value'    => function($data){ return $data->status ? '<i class="fa fa-check-square" style="color:#5CB85C"></i>' : '<i class="fa fa-close" style="color: #A94442"></i>';},
        'filter'=> ['0' => 'Deactive' , '1' => 'Active'],
      ],
      'title',
      [
        'attribute' => 'updater_id',
        'value' => function($data){return $data->updater->username ;}
      ],
      'updated_at:date',

      ['class' => 'yii\grid\ActionColumn'],
    ],
  ]); ?>

</div>