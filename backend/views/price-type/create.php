<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/19/15
 * Time: 16:34
 */

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PriceType */

$this->title = Yii::t('app', 'Create {modelClass}', [
  'modelClass' => 'Price Type',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Price Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="price-type-create">

  <?php echo $this->render('_form', [
    'model' => $model,
  ]) ?>

</div>