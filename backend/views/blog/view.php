<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Page */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


if(!empty($media_path = $model->image))
  $image_url = Yii::getAlias('@storageUrl').$media_path->media_path;
else
  $image_url = Yii::getAlias('@storageUrl')."/properties/no-image.png";

?>
<div class="page-view">

    <p>
      <?php echo Html::a(Yii::t('app', 'All Blogs'), ['index', 'id' => $model->id], ['class' => 'btn btn-info']) ?>
      <?php echo Html::a(Yii::t('app', 'Add New Blog'), ['create', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
      <?php echo Html::a(Yii::t('app', 'Update This Blog'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
      <?php echo Html::a(Yii::t('app', 'Delete This Blog'), ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
          'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
          'method' => 'post',
        ],
      ]) ?>
    </p>

    <?php echo DetailView::widget([
      'model' => $model,
      'attributes' => [
        'id',
        [
          'attribute' => 'status',
          'format' => 'html',
          'value' => $model->status ? '<span class="text-success">Yes</span>' : '<span class="text-danger">No</span>',
        ],
        [
          'label' => 'Image',
          'format' => 'html',
          'value' => "<img height='150' src='".$image_url."' />"
        ],
        [
          'attribute' => 'page_category_id',
          'value' => !empty($category_name = $model->category->title) ? $category_name : 'No category selected',
        ],
        [
          'label' => 'Blog Url',
          'attribute' => 'url_name',
          'format' => 'html',
          'value' => "<a href='".\Yii::getAlias('@frontendUrl')."/{$model->url_name}'".">".\Yii::getAlias('@frontendUrl')."/{$model->url_name}"."</a>",
        ],
          'title',
          'h1_tag',
          'body:html',
          'meta_keywords',
          'meta_description',
          'view_counter',
        [
          'label' => 'Source from',
          'format' => 'url',
          'attribute' => 'video_link',
        ],
        [
          'label' => 'Blog quote',
          'attribute' => 'additional_text',
        ],
        [
          'attribute' => 'url_name',
          'format' => 'html',
          'value' => "{$model->url_name}",
        ],
        'publication_date:date',
        [
          'attribute' => 'creator_id',
          'value' => $model->creator->username,
        ],
        'created_at:datetime',
        [
          'attribute' => 'updater_id',
          'value' => $model->updater->username,
        ],
        'updated_at:datetime',
        ],
    ]) ?>

</div>
