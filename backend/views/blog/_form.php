<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use trntv\yii\datetimepicker\DatetimepickerWidget;

/* @var $this yii\web\View */
/* @var $model common\models\Page */
/* @var $form yii\bootstrap\ActiveForm */

if($model->isNewRecord) $model->page_robot_indexing = 1;


?>


<div class="page-form">

  <?php $form = ActiveForm::begin(); ?>

  <?php echo $form->errorSummary($model); ?>

  <div class="row">
    <div class="col-sm-3 col-xs-6">
      <?php echo $form->field($model, 'status')->checkbox() ?>
    </div>
    <div class="col-sm-3 col-xs-6">
      <?php echo $form->field($model, 'page_category_id')->dropDownList(
        ArrayHelper::map(\common\models\PageCategory::find()->where(['page_type_id' => 2])
          ->active()->all(), 'id', 'title')
      ) ?>
    </div>
    <div class="col-sm-3 col-xs-6">
      <?= $form->field($model, 'publication_date')->widget(
        'trntv\yii\datetimepicker\DatetimepickerWidget',
        [
          //'phpDatetimeFormat' => 'yyyy-MM-dd\'T\'HH:mm:ssZZZZZ',
          'clientOptions' => [
            'minDate' => new \yii\web\JsExpression('new Date("2015-01-01")'),
            'allowInputToggle' => false,
            'sideBySide' => true,
            'widgetPositioning' => [
              'horizontal' => 'auto',
              'vertical' => 'auto'
            ]
          ]
        ]);
      ?>
    </div>
    <div class="col-sm-3 col-xs-6">
      <?php echo $form->field($model, 'url_name')->textInput(['maxlength' => true]) ?>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-12 col-md-6">
      <?php echo $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-sm-12 col-md-6">
      <?php echo $form->field($model, 'h1_tag')->textInput(['maxlength' => true]) ?>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-12">
      <?= $this->render("//layouts/_text_editor", [ 'form' => $form, 'model' => $model, 'attribute' => 'body']) ?>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-6">
      <?php echo $form->field($model, 'meta_keywords')
        ->textarea(['rows' => 3,'maxlength' => true]) ?>
    </div>
    <div class="col-sm-6">
      <?php echo $form->field($model, 'meta_description')
        ->textarea(['rows' => 3, 'maxlength' => true]) ?>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-12">
      <?= $form->field($model, 'page_robot_indexing')->checkbox()  ?>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6">
      <?php echo $form->field($model, 'video_link')->textInput(['maxlength' => true])->label('Source from') ?>
    </div>
    <div class="col-sm-6">
      <?php echo $form->field($model, 'additional_text')->textInput(['maxlength' => true])->label('Blog quote') ?>
    </div>
  </div>

  <div class="row">
    <div class="col-xs-7">
      <?= $this->render("//layouts/_upload_images", ['form' => $form, 'model' => $model, 'max_image_count' => 1]) ?>
    </div>
    <?php if(!empty($model->image)): ?>
        <div class="col-xs-5">
          <div class="thumbnail">
            <img src="<?= Yii::getAlias('@storageUrl')."/".$model->image['media_path'] ?>"/>
            <div class="caption text-center">
              <p><a href="<?= Url::to(['delete-image', 'id' => $model->image['id']]) ?>" class="btn btn-danger">delete image</a></p>
            </div>
          </div>
        </div>
    <?php endif; ?>
  </div>

  <div class="form-group">
    <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>