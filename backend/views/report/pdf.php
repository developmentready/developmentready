<?php
/**
 * Created by PhpStorm.
 * User: rashad
 * Date: 1/12/16
 * Time: 22:26
 */

use yii\helpers\Html;


//http://www.chartjs.org/docs/
\backend\assets\ChartAsset::register($this);

$id = $model->id;
$date = [];
$js_messages = [];
$js_calls = [];
$js_views = [];
$output = [];

$counter = 0;
foreach($calls = $data_array['calls'] as $item){
  array_push($js_calls, $item);
  if($counter == 0){
    $output['call']['max'] = $item;
    $output['call']['index'] = $counter;
  } else {
    if($output['call']['max'] < $item) {
      $output['call']['max'] = $item;
      $output['call']['index'] = $counter;
    }
  }
  $output[$counter]['call'] = $item;
  $counter++;
}

$counter = 0;
foreach($messages = $data_array['messages'] as $item){
  array_push($js_messages, $item);
  if($counter == 0){
    $output['message']['max'] = $item;
    $output['message']['index'] = $counter;
  } else {
    if($output['message']['max'] < $item){
      $output['message']['max'] = $item;
      $output['message']['index'] = $counter;
    }
  }
  $output[$counter]['message'] = $item;
  $counter++;
}

$counter = 0;
foreach($views = $data_array['views'] as $item){
  array_push($js_views, $item);
  $output[$counter]['view'] = $item;
  if($counter == 0){
    $output['view']['max'] = $item;
    $output['view']['index'] = $counter;
  } else {
    if($output['view']['max'] < $item){
      $output['view']['max'] = $item;
      $output['view']['index'] = $counter;
    }
  }
  $counter++;
}

$counter = 0;
foreach($dates = $data_array['date'] as $item){
  array_push($date, \Yii::$app->formatter->asDate($item, 'php:M d'));
  $output[$counter]['date'] = \Yii::$app->formatter->asDate($item, 'php:d M Y');
  $counter++;
}

if(!empty($file = $model->defaultImage)) $image = $file->file_name;
elseif(!empty($file = $model->image)) $image = $file->file_name;
$image_source = empty($image) ? \Yii::getAlias('@storageUrl')."/properties/no-image.png" : \Yii::getAlias('@storageUrl')."/properties/".$id."/".$image ;

?>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<script>
  var labels = [<?= '"'.implode('","', $date).'"' ?>];
  var callsData = [<?= '"'.implode('","', $js_calls).'"' ?>];
  var messagedData = [<?= '"'.implode('","', $js_messages).'"' ?>];
  var viewData = [<?= '"'.implode('","', $js_views).'"' ?>];
</script>

<style>
  .data-1{background-color:#f381b9}.data-2{background-color:#61e3a9}.data-3{background-color:#61a9f3}
  .nav.nav-pills{padding-left: 50px}.bg-primary, .bg-success{padding: 10px}
  #img-block{width: 50%; float: left}
</style>
<h1 class="text-center page-header bg-success"> Last <?= $report_for_last_days ?> days indicator with dates </h1>
<h4 class="text-center bg-primary"> <?= strip_tags($model->location) ?> </h4>
<div class="row">
  <div id="img-block" class="col-sm-4">
    <?= Html::img($image_source, ['width' => '100%']); ?>
  </div>
  <div class="col-sm-8">
    <ul>
      <li><strong>Id:</strong> #<?= $model->id ?></li>
      <li><strong>Price:</strong> <?= $model->propertyPrice->id == 6 ? '$ '.$model->price_in_numbers : $model->propertyPrice->title ?></li>
      <li><strong>Contract:</strong> <?= $model->sold ? 'Sold' : 'Sale' ?></li>
      <li><strong>Location:</strong> <?= strip_tags($model->location) ?> </li>
      <li><strong>Area:</strong> <?= $model->square ?> <?= !empty($measure = $model->areaType) ? $measure->title : ''?></li>
      <?php if(!empty($model->propertyTypes)): ?>
        <li><strong>Type:</strong> <?php foreach($model->propertyTypes as $item){ echo $item->title. ", "; } ?> </li>
      <?php endif; ?>
      <?php if(isset($model->contract_type_id)): ?>
        <li><strong><?= $model->permitStatus()[$model->contract_type_id] ?></strong>  <?= !empty($details = $model->contract_details) ? "<a>{$details}</a>" : '' ?> </li>
      <?php endif; ?>
      <li><strong>Link to property:</strong> <a target="_blank" href="<?= $link = Yii::getAlias("@frontendUrl")."/properties/{$model->slug}" ?>"> <?= $link ?></a> </li>
    </ul>
  </div>
</div>

<hr/>

<div class="row">
  <div class="col-sm-12">
    <h4 class="bg-primary">Last <?= $report_for_last_days ?> days indicator with dates</h4>
    <table id="tbl_1" class="table table-striped">
      <thead>
        <tr> <th>Date</th> <th>Views</th> <th>Call clicks</th> <th>Messaged</th> </tr>
      </thead>
      <tbody>
        <?php foreach($output as $k => $item): ?>
          <?php if(is_numeric($k)): ?>
            <tr>
              <th scope="row"><?= $item['date'] ?></th>
              <td><?= $item['view'] ?></td>
              <td><?= $item['call'] ?></td>
              <td><?= $item['message'] ?></td>
            </tr>
          <?php endif; ?>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
  <hr/>
  <div class="row">
    <div class="col-sm-12">
      <span class="badge data-3"><?= array_sum($views) ?> Views</span>
      <span class="badge data-1"><?= array_sum($calls) ?> Calls</span>
      <span class="badge data-2"><?= array_sum($messages) ?> Messages</span>
    </div>
  </div>
  <jpgraph table="tbl_1" type="bar" title="Property Statistics for last <?= $report_for_last_days ?> days"
           label-y="Count" label-x="Date" series="cols" data-row-end="0"
           show-values="1" width="800" legend-overlap="0" hide-grid="0" hide-y-axis="0" />
  <hr/>
  <div class="col-sm-12">
    <h4 class="bg-primary">Maximum indicators</h4>
    <table class="table table-striped">
      <thead>
        <tr><th>Action</th> <th>Date</th> <th>Maximum indicator</th></tr>
      </thead>
      <tbody>
      <tr> <th scope="row">View:</th> <td><?= $output[$output['view']['index']]['date'] ?></td> <td><?= $output['view']['max'] ?></td></tr>
      <tr> <th scope="row">Call</th> <td><?= $output[$output['call']['index']]['date'] ?></td> <td><?= $output['call']['max'] ?></td></tr>
      <tr> <th scope="row">Message</th> <td><?= $output[$output['message']['index']]['date'] ?></td> <td><?= $output['message']['max'] ?></td></tr>
      </tbody>
    </table>
  </div>
  <div class="col-ms-12">
    <?php if(!empty($contacts)): ?>
      <div class="col-sm-12">
        <h4 class="bg-primary">Contacted for this property:</h4>
        <table class="table table-striped">
          <thead>
            <tr><th style="width: 5%">Date</th> <th style="width: 15%">From</th> <th style="width: 15%">Phone</th> <th style="width: 20%">Subject</th><th style="width: 45%">Message</th></tr>
          </thead>
          <tbody>
          <?php foreach($contacts as $item): ?>
            <tr>
              <th scope="row"><?= \Yii::$app->formatter->asDate($item->created_at, 'php:d M Y') ?></th>
              <td><?= $item->name ?> (<?= $item->email ?>)</td>
              <td><?= $item->phone_number ?></td>
              <td><?= $item->subject ?></td>
              <td><?= $item->message ?></td>
            </tr>
          <?php endforeach; ?>
          </tbody>
        </table>
      </div>
    <?php endif; ?>
  </div>
</div>




