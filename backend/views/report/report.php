<?php
/**
 * Created by PhpStorm.
 * User: rashad
 * Date: 1/12/16
 * Time: 22:26
 */

//http://www.rgraph.net/demos/index.html

?>

<style>
  h1{text-align: center}
  table{border: 1px solid #444; }
  table td, thead th{border-bottom: 1px solid #444; text-align: center}
  tbody tr:last-child{border: none}
  table tr > td:first-child {font-weight: bolder}
</style>

<h1> July month report for agency: </h1>

<table>
  <thead>
  <th>Days</th> <th>Visits</th> <th>Calls</th> <th>Messages</th>
  </thead>
  <tbody>
  <tr><td>1</td> <td>120</td> <td>23</td> <td>3</td> </tr>
  <tr><td>2</td> <td>15</td> <td>12</td> <td>1</td> </tr>
  <tr><td>3</td> <td>15</td> <td>15</td> <td>5</td> </tr>
  <tr><td>4</td> <td>6</td> <td>9</td> <td>7</td> </tr>
  </tbody>
</table>

<canvas id="cvs" width="1000" height="250"></canvas>

<script>

  window.onload = function(){
    var dataset1 = [34,54,1,6,34,5,2];
    var dataset2 = [7,7,68,6,26,1,111];
    //var dataset3 = [124,44,65,66,23,112,134];

    var line = new RGraph.Line({
      id: 'cvs',
      data: [dataset1, dataset2],
      options: {
        spline: true,
        linewidth: 3,
        fillstyle: ['red','blue','#0f0'],
        labelsAbove : true,
        //labelsAboveBackground : 'blue',
        //labelsAboveColor : 'white',
        title: 'Messages',
        labelsAboveSize : 12,
        hmargin: 5,
        labels: ['Mon','Tue','Wed','Thu','Fri','Sat','Sun'],
        tooltips: ['Mon','Tue','Wed','Thu','Fri','Sat','Sun'],
        tickmarks: 'circle',
        ticksize: 10,
      }
    }).trace2();
  };
</script>