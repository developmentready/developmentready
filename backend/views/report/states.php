<?php
/**
 * Created by PhpStorm.
 * User: rashad
 * Date: 6/15/16
 * Time: 12:52
 */


use yii\helpers\Html;
use yii\grid\GridView;
use \yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'States Stats');
$this->params['breadcrumbs'][] = $this->title;
$states = \common\models\State::find()->active()->all();
?>

<div class="state-stat-index">

  <?php echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
      ['class' => 'yii\grid\SerialColumn'],
      'recorded_at:date',
      [
        'attribute' => 'property_id',
        'label' => 'State',
        'value' => function($data) {
          $model = \common\models\State::findOne( $data->property_id );
          return !empty($model) ? $model->title : 'Not Correct' ;
        },
        'filter' => \yii\helpers\ArrayHelper::map($states, 'id', 'title')
      ],
      'counter',
      ['class' => 'yii\grid\ActionColumn'],
    ],
  ]); ?>
</div>