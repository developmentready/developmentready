<?php
/**
 * Created by PhpStorm.
 * User: rashad
 * Date: 1/12/16
 * Time: 22:26
 */

use yii\helpers\Html;


//http://www.chartjs.org/docs/
\backend\assets\ChartAsset::register($this);

$id = $model->id;
$date = [];
$js_messages = [];
$js_calls = [];
$js_views = [];
$output = [];

$counter = 0;
foreach($calls = $data_array['calls'] as $item){
  array_push($js_calls, $item);
  if($counter == 0){
    $output['call']['max'] = $item;
    $output['call']['index'] = $counter;
  } else {
    if($output['call']['max'] < $item) {
      $output['call']['max'] = $item;
      $output['call']['index'] = $counter;
    }
  }
  $output[$counter]['call'] = $item;
  $counter++;
}

$counter = 0;
foreach($messages = $data_array['messages'] as $item){
  array_push($js_messages, $item);
  if($counter == 0){
    $output['message']['max'] = $item;
    $output['message']['index'] = $counter;
  } else {
    if($output['message']['max'] < $item){
      $output['message']['max'] = $item;
      $output['message']['index'] = $counter;
    }
  }
  $output[$counter]['message'] = $item;
  $counter++;
}

$counter = 0;
foreach($views = $data_array['views'] as $item){
  array_push($js_views, $item);
  $output[$counter]['view'] = $item;
  if($counter == 0){
    $output['view']['max'] = $item;
    $output['view']['index'] = $counter;
  } else {
    if($output['view']['max'] < $item){
      $output['view']['max'] = $item;
      $output['view']['index'] = $counter;
    }
  }
  $counter++;
}

$counter = 0;
foreach($dates = $data_array['date'] as $item){
  array_push($date, \Yii::$app->formatter->asDate($item, 'php:M d'));
  $output[$counter]['date'] = \Yii::$app->formatter->asDate($item, 'php:d M Y');
  $counter++;
}

if(!empty($file = $model->defaultImage)) $image = $file->file_name;
elseif(!empty($file = $model->image)) $image = $file->file_name;
$image_source = empty($image) ? \Yii::getAlias('@storageUrl')."/properties/no-image.png" : \Yii::getAlias('@storageUrl')."/properties/".$id."/".$image ;

?>

<script>
  var labels = [<?= '"'.implode('","', $date).'"' ?>];
  var callsData = [<?= '"'.implode('","', $js_calls).'"' ?>];
  var messagedData = [<?= '"'.implode('","', $js_messages).'"' ?>];
  var viewData = [<?= '"'.implode('","', $js_views).'"' ?>];
</script>

<?php
$js = <<< 'SCRIPT'

  var data = {
    labels: labels,
    datasets: [
      {
        label: "Calls", data: callsData,
        fillColor: "rgba(220,220,220,0.2)",strokeColor: "#d70206",pointColor: "#d70206",
        pointStrokeColor: "#fff",pointHighlightFill: "#fff",pointHighlightStroke: "#d70206",
      },
      {
        label: "Messages", data: messagedData,
        fillColor: "rgba(151,187,205,0.2)",strokeColor: "#f4c63d",pointColor: "#f4c63d",
        pointStrokeColor: "#fff",pointHighlightFill: "#fff",pointHighlightStroke: "#f4c63d",
      },
      {
        label: "Views", data: viewData,
        fillColor: "rgba(0,187,205,0.3)",strokeColor: "rgba(0,187,205,1)",
        pointColor: "rgba(0,187,205,1)",pointStrokeColor: "#fff",
        pointHighlightFill: "#fff",pointHighlightStroke: "rgba(151,187,205,1)",
      }
    ]
  };

  var options = {
    // Boolean - Whether to animate the chart
    animation: true,

    // Number - Number of animation steps
    animationSteps: 60,

    // String - Animation easing effect
    // Possible effects are:
    // [easeInOutQuart, linear, easeOutBounce, easeInBack, easeInOutQuad,
    //  easeOutQuart, easeOutQuad, easeInOutBounce, easeOutSine, easeInOutCubic,
    //  easeInExpo, easeInOutBack, easeInCirc, easeInOutElastic, easeOutBack,
    //  easeInQuad, easeInOutExpo, easeInQuart, easeOutQuint, easeInOutCirc,
    //  easeInSine, easeOutExpo, easeOutCirc, easeOutCubic, easeInQuint,
    //  easeInElastic, easeInOutSine, easeInOutQuint, easeInBounce,
    //  easeOutElastic, easeInCubic]
    animationEasing: "easeOutQuart",

    // Boolean - If we should show the scale at all
    showScale: true,

    // Boolean - If we want to override with a hard coded scale
    scaleOverride: false,

    // ** Required if scaleOverride is true **
    // Number - The number of steps in a hard coded scale
    scaleSteps: null,
    // Number - The value jump in the hard coded scale
    scaleStepWidth: null,
    // Number - The scale starting value
    scaleStartValue: null,

    // String - Colour of the scale line
    scaleLineColor: "rgba(0,0,0,.5)",

    // Number - Pixel width of the scale line
    scaleLineWidth: 1,

    // Boolean - Whether to show labels on the scale
    scaleShowLabels: true,

    // Interpolated JS string - can access value
    scaleLabel: "<%=value%>",

    // Boolean - Whether the scale should stick to integers, not floats even if drawing space is there
    scaleIntegersOnly: true,

    // Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
    scaleBeginAtZero: false,

    // String - Scale label font declaration for the scale label
    scaleFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",

    // Number - Scale label font size in pixels
    scaleFontSize: 14,

    // String - Scale label font weight style
    scaleFontStyle: "normal",

    // String - Scale label font colour
    scaleFontColor: "#666",

    // Boolean - whether or not the chart should be responsive and resize when the browser does.
    responsive: true,

    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio: true,

    // Boolean - Determines whether to draw tooltips on the canvas or not
    showTooltips: true,

    // Function - Determines whether to execute the customTooltips function instead of drawing the built in tooltips (See [Advanced - External Tooltips](#advanced-usage-custom-tooltips))
    customTooltips: false,

    // Array - Array of string names to attach tooltip events
    tooltipEvents: ["mousemove", "touchstart", "touchmove"],

    // String - Tooltip background colour
    tooltipFillColor: "rgba(0,0,0,0.8)",

    // String - Tooltip label font declaration for the scale label
    tooltipFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",

    // Number - Tooltip label font size in pixels
    tooltipFontSize: 14,

    // String - Tooltip font weight style
    tooltipFontStyle: "normal",

    // String - Tooltip label font colour
    tooltipFontColor: "#fff",

    // String - Tooltip title font declaration for the scale label
    tooltipTitleFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",

    // Number - Tooltip title font size in pixels
    tooltipTitleFontSize: 14,

    // String - Tooltip title font weight style
    tooltipTitleFontStyle: "bold",

    // String - Tooltip title font colour
    tooltipTitleFontColor: "#fff",

    // Number - pixel width of padding around tooltip text
    tooltipYPadding: 6,

    // Number - pixel width of padding around tooltip text
    tooltipXPadding: 6,

    // Number - Size of the caret on the tooltip
    tooltipCaretSize: 8,

    // Number - Pixel radius of the tooltip border
    tooltipCornerRadius: 0,

    // Number - Pixel offset from point x to tooltip edge
    tooltipXOffset: 10,

    // String - Template string for single tooltips
    tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %>",

    // String - Template string for multiple tooltips
    // multiTooltipTemplate: "<%= value %>",
    multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>",

    // Function - Will fire on animation progression.
    onAnimationProgress: function(){},

    // Function - Will fire on animation completion.
    onAnimationComplete: function(){
      document.getElementById("url").src = document.getElementById("myChart").toDataURL();
      document.getElementById("myChart").remove();
    }
  }

    // Get the context of the canvas element we want to select
  var ctx = document.getElementById("myChart").getContext("2d");
  var myLineChart = new Chart(ctx).Line(data, options);

SCRIPT;
$this->registerJs($js);

?>

<style>
  .data-1{background-color:#d70206}.data-2{background-color:#f4c63d}.data-3{background-color:rgba(0,187,205,1)}
  .nav.nav-pills{padding-left: 50px}.bg-primary, .bg-success{padding: 10px}
</style>
<h1 class="text-center page-header bg-success"> Last 30 days report for property: </h1>
<h4 class="text-center bg-primary"> <?= strip_tags($model->location) ?> </h4>
<div class="row">
  <div class="col-sm-4">
    <?= Html::img($image_source, ['width' => '100%']); ?>
  </div>
  <div class="col-sm-8">
    <ul>
      <li><strong>Id:</strong> #<?= $model->id ?></li>
      <li><strong>Price:</strong> <?= $model->propertyPrice->id == 6 ? '$ '.$model->price_in_numbers : $model->propertyPrice->title ?></li>
      <li><strong>Contract:</strong> <?= $model->sold ? 'Sold' : 'Sale' ?></li>
      <li><strong>Location:</strong> <?= strip_tags($model->location) ?> </li>
      <li><strong>Area:</strong> <?= $model->square ?> <?= !empty($measure = $model->areaType) ? $measure->title : ''?></li>
      <?php if(!empty($model->propertyTypes)): ?>
        <li><strong>Type:</strong> <?php foreach($model->propertyTypes as $item){ echo $item->title. ", "; } ?> </li>
      <?php endif; ?>
      <?php if(isset($model->contract_type_id)): ?>
        <li><strong><?= $model->permitStatus()[$model->contract_type_id] ?></strong>  <?= !empty($details = $model->contract_details) ? "<a>{$details}</a>" : '' ?> </li>
      <?php endif; ?>
      <li><strong>Link to property:</strong> <a target="_blank" href="<?= $link = Yii::getAlias("@frontendUrl")."/properties/{$model->slug}" ?>"> <?= $link ?></a> </li>
    </ul>
  </div>
</div>

<hr/>

<div class="row">
  <div class="col-sm-12">
    <ul class="nav nav-pills text-center" role="tablist">
      <li role="presentation"><span class="badge data-3"><?= array_sum($views) ?> Views</span></li>
      <li role="presentation"><span class="badge data-1"><?= array_sum($calls) ?> Calls</span></li>
      <li role="presentation"><span class="badge data-2"><?= array_sum($messages) ?> Messages</span></li>
    </ul>
  </div>
</div>

<hr/>

<canvas id="myChart"></canvas>
<img id="url" />
<hr/>

<div class="row">
  <?php if(!empty($contacts)): ?>
    <div class="col-sm-12">
      <h4 class="bg-primary">Contacted for this property:</h4>
      <table class="table table-striped">
        <thead>
        <tr><th>Date</th> <th>From</th> <th>Phone</th> <th>Subject</th><th>Message</th></tr>
        </thead>
        <tbody>
        <?php foreach($contacts as $item): ?>
          <tr>
            <th scope="row"><?= \Yii::$app->formatter->asDate($item->created_at, 'php:d M Y') ?></th>
            <td><?= $item->name ?> (<?= $item->email ?>)</td>
            <td><?= $item->phone_number ?></td>
            <td><?= $item->subject ?></td>
            <td><?= $item->message ?></td>
          </tr>
        <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  <?php endif; ?>
  <div class="col-sm-7">
    <h4 class="bg-primary">Last 30 days indicator with dates</h4>
    <table class="table table-striped">
      <thead>
        <tr> <th>Date</th> <th>Views</th> <th>Call clicks</th> <th>Messaged</th> </tr>
      </thead>
      <tbody>
        <?php foreach($output as $k => $item): ?>
          <?php if(is_numeric($k)): ?>
            <tr>
              <th scope="row"><?= $item['date'] ?></th>
              <td><?= $item['view'] ?></td>
              <td><?= $item['call'] ?></td>
              <td><?= $item['message'] ?></td>
            </tr>
          <?php endif; ?>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
  <div class="col-sm-5">
    <h4 class="bg-primary">Maximum indicators</h4>
    <table class="table table-striped">
      <thead>
      <tr><th>Action</th> <th>Date</th> <th>Maximum count</th></tr>
      </thead>
      <tbody>
      <tr> <th scope="row">View:</th> <td><?= $output[$output['view']['index']]['date'] ?></td> <td><?= $output['view']['max'] ?></td></tr>
      <tr> <th scope="row">Call</th> <td><?= $output[$output['call']['index']]['date'] ?></td> <td><?= $output['call']['max'] ?></td></tr>
      <tr> <th scope="row">Message</th> <td><?= $output[$output['message']['index']]['date'] ?></td> <td><?= $output['message']['max'] ?></td></tr>
      </tbody>
    </table>
  </div>
</div>




