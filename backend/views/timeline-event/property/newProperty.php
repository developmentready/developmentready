<?php
/**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 8/29/15
 * Time: 14:50
 */

$property = \common\models\Property::findOne($model->data['propertyId']);

?>

<div class="timeline-item">
  <span class="time">
    <i class="fa fa-clock-o"></i>
    <?php echo Yii::$app->formatter->asDatetime($model->created_at) ?>
  </span>

  <h3 class="timeline-header">
    <?php echo Yii::t('backend', 'You have new property!') ?>
    Added by <?= $model->application == 'backend' ? 'admin' : 'user account' ?>.
    Status:
    <?php if( !empty($property->status)) { echo $property->status ? '<span class="text-success">active</span>' : '<span class="text-danger text-bold text-uppercase"> - activation require</span>'; ?>
    <?php } else { echo "no record"; } ?>
    <span class="danger"><?php # $model->data['status'] == 0 ? Yii::t('common', 'Require approvement') : '' ?></span>
  </h3>

  <div class="timeline-body">
    <?php echo Yii::t('backend', 'New property Id: {identity} was created at {created_at}', [
      'identity' => $model->data['propertyId'],
      'created_at' => Yii::$app->formatter->asDatetime($model->data['created_at'])
    ]) ?>
  </div>

  <div class="timeline-footer">
    <?php echo \yii\helpers\Html::a(
      Yii::t('backend', 'Edit Property'),
      ['/property/update', 'id' => $model->data['propertyId']],
      ['class' => 'btn btn-info btn-sm']
    ) ?>
  </div>
</div>