<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/16/15
 * Time: 15:09
 */

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Agency */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Agencies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="agency-view">

  <p>
    <?php echo Html::a(Yii::t('app', 'All Agencies'), ['index'], ['class' => 'btn btn-info']) ?>
    <?php echo Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?php echo Html::a('Delete', ['delete', 'id' => $model->id], [
      'class' => 'btn btn-danger',
      'data' => [
        'confirm' => 'Are you sure you want to delete this item?',
        'method' => 'post',
      ],
    ]) ?>
  </p>

  <?php echo DetailView::widget([
    'model' => $model,
    'attributes' => [
      'id',
      'priority',
      [
        'attribute' => 'status',
        'format' => 'html',
        'value' => $model->status ? '<span class="text-success">Yes</span>' : '<span class="text-danger">No</span>',
      ],
      'title',
      [
        'attribute' => 'about',
        'format' => 'html',
        'value' => (!empty($about = $model->about)) ? "<div class='col-xs-12'>{$about}</div>" : 'No info about agency',
      ],
      [
        'label' => 'Brand Color',
        'format' => 'html',
        'value' => !empty($color = $model->color) ? "<div style='height: 40px; width: 40px; background: {$color}'></div>" : '',
      ],
      [
        'label' => 'Font Color',
        'format' => 'html',
        'value' => !empty($color = $model->secondary_color) ? "<div style='height: 40px; width: 40px; background: {$color}'></div>" : '',
      ],
      [
        'label' => 'Logo of agency',
        'format' => 'html',
        'value' => !empty($media_path = $model->image) ? "<img style='height: 140px;' src='".\Yii::getAlias('@storageUrl').$media_path->media_path."' />" : 'No Logo Uploaded',
      ],
      [
        'label' => 'Logo watermark',
        'format' => 'html',
        'value' => !empty($media_path = $model->logo_thumbnail) ? "<img style='width: 600px;' src='".\Yii::getAlias('@storageUrl').$media_path."' />" : 'No watermark',
      ],
      [
        'attribute' => 'creator_id',
        'value' => !empty($user = $model->creator) ? $user->username : 'Self user',
      ],
      'created_at:datetime',
      [
        'attribute' => 'updater_id',
        'value' => $model->updater->username,
      ],
      'updated_at:datetime',
    ],
  ]) ?>

</div>
