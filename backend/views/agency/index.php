<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/16/15
 * Time: 15:09
 */

use yii\helpers\Html;
use yii\grid\GridView;
use common\components\helpers\Setup;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\AgencySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Agencies';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>table tr img{max-width: 200px}</style>
	
<div class="agency-index">

  <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

  <p>
    <?php echo Html::a('New Agency', ['/user/create'], ['class' => 'btn btn-success']) ?>
  </p>

  <?php echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
      ['class' => 'yii\grid\SerialColumn'],

      [
        'attribute'=>'status',
        'format'   => 'html',
        'value'    => function($data){ return $data->status ? '<i class="fa fa-check-square" style="color:#5CB85C"></i>' : '<i class="fa fa-close" style="color: #A94442"></i>';},
        'filter'=> ['0' => 'Deactive' , '1' => 'Active'],
      ],
      'priority',
      'featured_aceny:boolean',
      'title',
      [
        'attribute' => 'about',
        'format' => 'html',
        'value' => function($data){return Setup::truncate($data->about, 120, "...") ;},
      ],
      [
        'attribute' => 'main_logo_id',
        'format' => 'html',
        'value' => function($data){ return !empty($media = $data->image) ? "<img style='width: 50px;' src='".\Yii::getAlias('@storageUrl').$media->media_path."' />" : 'No Logo Uploaded';},
      ],
      [
        'attribute' => 'updater_id',
        'value' => function($data){return !empty($user = $data->updater) ? $user->username : 'No username';}
      ],
      'updated_at:date',

      ['class' => 'yii\grid\ActionColumn'],
    ],
  ]); ?>

</div>
