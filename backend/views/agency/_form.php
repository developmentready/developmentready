<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/16/15
 * Time: 15:08
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Agency */
/* @var $form yii\bootstrap\ActiveForm */
?>


<div class="agency-form">

  <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>

  <?php echo $form->errorSummary($model); ?>

  <div class="row">
    <div class="col-xs-6 col-sm-4">
      <?php echo $form->field($model, 'status')->checkbox(['checked' => true]) ?>
    </div>
    <div class="col-xs-6 col-sm-4">
      <?php echo $form->field($model, 'featured_aceny')->checkbox(['checked' => false]) ?>
    </div>
    <div class="col-xs-6 col-sm-4">
      <?php echo $form->field($model, 'show_logo_single_page')->checkbox(['checked' => false]) ?>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-8">
      <div class="row">
        <div class="col-xs-6">
          <?php echo $form->field($model, 'priority')->textInput() ?>
        </div>
        <div class="col-xs-6">
          <?php echo $form->field($model, 'user_id')->dropDownList(
            \yii\helpers\ArrayHelper::map(\common\models\User::find()->where(['user_type'=>1])->orderBy('name')->all(),
              'id', 'name')
            ,['prompt' => 'Select user']) ?>
        </div>
      </div>
    </div>
    <div class="col-sm-4">
      <?php echo $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-sm-4">
      <?php echo $form->field($model, 'ext_id')->textInput(['maxlength' => true]) ?>
    </div>
  </div>

  <hr class="col-xs-12"/>

  <div class="row">
    <div class="col-xs-7">
      <?= $this->render("//layouts/_upload_images", ['form' => $form, 'model' => $model, 'max_image_count' => 1]) ?>
    </div>
    <?php if(!empty($model->image)): ?>
      <div class="col-xs-5">
        <div class="thumbnail">
          <img src="<?= Yii::getAlias('@storageUrl')."/".$model->image['media_path'] ?>"/>
          <div class="caption text-center">
            <p><a href="<?= Url::to(['delete-image', 'id' => $model->image['id']]) ?>" class="btn btn-danger">delete image</a></p>
          </div>
        </div>
      </div>
    <?php endif; ?>
  </div>

  <hr class="col-xs-12"/>

  <div class="row">
    <div class="col-xs-4">
      <label>Logo Background Color:</label>
      <div id="brand-color" class="input-group">
        <span class="input-group-addon"><i></i></span>
        <?php echo $form->field($model, 'color', ['template' => '{input}'])->textInput(['maxlength' => true])->label(false) ?>
      </div>
      <label>Font Color:</label>
      <div id="font-color" class="input-group">
        <span class="input-group-addon"><i></i></span>
        <?php echo $form->field($model, 'secondary_color', ['template' => '{input}'])->textInput(['maxlength' => true])->label(false) ?>
      </div>
    </div>
    <div class="col-xs-8">
      <?php echo $form->field($model, 'address')->textarea(['maxlength' => true, 'rows' => 4]) ?>
    </div>
  </div>

  <div class="row">
    <div class="col-md-4 col-sm-6">
      <?php echo $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-4 col-sm-6">
      <?php echo $form->field($model, 'mobile')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-4 col-sm-6">
      <?php echo $form->field($model, 'email')->textInput(['maxlength' => true, 'type' => 'email']) ?>
    </div>
    <div class="col-md-4 col-sm-6">
      <?php echo $form->field($model, 'fax')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-4 col-sm-6">
      <?php echo $form->field($model, 'web')->textInput(['maxlength' => true]) ?>
    </div>
  </div>

  <?= $this->render("//layouts/_text_editor", [ 'form' => $form, 'model' => $model, 'attribute' => 'about']) ?>

  <hr class="col-xs-12"/>

  <?= $this->render("//layouts/_map_position", ['form' => $form, 'model' => $model]) ?>

  <hr class="col-xs-12"/>

  <div class="form-group">
    <?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>