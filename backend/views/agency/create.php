<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/17/15
 * Time: 02:13
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Agency */

$this->title = 'Add new Agency';
$this->params['breadcrumbs'][] = ['label' => 'Agencies', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Add new';
?>

<div class="agency-update">

  <?php echo $this->render('_form', [
    'model' => $model,
  ]) ?>

</div>