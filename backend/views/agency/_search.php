<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/16/15
 * Time: 15:09
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\AgencySearch */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="agency-search">

  <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
  ]); ?>

  <?php echo $form->field($model, 'id') ?>

  <?php echo $form->field($model, 'name') ?>

  <?php echo $form->field($model, 'about') ?>

  <?php echo $form->field($model, 'color') ?>

  <?php echo $form->field($model, 'secondary_color') ?>

  <?php // echo $form->field($model, 'main_logo') ?>

  <?php // echo $form->field($model, 'logo_thumbnail') ?>

  <?php // echo $form->field($model, 'creator_id') ?>

  <?php // echo $form->field($model, 'created_at') ?>

  <?php // echo $form->field($model, 'updater_id') ?>

  <?php // echo $form->field($model, 'updated_at') ?>

  <div class="form-group">
    <?php echo Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
    <?php echo Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>