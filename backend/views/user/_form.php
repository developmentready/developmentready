<?php

use common\models\User;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\UserForm */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $roles yii\rbac\Role[] */
/* @var $permissions yii\rbac\Permission[] */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>
  <div class="row">
    <div class="col-xs-6">
      <?php echo $form->field($model, 'name') ?>
    </div>
    <div class="col-xs-6">
      <?php echo $form->field($model, 'username')->label('User name only for local users') ?>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-4 col-xs-6">
      <?php echo $form->field($model, 'email') ?>
    </div>
    <div class="col-sm-4 col-xs-6">
      <?php echo $form->field($model, 'password')->passwordInput() ?>
    </div>
    <div class="col-sm-4">
      <?php echo $form->field($model, 'user_type')->dropDownList(
        [ 1=>'Agency', 2 => 'Common user' ], ['prompt' => 'User type']) ?>
    </div>
  </div>

  <div class="row">
    <div class="col-xs-6">
      <?php echo $form->field($model, 'status')->label(Yii::t('app', 'Active ?'))->checkbox() ?>
    </div>
    <div class="col-xs-6">
      <?php echo $form->field($model, 'roles')->checkboxList($roles) ?>
    </div>
  </div>

  <div class="form-group">
      <?php echo Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
  </div>
<?php ActiveForm::end(); ?>

</div>
