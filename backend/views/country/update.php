<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Country */

$active_code = NULL;

$this->title = Yii::t('common', 'Update Country');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Countries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="country-update">
  <h1><?= Html::encode($this->title) ?></h1>
  <div class="page-type-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
      <div class="col-md-3 col-sm-12">
        <?= $form->field($model, "code")->textInput(['readOnly' => true]); ?>
      </div>
      <div class="col-md-3 col-sm-12">
        <?= $form->field($model, "title")->textInput(); ?>
      </div>
    </div>

    <hr class="separator col-sm-12"/>

    <div class="form-group">
        <div class="col-md-3 col-sm-6">
          <?= $form->field($model, 'status')->checkbox() ?>
        </div>
        <div class="col-md-9 col-sm-6">
          <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
    </div>
</div>

</div>
