<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Page */


$this->title = \common\components\helpers\Setup::truncate($model->title, 40, "...");
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="page-update">

  <?php echo $this->render('_form', [
    'model' => $model,
    'parent' => $parent,
  ]) ?>

</div>