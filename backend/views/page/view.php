<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Page */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


if($model->page_template_id == 5){
  $url = Yii::getAlias('@frontendUrl')."/".$model->url_name;
} else {
  $url = \Yii::getAlias('@frontendUrl')."/".\Yii::$app->controller->id."/{$model->url_name}";
}

?>
<div class="page-view">

  <p>
    <?php echo Html::a(Yii::t('app', 'All'), ['index', 'id' => $model->id], ['class' => 'btn btn-info']) ?>
    <?php echo Html::a(Yii::t('app', 'Add New'), ['create', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
    <?php echo Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?php echo Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
      'class' => 'btn btn-danger',
      'data' => [
        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
        'method' => 'post',
      ],
    ]) ?>
  </p>

  <?php echo DetailView::widget([
    'model' => $model,
    'attributes' => [
      'id',
      [
        'attribute' => 'status',
        'format' => 'html',
        'value' => $model->status ? '<span class="text-success">Yes</span>' : '<span class="text-danger">No</span>',
      ],
      'priority',
      [
        'attribute' => 'parent_id',
        'value' => !empty($parent = $model->parent) ? $parent->title : "No Parent page",
      ],
      [
        'attribute' => 'menu_type',
        'value' => $model->menuPosition()[$model->menu_type],
      ],
      [
        'attribute' => 'url_name',
        'format' => 'html',
        'value' => "<a href='".$url."'>".$url."</a>",
      ],
      'title',
      'h1_tag:html',
      'body:html',
      'page_robot_indexing:boolean',
      'meta_keywords',
      'meta_description',
      [
        'attribute' => 'creator_id',
        'value' => $model->creator->username,
      ],
      'created_at:datetime',
      [
        'attribute' => 'updater_id',
        'value' => $model->updater->username,
      ],
      'updated_at:datetime',
    ],
  ]) ?>

</div>