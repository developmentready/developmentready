<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Pages');
$this->params['breadcrumbs'][] = $this->title;
?>

  <div id="menu-strcuture-block" class="col-xs-3">
    <h3>Menu Structure:</h3>
    <ul>
      <?= \common\components\helpers\MenuHelper::getMenuStructure() ?>
    </ul>
  </div>
  <div class="page-index col-xs-9">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
      <?php echo Html::a(Yii::t('app', 'Create {modelClass}', ['modelClass' => 'Page',
      ]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>



    <?php echo GridView::widget([
      'dataProvider' => $dataProvider,
      'filterModel' => $searchModel,
      'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
          'attribute'=>'status',
          'format'   => 'html',
          'value'    => function($data){ return $data->status ? '<i class="fa fa-check-square" style="color:#5CB85C"></i>' : '<i class="fa fa-close" style="color: #A94442"></i>';},
          'filter'=> ['0' => 'Deactive' , '1' => 'Active'],
        ],
        'priority',
        'title',
        [
          'attribute'=>'parent_id',
          'format'   => 'html',
          'value'    => function($data){ return !empty($title = $data->parent) ? $title->title : 'No parent page';},
        ],
        [
          'attribute'=>'menu_type',
          'format'   => 'html',
          'value'    => function($data){ return $data->menuPosition()[$data->menu_type];},
        ],

        [
          'attribute' => 'updater_id',
          'value' => function($data){return $data->updaterName ;}
        ],
        'updated_at:date',

        ['class' => 'yii\grid\ActionColumn'],
      ],
    ]); ?>
  </div>