<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Testimonials');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-index">

  <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

  <p>
    <?php echo Html::a(Yii::t('app', 'Create Testimonial'), ['create'], ['class' => 'btn btn-success']) ?>
  </p>

  <?php echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
      ['class' => 'yii\grid\SerialColumn'],

        'id',
        [
          'attribute'=>'status',
          'format'   => 'html',
          'value'    => function($data){ return $data->status ? '<i class="fa fa-check-square" style="color:#5CB85C"></i>' : '<i class="fa fa-close" style="color: #A94442"></i>';},
          'filter'=> ['0' => 'Deactive' , '1' => 'Active'],
        ],
        [
          'label'   => 'Logo Image',
          'format'  => 'html',
          'value'   => function($data){
            $out = empty($media_path = $data->image) ? \Yii::getAlias('@storageUrl')."/properties/no-image.png" : \Yii::getAlias('@storageUrl').$media_path->media_path;
            return Html::img($out, ['width' => '70px']);

          },
        ],
        'priority',
        [
          'attribute' => 'title',
          'label' => 'Person Name',
        ],
        'body:html',
        [
          'attribute' => 'updater_id',
          'value' => function($data){ return !empty($user = $data->updater) ? $user->username : 'No user' ;}
        ],
        'updated_at:date',

      ['class' => 'yii\grid\ActionColumn'],
    ],
  ]); ?>

</div>
