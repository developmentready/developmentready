<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Page */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-view">

  <p>
    <?php echo Html::a(Yii::t('app', 'All Testimonials'), ['index', 'id' => $model->id], ['class' => 'btn btn-info']) ?>
    <?php echo Html::a(Yii::t('app', 'Add New Testimonial'), ['create', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
    <?php echo Html::a(Yii::t('app', 'Update Testimonial'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?php echo Html::a(Yii::t('app', 'Delete Testimonial'), ['delete', 'id' => $model->id], [
      'class' => 'btn btn-danger',
      'data' => [
        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
        'method' => 'post',
      ],
    ]) ?>
  </p>

  <?php echo DetailView::widget([
    'model' => $model,
    'attributes' => [
      'id',
      [
        'attribute' => 'status',
        'format' => 'html',
        'value' => $model->status ? '<span class="text-success">Yes</span>' : '<span class="text-danger">No</span>',
      ],
      'priority',
      [
        'label' => 'Logo of agency',
        'format' => 'html',
        'value' => !empty($media_path = $model->image) ? "<img style='height: 140px;' src='".\Yii::getAlias('@storageUrl').$media_path->media_path."' />" : 'No Logo Uploaded',
      ],
      [
        'attribute' => 'title',
        'label' => 'Person Name',
      ],
      'body:html',
      [
        'attribute' => 'creator_id',
        'value' => $model->creator->username,
      ],
      'created_at:datetime',
      [
        'attribute' => 'updater_id',
        'value' => $model->updater->username,
      ],
      'updated_at:datetime',
    ],
  ]) ?>

</div>
