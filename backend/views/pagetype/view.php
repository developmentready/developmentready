<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PageType */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Page Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-type-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
              'attribute'=>'status',
              'format' => 'html',
              'value' => $model->status ? '<i class="fa fa-check-square fa-2x" style="color:#5CB85C"></i>' : '<i class="fa fa-close fa-2x" style="color: #A94442"></i>'
            ],
            ['attribute'=>'createName', 'format'=>'raw'],
            ['attribute'=>'updateName', 'format'=>'raw'],
            ['attribute'=>'created_at', 'value'=>date('d-m-Y H:i', $model->created_at)],
            ['attribute'=>'updated_at', 'value' => date('d-m-Y H:i', $model->updated_at)],
        ],
    ]) ?>

</div>
