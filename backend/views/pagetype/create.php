<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PageType */

$this->title = Yii::t('backend', 'Create Page Type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Page Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
      'model' => $model,
      'lang_model' => $lang_model,
      'active_langs' => $active_langs,
    ]) ?>

</div>
