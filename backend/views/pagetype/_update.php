<?php
/* 
 * This file was created by Rashad Aliyev (RAliyev@avagr.com)
 *  on Mar 15, 2015 
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PageType */
/* @var $form yii\widgets\ActiveForm */

$active_code = NULL;
$missin_langs = [];
?>

<div class="page-type-form">
  <?php $form = ActiveForm::begin(); ?>
  <?= $form->field($model, 'status')->dropDownList(['0' => Yii::t('backend','Disabled') , '1' => Yii::t('backend','Active')]) ?>

  <div role="tabpanel">
    <ul class="nav nav-tabs" role="tablist">
    <?php foreach($active_langs as $lang): ?>
      <?php $lang->default === 1 ? $active_code = $lang->code : '' ?>
      <li role="presentation" class="<?= ($active_code === $lang->code) ? 'active' : '' ?>"><a href="#<?= $lang->code ?>-tab" aria-controls="<?= $lang->code ?>-tab" role="tab" data-toggle="tab"><?= strtoupper($lang->title) ?></a></li>
    <?php endforeach; ?>
    </ul>
    <br/>
    <div class="tab-content">
      <?php foreach($active_langs as $lang): ?>
      <div role="tabpanel" class="tab-pane <?= $active_code === $lang->code ? 'active' : '' ?>" id="<?= $lang->code ?>-tab">
        <?php foreach($lang_model as $k => $v): ?>
          <?php if($v->lang_id == $lang->id): ?>
            <?php // Html::activeHiddenInput($one_model, "[$one_model->lang_id]lang_id", ['value' => $one_model->lang_id]) ?>
            <?= $form->field($v, "[$k]title")->textInput(['maxlength' => 512]); ?>
          <?php endif; ?>
          <?php # Here we will code program which will check if language_was before inserted
                # if not we will add new tabs for this perpose like in _form new insert
                # $one_model->id ? :  ?>
        <?php endforeach; ?>
      </div>
      <?php endforeach; ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
  </div>
</div>