<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PageType */
/* @var $form yii\widgets\ActiveForm */

$active_code = NULL;

?>

<div class="page-type-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'status')->dropDownList(['0' => Yii::t('backend','Disabled') , '1' => Yii::t('backend','Active')]) ?>
  
    <div role="tabpanel">
      <ul class="nav nav-tabs" role="tablist">
      <?php foreach ($active_langs as $lang): ?>
        <?php $lang->default === 1 ? $active_code = $lang->code : '' ?>
        <li role="presentation" class="<?= ($active_code === $lang->code) ? 'active' : '' ?>"><a href="#<?= $lang->code ?>-tab" aria-controls="<?= $lang->code ?>-tab" role="tab" data-toggle="tab"><?= strtoupper($lang->title) ?></a></li>
      <?php endforeach; ?>
      </ul>
      <br/>
      <div class="tab-content">
        <?php foreach ($active_langs as $lang): ?>
        <div role="tabpanel" class="tab-pane <?= $active_code === $lang->code ? 'active' : '' ?>" id="<?= $lang->code ?>-tab">
          <?= Html::activeHiddenInput($lang_model, "[$lang->id]lang_id", ['value' => $lang->id]) ?>
          <?= $form->field($lang_model, "[$lang->id]title")->textInput(['maxlength' => 512]); ?>
        </div>
        <?php endforeach; ?>
      </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

