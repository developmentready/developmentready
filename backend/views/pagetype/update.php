<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PageType */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
    'modelClass' => 'Page Type',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Page Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="page-type-update">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_update', [
      'model' => $model,
      'lang_model' => $lang_model,
      'active_langs' => $active_langs,
    ]) ?>

</div>
