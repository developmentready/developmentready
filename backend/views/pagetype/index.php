<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Page Types');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-type-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('backend', 'Create Page Type'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
              'attribute'=>'status',
              'format' => 'html',
              'value' => function($data){ return $data->status ? '<i class="fa fa-check-square" style="color:#5CB85C"></i>' : '<i class="fa fa-close" style="color: #A94442"></i>';}
            ],
            [
              'attribute' => Yii::t('common','Title'),
              'format' => 'html',
              'value' => function($data){ $arr = $data->getLangtile($data->id, $lang = Yii::$app->language); return isset($arr[0]['title']) ? $arr[0]['title'] : '';}
            ],
            ['attribute'=>'createName', 'format'=>'raw'],
            [
              'attribute' => 'created_at',
              'value' => function($data){ return date("d-M-Y", $data->created_at);},
            ],
            ['attribute'=>'updateName', 'format'=>'raw'],
            [
              'attribute' => 'updated_at',
              'value' => function($data){ return date("d-M-Y", $data->created_at);},
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
