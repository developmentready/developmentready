<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PageCategory */

$this->title = Yii::t('app', 'Create Blog Category');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Blog Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-category-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
