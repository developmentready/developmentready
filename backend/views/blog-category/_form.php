<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PageCategory */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="page-category-form">

  <?php $form = ActiveForm::begin(); ?>

  <?php echo $form->errorSummary($model); ?>

  <div class="row">
    <div class="col-xs-3"><?php echo $form->field($model, 'status')->checkbox() ?></div>
    <div class="col-xs-9"><?php echo $form->field($model, 'title')->textInput(['maxlength' => true]) ?></div>
  </div>
  <div class="form-group">
    <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>
