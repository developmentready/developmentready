<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Page */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="page-form">

  <?php $form = ActiveForm::begin(); ?>

  <?php echo $form->errorSummary($model); ?>

  <div class="row">
    <div class="col-xs-6">
      <?php echo $form->field($model, 'status')->checkbox() ?>
    </div>
    <div class="col-xs-6">
      <?php echo $form->field($model, 'priority')->textInput() ?>
    </div>
  </div>

  <hr class="col-xs-12" />

  <div class="row">
    <div class="col-xs-6 col-md-4">
      <?php echo $form->field($model, 'title')->textInput(['maxlength' => true])
        ->label('Person Name')?>
    </div>
    <div class="col-xs-6 col-md-4">
      <?php echo $form->field($model, 'h1_tag')->textInput(['maxlength' => true])
          ->label('Position & Organization name')?>
    </div>
    <div class="col-sm-12 col-md-4">
      <?php echo $form->field($model, 'menu_label')->textInput(['maxlength' => true])
        ->label('Agency Name')?>
    </div>
  </div>

  <hr class="col-xs-12" />

  <div class="row">
    <div class="col-sm-12">
      <?= $this->render("//layouts/_text_editor", [ 'form' => $form, 'model' => $model, 'attribute' => 'body']) ?>
    </div>
  </div>

  <div class="form-group">
      <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
  </div>

  <?php ActiveForm::end(); ?>
</div>
