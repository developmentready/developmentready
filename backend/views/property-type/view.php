<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/19/15
 * Time: 16:36
 */

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PropertyType */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Property Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="property-type-view">

  <p>
    <?php echo Html::a(Yii::t('app', 'All'), ['index', 'id' => $model->id], ['class' => 'btn btn-info']) ?>
    <?php echo Html::a(Yii::t('app', 'Add New'), ['create', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
    <?php echo Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?php echo Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
      'class' => 'btn btn-danger',
      'data' => [
        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
        'method' => 'post',
      ],
    ]) ?>
  </p>

  <?php echo DetailView::widget([
    'model' => $model,
    'attributes' => [
      'id',
      [
        'attribute' => 'status',
        'format' => 'html',
        'value' => $model->status ? '<span class="text-success">Yes</span>' : '<span class="text-danger">No</span>',
      ],
      'title',
      [
        'attribute' => 'creator_id',
        'value' => $model->creator->username,
      ],
      'created_at:datetime',
      [
        'attribute' => 'updater_id',
        'value' => $model->updater->username,
      ],
      'updated_at:datetime',
    ],
  ]) ?>

</div>