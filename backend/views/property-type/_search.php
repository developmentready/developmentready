<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/19/15
 * Time: 16:33
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\PropertyTypeSearch */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="property-type-search">

  <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
  ]); ?>

  <?php echo $form->field($model, 'id') ?>

  <?php echo $form->field($model, 'status') ?>

  <?php echo $form->field($model, 'title') ?>

  <?php echo $form->field($model, 'creator_id') ?>

  <?php echo $form->field($model, 'created_at') ?>

  <?php echo $form->field($model, 'updater_id') ?>

  <?php echo $form->field($model, 'updated_at') ?>

  <div class="form-group">
    <?php echo Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
    <?php echo Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>