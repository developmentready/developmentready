<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/18/15
 * Time: 08:07
 */

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\AgencyAgentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Agency Agents');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agency-agent-index">

  <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

  <p>
    <?php echo Html::a(Yii::t('app', 'Create {modelClass}', [
      'modelClass' => 'Agency Agent',
    ]), ['create'], ['class' => 'btn btn-success']) ?>
  </p>

  <?php echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
      ['class' => 'yii\grid\SerialColumn'],
      [
        'attribute'=>'status',
        'format'   => 'html',
        'value'    => function($data){ return $data->status ? '<i class="fa fa-check-square" style="color:#5CB85C"></i>' : '<i class="fa fa-close" style="color: #A94442"></i>';},
        'filter'=> ['0' => 'Deactive' , '1' => 'Active'],
      ],
      'full_name',
      [
        'attribute'=>'agency_id',
        'value'  => function($data){ return !empty($agency_name = $data->agency->title) ? $agency_name : 'No Agency Selected';},
        'filter'=> \yii\helpers\ArrayHelper::map(\common\models\Agency::find()->active()->all(), 'id' , 'title'),
      ],
      [
        'attribute'=>'position_id',
        'value'  => function($data){ return !empty($position = $data->position) ? $position->title : 'No position was set';},
        'filter'=> \yii\helpers\ArrayHelper::map(\common\models\AgencyPosition::find()->active()->all(), 'id' , 'title'),
      ],
       'mobile',
       'email:email',
       'updated_at:date',

      ['class' => 'yii\grid\ActionColumn'],
    ],
  ]); ?>

</div>
