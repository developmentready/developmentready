<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/18/15
 * Time: 08:08
 */

use yii\helpers\Html;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $model common\models\AgencyAgent */

$this->title = $model->full_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Agency Agents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agency-agent-view">

  <p>
    <?php echo Html::a(Yii::t('app', 'All'), ['index', 'id' => $model->id], ['class' => 'btn btn-info']) ?>
    <?php echo Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?php echo Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
      'class' => 'btn btn-danger',
      'data' => [
        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
        'method' => 'post',
      ],
    ]) ?>
  </p>

  <?php echo DetailView::widget([
    'model' => $model,
    'attributes' => [
      'id',
      [
        'attribute' => 'status',
        'format' => 'html',
        'value' => $model->status ? '<span class="text-success">Yes</span>' : '<span class="text-danger">No</span>',
      ],
      [
        'label' => 'Agent Image',
        'format' => 'html',
        'value' => !empty($media = $model->image) ? "<img style='height: 140px;' src='".\Yii::getAlias('@storageUrl').$media->media_path."' />" : 'No Logo Uploaded',
      ],
      'full_name',
      'about:ntext',
      [
        'label' => 'Agency Name',
        'value' => $model->agency->title,
      ],
      [
        'label' => 'Position',
        'value' => !empty($position = $model->position) ? $position->title : 'Not Set',
      ],
      'mobile',
      'email:email',
      [
        'label' => 'Creator',
        'value' => $model->creatorName,
      ],
      'created_at:date',
      [
        'attribute' => 'updater_id',
        'value' => $model->updaterName,
      ],
      'updated_at:date',
    ],
  ]) ?>

</div>