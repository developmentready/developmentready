<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/18/15
 * Time: 08:07
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AgencyAgent */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Agency Agent',
  ]) . ' ' . $model->full_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Agency Agents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->full_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="agency-agent-update">

  <?php echo $this->render('_form', [
    'model' => $model,
    'agencies' => $agencies,
    'positions' => $positions
  ]) ?>

</div>