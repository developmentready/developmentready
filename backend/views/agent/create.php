<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/18/15
 * Time: 08:07
 */

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AgencyAgent */

$this->title = Yii::t('app', 'Create {modelClass}', [
  'modelClass' => 'Agency Agent',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Agency Agents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agency-agent-create">

  <?php echo $this->render('_form', [
    'model' => $model,
    'agencies' => $agencies,
    'positions' => $positions
  ]) ?>

</div>