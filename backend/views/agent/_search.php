<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/18/15
 * Time: 08:07
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\AgencyAgentSearch */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="agency-agent-search">

  <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
  ]); ?>

  <?php echo $form->field($model, 'id') ?>

  <?php echo $form->field($model, 'status') ?>

  <?php echo $form->field($model, 'full_name') ?>

  <?php echo $form->field($model, 'about') ?>

  <?php echo $form->field($model, 'agency_id') ?>

  <?php // echo $form->field($model, 'position_id') ?>

  <?php // echo $form->field($model, 'mobile') ?>

  <?php // echo $form->field($model, 'email') ?>

  <?php // echo $form->field($model, 'creator_id') ?>

  <?php // echo $form->field($model, 'created_at') ?>

  <?php // echo $form->field($model, 'updater_id') ?>

  <?php // echo $form->field($model, 'updated_at') ?>

  <div class="form-group">
    <?php echo Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
    <?php echo Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>