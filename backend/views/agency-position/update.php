<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/18/15
 * Time: 08:05
 */
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AgencyPosition */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Agency Position',
  ]) . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Agency Positions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="agency-position-update">

  <?php echo $this->render('_form', [
    'model' => $model,
  ]) ?>

</div>