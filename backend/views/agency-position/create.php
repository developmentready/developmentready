<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/18/15
 * Time: 08:02
 */

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AgencyPosition */

$this->title = Yii::t('app', 'Create {modelClass}', [
  'modelClass' => 'Agency Position',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Agency Positions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agency-position-create">

  <?php echo $this->render('_form', [
    'model' => $model,
  ]) ?>

</div>