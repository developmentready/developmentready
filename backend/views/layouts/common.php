<?php
/**
 * @var $this yii\web\View
 */
use backend\widgets\Menu;
use common\models\TimelineEvent;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

?>
<?php $this->beginContent('@backend/views/layouts/base.php'); ?>
    <div class="wrapper">
        <!-- header logo: style can be found in header.less -->
        <header class="main-header">
            <a href="<?php echo Yii::getAlias('@frontendUrl') ?>" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                <?php echo Yii::$app->name ?>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only"><?php echo 'Toggle navigation' ?></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li id="timeline-notifications" class="notifications-menu">
                            <a href="<?php echo Url::to(['/timeline-event/index']) ?>">
                                <i class="fa fa-bell"></i>
                                <span class="label label-success">
                                    <?php echo TimelineEvent::find()->today()->count() ?>
                                </span>
                            </a>
                        </li>
                        <!-- Notifications: style can be found in dropdown.less -->
                        <li id="log-dropdown" class="dropdown notifications-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                              <i class="fa fa-warning"></i>
                            <span class="label label-danger">
                              <?php echo \backend\models\SystemLog::find()->count() ?>
                            </span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header"><?php echo 'You have '.\backend\models\SystemLog::find()->count().' log items'; ?></li>
                                <li>
                                    <!-- inner menu: contains the actual data -->
                                    <ul class="menu">
                                        <?php foreach(\backend\models\SystemLog::find()->orderBy(['log_time'=>SORT_DESC])->limit(5)->all() as $logEntry): ?>
                                            <li>
                                                <a href="<?php echo Yii::$app->urlManager->createUrl(['/log/view', 'id'=>$logEntry->id]) ?>">
                                                    <i class="fa fa-warning <?php echo $logEntry->level == \yii\log\Logger::LEVEL_ERROR ? 'text-red' : 'text-yellow' ?>"></i>
                                                    <?php echo $logEntry->category ?>
                                                </a>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                </li>
                                <li class="footer">
                                    <?php echo Html::a('View all', ['/log/index']) ?>
                                </li>
                            </ul>
                        </li>
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="<?php echo Yii::$app->user->identity->userProfile->getAvatar() ?: '/img/anonymous.jpg' ?>" class="user-image">
                                <span><?php echo Yii::$app->user->identity->username ?> <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header light-blue">
                                    <img src="<?php echo Yii::$app->user->identity->userProfile->getAvatar() ?: '/img/anonymous.jpg' ?>" class="img-circle" alt="User Image" />
                                    <p>
                                        <?php Yii::$app->user->identity->username ?>
                                        <small>
                                            <?php echo 'Member since {0, date, short}', Yii::$app->user->identity->created_at ?>
                                        </small>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <?php echo Html::a('Profile', ['/sign-in/profile'], ['class'=>'btn btn-default btn-flat']) ?>
                                    </div>
                                    <div class="pull-left">
                                        <?php echo Html::a('Account', ['/sign-in/account'], ['class'=>'btn btn-default btn-flat']) ?>
                                    </div>
                                    <div class="pull-right">
                                        <?php echo Html::a('Logout', ['/sign-in/logout'], ['class'=>'btn btn-default btn-flat', 'data-method' => 'post']) ?>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <?php echo Html::a('<i class="fa fa-cogs"></i>', ['/site/settings'])?>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- Sidebar user panel -->
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="<?php echo Yii::$app->user->identity->userProfile->getAvatar() ?: '/img/anonymous.jpg' ?>" class="img-circle" />
                    </div>
                    <div class="pull-left info">
                        <p><?php echo 'Hello, '.Yii::$app->user->identity->getPublicIdentity() ?></p>
                        <a href="<?php echo Url::to(['/sign-in/profile']) ?>">
                            <i class="fa fa-circle text-success"></i>
                            <?php echo Yii::$app->formatter->asDatetime(time()) ?>
                        </a>
                    </div>
                </div>
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <?php echo Menu::widget([
                    'options'=>['class'=>'sidebar-menu'],
                    'labelTemplate' => '<a href="#">{icon}<span>{label}</span>{right-icon}{badge}</a>',
                    'linkTemplate' => '<a href="{url}">{icon}<span>{label}</span>{right-icon}{badge}</a>',
                    'submenuTemplate'=>"\n<ul class=\"treeview-menu\">\n{items}\n</ul>\n",
                    'activateParents'=>true,
                    'items'=>[
                      [
                        'label'=>'Timeline',
                        'icon'=>'<i class="fa fa-bar-chart-o"></i>',
                        'url'=>['/timeline-event/index'],
                        'badge'=> TimelineEvent::find()->today()->count(),
                        'badgeBgClass'=>'label-success',
                      ],
                      [
                        'label'=>'Properties',
                        'icon'=>'<i class="fa fa-building"></i>',
                        'url'=>['/property/index'],
                      ],
                      [
                        'label'=>'Property Filters',
                        'icon'=>'<i class="fa fa-filter"></i>',
                        'options'=>['class'=>'treeview'],
                        'items'=> [
                          ['label'=>'Featured Properties', 'url'=>['/property/featured'], 'icon'=>'<i class="fa fa-usd"></i>'],
                          ['label'=>'Property Statistics', 'url'=>['/property/stats'], 'icon'=>'<i class="fa fa-bar-chart"></i>'],
                          ['label'=>'States Stats', 'url'=>['/report/states'], 'icon'=>'<i class="fa fa-bar-chart"></i>'],
                        ]
                      ],
                      [
                        'label'=>'Property Settings',
                        'icon'=>'<i class="fa fa-cogs"></i>',
                        'options'=>['class'=>'treeview'],
                        'items'=>[
                          ['label'=>'Property type', 'url'=>['/property-type/index'], 'icon'=>'<i class="fa fa-angle-double-right"></i>'],
                          ['label'=>'Price types', 'url'=>['/price-type/index'], 'icon'=>'<i class="fa fa-angle-double-right"></i>'],
                          ['label'=>'Area Types', 'url'=>['/area-type/index'], 'icon'=>'<i class="fa fa-angle-double-right"></i>'],
                        ]
                      ],
                    [
                      'label'=>'Agencies',
                      'icon'=>'<i class="fa fa-briefcase"></i>',
                      'options'=>['class'=>'treeview'],
                      'items'=>[
                        ['label'=>'Agencies', 'url'=>['/agency/index'], 'icon'=>'<i class="fa fa-angle-double-right"></i>'],
                        ['label'=>'Agents', 'url'=>['/agent/index'], 'icon'=>'<i class="fa fa-angle-double-right"></i>'],
                        ['label'=>'Agency Positions', 'url'=>['/agency-position/index'], 'icon'=>'<i class="fa fa-angle-double-right"></i>'],
                      ]
                    ],
                    [
                      'label'=>'Locations',
                      'icon'=>'<i class="fa fa-map-marker"></i>',
                      'options'=>['class'=>'treeview'],
                      'items'=>[
                        ['label'=>'States', 'url'=>['/state/index'], 'icon'=>'<i class="fa fa-angle-double-right"></i>'],
                        ['label'=>'Suburbs', 'url'=>['/suburb/index'], 'icon'=>'<i class="fa fa-angle-double-right"></i>'],
                      ]
                    ],
                    ['label'=>'Pages', 'url'=>['/page/index'], 'icon'=>'<i class="fa fa-file"></i>'],
                    [
                      'label'=>'Messages',
                      'icon'=>'<i class="fa fa-envelope"></i>',
                      'options'=>['class'=>'treeview'],
                      'items'=>[
                        ['label'=>'Subscribers', 'url'=>['/contact/subscribers'], 'icon'=>'<i class="fa fa-angle-double-right"></i>'],
                        ['label'=>'Media kits requests and messages', 'url'=>['/contact/messages'], 'icon'=>'<i class="fa fa-angle-double-right"></i>'],
                        ['label'=>'From Properties', 'url'=>['/contact/property'], 'icon'=>'<i class="fa fa-angle-double-right"></i>'],
                        ['label'=>'From Agencies', 'url'=>['/contact/agency'], 'icon'=>'<i class="fa fa-angle-double-right"></i>'],
                      ]
                    ],
                    [
                      'label'=>'Content',
                      'icon'=>'<i class="fa fa-pencil-square-o"></i>',
                      'options'=>['class'=>'treeview'],
                      'items'=>[
                        ['label'=>'Blog', 'url'=>['/blog/index'], 'icon'=>'<i class="fa fa-angle-double-right"></i>'],
                        ['label'=>'Blog Category', 'url'=>['/blog-category/index'], 'icon'=>'<i class="fa fa-angle-double-right"></i>'],
                        ['label'=>'Testimonial', 'url'=>['/testimonial/index'], 'icon'=>'<i class="fa fa-angle-double-right"></i>'],
                        ['label'=>'What People Say', 'url'=>['/feedback/index'], 'icon'=>'<i class="fa fa-angle-double-right"></i>'],
                      ]
                    ],
                    [
                      'label'=>'Users',
                      'icon'=>'<i class="fa fa-users"></i>',
                      'url'=>['/user/index'],
                      'visible'=>Yii::$app->user->can('administrator')
                    ],
                    [
                      'label'=>'System',
                      'icon'=>'<i class="fa fa-cogs"></i>',
                      'options'=>['class'=>'treeview'],
                      'items'=>[
                        ['label'=>'Page Settings', 'url'=>['/settings/index'], 'icon'=>'<i class="fa fa-angle-double-right"></i>'],
                        ['label'=>'Key-Value Storage', 'url'=>['/key-storage/index'], 'icon'=>'<i class="fa fa-angle-double-right"></i>'],
                        ['label'=>'File Storage', 'url'=>['/file-storage/index'], 'icon'=>'<i class="fa fa-angle-double-right"></i>'],
                        //['label'=>'Cache', 'url'=>['/cache/index'], 'icon'=>'<i class="fa fa-angle-double-right"></i>'],
                        //['label'=>'File Manager', 'url'=>['/file-manager/index'], 'icon'=>'<i class="fa fa-angle-double-right"></i>'],
                        [
                          'label'=>'System Information',
                          'url'=>['/system-information/index'],
                          'icon'=>'<i class="fa fa-angle-double-right"></i>'
                        ],
//                        [
//                          'label'=>'Logs',
//                          'url'=>['/log/index'],
//                          'icon'=>'<i class="fa fa-angle-double-right"></i>',
//                          'badge'=>\backend\models\SystemLog::find()->count(),
//                          'badgeBgClass'=>'label-danger',
//                        ],
                      ]
                    ]
                  ]
                ]) ?>
            </section>
            <!-- /.sidebar -->
        </aside>

        <!-- Right side column. Contains the navbar and content of the page -->
        <aside class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    <?php echo $this->title ?>
                    <?php if (isset($this->params['subtitle'])): ?>
                        <small><?php echo $this->params['subtitle'] ?></small>
                    <?php endif; ?>
                </h1>

                <?php echo Breadcrumbs::widget([
                    'tag'=>'ol',
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
            </section>

            <!-- Main content -->
            <section class="content">
                <?php if (Yii::$app->session->hasFlash('alert')):?>
                    <?php echo \yii\bootstrap\Alert::widget([
                        'body'=>ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'body'),
                        'options'=>ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'options'),
                    ])?>
                <?php endif; ?>
                <?php if (Yii::$app->session->hasFlash('success')){?>
                    <div class="text-center page-header" style="background-color: #91d675; padding: 8px;">
                        <?=Yii::$app->session->getFlash('success');?>
                    </div>
                <?php }elseif(Yii::$app->session->hasFlash('error')){?>
                    <div class="text-center page-header" style="background-color: #FF0000; padding: 8px;">
                        <?=Yii::$app->session->getFlash('error');?>
                    </div>
                <?php } ?>
                <?php echo $content ?>
            </section><!-- /.content -->
        </aside><!-- /.right-side -->
    </div><!-- ./wrapper -->

<?php $this->endContent(); ?>