<?php
/**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 7/11/15
 * Time: 14:12
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Address */
/* @var $form yii\widgets\ActiveForm */

$active_code = NULL;

$this->title = Yii::t('common', 'Create');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'All list'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="create-form">
  <?php $form = ActiveForm::begin(); ?>

  <div class="row">
    <div id="first" class="col-md-3 col-sm-6">
      <?= $form->field($model, "priority")->textInput(['value' => 0]); ?>
    </div>
    <?php if(!empty($parent_model)): ?>
      <div class="col-sm-3">
        <?= $form->field($model, 'parent_id')->dropDownList(\yii\helpers\ArrayHelper::map(
          $parent_model, 'id', "{$parent_lang_model}.title"), ['prompt'=>Yii::t('common','Select')]) ?>
      </div>
    <?php endif; ?>
  </div>
  <hr class="separator col-sm-12"/>
  <div role="tabpanel col-sm-12">
    <ul class="nav nav-tabs" role="tablist">
      <?php foreach ($alangs as $lang): ?>
        <?php $lang->default === 1 ? $active_code = $lang->code : '' ?>
        <li role="presentation" class="<?= ($active_code === $lang->code) ? 'active' : '' ?>"><a href="#<?= $lang->code ?>-tab" aria-controls="<?= $lang->code ?>-tab" role="tab" data-toggle="tab"><?= strtoupper($lang->title) ?></a></li>
      <?php endforeach; ?>
    </ul>
    <br/>
    <div class="tab-content">
      <?php foreach ($alangs as $lang): ?>
        <div role="tabpanel" class="tab-pane <?= $active_code === $lang->code ? 'active' : '' ?>" id="<?= $lang->code ?>-tab">
          <?= Html::activeHiddenInput($lmodel, "[$lang->id]lang_id", ['value' => $lang->id]) ?>
          <?= $this->render('@backend/views/layouts/_createTitleLang', ['form' => $form, 'lmodel' => $lmodel, 'lang' => $lang]) ?>
        </div>
      <?php endforeach; ?>
    </div>
  </div>

  <hr class="separator col-sm-12"/>
  <?= $this->render('@backend/views/layouts/_formFooter', ['form' => $form, 'model' => $model]) ?>


  <?php ActiveForm::end(); ?>
</div>

