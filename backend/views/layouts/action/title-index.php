<?php
/**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 7/11/15
 * Time: 17:04
 */

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common', 'All list');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="country-index">
  <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
  <p>
    <?= Html::a(Yii::t('common', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
  </p>

  <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
      ['class' => 'yii\grid\SerialColumn'],
      [
        'attribute'=>'status',
        'format'   => 'html',
        'value'    => function($data){ return $data->status ? '<i class="fa fa-check-square" style="color:#5CB85C"></i>' : '<i class="fa fa-close" style="color: #A94442"></i>';},
        'filter'=> ['0' => Yii::t('common', 'Deactive') , '1' => Yii::t('common','Active')],
      ],
      //['title'],

      ['class' => 'yii\grid\ActionColumn',
        'template'=>'{update}{delete}'],
    ],
  ]); ?>

</div>
