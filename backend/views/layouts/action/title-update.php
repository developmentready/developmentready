<?php
/**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 7/11/15
 * Time: 17:47
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Address */

$active_code = NULL;

$this->title = Yii::t('common', 'Update') . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'All list'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('common', 'Update');
?>

<div class="update">
  <h1><?= Html::encode($this->title) ?></h1>

  <?php $form = ActiveForm::begin(); ?>
  <div class="row">
    <div id="first" class="col-md-3 col-sm-6">
      <?= $form->field($model, "priority")->textInput(); ?>
    </div>
    <?php if(!empty($parent_model)): ?>
      <div class="col-sm-3">
        <?= $form->field($model, 'parent_id')->dropDownList(\yii\helpers\ArrayHelper::map(
          $parent_model, 'id', "translation.title"), ['prompt'=>Yii::t('common','Select')]) ?>
      </div>
    <?php endif; ?>
  </div>

  <hr class="separator col-sm-12"/>
  <h3><?= Yii::t('common', 'Title') ?>:</h3>
  <hr class="separator col-sm-12"/>
  <div role="tabpanel">
    <ul class="nav nav-tabs" role="tablist">
      <?php foreach($alangs as $lang): ?>
        <?php $lang->default === 1 ? $active_code = $lang->code : '' ?>
        <li role="presentation" class="<?= ($active_code === $lang->code) ? 'active' : '' ?>"><a href="#<?= $lang->code ?>-tab" aria-controls="<?= $lang->code ?>-tab" role="tab" data-toggle="tab"><?= strtoupper($lang->title) ?></a></li>
      <?php endforeach; ?>
    </ul>
    <br/>
    <div class="tab-content">
      <?php foreach($alangs as $lang): ?>
        <div role="tabpanel" class="tab-pane <?= $active_code === $lang->code ? 'active' : '' ?>" id="<?= $lang->code ?>-tab">
          <?php foreach($lmodel as $k => $v): ?>
            <?php if($v->lang_id == $lang->id): ?>
              <?= $this->render('@backend/views/layouts/_updateTitleLang', ['form' => $form, 'v' => $v, 'k' => $k]) ?>
            <?php endif; ?>
            <?php # Here we will code program which will check if language_was before inserted
            # if not we will add new tabs for this perpose like in _form new insert
            # $one_model->id ? :  ?>
          <?php endforeach; ?>
        </div>
      <?php endforeach; ?>
    </div>
    <hr class="separator col-sm-12"/>
    <?= $this->render('@backend/views/layouts/_formFooter', ['form' => $form, 'model' => $model]) ?>
    <?php ActiveForm::end(); ?>
  </div>

</div>