<?php
/**
 * Created by PhpStorm.
 * User: rashad
 * Date: 1/12/16
 * Time: 22:22
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/* @var $this \yii\web\View */
/* @var $content string */

\backend\assets\ChartAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?php echo Yii::$app->language ?>">
<head>
  <meta charset="<?php echo Yii::$app->charset ?>">
  <meta name="robots" content="noindex">
  <?php echo Html::csrfMetaTags() ?>
  <title><?php echo Html::encode($this->title) ?></title>
  <?php $this->head() ?>
</head>
<?php $this->beginBody() ?>

  <?php echo $content ?>

<?php $this->endBody() ?>


</html>
<?php $this->endPage() ?>
