<?php

/* 
 * This file was created by Rashad Aliyev (RAliyev@avagr.com)
 *  on Apr 11, 2015 
 */

use yii\helpers\Html;

?>

<div class="form-group">
  <style>.field-page-status{height: 35px}</style>
  <div class="col-md-3 col-sm-6" style="float:left; font-size: 1.5em">
    <?= $form->field($model, 'status')->checkbox() ?>
    <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
  </div>
</div>
<hr class="separator col-sm-12"/><hr class="separator col-sm-12"/>