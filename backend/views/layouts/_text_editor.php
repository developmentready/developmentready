<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/17/15
 * Time: 09:51
 */

echo $form->field($model, $attribute)->widget(
  \yii\imperavi\Widget::className(),
  [
    'plugins' => ['fullscreen', 'table', 'video'],
    'options'=>[
      'formattingAdd' => [
        [ 'tag' => 'h4', 'title' => Yii::t('common','Blue Block'), 'class' => 'blue-block',],
        [ 'tag' => 'h4', 'title' => Yii::t('common','Brand Header'), 'class' => 'brand-block' ],
        [ 'tag' => 'p', 'title' => Yii::t('common','Navy Paragraph'), 'class' => 'navy-paragrap' ],
        [ 'tag' => 'span', 'title' => Yii::t('common','Red Text'), 'style' => 'color: #cf181e;'],
        [ 'tag' => 'span', 'title' => Yii::t('common','Big Red'), 'style' => 'font-size: 20px; color: cf181e;', 'class' => 'big-red'],
        [ 'tag' =>'span', 'title' => Yii::t('common','Font Size {size}px',['size' => '20']), 'style' => 'font-size: 20px;','class' => 'font-size-20'],
        [ 'tag' => 'span', 'title' => Yii::t('common','Font Georgia'), 'style' => 'font-family: Georgia;','class' => 'font-family-georgia'],
        [ 'tag' => 'code', 'title' => Yii::t('common','Code')],
        [ 'tag' => 'mark', 'title' => Yii::t('common','Marked')],
      ],
      'minHeight'=>200,
      //'maxHeight'=>400,
      'placeholder' => \Yii::t('common', 'Start typing about agency from here...'),
      'buttonSource'=>true,
      'convertDivs'=>false,
      'pastePlainText' => true,
      'cleanOnPaste' => false,
      'cleanSpaces' => false,
      'removeEmptyTags'=>true,
      'replaceDivs' => false,
      'paragraphize' => true,
      'cleanStyleOnEnter' => false,
      //'allowedTags' => ['p', 'h1', 'h2', 'pre'],
      //'removeDataAttr' => true,
      //'linebreaks' => true,
      'imageUpload'=>Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
    ]
  ]
);
