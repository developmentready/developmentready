<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/21/15
 * Time: 15:33
 */

use trntv\filekit\widget\Upload;


echo $form->field($model, 'fileAttribute')->widget(
  Upload::className(),
  [
    'url' => ['/file-storage/upload'],
    'sortable' => true,
    'maxFileSize' => 10 * 1024 * 1024, // 10 MiB
    'maxNumberOfFiles' => 1
  ])->label('Featured image');