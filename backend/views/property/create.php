<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/19/15
 * Time: 00:15
 */
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Property */

$this->title = Yii::t('app', 'Create {modelClass}', [
  'modelClass' => 'Property',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Properties'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="property-create">

  <?php echo $this->render('_form', [
    'model' => $model,
    //'property_types' => $property_types
  ]) ?>

</div>