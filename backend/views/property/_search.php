<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/19/15
 * Time: 00:14
 */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\PropertySearch */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="property-search">

  <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
  ]); ?>

  <?php echo $form->field($model, 'id') ?>

  <?php echo $form->field($model, 'priority') ?>

  <?php echo $form->field($model, 'status') ?>

  <?php echo $form->field($model, 'description') ?>

  <?php echo $form->field($model, 'user_id') ?>

  <?php // echo $form->field($model, 'property_type_id') ?>

  <?php // echo $form->field($model, 'price_type_id') ?>

  <?php // echo $form->field($model, 'price_in_numbers') ?>

  <?php // echo $form->field($model, 'square') ?>

  <?php // echo $form->field($model, 'square_measure_id') ?>

  <?php // echo $form->field($model, 'contract_type_id') ?>

  <?php // echo $form->field($model, 'contract_details') ?>

  <?php // echo $form->field($model, 'suburb_id') ?>

  <?php // echo $form->field($model, 'location') ?>

  <?php // echo $form->field($model, 'lat') ?>

  <?php // echo $form->field($model, 'lng') ?>

  <?php // echo $form->field($model, 'view_count') ?>

  <?php // echo $form->field($model, 'deleted') ?>

  <?php // echo $form->field($model, 'accept_terms') ?>

  <?php // echo $form->field($model, 'advertised_till') ?>

  <?php // echo $form->field($model, 'featured_till') ?>

  <?php // echo $form->field($model, 'creator_id') ?>

  <?php // echo $form->field($model, 'created_at') ?>

  <?php // echo $form->field($model, 'updater_id') ?>

  <?php // echo $form->field($model, 'updated_at') ?>

  <div class="form-group">
    <?php echo Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
    <?php echo Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>