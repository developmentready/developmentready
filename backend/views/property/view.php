<?php
/**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 8/29/15
 * Time: 15:04
 */

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Property */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Properties'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$property_types = '';

$property_types_model = \common\models\PropertyTypeAssn::find()->where(['property_id' => $model->id])->all();
foreach($property_types_model as $item) $property_types .= $item->type->title.", ";


if($model->price_type_id == 6 && is_numeric($model->price_in_numbers)){
  $property_price = $model->propertyPrice->title." (".number_format($model->price_in_numbers, 2, '.', ' ').")";
} else {
  $property_price = $model->propertyPrice->title." ";
  if(!empty($model->price_in_numbers)){ $property_price .=  $model->price_in_numbers;}
}

if(!empty($file = $model->defaultImage)){
  $image_url = Yii::getAlias('@storageUrl')."/properties/{$model->id}/".$file->file_name;
}
elseif(!empty($file = $model->image))
  $image_url = Yii::getAlias('@storageUrl')."/properties/{$model->id}/".$file->file_name;
else
  $image_url = Yii::getAlias('@storageUrl')."/properties/no-image.png";


if(!empty($users = $model->propertyUsers)){
  $agencies = '';
  foreach($users as $agent){
    if($agent->user_type == 1)
      $agencies .= $agent->agency->title. ", ";
    else
      $agencies .= $agent->userProfile->full_name;
  }
} else {
  $agencies = 'No Agency';
}

if(!empty($model->contract_type_id)){
  $permit_status = $model->permitStatus()[$model->contract_type_id];
} else {
  $permit_status = 'Not set';
}

?>
<div class="property-view">

  <p>
    <?php echo Html::a(Yii::t('app', 'All'), ['index', 'id' => $model->id], ['class' => 'btn btn-info']) ?>
    <?php echo Html::a(Yii::t('app', 'Add New'), ['create', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
    <?php echo Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?php echo Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
      'class' => 'btn btn-danger',
      'data' => [
        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
        'method' => 'post',
      ],
    ]) ?>
  </p>

  <?php echo DetailView::widget([
    'model' => $model,
    'attributes' => [
      'id',
      [
        'attribute' => 'status',
        'format' => 'html',
        'value' => $model->status ? '<span class="text-success">Yes</span>' : '<span class="text-danger">No</span>',
      ],
      [
        'label' => 'Link to property',
        'format' => 'html',
        'value' => "<a target='_blank' href='".Yii::getAlias('@frontendUrl/properties/').$model->slug."'>".Yii::getAlias('@frontendUrl/properties/').$model->slug."</a>",
      ],
      [
        'label' => 'Image',
        'format' => 'html',
        'value' => "<img height='150' src='".$image_url."' />"
      ],
      [
        'attribute' => 'description',
        'format' => 'html',
      ],
      [
        'label' => 'Property Owner',
        'value' => $agencies,
      ],
      'featured_till:date',
      [
        'attribute' => 'property_type_id',
        'value' => $property_types,
      ],
      [
        'label' => 'Property Price',
        'value' => $property_price,
      ],
      [
        'label' => 'Square',
        'value' => !empty($model->square && $mea = $model->areaType) ? $model->square." / ".$mea->title : 'No square indicated',
      ],
      [
        'attribute' => 'contract_type_id',
        'value' => $permit_status
      ],
      [
        'attribute' => 'suburb_id',
        'value' => $model->suburb->title." ({$model->suburb->post_code})",
      ],
      [
        'label' => 'Property location',
        'value' => $model->location . ". Map pistion (Lat: {$model->lat}, Long{$model->lng})",
      ],
      'meta_keywords',
      'meta_description'
    ],
  ]) ?>

</div>