<?php
/**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 8/29/15
 * Time: 15:03
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PropertySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Properties');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="property-index">

  <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

  <p>
    <?php echo Html::a(Yii::t('app', 'Create {modelClass}', [
      'modelClass' => 'Property',
    ]), ['create'], ['class' => 'btn btn-success']) ?>
  </p>

  <?php echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
      ['class' => 'yii\grid\SerialColumn'],

      'id',
      [
        'attribute'=>'status',
        'format'   => 'html',
        'value'    => function($data){ return $data->status ? '<i class="fa fa-check-square" style="color:#5CB85C"></i>' : '<i class="fa fa-close" style="color: #A94442"></i>';},
        'filter'=> ['0' => 'Deactive' , '1' => 'Active'],
      ],
      [
        'label'=> 'Agency\\User',
        'attribute' => 'user',
        'value'    => function($data){
          $output = '';
          if(!empty($users = $data->propertyUsers)){
            foreach($users as $user){
              if($user->user_type == 1)
                $output .= !empty($agency = $user->agency) ? $agency->title : 'No name'. ", ";
              else
                $output .= $user->userProfile->full_name;
            }
          } else {
            $output = 'No Agency';
          }

          return $output;},
        //'filter'   => ArrayHelper::map(common\models\Agency::find()->active()->all(), 'user_id', 'title'),
      ],
      'location:text',
      [
        'label'   => 'Property Image',
        'format'  => 'html',
        //'attribute' => 'image',
        'value'   => function($data){

          if(!empty($file = $data->defaultImage)) $image = $file->file_name;
          elseif(!empty($file = $data->image)) $image = $file->file_name;
          $id = $data->id ;
          $out = empty($image) ? \Yii::getAlias('@storageUrl')."/properties/no-image.png" : \Yii::getAlias('@storageUrl')."/properties/".$id."/".$image ;
          return Html::img($out, ['width' => '70px']);

        },
      ],
      [
        'attribute'=> 'contract_type_id',
        'value'    => function($data){ $out = !empty($data->contract_type_id) ? $data->permitStatus()[$data->contract_type_id] : 'No Permit Status Selected' ; return $out;},
        'filter'   => ['0' => 'Permit Potential', '1' => 'Permit Ready'],
      ],
      [
        'attribute'=> 'sold',
        'value'    => function($data){return $data->soldStatus()[$data->sold];},
        'filter'   => ['0' => 'No', '1' => 'Yes'],
      ],
      'updated_at:date',

      ['class' => 'yii\grid\ActionColumn'],
    ],
  ]); ?>

</div>