<?php
/**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 8/29/15
 * Time: 15:03
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PropertySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $data common\models\Property */

$this->title = Yii::t('app', 'Properties');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="property-index">

  <div class="row">
    <div class="col-sm-3">
      <p>
        <?php echo Html::a(Yii::t('app', 'Create {modelClass}', [
          'modelClass' => 'Property',
        ]), ['create'], ['class' => 'btn btn-success']) ?>
      </p>
    </div>
    <div class="col-sm-9">
      <div class="row">

      </div>
    </div>
  </div>

  <?php echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
      ['class' => 'yii\grid\SerialColumn'],

      'id',
      [
        'label' => 'Views',
        'attribute' => 'views_count',
        'format' => 'html',
        'value' => function($data){ return empty($data->reportViews) ? 0 : $data->reportViews;},
      ],
      [
        'label' => 'Count of calls',
        'attribute' => 'call_count',
        'format' => 'html',
        'value' => function($data){ return empty($data->reportCalls) ? 0 : $data->reportCalls; },
      ],
      [
        'label' => 'Messages count',
        'attribute' => 'message_count',
        'format' => 'html',
        'value' => function($data){ return empty($data->reportMessages) ? 0 : $data->reportMessages; },
      ],
      'location:text',
      [
        'label'   => 'Property Image',
        'format'  => 'html',
        'value'   => function($data){

          if(!empty($file = $data->defaultImage)) $image = $file->file_name;
          elseif(!empty($file = $data->image)) $image = $file->file_name;
          $id = $data->id ;
          $out = empty($image) ? \Yii::getAlias('@storageUrl')."/properties/no-image.png" : \Yii::getAlias('@storageUrl')."/properties/".$id."/".$image ;
          return Html::img($out, ['width' => '70px']);

        },
      ],
      [
        'attribute'=> 'sold',
        'value'    => function($data){return $data->soldStatus()[$data->sold];},
        'filter'   => ['0' => 'No', '1' => 'Yes'],
      ],
      'publication_date:date',

      [
        'class' => 'yii\grid\ActionColumn',
        'controller' => 'report',
        'template' => '{view}',
        'buttons' => [
          'view' => function ($url) {
            return Html::a(
              '<span class="glyphicon glyphicon-stats"></span>',
              $url,
              [
                'title' => 'View Stats',
                'data-pjax' => '0',
              ]
            );
          },
        ],
      ],
    ],
  ]); ?>

</div>