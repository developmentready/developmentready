<?php
/**
 * Created by PhpStorm.
 * User: RAliyev@avagr.com AVANTI LLC
 * Date: 8/29/15
 * Time: 15:04
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Property */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Property',
  ]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Properties'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="property-update">

  <?php echo $this->render('_form', [
    'model' => $model,
    //'property_types' => $property_types
  ]) ?>

</div>