<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/19/15
 * Time: 00:13
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\web\JsExpression;
use trntv\yii\datetimepicker\DatetimepickerWidget;


/* @var $this yii\web\View */
/* @var $model common\models\Property */
/* @var $form yii\bootstrap\ActiveForm */

$model_id = $model->isNewRecord ? 0 : $model->id ;
$keys = [];
$ids = '';

if(!$model->isNewRecord){
  $agencies = \common\models\PropertyUser::find()->select('user_id')->where(['property_id' => $model->id])->all();
  if($agencies){
    foreach ($agencies as $user) {
      array_push($keys, $user->user_id);
      $ids .= $user->user_id.",";
    }
  }
}

$ids = rtrim($ids, ",");

?>

<div class="property-form">

  <?php $form = ActiveForm::begin(); ?>

  <?php echo $form->errorSummary($model); ?>

  <div class="row">
    <div class="col-sm-2 col-xs-6">
      <?php echo $form->field($model, 'status')->checkbox() ?>
    </div>
    <div class="col-sm-2 col-xs-6">
      <?php echo $form->field($model, 'sold')->checkbox() ?>
    </div>
    <div class="col-sm-2 col-xs-6">
      <?php echo $form->field($model, 'under_offer')->checkbox() ?>
    </div>
    <div class="col-sm-4 col-xs-6">
      <?= $form->field($model, 'page_robot_indexing')->checkbox()  ?>
    </div>
  </div>
  <hr class="col-xs-12"/>
  <div class="row">
    <div class="col-sm-4">
      <?php echo $form->field($model, 'contract_type_id')->inline(true)->radioList(
        $model->permitStatus(), ['inline' => 'true']) ?>
    </div>
    <div id="permit-details"  class="col-sm-8">
      <?php echo $form->field($model, 'contract_details')->textInput(['maxlength' => true]) ?>
    </div>
  </div>
  <hr class="col-xs-12"/>
  <div class="row">
    <div class="col-xs-6">
      <?= $form->field($model, 'show_agency_banner')->checkbox() ?>
    </div>
    <div class="col-xs-6">
      <?= $form->field($model, 'use_watermark')->checkbox() ?>
    </div>
  </div>
  <hr class="col-xs-12" />
  <!-- Default Image -->
  <div class="row">
    <div class="col-xs-7">
      <?= $this->render("//layouts/_upload_default_image", ['form' => $form, 'model' => $model]) ?>
    </div>
    <div class="col-xs-5">
      <?php if($default_image = $model->defaultImage): ?>
      <div class="thumbnail">
        <img src="<?= \Yii::getAlias('@storageUrl')."/properties/{$model->id}/".$default_image->file_name ?>" />
      </div>
      <?php endif; ?>
    </div>
  </div>

  <hr class="col-xs-12" />

  <div class="row">
  <?php if(\Yii::$app->user->can('webAdmin')): ?>
    <div class="col-sm-3">
      <?= $form->field($model, 'publication_date')->widget(
        'trntv\yii\datetimepicker\DatetimepickerWidget',
        [
          //'phpDatetimeFormat' => 'yyyy-MM-dd\'T\'HH:mm:ssZZZZZ',
          'clientOptions' => [
            'minDate' => new \yii\web\JsExpression('new Date("2015-01-01")'),
            'allowInputToggle' => false,
            'sideBySide' => true,
            'widgetPositioning' => [
              'horizontal' => 'auto',
              'vertical' => 'auto'
            ]
          ]
        ]);
      ?>
    </div>
    <div class="col-sm-3">
      <?= $form->field($model, 'featured_till')->widget(
        'trntv\yii\datetimepicker\DatetimepickerWidget',
        [
          //'phpDatetimeFormat' => 'yyyy-MM-dd\'T\'HH:mm:ssZZZZZ',
          'clientOptions' => [
            'minDate' => new \yii\web\JsExpression('new Date("2015-01-01")'),
            'allowInputToggle' => false,
            'sideBySide' => true,
            'widgetPositioning' => [
              'horizontal' => 'auto',
              'vertical' => 'auto'
            ]
          ]
        ]);
      ?>
    </div>
  <?php endif; ?>
    <div class="col-sm-6">
      <?php echo $form->field($model, 'propertyTypes')->inline(true)
        ->checkboxList(ArrayHelper::map(\common\models\PropertyType::find()->active()->all(),
          'id', 'title')) ?>
    </div>
  </div>
  <hr class="col-xs-12" />
  <?php if(\Yii::$app->user->can('webAdmin')): ?>
  <div class="row">
    <div class="col-xs-6">
      <?=
      Select2::widget([
        //'model' => $model,
        'id' => 'property-agencies',
        'name' => 'Property[agencies][]',
        'data' => ArrayHelper::map(\common\models\Agency::find()->active()->all(),
          'user_id', 'title'),
        'value' => $keys,
        'options' => [
          'placeholder' => 'Select agency ...', 'multiple' => true
        ],
        'pluginOptions' => [
          'maximumSelectionLength' => 2,
        ],
        'pluginEvents' => [
          "change" => "function(){
            $.ajax({
              data : 'id=' + $('#property-agencies').val(),
              url: '/property/agents-list?model_id=$model_id',
              success: function (data) {
                $('#agents_list').html(data);
              },
            })
          }"
        ],
      ]); ?>
    </div>

    <div class="col-xs-6">
      <div id="agents_list">
        <?php $output=''; ?>
        <?php
        if(!empty($ids) && !is_null($ids)){
          $users = \common\models\User::find()->joinWith('agency')->where("{{%user}}.id IN ({$ids}) ")->all();
          foreach($users as $item){
            if(!empty($item) && $item->user_type == 1){
              $output .= "<p class='text-success'>".$item->agency->title." agents: </p>";
              if(!empty($agents = $item->agency->agencyAgent)){
                foreach($agents as $agent){
                  $checker = '';
                  if($model->id > 0){
                    $is_user_checked = \common\models\PropertyAgents::find()
                      ->where(['property_id' => $model_id, 'agent_id' => $agent->id])->one();
                    $checker = empty($is_user_checked) ? "" : "checked";
                  }
                  $output .= '<div class="col-sm-6"><label class="checkbox">';
                  $output .= '<input type="checkbox" '.$checker.' name="Property[agents][]" value="'.$agent->id.'"> '.$agent->full_name.'';
                  $output .= '</label></div>';
                }
                $output .= "<hr class='col-xs-12' />";
              } else {
                $output .= "No agents";
              }
            }
          }
        }

        ?>
        <?= $output ?>
      </div>
    </div>
  </div>
  <hr class="col-xs-12" />
  <?php endif; ?>

  <div class="row">
    <div class="col-sm-3 col-xs-6">
      <?php echo $form->field($model, 'price_type_id')->dropDownList(
        ArrayHelper::map(\common\models\PriceType::find()->active()->all(), 'id', 'title'),
        ['prompt' => 'Please select']) ?>
    </div>
    <div class="input-wrapper col-sm-3 col-xs-6">
      <div id="price-in-number" >
        <label>Price in numbers: </label>
        <div class="input-group">
          <span class="input-group-addon">$</span>
          <?php echo $form->field($model, 'price_in_numbers',['template' => '{input}'])
            ->textInput() ?>
        </div>
      </div>
      <div id="eoi-description">
        <?= $form->field($model, 'eoi_description')->textInput() ?>
      </div>
    </div>
    <div id="" class="col-sm-6">
      <div class="row">
        <div class="col-xs-7">
          <?php echo $form->field($model, 'square')->textInput() ?>
          <?php echo $form->field($model, 'square_min')->textInput() ?>
        </div>
        <div class="col-xs-5">
          <?php echo $form->field($model, 'square_measure_id')
            ->dropDownList(ArrayHelper::map(\common\models\AreaType::find()->active()->all(),
              'id', 'title'),['prompt' => 'Please select'])?>
        </div>
      </div>
    </div>
  </div>

  <hr class="col-xs-12" />

  <?= $this->render("//layouts/_text_editor", [ 'form' => $form, 'model' => $model, 'attribute' => 'description']) ?>

  <hr class="col-xs-12" />
  <div class="row">
    <div class="col-sm-12">
      <?= $this->render("//layouts/_upload_images", ['form' => $form, 'model' => $model, 'max_image_count' => 10]) ?>
    </div>
    <?php if(!empty($model->images)): ?>
      <div id="sortable">
        <?php $image_url = Yii::getAlias('@storageUrl')."/properties/{$model->id}" ?>
        <?php foreach($model->images as $item): ?>
          <div class="col-sm-3" data-id="<?=$item->id;?>">
            <div class="thumbnail">
              <img src="<?= $image_url."/".$item->file_name ?>"/>
              <div class="caption text-center">
                <p><a href="<?= Url::to(['delete-image', 'id' => $item->id]) ?>" class="btn btn-danger">delete image</a></p>
              </div>
            </div>
          </div>
        <?php endforeach; ?>
      </div>
    <?php endif; ?>
  </div>

  <div class="row">
    <div class="col-sm-5">
      <?php $suburb = empty($suburb_model = \common\models\Suburb::findOne($model->suburb_id)) ? '' : $suburb_model->title."(".$suburb_model->post_code.")"; ?>
      <?=
      // Normal select with ActiveForm & model
      $form->field($model, 'suburb_id')->widget(Select2::classname(), [
        'initValueText' => $suburb,
        'options' => ['placeholder' => 'Select a suburb ...'],
        'pluginOptions' => [
          'allowClear' => true,
          'minimumInputLength' => 3,
          'ajax' => [
            'url' => Url::to(['suburb-list']),
            'dataType' => 'json',
            'data' => new JsExpression('function(params) { return {q:params.term}; }')
          ],
          'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
          'templateResult' => new JsExpression('function(suburb) { if(!suburb.post_code) suburb.post_code = "Post Code"; return suburb.text + " : " + suburb.post_code ; }'),
          'templateSelection' => new JsExpression('function (suburb) { if(!suburb.post_code) suburb.post_code = "or post code"; return suburb.text + " : " + suburb.post_code; }'),
        ],
      ]);
      ?>
    </div>
    <div class="col-sm-7">
      <?php echo $form->field($model, 'location')->textInput(['maxlength' => true]) ?>
    </div>
  </div>

  <hr class="col-xs-12"/>
  <?= $this->render("//layouts/_map_position", ['form' => $form, 'model' => $model]) ?>


  <hr class="col-xs-12"/>
  <div class="row">
    <div class="col-sm-6">
      <?php echo $form->field($model, 'meta_keywords')
        ->textarea(['rows' => 3,'maxlength' => true]) ?>
    </div>
    <div class="col-sm-6">
      <?php echo $form->field($model, 'meta_description')
        ->textarea(['rows' => 3, 'maxlength' => true]) ?>
    </div>
  </div>
  <hr class="col-xs-12"/>

  <div class="form-group">
    <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
  </div>
  <div id="img_sort_mass"></div>
  <?php ActiveForm::end(); ?>
</div>
<style>
  body{
    overflow-x: inherit;
    overflow-y: inherit;
  }
</style>