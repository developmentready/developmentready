<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/16/15
 * Time: 14:59
 */

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SuburbSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Suburbs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="suburb-index">

  <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

  <p>
    <?php echo Html::a('Create Suburb', ['create'], ['class' => 'btn btn-success']) ?>
  </p>

  <?php echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
      ['class' => 'yii\grid\SerialColumn'],

      'id',
      [
        'attribute'=>'status',
        'format'   => 'html',
        'value'    => function($data){ return $data->status ? '<i class="fa fa-check-square" style="color:#5CB85C"></i>' : '<i class="fa fa-close" style="color: #A94442"></i>';},
        'filter'=> ['0' => 'Deactive' , '1' => 'Active'],
      ],
      'title',
      'post_code',
      [
        'attribute' => 'state',
        'value' => function($data){return $data->state->title;},
      ],
      [
        'attribute' => 'is_city',
        'format' => 'html',
        'value' => function($data){ return $data->is_city ? '<span class="text-success">Yes</span>' : '<span class="text-danger">No</span>';},
      ],
      // 'belongs_to_city',
      // 'default_city_of_state',
      // 'post_code',
      // 'ltd',
      // 'lng',
      // 'creator_id',
      // 'created_at',
      // 'updater_id',
      // 'updated_at',

      ['class' => 'yii\grid\ActionColumn'],
    ],
  ]); ?>

</div>