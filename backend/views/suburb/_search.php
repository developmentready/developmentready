<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/16/15
 * Time: 14:58
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\SuburbSearch */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="suburb-search">

  <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
  ]); ?>

  <?php echo $form->field($model, 'id') ?>

  <?php echo $form->field($model, 'status') ?>

  <?php echo $form->field($model, 'title') ?>

  <?php echo $form->field($model, 'state_id') ?>

  <?php echo $form->field($model, 'is_city') ?>

  <div class="form-group">
    <?php echo Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
    <?php echo Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>