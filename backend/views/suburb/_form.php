<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/16/15
 * Time: 14:55
 */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Suburb */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="suburb-form">

  <?php $form = ActiveForm::begin(); ?>

  <?php echo $form->errorSummary($model); ?>


<div class="row">
  <div class="col-sm-6">
    <?php echo $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
  </div>
  <div class="col-sm-6">
    <?php echo $form->field($model, 'post_code')->textInput(['maxlength' => true]) ?>
  </div>
</div>
  <div class="row">
    <div class="col-sm-6">
      <?php echo $form->field($model, 'state_id')->dropDownList(
        ArrayHelper::map($states, 'id' , 'title'),['prompt' => 'Please select']
      ) ?>
    </div>
    <div class="col-sm-6">
      <?php echo $form->field($model, 'belongs_to_city')->dropDownList(
        ArrayHelper::map($parent_city, 'id', 'title'), ['prompt' => 'Please select']
      ) ?>
    </div>
  </div>

  <hr class="col-xs-12"/>

  <?php echo $form->field($model, 'status', ['inline' => true])->checkbox() ?>
  <?php echo $form->field($model, 'is_city', ['inline' => true])->checkbox() ?>
  <?php echo $form->field($model, 'default_city_of_state', ['inline' => true])->checkbox() ?>

  <hr class="col-xs-12"/>

  <?= $this->render("//layouts/_map_position", ['form' => $form, 'model' => $model]) ?>

  <hr class="col-xs-12"/>
  <div class="form-group">
    <?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>