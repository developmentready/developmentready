<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/16/15
 * Time: 14:59
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Suburb */

$this->title = 'Update Suburb: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Suburbs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="suburb-update">

  <?php echo $this->render('_form', [
    'model' => $model,
    'parent_city' => $parent_city,
    'states' => $states
  ]) ?>

</div>