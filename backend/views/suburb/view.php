<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/16/15
 * Time: 14:59
 */

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Suburb */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Suburbs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="suburb-view">

  <p>
    <?= Html::a(Yii::t('app', 'All Suburbs'), ['index'], ['class' => 'btn btn-info']) ?>
    <?php echo Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?php echo Html::a('Delete', ['delete', 'id' => $model->id], [
      'class' => 'btn btn-danger',
      'data' => [
        'confirm' => 'Are you sure you want to delete this item?',
        'method' => 'post',
      ],
    ]) ?>
  </p>

  <?php echo DetailView::widget([
    'model' => $model,
    'attributes' => [
      'id',
      [
        'attribute' => 'status',
        'format' => 'html',
        'value' => $model->status ? '<span class="text-success">Active</span>' : '<span class="text-danger">Deactive</span>',
      ],
      [
        'attribute' => 'default_city_of_state',
        'format' => 'html',
        'value' => $model->default_city_of_state ? '<span class="text-success">Yes</span>' : '<span class="text-danger">No</span>',
      ],
      'title',
      [
        'attribute' => 'state_id',
        'value' => $model->state->title
      ],
      'post_code',
      [
        'attribute' => 'is_city',
        'format' => 'html',
        'value' => $model->is_city ? '<span class="text-success">Yes</span>' : '<span class="text-danger">no</span>',
      ],
      [
        'attribute' => 'default_city_of_state',
        'format' => 'html',
        'value' => $model->default_city_of_state ? '<span class="text-success">Yes</span>' : '<span class="text-danger">No</span>',
      ],
      [
        'attribute' => 'belongs_to_city',
        'value' => ($city = $model->parentCity->title) ? $city : 'No Defined'
      ],
      [
        'attribute' => 'creator_id',
        'value' => $model->creator->username,
      ],
      'created_at:datetime',
      [
        'attribute' => 'updater_id',
        'value' => $model->updater->username,
      ],
      'updated_at:datetime',
    ],
  ]) ?>

</div>