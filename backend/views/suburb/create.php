<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 11/16/15
 * Time: 14:59
 */

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Suburb */

$this->title = 'Create Suburb';
$this->params['breadcrumbs'][] = ['label' => 'Suburbs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="suburb-create">

  <?php echo $this->render('_form', [
    'model' => $model,
    'parent_city' => $parent_city,
    'states' => $states
  ]) ?>

</div>