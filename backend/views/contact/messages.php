<?php
/**
 * Created by PhpStorm.
 * User: aliyev.resad@gmail.com Rashad Aliyev
 * Date: 12/4/15
 * Time: 01:30
 */

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ContactSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common', 'Contact Forms');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-form-index">

  <h1><?= Html::encode($this->title) ?></h1>
  <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

  <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
      ['class' => 'yii\grid\SerialColumn'],
      'name',
      [
        'label' => 'Company',
        'attribute' => 'subject'
      ],
      [
        'label' => 'From page',
        'attribute' => 'slug',
        'value' => function($data){return $data->slug;}
      ],
      'email:email',
      'message',
      [
        'label' => 'Number',
        'attribute' =>'phone_number',
      ],
      'created_at:date',
      ['class' => 'yii\grid\ActionColumn'],
    ],
  ]); ?>

</div>