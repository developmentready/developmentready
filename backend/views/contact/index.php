<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ContactSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common', 'Contact Forms');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-form-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
          ['class' => 'yii\grid\SerialColumn'],
          [
            'attribute' => 'name',
            'contentOptions' => ['style'=>'min-width: 120px;']
          ],
          [
            'attribute' => 'email',
            'contentOptions' => ['style'=>'min-width: 180px;']
          ],
          [
            'label' => 'Link to message page',
            'format' => 'url',
            'attribute' => 'slug',
            'value' => function($data){
              $category = $data->contact_type == 3 ? 'properties' : 'agencies' ;
              return \Yii::getAlias('@frontendUrl')."/$category/{$data->slug}";
            }
          ],
          'subject',
          'message:ntext',
          'phone_number',
          'created_at:date',
          ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
